//
//  AsyncImageView.h
//  Simu Tv
//
//  Created on 8/17/14.
//  Copyright (c) 2014. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Header.h"
@interface AsyncImageView : UIView {
	//could instead be a subclass of UIImageView instead of UIView, depending on what other features you want to 
	// to build into this class?
	NSURLConnection* connection; //keep a reference to the connection so we can cancel download in dealloc
	NSMutableData* data; //keep reference to the data so we can collect it as it downloads
	//but where is the UIImage reference? We keep it in self.subviews - no need to re-code what we have in the parent class
	CGRect myframe;
    AppDelegate *delegate;
}

@property (nonatomic) CGRect myframe;
- (void)loadImageFromURL:(NSURL*)url;
- (void)loadImageFromData:(NSData*)imgdata;
- (void)loadImageFromFile:(NSString*)filename;
- (void)loadImageFromImage:(UIImage*)tempimg;
- (UIImage*) image;

@end
