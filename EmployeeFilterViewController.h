//
//  EmployeeFilterViewController.h
//  iWork
//
//  Created by Shailendra on 08/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeeFilterViewController : UIViewController

@property(nonatomic,retain)NSString *isComefrom;
@property(nonatomic,retain)NSDictionary *EmployeeDic;

@end
