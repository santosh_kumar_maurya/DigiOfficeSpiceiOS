//
//  LinemanagerMyTaskViewController.m
//  iWork
//
//  Created by Shailendra on 31/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LinemanagerMyTaskViewController.h"
#import "Header.h"

@interface LinemanagerMyTaskViewController ()<UITableViewDelegate,UITextViewDelegate>{
    IBOutlet UITableView * MyTaskTableView;
    
    NSMutableArray *TaskArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    WebApiService *Api;
    NSDictionary *MyTaskDic;
    NSDictionary *ResponseDic;
}

@end

@implementation LinemanagerMyTaskViewController

- (void)viewDidLoad {
    CheckValue = @"MYTASK";
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    MyTaskTableView.estimatedRowHeight = 50;
    MyTaskTableView.backgroundColor = delegate.BackgroudColor;
    MyTaskTableView.rowHeight = UITableViewAutomaticDimension;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [MyTaskTableView addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskWebApi) withObject:nil afterDelay:0.1];
   
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"MYTASK" object:nil];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self TaskWebApi];
}
- (void)TaskWebApi {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MYTASK"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"])
            params = @ {
                @"e_date": @"",
                @"employee_Id": [ApplicationState userId],
                @"s_date": @""
            };
            ResponseDic =  [Api WebApi:params Url:@"myDashTaskCount"];
        }
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                MyTaskDic = [ResponseDic valueForKey:@"object"];
                NSLog(@"MyTaskDic==%@",MyTaskDic);
            }
            [MyTaskTableView reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        SubmitCell *Cell = (SubmitCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        return Cell;
    }
    else if(indexPath.row == 1){
        MyTaskCell *Cell = (MyTaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"me"]))){
            Cell.TaskCreationLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic  valueForKey:@"me"]];
        }
        else{
            Cell.TaskCreationLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"line_manager"]))){
            Cell.TaskCompletionLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"line_manager"]];
        }
        else{
            Cell.TaskCompletionLabel.text  = @"";
        }
        return Cell;
    }
    else if(indexPath.row == 2){
        TaskStatusCell *Cell = (TaskStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
      
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"approved_rejected"]))){
            Cell.CompleteLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"approved_rejected"]];
        }
        else{
            Cell.CompleteLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"ongoing_on_schedule"]))){
            Cell.OnScheduleLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"ongoing_on_schedule"]];
        }
        else{
            Cell.OnScheduleLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"ongoing_behind_schedule"]))){
            Cell.BehindLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"ongoing_behind_schedule"]];
        }
        else{
            Cell.BehindLabel.text  = @"";
        }
        return Cell;
    }
    else{
        MyKpiCell *Cell = (MyKpiCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
        Cell.ScrollViews.bounces =YES;
        Cell.ScrollViews.pagingEnabled = YES;
        Cell.ScrollViews.contentSize = CGSizeMake(Cell.ScrollViews.frame.size.width + 90, Cell.ScrollViews.frame.size.height);
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"kpi"]))){
            Cell.KPILabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"kpi"]];
        }
        else{
            Cell.KPILabel.text  = @"";
        }
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"feedback_as_stackholder"]))){
            Cell.FeedbackAsStackLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"feedback_as_stackholder"]];
        }
        else{
            Cell.FeedbackAsStackLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"myfeedback"]))){
            Cell.FeedbackLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"myfeedback"]];
        }
        else{
            Cell.FeedbackLabel.text  = @"";
        }
        return Cell;
    }
    
}

-(IBAction)SubmitTaskAction:(id)sender{    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    CreateTaskScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateTaskScreen"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)SubmitProgressAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyTaskScreenParentViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskScreenParentViewController"];
    viewController.isComeFrom = @"PROGRESS";
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)GiveFeedBackAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbackViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbackViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)GotoTaskAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NewTaskScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewTaskScreen"];
    viewController.taskType = 1;
    [[self navigationController] pushViewController:viewController animated:YES];
}
-(IBAction)GotoAssignmentAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NewTaskScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewTaskScreen"];
    viewController.taskType = 2;
    [[self navigationController] pushViewController:viewController animated:YES];
}
-(IBAction)ViewAllTasksAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyTaskScreenParentViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskScreenParentViewController"];
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)CompleteAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskStatusScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskStatusScreen"];
    viewController.taskType = 3;
    viewController.isManager = YES;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)OnSchuduleAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskStatusScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskStatusScreen"];
    viewController.taskType = 1;
    viewController.isManager = YES;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)BhindAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskStatusScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskStatusScreen"];
    viewController.taskType = 2;
    viewController.isManager = YES;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)FilterAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbacFilterViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbacFilterViewController"];
    ObjController.isComeFrom =@"MYTASK";
    [[self navigationController] presentViewController:ObjController animated:YES completion:nil];
}
-(IBAction)KPIAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyKPIScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyKPIScreen"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)FeedbackAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    FeedbackScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackScreen"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)FeedbackAsStackHoderAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    FeedbackStackHolderViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackStackHolderViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
   NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:1];
   MyKpiCell *Cell = [MyTaskTableView cellForRowAtIndexPath:indexPath];
    if([scrollView isEqual:Cell.ScrollViews]){
          Cell.ScrollViews.contentSize = CGSizeMake(Cell.ScrollViews.frame.size.width + 90, Cell.ScrollViews.frame.size.height);
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
