#import "TaskDetailManagerScreen.h"
#import "Header.h"

const UIEdgeInsets textInsetsMine4 = {5, 10, 11, 17};
const UIEdgeInsets texttextInsetsSomeone4 = {5, 15, 11, 10};

@interface TaskDetailManagerScreen ()
{
    UIRefreshControl *refreshControl;
    UIButton *screenView;
    NSDictionary *params;
    WebApiService *Api;
    NSDictionary *ResponseDic;
    NSString *SubTask;
    NSArray* subTaskData;
    NSMutableArray *stakeHolderData;
    float ProgressValue;
    
}
@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation TaskDetailManagerScreen
@synthesize isShowDetail,count,taskID,taskType,dataDict, isMe, isManager,isComeFrom;

- (void)viewDidLoad
{
    [super viewDidLoad];
    ProgressValue = 0.0;
    SelectBtn = @"";
    SubTask = @"";
    Api = [[WebApiService alloc] init];
    self.view.backgroundColor = delegate.BackgroudColor;
    self.navigationController.navigationBarHidden=TRUE;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    isServerResponded = FALSE;
    self.activityIndicatorView = [UIActivityIndicatorView new];
    isShowDetail= TRUE;
    isMe = TRUE;
    [self ScreenDesign];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskDetailsManagerWebApi) withObject:nil afterDelay:0.5];
}
-(void)TaskDetailsManagerWebApi{
    if(delegate.isInternetConnected){
        if([SelectBtn isEqualToString:@"CANCEL"]){
            params = @ {
                @"comment": CommentTextView.text,
                @"status":@"4",
                @"taskId": taskID,
                @"user_Id"  : [ApplicationState userId],
            };
            ResponseDic = [Api WebApi:params Url:@"cancelTask"];
        }
        else if([SelectBtn isEqualToString:@"COMMENT"]||[SelectBtn isEqualToString:@"COMMENTBTN"]){
            params = @ {
                @"comment": CommentTextView.text,
                @"subTask":SubTask,
                @"taskId": taskID,
                @"user_id"  : [ApplicationState userId],
            };
            ResponseDic = [Api WebApi:params Url:@"addComment"];
        }
        else{
            if([isComeFrom isEqualToString:@"ASKFORPROGRESS"]){
              ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"askForProgressTaskDea?taskId=%@&user_id=%@",taskID,[ApplicationState userId]]];
            }
            else{
              ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getTaskDetail?taskId=%@&userId=%@",taskID,[ApplicationState userId]]];
            }
        }
        isServerResponded = YES;
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                if([SelectBtn isEqualToString:@"CANCEL"]){
                    [[self navigationController] popViewControllerAnimated:YES];
                }
                else if([SelectBtn isEqualToString:@"COMMENT"]){
                    [[self navigationController] popViewControllerAnimated:YES];
                }
                else if([SelectBtn isEqualToString:@"COMMENTBTN"]){
                    [self getConversation];
                }
                else{
                    dataArray = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                    subTaskData = [[ResponseDic valueForKey:@"object"] valueForKey:@"subtask"];
                    stakeHolderData = [[ResponseDic valueForKey:@"object"] valueForKey:@"stakeholderData"];
                    ProgressValue = [[[ResponseDic valueForKey:@"object"] valueForKey:@"percentage"] floatValue];
                }
            }
            else{
                [LoadingManager hideLoadingView:self.view];
               [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
            [tab reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
          [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void) getConversation{
    if([delegate isInternetConnected]){
        ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getComment?taskId=%@&userId=%@",taskID,[ApplicationState userId]]];
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                dataConv = [[ResponseDic valueForKey:@"object"] valueForKey:@"commentData"];
            }
            else{
                [self noMessageScreen:NSLocalizedString(@"NO_RECORD", nil)];
            }
            [tab reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self noMessageScreen:NSLocalizedString(@"NO_RECORD", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (void) ScreenDesign{
    mainScreenView=[[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    mainScreenView.backgroundColor=delegate.BackgroudColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor = [ColorCategory PurperColor];
    [mainScreenView addSubview:headerview];
    
    UIButton *backButton=[[UIButton alloc] initWithFrame:CGRectMake(5, 35, 30, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    backButton.tag=20;
    CGPoint center = backButton.center;
    center.y = headerview.frame.size.height / 2 +3;
    [backButton setCenter:center];
    [headerview addSubview:backButton];
    
    UIButton* HomeButton=[UIButton buttonWithType:UIButtonTypeCustom];
    HomeButton.frame=CGRectMake(delegate.devicewidth-35, 10, 18, 18);
    HomeButton.tag=20;
    center = HomeButton.center;
    center.y = headerview.frame.size.height / 2 + 3;
    [HomeButton setCenter:center];
    [HomeButton addTarget:self action:@selector(GotobackContainer) forControlEvents:UIControlEventTouchUpInside];
    [HomeButton setImage:[UIImage imageNamed:@"HomeImage"]  forState:UIControlStateNormal];
    [headerview addSubview:HomeButton];
    
    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(45, backButton.frame.origin.y+12, 200, 20)];
    headerlab.textColor=delegate.yellowColor;
    headerlab.font=delegate.headFont;
    headerlab.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),taskID];
    headerlab.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab];
    
    UIButton *flatButton=[[UIButton alloc] initWithFrame:CGRectMake(0, delegate.headerheight, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton.backgroundColor=[UIColor whiteColor];
    flatButton_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton_lab.backgroundColor = [ColorCategory PurperColor];
    flatButton_lab.text = NSLocalizedString(@"DETAILS_CAPS", nil);
    flatButton_lab.textAlignment=ALIGN_CENTER;
    flatButton_lab.font=delegate.normalFont;
    flatButton_lab.textColor=[UIColor whiteColor];
    [flatButton addSubview:flatButton_lab];
    [flatButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton.tag=300;
    [mainScreenView addSubview:flatButton];
    
    UIButton *flatButton1=[[UIButton alloc] initWithFrame:CGRectMake(mainScreenView.frame.size.width/2,delegate.headerheight,mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton1.backgroundColor=[UIColor whiteColor];
    flatButton1_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton1_lab.text=NSLocalizedString(@"COMMENTS_CAPS", nil);
    flatButton1_lab.backgroundColor = [ColorCategory PurperColor];
    flatButton1_lab.textAlignment=ALIGN_CENTER;
    flatButton1_lab.font=delegate.normalFont;
    flatButton1_lab.textColor=[UIColor whiteColor];
    [flatButton1 addSubview:flatButton1_lab];
    [flatButton1 addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton1.tag=301;
    [mainScreenView addSubview:flatButton1];
    
    sel_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight-2, mainScreenView.frame.size.width/2, 2)];
    sel_lab.backgroundColor = [UIColor whiteColor];
    [mainScreenView addSubview:sel_lab];
    
    tab=[[UITableView alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight, mainScreenView.frame.size.width,mainScreenView.frame.size.height)];
    tab.delegate=self;
    tab.dataSource=self;
    tab.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab.backgroundColor=delegate.BackgroudColor;
    [mainScreenView addSubview:tab];
    [self.view addSubview:mainScreenView];

    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 20, [UIScreen mainScreen].bounds.size.height - 44 - 20, 44, 44);
    addButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"plus_fab"] andPressedImage:[UIImage imageNamed:@"cross"] withScrollview:nil];
    addButton.imageArray = @[@"fb-icon",@"google-icon"];
    addButton.labelArray = @[@"Assign Task",@"Create Task"];
    addButton.hideWhileScrolling = YES;
    addButton.isCommentScreen = TRUE;
    addButton.delegate = self;
    [self.view addSubview:mainScreenView];
    [self AddRefreshControl];
}
-(void)AddRefreshControl{
    [refreshControl removeFromSuperview];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
}
- (void)reloadData{
    [self TaskDetailsManagerWebApi];
    [self getConversation];
}
-(void)screenTaskDetail:(UITableViewCell *)cell{
    int rightPad = delegate.devicewidth-delegate.margin*2;
    UIButton *mainView=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    mainView.backgroundColor = delegate.BackgroudColor;
    mainView.backgroundColor = delegate.BackgroudColor;
    mainView.layer.cornerRadius = 2.0;
    mainView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    mainView.layer.borderWidth = 0.5;
    mainView.layer.masksToBounds =YES;
    mainView.backgroundColor=[UIColor whiteColor];
    mainView.layer.shadowRadius = 3.0f;
    mainView.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
    mainView.layer.shadowOpacity = 0.7f;
    mainView.layer.masksToBounds = NO;
    
    [cell addSubview:mainView];
    int ypos = 15;
    UILabel *createdOnLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/3, 15)];
    createdOnLabel.text =NSLocalizedString(@"CREATED_ON", nil);
    createdOnLabel.font = delegate.contentSmallFont;
    createdOnLabel.textColor = [UIColor blackColor];
    createdOnLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOnLabel];
    
    UILabel *createdOn =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos+17, delegate.devicewidth/3, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"creationDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"creationDate"];
        createdOn.text = [self DateFormateChange:datenumber];
    }
    else{
        createdOn.text = @"";
    }
    
    createdOn.font = delegate.contentSmallFont;
    createdOn.textColor = delegate.dimBlack;
    createdOn.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOn];
    createdOnLabel.frame = CGRectMake(10, ypos, createdOn.intrinsicContentSize.width, 15);
    
    int xpos = delegate.devicewidth/2-((delegate.devicewidth/3)/2);
    UILabel *startDateLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/3, 15)];
    startDateLabel.text = NSLocalizedString(@"ACTUAL_START_DATE", nil);
    startDateLabel.textColor = [UIColor blackColor];
    startDateLabel.font = delegate.contentSmallFont;
    startDateLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:startDateLabel];
    
    UILabel *startDate =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, delegate.devicewidth/3, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"];
        startDate.text = [self DateFormateChange:datenumber];
    }
    else{
        startDate.text = @"";
    }

    startDate.font = delegate.contentSmallFont;
    startDate.textAlignment = ALIGN_LEFT;
    startDate.textColor = delegate.dimBlack;
    [mainView addSubview:startDate];
    
    if(![startDate.text isEqualToString:@""]){
        xpos = delegate.devicewidth/2 - (startDate.intrinsicContentSize.width/2);
        startDateLabel.frame = CGRectMake(xpos, ypos, startDate.intrinsicContentSize.width+30, 15);
        startDate.frame = CGRectMake(xpos, ypos+17, startDate.intrinsicContentSize.width, 15);
    }
   
    
    xpos = delegate.devicewidth-(delegate.devicewidth/3);
    UILabel *dayLeftLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/3, 15)];
    dayLeftLabel.text =NSLocalizedString(@"TIME_LEFT_WITHOUT_COLON", nil);
    dayLeftLabel.textColor = [UIColor blackColor];
    dayLeftLabel.font = delegate.contentSmallFont;
    dayLeftLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:dayLeftLabel];
    
    xpos = delegate.devicewidth - dayLeftLabel.intrinsicContentSize.width - 15;
    dayLeftLabel.frame = CGRectMake(xpos, ypos, dayLeftLabel.intrinsicContentSize.width, 15);
    
    UILabel *dayLeft =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, dayLeftLabel.intrinsicContentSize.width, 15)];
    
    dayLeft.text = [[dataArray objectAtIndex:0] objectForKey:@"timeLeft"];
    dayLeft.font = delegate.contentSmallFont;
    dayLeft.textAlignment = ALIGN_LEFT;
    dayLeft.textColor = delegate.dimBlack;
    [mainView addSubview:dayLeft];
    
    if (dayLeftLabel.intrinsicContentSize.width > dayLeft.intrinsicContentSize.width)
        dayLeft.frame = CGRectMake(xpos, ypos+17, dayLeftLabel.intrinsicContentSize.width, 15);
    else
        dayLeft.frame = CGRectMake(xpos, ypos+17, dayLeft.intrinsicContentSize.width, 15);
    
    if([dayLeft.text containsString:@"-"]){
        dayLeft.text = [dayLeft.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        dayLeftLabel.text = @"Overdue";
        dayLeftLabel.frame = CGRectMake(xpos, ypos, dayLeftLabel.intrinsicContentSize.width+20, 15);
    }
    
    int saveYPos = ypos = ypos + 30 + 15;
    screenView=[[UIButton alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, delegate.deviceheight)];
    screenView.backgroundColor=[UIColor whiteColor];
    [mainView addSubview:screenView];
    cell.backgroundColor=[UIColor clearColor];
    
     ypos = 15;
     UILabel *status1 =
     [[UILabel alloc] initWithFrame:CGRectMake(rightPad/2, ypos, rightPad/2-delegate.margin, 20)];
     status1.font = delegate.headFont;
     status1.textAlignment = ALIGN_CENTER;
     status1.text = NSLocalizedString(@"IN_PROGRESS", nil);
     status1.textColor = delegate.inprogColor;
     CGPoint center = status1.center;
     center.x = screenView.frame.size.width / 2 + 20;
     [status1 setCenter:center];
     [screenView addSubview:status1];
     
     UIImageView *statusLogo;
     statusLogo=[[UIImageView alloc] initWithFrame:CGRectMake(rightPad/2, ypos, 20, 20)];
     statusLogo.image=[UIImage imageNamed:@"inprogressblue"];
     center = statusLogo.center;
     center.x = screenView.frame.size.width / 2 - 50;
     [statusLogo setCenter:center];
     [screenView addSubview:statusLogo];
     
     ypos = ypos + statusLogo.frame.size.height + 15;
     UIView *lineView0 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,
     rightPad, 0.3)];
     lineView0.backgroundColor = delegate.borderColor;
     [screenView addSubview:lineView0];
     
     ypos = ypos + 15;
    UILabel *taskNameLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, rightPad-delegate.margin*2, 15)];
    taskNameLabel.text = NSLocalizedString(@"TASK_NAME_WITH_COLON", nil);
    taskNameLabel.font = delegate.contentFont;
    taskNameLabel.textColor=[UIColor blackColor];
    taskNameLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:taskNameLabel];
    
    
    NSString *taskInfo = [[dataArray objectAtIndex:0] objectForKey:@"taskName"];
    if (taskInfo.length > 0){
        taskInfo = [NSString stringWithFormat:@"%@%@",[[taskInfo substringToIndex:1] uppercaseString],[taskInfo substringFromIndex:1] ];
    }
    
    xpos = taskNameLabel.intrinsicContentSize.width + 10;
    UILabel *taskName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 50)];
    taskName.text = taskInfo;
    taskName.font = delegate.contentFont;
    taskName.textColor=delegate.dimBlack;
    taskName.textAlignment = ALIGN_LEFT;
    taskName.numberOfLines = 0;
    [taskName sizeToFit];
    [screenView addSubview:taskName];
    
    ypos = ypos + taskName.frame.size.height + 15;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView];
    
    ypos = ypos + 15;
    UILabel *StartDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    StartDateStaticLabel.text = NSLocalizedString(@"START_DATE_WITH_COLON", nil);
    StartDateStaticLabel.font = delegate.contentFont;
    StartDateStaticLabel.textColor = [UIColor blackColor];
    StartDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateStaticLabel];
    
    xpos = StartDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *StartDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
  
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"startDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"startDate"];
        StartDateValueLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        StartDateValueLabel.text = @"";
    }
    StartDateValueLabel.font = delegate.contentFont;
    StartDateValueLabel.textColor = delegate.dimBlack;
    StartDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateValueLabel];
    
    ypos = ypos + StartDateStaticLabel.frame.size.height + 15;
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView1.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView1];
    
    ypos = ypos + 15;
    UILabel *EndDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    EndDateStaticLabel.text = NSLocalizedString(@"END_DATE_WITH_COLON", nil);
    EndDateStaticLabel.font = delegate.contentFont;
    EndDateStaticLabel.textColor = [UIColor blackColor];
    EndDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateStaticLabel];
    
    xpos = EndDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *EndDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"endDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"endDate"];
        EndDateValueLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        EndDateValueLabel.text = @"";
    }
    EndDateValueLabel.font = delegate.contentFont;
    EndDateValueLabel.textColor = delegate.dimBlack;
    EndDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateValueLabel];
    
    ypos = ypos + EndDateValueLabel.frame.size.height + 15;
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView2.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView2];
    
    ypos = ypos + 15;
    UILabel *DurationStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    DurationStaticLabel.text = NSLocalizedString(@"DURATION_WITH_COLON", nil);
    DurationStaticLabel.font = delegate.contentFont;
    DurationStaticLabel.textColor = [UIColor blackColor];
    DurationStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:DurationStaticLabel];
    
    xpos = EndDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *DurationValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    DurationValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"duration"];
    DurationValueLabel.font = delegate.contentFont;
    DurationValueLabel.textColor = delegate.dimBlack;
    DurationValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:DurationValueLabel];
    ypos = ypos + DurationValueLabel.frame.size.height + 15;
    
    UIView *lineView3 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView3.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView3];
    
    ypos = ypos + 15;
    UILabel *createdByLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    createdByLabel.text = NSLocalizedString(@"TYPE_WITH_COLON", nil);
    createdByLabel.font = delegate.contentFont;
    createdByLabel.textColor = [UIColor blackColor];
    createdByLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdByLabel];
    
    UILabel *createdBy =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos+17, delegate.devicewidth/2, 15)];
    createdBy.text = [[dataArray objectAtIndex:0] objectForKey:@"type"];
    createdBy.font = delegate.contentFont;
    createdBy.textColor = delegate.dimBlack;
    createdBy.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdBy];
    
    createdBy.frame = CGRectMake(createdByLabel.intrinsicContentSize.width+10, ypos, rightPad, 15);
    createdByLabel.frame = CGRectMake(10, ypos, createdByLabel.intrinsicContentSize.width, 15);
    
    ypos = ypos + createdByLabel.frame.size.height + 15;
    UIView *lineView10 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView10.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView10];

    ypos = ypos + 15;
    UILabel *kpiLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
    kpiLabel.text = NSLocalizedString(@"KPI_WITH_COLON", nil);
    kpiLabel.textColor = [UIColor blackColor];
    kpiLabel.font = delegate.contentFont;
    createdBy.numberOfLines = 0;
    kpiLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiLabel];
    
    xpos = kpiLabel.intrinsicContentSize.width + 10;
    UILabel *kpiName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-30, 50)];
    kpiName.text = [NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:0]  objectForKey:@"kpi"]];
    kpiName.textColor = delegate.dimBlack;
    kpiName.font = delegate.contentFont;
    kpiName.numberOfLines = 0;
    [kpiName sizeToFit];
    kpiName.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiName];
    
    if (kpiName.text.length > 0)
        ypos = ypos + kpiName.frame.size.height + 15;
    else
        ypos = ypos + kpiLabel.frame.size.height + 15;
    UIView *lineView23 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView23.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView23];
    
    ypos = ypos + 15;
    UIProgressView *progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    [progressView setFrame:CGRectMake(10, ypos, delegate.devicewidth-delegate.margin*4, 15)];
    progressView.progress = 1.0f;
    progressView.backgroundColor = delegate.ProgressColor;
    progressView.progress = ProgressValue;
    [progressView.layer setCornerRadius:10];
    progressView.layer.masksToBounds = TRUE;
    progressView.clipsToBounds = TRUE;
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 5.0f);
    progressView.transform = transform;
    progressView.progressTintColor = delegate.redColor;
    [screenView addSubview: progressView];
    
    ypos = ypos + progressView.frame.size.height + 15;
    int subTaskHeight = 0;

    if (((NSNull*)subTaskData != [NSNull null]) && subTaskData.count > 0 && [subTaskData[0] length] > 0){
        for (int i=0; i < subTaskData.count; i++){
            UIView *lineView4 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView4.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView4];
            
            ypos = ypos + 15;
            UILabel *subtaskLabel =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            subtaskLabel.text = NSLocalizedString(@"SUBTASK_WITH_COLON", nil);
            subtaskLabel.font = delegate.contentFont;
            subtaskLabel.textColor =[UIColor blackColor];
            subtaskLabel.textAlignment = ALIGN_LEFT;
            [screenView addSubview:subtaskLabel];
            
            xpos = subtaskLabel.intrinsicContentSize.width + 10;
            UILabel *subtask =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 500)];
            subtask.text = subTaskData[i];
            subtask.font = delegate.contentFont;
            subtask.textColor = delegate.dimBlack;
            subtask.numberOfLines=0;
            subtask.textAlignment = ALIGN_LEFT;
            [subtask sizeToFit];
            [screenView addSubview:subtask];
            subTaskHeight = subTaskHeight + subtask.frame.size.width;
            ypos = ypos + subtask.frame.size.height + 15;
        }
    }
    
    UIView *lineView5 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView5.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView5];
    
    ypos = ypos + 15;
    UILabel *taskDetailLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth-delegate.margin*2-5, 15)];
    taskDetailLabel.text = NSLocalizedString(@"TASK_DETAILS_WITH_COLON", nil);
    taskDetailLabel.font = delegate.contentFont;
    taskDetailLabel.textColor = [UIColor blackColor];;
    taskDetailLabel.textAlignment = ALIGN_LEFT;
    taskDetailLabel.numberOfLines = 0;
    [taskDetailLabel sizeToFit];
    [screenView addSubview:taskDetailLabel];
    
    xpos = taskDetailLabel.intrinsicContentSize.width + 10;
    UILabel *taskDetail =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
    taskDetail.text = [[dataArray objectAtIndex:0] objectForKey:@"taskDetail"];
    taskDetail.font = delegate.contentFont;
    taskDetail.textColor = delegate.dimBlack;;
    taskDetail.textAlignment = ALIGN_LEFT;
    taskDetail.numberOfLines = 0;
    [taskDetail sizeToFit];
    [screenView addSubview:taskDetail];
    
    if (taskDetail.text.length > 0)
        ypos = ypos + taskDetail.frame.size.height + 15;
    else
        ypos = ypos + taskDetailLabel.frame.size.height + 15;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, ypos, delegate.devicewidth-delegate.margin*2, 40)];
    [screenView addSubview:footerView];
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2, 0, footerView.frame.size.width/2, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.normalFont;
    [submitButton setTitle:NSLocalizedString(@"ADD_COMMENT", nil) forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag = 1000;
    [footerView addSubview:submitButton];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2, 40)];
    cancelButton.backgroundColor = delegate.yellowColor;
    cancelButton.clipsToBounds = YES;
    cancelButton.titleLabel.font = delegate.normalFont;
    if (isManager == YES) {
        [cancelButton setTitle:NSLocalizedString(@"CANCEL_TASK", nil) forState:UIControlStateNormal];
       
    }
    else{
        [cancelButton setTitle:NSLocalizedString(@"MARK_COMPETE", nil) forState:UIControlStateNormal];
    }
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.tag = 2000;
    [footerView addSubview:cancelButton];
    int height = ypos + footerView.frame.size.height;
    screenView.frame = CGRectMake(delegate.margin, saveYPos, delegate.devicewidth-delegate.margin*2, height);
}
- (void)showCommentAlert {
    SelectBtn = @"COMMENTBTN";
    [self CreateCommentView];
}
-(void)CreateCommentView{
    CoomentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    CoomentView.backgroundColor = [UIColor clearColor];
    UIImageView *ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    ImageView.backgroundColor = [UIColor blackColor];
    ImageView.alpha = 0.3;
    UIView *InnerView = [[UIView alloc] initWithFrame:CGRectMake(10, delegate.deviceheight/2 - 100, delegate.devicewidth -20, 200)];
    CGPoint center = InnerView.center;
    center.x = CoomentView.center.x;
    InnerView.center = center;
    InnerView.layer.cornerRadius = 5.0;
    InnerView.backgroundColor = [UIColor whiteColor];
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(0, InnerView.frame.size.height-45, InnerView.frame.size.width, 45)];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: BtnView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    BtnView.layer.mask = maskLayer1;
  
    UILabel *AddCommentLabel;
    if([SelectBtn isEqualToString:@"CANCEL"]){
       AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 0)];
    }
    else{
      AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 45)];
    }
    AddCommentLabel.backgroundColor = delegate.yellowColor;
    AddCommentLabel.textColor = [UIColor whiteColor];
    AddCommentLabel.font = delegate.headFont;
    AddCommentLabel.textAlignment = NSTextAlignmentCenter;
    AddCommentLabel.text = NSLocalizedString(@"ADD_COMMENT", nil);
    
    CAShapeLayer * AddCommentmaskLayer1 = [CAShapeLayer layer];
    AddCommentmaskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: AddCommentLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){5.0}].CGPath;
    AddCommentLabel.layer.mask = AddCommentmaskLayer1;
   
    UILabel *AddCommentStaticLabel;
    if([SelectBtn isEqualToString:@"CANCEL"]){
        AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 35, 150, 15)];
    }
    else{
      AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, AddCommentLabel.frame.origin.y+AddCommentLabel.frame.size.height + 10, 150, 15)];
    }
    
    AddCommentStaticLabel.textColor = [UIColor blackColor];
    AddCommentStaticLabel.font = delegate.contentFont;
    AddCommentStaticLabel.textAlignment = NSTextAlignmentLeft;
    AddCommentStaticLabel.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    
    CommentTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, AddCommentStaticLabel.frame.origin.y+AddCommentStaticLabel.frame.size.height + 10, AddCommentLabel.frame.size.width -20, BtnView.frame.origin.y - 90)];
    CommentTextView.font = delegate.contentFont;
    CommentTextView.delegate = self;
    CommentTextView.layer.cornerRadius = 5.0;
    CommentTextView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.layer.borderWidth = 0.5;
    CommentTextView.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    CommentTextView.backgroundColor = [UIColor clearColor];
    
    UIButton *SubmitBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, BtnView.frame.size.width/2, 45)];
    [SubmitBtn addTarget:self action:@selector(SubmitAction) forControlEvents:UIControlEventTouchUpInside];
    SubmitBtn.backgroundColor = delegate.yellowColor;
    SubmitBtn.titleLabel.font = delegate.normalFont;
    [SubmitBtn setTitle:NSLocalizedString(@"SUBMIT_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    UIButton *CancelBtn=[[UIButton alloc] initWithFrame:CGRectMake(BtnView.frame.size.width/2, 0, BtnView.frame.size.width/2, 45)];
    [CancelBtn addTarget:self action:@selector(CancelAction) forControlEvents:UIControlEventTouchUpInside];
    CancelBtn.backgroundColor = delegate.redColor;
    CancelBtn.titleLabel.font = delegate.normalFont;
    [CancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    [BtnView addSubview:SubmitBtn];
    [BtnView addSubview:CancelBtn];
    [InnerView addSubview:BtnView];
    [InnerView addSubview:CommentTextView];
    [InnerView addSubview:AddCommentLabel];
    [InnerView addSubview:AddCommentStaticLabel];
    [CoomentView addSubview:ImageView];
    [CoomentView addSubview:InnerView];
    [self.view addSubview:CoomentView];
}
-(void)CancelAction{
    CoomentView.hidden = YES;
}
-(void)SubmitAction{
    
    if([CommentTextView.text isEqualToString:NSLocalizedString(@"ADD_COMMENT_SMALL", nil)] ||[CommentTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_ADD_COMMENT", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        if([SelectBtn isEqualToString:@"CANCEL"]){
            [LoadingManager showLoadingView:self.view];
            [self performSelector:@selector(TaskDetailsManagerWebApi) withObject:nil afterDelay:0.5];
        }
        else if([SelectBtn isEqualToString:@"COMMENT"]||[SelectBtn isEqualToString:@"COMMENTBTN"]){
            NSString* str = @"";
            if(IsSafeStringPlus(TrToString(subTaskData))){
                SubTask = [subTaskData objectAtIndex:0];
            }
            if([SubTask isEqual:[NSNull null]]){
                str = @"";
            }
            else{
                if([SubTask isEqualToString:@"null"] ||[SubTask isEqualToString:@""]|| [SubTask isEqualToString:@"(null)"]){
                    str = @"";
                }
                else{
                    str = SubTask;
                    SubTask = str;
                }
            }
            [LoadingManager showLoadingView:self.view];
            [self performSelector:@selector(TaskDetailsManagerWebApi) withObject:nil afterDelay:0.5];
            CoomentView.hidden = YES;
            [CommentTextView resignFirstResponder];
        }
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:NSLocalizedString(@"ADD_COMMENT_SMALL", nil)]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
        textView.textColor = [UIColor darkGrayColor]; //optional
    }
    [textView resignFirstResponder];
}
-(IBAction)btnfun:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==20){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else if(btn.tag==300){
        isShowDetail = TRUE;
         [addButton removeFromSuperview];
        tab.backgroundColor = delegate.BackgroudColor;
        sel_lab.frame=CGRectMake(0, delegate.headerheight+delegate.tabheight-2, delegate.devicewidth/2, 2);
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(TaskDetailsManagerWebApi) withObject:nil afterDelay:0.5];
    }
    else if(btn.tag==301){
        isShowDetail = FALSE;
        [self.view addSubview:addButton];
        tab.backgroundColor = delegate.BackgroudColor;
        sel_lab.frame=CGRectMake(delegate.devicewidth/2, delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/2, 2);
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(getConversation) withObject:nil afterDelay:0.5];
    }
    else if (2000 == btn.tag){
        SelectBtn = @"CANCEL";
        [self CreateCommentView];
    }
    else if (1000 == btn.tag){
        SelectBtn = @"COMMENT";
        [self CreateCommentView];
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (FALSE == isServerResponded)
        return 0;
    [msgvw removeFromSuperview];
    if (isShowDetail){
        return 1;
    }
    else{
        if ([dataConv count] <= 0){
            [self noMessageScreen:NSLocalizedString(@"SORRY_NO_COMMENT", nil)];
        }
        else
            return [dataConv count];
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (TRUE == isShowDetail){
        return screenView.frame.size.height + 280;;
    }
    else{
        int indx=(int)indexPath.row;
        NSString * str = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
        UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        CGSize size1 = CGSizeMake(230,9999);
        CGRect textRect = [str boundingRectWithSize:size1 options:NSStringDrawingUsesLineFragmentOrigin  attributes:@{NSFontAttributeName:font}  context:nil];
        CGSize size = textRect.size;
        int temp = size.height+textInsetsMine4.bottom+textInsetsMine4.top;
        return temp + 40;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(delegate.margin, 5, delegate.devicewidth-delegate.margin, 50);
    myLabel.font = delegate.boldFont;
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    return headerView;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
    }
    if (TRUE == isShowDetail){
        [self screenTaskDetail:cell];
    }
    else{
        cell.backgroundColor = delegate.BackgroudColor;
        int indx=(int)indexPath.row;
        CGFloat y = 0;
        UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        CGSize size1 = CGSizeMake(230,9999);
        NSString *str = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
        CGRect textRect = [str boundingRectWithSize:size1
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:font}
                                            context:nil];
        
        CGSize size = textRect.size;
        
        if ([[[dataConv objectAtIndex:indx] objectForKey:@"userId"] isEqualToString:[ApplicationState userId]]){
            CGFloat x = delegate.devicewidth - size.width - textInsetsMine4.left - textInsetsMine4.right;
            
            UIImageView *profImg=[[UIImageView alloc] initWithFrame:CGRectMake(delegate.devicewidth-40, y+20,30, 30)];
            UIImage* imagePath = [[UIImage imageNamed:@"profile_image_default"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            profImg.image = imagePath;
            [cell addSubview:profImg];
            
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:indx] objectForKey:@"userImg"]))){
                UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]]]];
                if(images == nil){
                    NSLog(@"image nil");
                    profImg.image = [UIImage imageNamed:@"profile_image_default"];
                }
                else{
                    NSString *URL = [NSString stringWithFormat:@"%@",[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]];
                    [profImg sd_setImageWithURL:[NSURL URLWithString:URL]];
                }
            }
            else{
                profImg.image = [UIImage imageNamed:@"profile_image_default"];
            }
            profImg.layer.borderColor = [UIColor whiteColor].CGColor;
            profImg.layer.borderWidth = 1;
            profImg.layer.cornerRadius = 15;
            profImg.clipsToBounds = YES;
            
            UILabel *userImg=[[UILabel alloc] initWithFrame:CGRectMake(x-50, y+20,size.width+textInsetsMine4.left+textInsetsMine4.right, size.height+textInsetsMine4.bottom+textInsetsMine4.top)];
            
            userImg.layer.borderWidth = 2.0;
            userImg.layer.borderColor = delegate.yellowColor.CGColor;
            userImg.layer.cornerRadius = 8;
            userImg.layer.masksToBounds = true;
            [cell addSubview:userImg];
            
            UILabel *lastMsg=[[UILabel alloc] initWithFrame:CGRectMake(x-40, y+textInsetsMine4.top+22, size.width,size.height)];
            
            lastMsg.font=[UIFont systemFontOfSize:[UIFont systemFontSize]];
            lastMsg.textColor=delegate.blackcolor;
            lastMsg.lineBreakMode = NSLineBreakByWordWrapping;
            lastMsg.numberOfLines = 0;
            lastMsg.textAlignment = ALIGN_LEFT;
            lastMsg.text = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
            [cell addSubview:lastMsg];
            
//            UILabel *subTask_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, y,delegate.devicewidth, 20)];
//            subTask_lab.font=delegate.mediumFont;
//            subTask_lab.textColor=delegate.blackcolor;
//            subTask_lab.textAlignment = ALIGN_CENTER;
//            
//            NSString *SubTask1 = [[dataConv objectAtIndex:indx] objectForKey:@"subtask"];
//            if([SubTask1 isEqual:[NSNull null]]){
//                subTask_lab.text = @"";
//            }
//            else{
//                if([SubTask1 isEqualToString:@"null"] ||[SubTask1 isEqualToString:@""] || SubTask1 == nil|| [SubTask1 isEqualToString:@"(null)"]){
//                    subTask_lab.text = @"";
//                }
//                else{
//                    subTask_lab.text = SubTask1;
//                }
//            }
//
//            [cell addSubview:subTask_lab];
            CGFloat ypos = y+textInsetsMine4.top+22+size.height+10;
            UILabel *date_lab=[[UILabel alloc] initWithFrame:CGRectMake(delegate.devicewidth-140, ypos,120, 20)];
            date_lab.font=delegate.smallFont2;
            date_lab.textColor=delegate.dimBlack;
           
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:0] objectForKey:@"creationDate"]))){
                NSNumber *datenumber = [[dataConv objectAtIndex:0] objectForKey:@"creationDate"];
                date_lab.text = [self DateFormateChange:datenumber];
            }
            else{
                date_lab.text = @"";
            }
            [cell addSubview:date_lab];
            isMe = FALSE;
        }
        else{
            isMe = TRUE;
            CGFloat x = 5;
            UIImageView *profImg=[[UIImageView alloc] initWithFrame:CGRectMake(x, y+20,30, 30)];
            UIImage* imagePath = [[UIImage imageNamed:@"profile_image_default"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            profImg.image = imagePath;
            [cell addSubview:profImg];
            
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:indx] objectForKey:@"userImg"]))){
                UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]]]];
                if(images == nil){
                    NSLog(@"image nil");
                    profImg.image = [UIImage imageNamed:@"profile_image_default"];
                }
                else{
                    NSString *URL = [NSString stringWithFormat:@"%@",[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]];
                    [profImg sd_setImageWithURL:[NSURL URLWithString:URL]];
                }
            }
            else{
                profImg.image = [UIImage imageNamed:@"profile_image_default"];
            }
            profImg.layer.borderColor = [UIColor whiteColor].CGColor;
            profImg.layer.borderWidth = 1;
            profImg.layer.cornerRadius = 15;
            profImg.clipsToBounds = YES;
            
            x = x + profImg.frame.size.width;
            UILabel *userImg=[[UILabel alloc] initWithFrame:CGRectMake(x+10, y+20,size.width+texttextInsetsSomeone4.left+texttextInsetsSomeone4.right, size.height+texttextInsetsSomeone4.bottom+texttextInsetsSomeone4.top)];
            userImg.layer.borderWidth = 2.0;
            userImg.layer.borderColor = delegate.redColor.CGColor;
            userImg.layer.cornerRadius = 8;
            userImg.layer.masksToBounds = true;
            [cell addSubview:userImg];
            
            UILabel *lastMsg=[[UILabel alloc] initWithFrame:CGRectMake(x+texttextInsetsSomeone4.left+2, y+texttextInsetsSomeone4.top+22, size.width,size.height)];
            lastMsg.font=[UIFont systemFontOfSize:[UIFont systemFontSize]];
            lastMsg.textColor=delegate.blackcolor;
            lastMsg.lineBreakMode = NSLineBreakByWordWrapping;
            lastMsg.numberOfLines = 0;
            lastMsg.textAlignment = ALIGN_LEFT;
            lastMsg.text = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
            [cell addSubview:lastMsg];
            CGFloat ypos = y+texttextInsetsSomeone4.top+22+size.height+10;
            UILabel *date_lab=[[UILabel alloc] initWithFrame:CGRectMake(10, ypos,150,20)];
            date_lab.font=delegate.smallFont2;
            date_lab.textColor=delegate.dimBlack;
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:0] objectForKey:@"creationDate"]))){
                NSNumber *datenumber = [[dataConv objectAtIndex:0] objectForKey:@"creationDate"];
                date_lab.text = [self DateFormateChange:datenumber];
            }
            else{
                date_lab.text = @"";
            }
            [cell addSubview:date_lab];
            
            x=10;
//            UILabel *subTask_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, y,delegate.devicewidth, 20)];
//            subTask_lab.font=delegate.mediumFont;
//            subTask_lab.textColor=delegate.blackcolor;
//            subTask_lab.textAlignment = ALIGN_CENTER;
//            NSString *SubTask1 = [[dataConv objectAtIndex:indx] objectForKey:@"subtask"];
//            if([SubTask1 isEqual:[NSNull null]]){
//                subTask_lab.text = @"";
//            }
//            else{
//                if([SubTask1 isEqualToString:@"null"] ||[SubTask1 isEqualToString:@""] || SubTask1 == nil|| [SubTask1 isEqualToString:@"(null)"]){
//                    subTask_lab.text = @"";
//                }
//                else{
//                    subTask_lab.text = SubTask1;
//                }
//            }
//            [cell addSubview:subTask_lab];
        }
    }
    return cell;
}
-(void)noMessageScreen:(NSString*)message{
    msgvw=[[UIView alloc] initWithFrame:tab.frame];
    msgvw.backgroundColor = delegate.BackgroudColor;
    UIImageView *nomsgimg;
    UILabel *tab_lab;
    if (delegate.deviceheight <= 460){
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-100)/2,
                                                               0, 100,100)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    else{
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-100)/2,
                                                               20, 100,100)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    nomsgimg.image=[UIImage imageNamed:@"no_comment"];
    [msgvw addSubview:nomsgimg];
    tab_lab.text = message;
    tab_lab.font=delegate.ooredoo;
    tab_lab.textColor=delegate.dimBlack;
    tab_lab.lineBreakMode = NSLineBreakByWordWrapping;
    tab_lab.numberOfLines = 0;
    tab_lab.textAlignment=ALIGN_CENTER;
    [msgvw addSubview:tab_lab];
    [tab addSubview:msgvw];
    tab.scrollEnabled=FALSE;
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
@end
