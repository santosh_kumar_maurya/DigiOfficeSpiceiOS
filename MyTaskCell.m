//
//  MyTaskCell.m
//  iWork
//
//  Created by Shailendra on 04/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTaskCell.h"

@implementation MyTaskCell

- (void)awakeFromNib {
    [super awakeFromNib];
      self.backgroundColor = [UIColor clearColor];
    _TaskView.layer.cornerRadius = 6.0;
    _TaskView.maskView.layer.cornerRadius = 7.0f;
    _TaskView.layer.shadowRadius = 3.0f;
    _TaskView.layer.shadowColor = [UIColor blackColor].CGColor;
    _TaskView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _TaskView.layer.shadowOpacity = 0.7f;
    _TaskView.layer.masksToBounds = NO;

}

@end
