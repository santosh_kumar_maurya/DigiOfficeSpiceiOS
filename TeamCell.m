//
//  TeamCell.m
//  iWork
//
//  Created by Shailendra on 04/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "TeamCell.h"
#import "Header.h"
@implementation TeamCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    _MyTeamView.layer.cornerRadius = 6.0;
    _MyTeamView.maskView.layer.cornerRadius = 7.0f;
    _MyTeamView.layer.shadowRadius = 3.0f;
    _MyTeamView.layer.shadowColor = [UIColor blackColor].CGColor;
    _MyTeamView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _MyTeamView.layer.shadowOpacity = 0.7f;
    _MyTeamView.layer.masksToBounds = NO;
    
    _ProfileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _ProfileImageView.layer.borderWidth = 2;
    _ProfileImageView.layer.cornerRadius = 20;
    _ProfileImageView.clipsToBounds = YES;
    
    self.FilterView.backgroundColor = [UIColor whiteColor];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.FilterView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.FilterView.layer.mask = maskLayer1;
}
- (void)configureCell:(NSDictionary *)info{

    if(IsSafeStringPlus(TrToString(info[@"empImg"]))){
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:info[@"empImg"]]]];
        if(images == nil){
            NSLog(@"image nil");
            _ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            NSString *URL = [NSString stringWithFormat:@"%@",info[@"empImg"]];
            [_ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        _ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
    }
    if(IsSafeStringPlus(TrToString(info[@"empName"]))){
        _UserNameLabel.text = [NSString stringWithFormat:@"%@", info[@"empName"]];
    }
    else{
        _UserNameLabel.text = @"";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"taskCompleted"]))){
        _CompletedLabel.text = [NSString stringWithFormat:@"%@", info[@"taskCompleted"]];
    }
    else{
        _CompletedLabel.text = @"";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskOnSchedule"]))){
        _OnScheduleLabel.text = [NSString stringWithFormat:@"%@", info[@"taskOnSchedule"]];
    }
    else{
        _OnScheduleLabel.text = @"";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskBehind"]))){
        _BehindLabel.text = [NSString stringWithFormat:@"%@", info[@"taskBehind"]];
    }
    else{
        _BehindLabel.text = @"";
    }
    
}

@end
