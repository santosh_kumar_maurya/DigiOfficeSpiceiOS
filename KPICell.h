//
//  KPICell.h
//  iWork
//
//  Created by Shailendra on 29/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPICell : UITableViewCell

@property(nonnull,retain)IBOutlet UILabel *KPILabel;
@property(nonnull,retain)IBOutlet UIView *CustomView;
@end
