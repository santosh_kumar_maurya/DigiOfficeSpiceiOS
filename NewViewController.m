//
//  NewViewController.m
//  iWork
//
//  Created by Shailendra on 08/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "NewViewController.h"
#import "Header.h"
@interface NewViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *NewTableView;
    NSMutableArray *NewArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    
    IBOutlet UILabel *NoDataLabel;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    UIView *CoomentView;
    UITextView *CommentTextView;
    
}

@end

@implementation NewViewController

- (void)viewDidLoad {
    offset = 0;
    limit = 50;
    Api = [[WebApiService alloc] init];
    NewArray = [[NSMutableArray alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    NewTableView.estimatedRowHeight = 50;
    NewTableView.backgroundColor = delegate.BackgroudColor;
    NewTableView.rowHeight = UITableViewAutomaticDimension;
    NewTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, NewTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [NewTableView addSubview:refreshControl];
    
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [NewTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(NewTaskWebApi) withObject:nil afterDelay:0.5];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"NEWTASK" object:nil];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    NewArray = [[NSMutableArray alloc] init];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(NewTaskWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self ClearData];
    [self NewTaskWebApi];
}
-(void)ClearData{
    offset = 0;
    limit = 50;
    NewArray = [[NSMutableArray alloc] init];
}
- (void)NewTaskWebApi {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"FILTER"]){
            ResponseDic =  [Api WebApi:params Url:@"viewAllNewDashTask"];
        }
        else{
            params = @ {
                @"eEndDate" : @"",
                @"eStartDate" :@"",
                @"taskId" :@"",
                @"task_request_status": @"",
                @"task_type": @"",
                @"employeeId" : [ApplicationState userId],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
            ResponseDic =  [Api WebApi:params Url:@"viewAllNewDashTask"];
        }
        [refreshControl endRefreshing];
        
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"NewTaskResponseDic==%@",ResponseDic);
            NoDataLabel.hidden = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    [NewTableView reloadData];
                }
                else if(ResponseArrays.count == 0 && NewArray.count == 0){
                   [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(NewArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No record found";
    }
    [NewTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self NewTaskWebApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [NewArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [NewArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    Cell.DetailsBtn.tag = indexPath.section;
    [Cell.DetailsBtn addTarget:self action:@selector(StartCancelDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    Cell.RatingView.hidden = YES;
    Cell.StausImageView.hidden = NO;
    Cell.StatusLabel.hidden = NO;
    if(NewArray.count>0){
        NSDictionary * responseData = [NewArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
        if ([[[NewArray objectAtIndex:indexPath.section] objectForKey:@"new_status"] isEqualToString:@"New"]){
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
            
            Cell.StatusLabel.text = @"New";
            Cell.StatusLabel.textColor = delegate.pendingColor;
            Cell.StausImageView.image = [UIImage imageNamed:@"pending_new"];
        }
        else if ([[[NewArray objectAtIndex:indexPath.section] valueForKey:@"new_status"] isEqualToString:@"new"]){
            
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
            Cell.StatusLabel.text = @"New";
            Cell.StatusLabel.textColor = delegate.pendingColor;
            Cell.StausImageView.image = [UIImage imageNamed:@"pending_new"];
        }
        else if ([[[NewArray objectAtIndex:indexPath.section] objectForKey:@"new_status"]  isEqualToString:@"In-Progress"]){
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
        }
        else if ([[[NewArray objectAtIndex:indexPath.section] valueForKey:@"new_status"]  isEqualToString:@"Assigned"]){
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"START", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
        }
        else if ([[[NewArray objectAtIndex:indexPath.section] valueForKey:@"new_status"] isEqualToString:@"Declined"] ||([[[NewArray objectAtIndex:indexPath.section] valueForKey:@"status"] isEqualToString:@"Discarded"])){
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =YES;
        }
        else if ([[[NewArray objectAtIndex:indexPath.section] valueForKey:@"new_status"] isEqualToString:@"Approved"] ||([[[NewArray objectAtIndex:indexPath.section] valueForKey:@"status"] isEqualToString:@"Rejected"])){
            Cell.RatingView.hidden = NO;
            Cell.StausImageView.hidden = YES;
            Cell.StatusLabel.hidden = YES;
            Cell.DetailsBtn.hidden =NO;
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
        }
        else{
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
        }
    }
    return Cell;
}
-(void)CommentView{
    
    CoomentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    CoomentView.backgroundColor = [UIColor clearColor];
    UIImageView *ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    ImageView.backgroundColor = [UIColor blackColor];
    ImageView.alpha = 0.3;
    UIView *InnerView = [[UIView alloc] initWithFrame:CGRectMake(10, delegate.deviceheight/2 - 100, delegate.devicewidth -20, 150)];
    CGPoint center = InnerView.center;
    center.x = CoomentView.center.x;
    InnerView.center = center;
    InnerView.layer.cornerRadius = 5.0;
    InnerView.backgroundColor = [UIColor whiteColor];
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(0, InnerView.frame.size.height-40, InnerView.frame.size.width, 40)];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: BtnView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    BtnView.layer.mask = maskLayer1;
    
    UILabel *AddCommentLabel;
    AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 45)];
    AddCommentLabel.backgroundColor = delegate.yellowColor;
    AddCommentLabel.textColor = [UIColor blackColor];
    AddCommentLabel.font = delegate.headFont;
    AddCommentLabel.textAlignment = NSTextAlignmentCenter;
    if([[[NewArray objectAtIndex:TagValue] objectForKey:@"new_status"] isEqualToString:@"Assigned"]){
        AddCommentLabel.text = NSLocalizedString(@"START_TASK", nil);
    }
    else{
        AddCommentLabel.text = NSLocalizedString(@"CANCEL_TASK_SMALL", nil);
    }
    
    CAShapeLayer * AddCommentmaskLayer1 = [CAShapeLayer layer];
    AddCommentmaskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: AddCommentLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){5.0}].CGPath;
    AddCommentLabel.layer.mask = AddCommentmaskLayer1;
    
    UILabel *AddCommentStaticLabel;
    AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, AddCommentLabel.frame.origin.y+AddCommentLabel.frame.size.height + 10, delegate.devicewidth-40, 40)];
    AddCommentStaticLabel.textColor = [UIColor blackColor];
    AddCommentStaticLabel.font = delegate.normalFont;
    AddCommentStaticLabel.numberOfLines = 0;
    AddCommentStaticLabel.textAlignment = NSTextAlignmentCenter;
    if([[[NewArray objectAtIndex:TagValue] objectForKey:@"new_status"] isEqualToString:@"Assigned"]){
        AddCommentStaticLabel.text = NSLocalizedString(@"ARE_YOU_SURE_START", nil);
    }
    else{
        AddCommentStaticLabel.text = NSLocalizedString(@"ARE_YOU_SURE_CANCEL", nil);
    }
    
    UIButton *SubmitBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, BtnView.frame.size.width/2, 40)];
    [SubmitBtn addTarget:self action:@selector(SubmitAction) forControlEvents:UIControlEventTouchUpInside];
    SubmitBtn.backgroundColor = delegate.yellowColor;
    SubmitBtn.titleLabel.font = delegate.normalFont;
    [SubmitBtn setTitle:NSLocalizedString(@"OKAY", nil) forState:UIControlStateNormal];
    
    UIButton *CancelBtn=[[UIButton alloc] initWithFrame:CGRectMake(BtnView.frame.size.width/2, 0, BtnView.frame.size.width/2, 40)];
    [CancelBtn addTarget:self action:@selector(CancelAction) forControlEvents:UIControlEventTouchUpInside];
    CancelBtn.backgroundColor = delegate.redColor;
    CancelBtn.titleLabel.font = delegate.normalFont;
    [CancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    [BtnView addSubview:SubmitBtn];
    [BtnView addSubview:CancelBtn];
    [InnerView addSubview:BtnView];
    [InnerView addSubview:AddCommentLabel];
    [InnerView addSubview:AddCommentStaticLabel];
    [CoomentView addSubview:ImageView];
    [CoomentView addSubview:InnerView];
    
    [self.view addSubview:CoomentView];
}
-(void)CancelAction{
    CoomentView.hidden = YES;
}
-(void)SubmitAction{
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(CancelTaskSubmitAction) withObject:nil afterDelay:0.5];
    
}
-(void)CancelTaskSubmitAction{
    if(delegate.isInternetConnected){
        NSString* TaskID = [[NewArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if([[[NewArray objectAtIndex:TagValue] objectForKey:@"new_status"] isEqualToString:@"Assigned"]){
            ResponseDic =  [Api WebApi:nil Url:[NSString stringWithFormat:@"startButton?employeeId=%@&taskId=%@",[ApplicationState userId],TaskID]];
        }
        else{
            params = @ {
                @"comment" : @"",
                @"status" : @"4",
                @"taskId" : TaskID,
                @"user_id" : [ApplicationState userId]
            };
            ResponseDic =  [Api WebApi:params Url:@"cancelTask"];
        }
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            [self ClearData];
            CoomentView.hidden = YES;
            [self NewTaskWebApi];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)StartCancelDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]||[sender.titleLabel.text isEqualToString:NSLocalizedString(@"START", nil)]){
        [self CommentView];
    }
    else{
        NSString* str = [[NewArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if ([[[NewArray objectAtIndex:TagValue] objectForKey:@"new_status"] isEqualToString:@"In-Progress"]){
            TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
            viewController.taskID = str;
            viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else{
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            viewController.taskID = str;
            viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
