//
//  AppearanceManager.m
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppearanceManager.h"
#import "UIColor+AppColor.h"

@implementation AppearanceManager

+ (void)initialize {
    [self applyGlobalAppearance];
}

+ (void)applyGlobalAppearance {
    
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
    
     [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
     [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
    
    

    //Set the tabbar items attributes    
    [[UIBarButtonItem appearance]setTintColor:[UIColor blackColor]];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor blackColor]} forState:UIControlStateNormal];
    
    //Set the navigation bar items attributes
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor blackColor],
                                               NSForegroundColorAttributeName,
                                               [UIFont systemFontOfSize:18.0],
                                               NSFontAttributeName,
                                               nil];
    
    [UITextField appearance].tintColor = [UIColor whiteColor];
    [UITextView appearance].tintColor = [UIColor whiteColor];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    //set the navigation bar color
    [[UINavigationBar appearance] setBarTintColor:[UIColor appsThemeColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    [[UITextView appearance] setTintColor:[UIColor blackColor]];
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    [[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setTintColor:[UIColor appsThemeColor]];
    
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIColor appsThemeColor],
                                  NSForegroundColorAttributeName,
                                  nil]
                                 forState:UIControlStateNormal];

    
    [[UITextView appearance] setLinkTextAttributes: @
    { NSForegroundColorAttributeName: [UIColor blueColor]
    }];
    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"back_icon"]];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"back_icon"]];
   
    
    
}



@end
