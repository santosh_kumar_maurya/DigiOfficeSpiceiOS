//
//  ApplicationState.h
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplicationState : NSObject

+ (void)setPushToken:(NSString *)aPushToken;
+ (NSString *)currentPushToken;
+ (void)removePushToken ;

+ (BOOL)hasUserOpenedFirstTime;
+ (void)setUserHasOpenedFirstTime;

+ (void)setWalkThroughShown;
+ (BOOL)isWalkThroughShown;


+ (void)setUserIsLoggedIn;
+ (BOOL)hasUserIsLoggedIn;
+ (void)setUserIsLoggedOut;

+ (void)setUserocation:(NSString *)emailId;
+ (NSString *)userLocation;


+ (void)setUserId:(NSString *)userId;
+ (NSString *)userId;
+ (void)setUserKey:(NSString *)userKey;
+ (NSString *)GetUserKey;
+ (void)setToken:(NSString *)Token;
+ (NSString *)GetToken;
+ (void)setHandsetID:(NSString *)Token;
+ (NSString *)GetHandsetID;
    
+ (void)setUserLoginData:(NSDictionary *)userInfo;
+ (NSDictionary *)getUserLoginData;

+ (void)setUserLocationData:(NSDictionary *)userInfo;
+ (NSDictionary *)getUserLocationData ;

+ (void)setUserDashBoardData:(NSDictionary *)userInfo;
+ (NSDictionary *)getUserDashBoardData;

+ (BOOL)rememberMeStatus;
+ (void)setUserRememberMeStatus:(BOOL)status;

+ (void)setUserEmailId:(NSString *)emailId;
+ (NSString *)userEmailId;


+ (void)setUserPassword:(NSString *)emailId;
+ (NSString *)userPassword;

+ (NSInteger)totalNotificationCount;
+ (void)setTotalNotificationCount:(NSInteger)notiCount;


@end
