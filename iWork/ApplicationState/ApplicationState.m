//
//  ApplicationState.m
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import "ApplicationState.h"

static NSString *kApplicationPushTokenKey = @"PushToken";
static NSString *kIsUserOpenedFirstTime =   @"IsUserOpenedFirstTime";
static NSString *kIsWalkThroughHasShown =   @"kIsWalkThroughHasShown";
static NSString *kUserEmailId = @"kUserEmailId";
static NSString *kUserPassword = @"kUserPassword";

static NSString *kUserHasLoggedIn = @"kUserHasLoggedIn";
static NSString *kUserLocation = @"kUserLocation";

static NSString *kUserId = @"kUserId";
static NSString *kUserLoginInfo = @"kUserLoginInfo";
static NSString *kUserLocationInfo = @"kUserLocationInfo";
static NSString *kUserDashBoardInfo = @"kUserDashBoardInfo";
static NSString *kRemeberMeStatus = @"kRemeberMeStatus";

static NSString *kUnreadBadgeCount = @"kUnreadBadgeCount";
static NSString *kUserKey = @"kuserKey";
static NSString *kToken = @"kToken";
static NSString *kHandsetId = @"kHandsetId";

#define USER_DEFAULT [NSUserDefaults standardUserDefaults]

@implementation ApplicationState

#pragma mark- Push Helper
+ (void)setPushToken:(NSString *)aPushToken {
    if ( !aPushToken ) {
        return;
    }
    
    [USER_DEFAULT setObject:aPushToken forKey:kApplicationPushTokenKey];
    [USER_DEFAULT synchronize];
}

+ (NSString *)currentPushToken {
    return  [USER_DEFAULT objectForKey:kApplicationPushTokenKey];
}

+ (void)removePushToken {
    [USER_DEFAULT removeObjectForKey:kApplicationPushTokenKey];
    [USER_DEFAULT synchronize];
}

#pragma mark - Only first Time

+ (BOOL)hasUserOpenedFirstTime {
    return [USER_DEFAULT boolForKey:kIsUserOpenedFirstTime];
}

+ (void)setUserHasOpenedFirstTime {
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:kIsUserOpenedFirstTime];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - walkthroug Shown

+ (void)setWalkThroughShown {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsWalkThroughHasShown];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isWalkThroughShown {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kIsWalkThroughHasShown];
}



#pragma mark - User logged in
+ (void)setUserIsLoggedIn {
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserHasLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
+ (BOOL)hasUserIsLoggedIn {
    return [USER_DEFAULT boolForKey:kUserHasLoggedIn];
}

+ (void)setUserIsLoggedOut {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUserHasLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+ (void)setUserId:(NSString *)userId {
    [[NSUserDefaults standardUserDefaults]setValue:userId forKey:kUserId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)userId {
    return [[NSUserDefaults standardUserDefaults]stringForKey:kUserId];
}
+ (void)setUserKey:(NSString *)userKey {
    [[NSUserDefaults standardUserDefaults]setValue:userKey forKey:kUserKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)GetUserKey {
    return [[NSUserDefaults standardUserDefaults]stringForKey:kUserKey];
}
 + (void)setToken:(NSString *)Token {
    [[NSUserDefaults standardUserDefaults]setValue:Token forKey:kToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
    
+ (NSString *)GetToken {
    return [[NSUserDefaults standardUserDefaults]stringForKey:kToken];
}
+ (void)setHandsetID:(NSString *)Token{
    [[NSUserDefaults standardUserDefaults]setValue:Token forKey:kHandsetId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)GetHandsetID{
    return [[NSUserDefaults standardUserDefaults]stringForKey:kHandsetId];
}
+ (void)setUserLoginData:(NSDictionary *)userInfo {

    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:userInfo];
    [[NSUserDefaults standardUserDefaults]setObject:data forKey:kUserLoginInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSDictionary *)getUserLoginData {
    
    NSData *data = [[NSUserDefaults standardUserDefaults]objectForKey:kUserLoginInfo];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+ (void)setUserLocationData:(NSDictionary *)userInfo {
    
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:userInfo];
    [[NSUserDefaults standardUserDefaults]setObject:data forKey:kUserLocationInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSDictionary *)getUserLocationData {
    
    NSData *data = [[NSUserDefaults standardUserDefaults]objectForKey:kUserLocationInfo];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}


+ (void)setUserDashBoardData:(NSDictionary *)userInfo {
    
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:userInfo];
    [[NSUserDefaults standardUserDefaults]setObject:data forKey:kUserDashBoardInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSDictionary *)getUserDashBoardData {
    
    NSData *data = [[NSUserDefaults standardUserDefaults]objectForKey:kUserDashBoardInfo];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

#pragma mark - Email Id
+ (void)setUserocation:(NSString *)emailId {
    [[NSUserDefaults standardUserDefaults]setValue:emailId forKey:kUserLocation];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)userLocation {
    return [[NSUserDefaults standardUserDefaults]stringForKey:kUserLocation];
}

#pragma mark - Email Id
+ (void)setUserEmailId:(NSString *)emailId {
    [[NSUserDefaults standardUserDefaults]setValue:emailId forKey:kUserEmailId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)userEmailId {
    return [[NSUserDefaults standardUserDefaults]stringForKey:kUserEmailId];
}

+ (void)setUserPassword:(NSString *)emailId {
    [[NSUserDefaults standardUserDefaults]setValue:emailId forKey:kUserPassword];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)userPassword {
    return [[NSUserDefaults standardUserDefaults]stringForKey:kUserPassword];
}

+ (BOOL)rememberMeStatus {
    return [USER_DEFAULT boolForKey:kRemeberMeStatus];
}

+ (void)setUserRememberMeStatus:(BOOL)status {
    [[NSUserDefaults standardUserDefaults] setBool:status forKey:kRemeberMeStatus];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger)totalNotificationCount {
    return [USER_DEFAULT integerForKey:kUnreadBadgeCount];
}

+ (void)setTotalNotificationCount:(NSInteger)notiCount {
    [[NSUserDefaults standardUserDefaults] setInteger:notiCount forKey:kUnreadBadgeCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
