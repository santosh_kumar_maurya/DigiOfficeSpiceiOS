//
//  Shared.h
//  iWork
//
//  Created by Fourbrick on 15/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#ifndef Shared_h
#define Shared_h

#import <UIKit/UIKit.h>


#define IsSafeArray(a)              ((a)&&(![(a) isEqual:[NSNull null]]) &&((a).count>0))

#define TrToString(args)[NSString stringWithFormat:@"%@",args]
#define IsSafeString(a)             ((a)&& (![(a) isEqual:[NSNull null]]) &&((a).length>0))
#define SafeString(a)               ((((a)==nil)||([(a) isEqual:[NSNull null]])||((a).length==0))?@" ":(a))
#define IsSafeStringPlus(a)             ((a)\
&&(![(a) isEqual:[NSNull null]]) &&((a).length>0)\
&&!([[NSString stringWithFormat:@"%@",(a)] isEqualToString:@"<null>"])\
&&!([[NSString stringWithFormat:@"%@",(a)] isEqualToString:@"(null)"]))

#define kiWorkLocationCreatedNotification  @"kiWorkLocationCreatedNotification"

#define BASE_URL @"http://digioffice.spicelabs.in/iworkapi/"
#define BASE_URL_PMS @"http://digioffice.spicelabs.in/pmsapi/"

// QA
//#define BASE_URL @"http://qadigioffice.spicelabs.in/iworkapi/"
//#define BASE_URL_PMS @"http://qadigioffice.spicelabs.in/pmsapi/"


// Spice
//#define BASE_URL @"http://spicedigioffice.spicelabs.in/iworkapi/"
//#define BASE_URL_PMS @"http://spicedigioffice.spicelabs.in/pmsapi/"



//#define BASE_URL @"http://139.59.225.255/iwork/"
//#define BASE_URL_PMS  @"http://139.59.225.255/"
//#define BASE_URL @"http://iwork.indosatooredoo.com/iwork/"
//#define BASE_URL_PMS  @"http://iwork.indosatooredoo.com/"

#endif /* Shared_h */
