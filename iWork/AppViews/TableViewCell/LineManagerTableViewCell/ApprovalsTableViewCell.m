//
//  ApprovalsTableViewCell.m
//  iWork
//
//  Created by Fourbrick on 18/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "ApprovalsTableViewCell.h"
#import "Shared.h"

@implementation ApprovalsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 0.5;
    
}

- (void)configureApprovalCell:(NSDictionary *)info {

    if(IsSafeStringPlus(TrToString(info[@"userId"]))) {
        _employeeId.text = [NSString stringWithFormat:@"%@",info[@"userId"]];
    } else {
        _employeeId.text = @" ";
    }

    if(IsSafeStringPlus(TrToString(info[@"userName"]))) {
        _employeeName.text = info[@"userName"];
    } else {
        _employeeName.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"requestId"]))) {
        _requestId.text = [NSString stringWithFormat:@"%@",info[@"requestId"]];

    } else {
        _requestId.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"requestDate"]))) {
        NSNumber *datenumber = info[@"requestDate"];
        _requestDate.text = [self DateFormateChange:datenumber];
    } else {
        _requestDate.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"locType"]))) {
        _iWorkLocationTypeLabel.text = info[@"locType"];
    } else {
        _iWorkLocationTypeLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"dateTime"]))) {
        NSNumber *datenumber = info[@"dateTime"];
        _iworkDate.text = [self DateFormateChange:datenumber];
    } else {
        _iworkDate.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"location"]))) {
        _location.text = info[@"location"];
    } else {
        _location.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"task"]))) {
        _TaskLabel.text = info[@"task"];
    } else {
        _TaskLabel.text = @" ";
    }

}

-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
@end
