//
//  ApprovalsTableViewCell.h
//  iWork
//
//  Created by Fourbrick on 18/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApprovalsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *employeeId;
@property (weak, nonatomic) IBOutlet UILabel *employeeName;
@property (weak, nonatomic) IBOutlet UILabel *requestId;
@property (weak, nonatomic) IBOutlet UILabel *requestDate;
@property (weak, nonatomic) IBOutlet UILabel *EmployeeIDStaticLabel;

@property (weak, nonatomic) IBOutlet UILabel *EmployeeNameStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *iworkDate;
@property (weak, nonatomic) IBOutlet UILabel *location;
@property (weak, nonatomic) IBOutlet UIButton *approveButton;
@property (weak, nonatomic) IBOutlet UIButton *rejectButton;
@property (weak, nonatomic) IBOutlet UILabel *TaskLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkLocationTypeStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkLocationTypeLabel;

- (void)configureApprovalCell:(NSDictionary *)info;


@end
