//
//  EmployeeRequestCell.m
//  iWork
//
//  Created by Fourbrick on 18/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "EmployeeRequestCell.h"
#import "Shared.h"

@implementation EmployeeRequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)configureCell:(NSDictionary *)info {
    
    _requestIdLabel.text = @"";
    if(IsSafeStringPlus(TrToString(info[@"user_requests_id"]))) {
        _requestIdLabel.text = info[@"user_requests_id"];
    } else {
        _requestIdLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"request_date_time"]))) {
        _iWorkDateLabel.text = [self dateToFormatedDate:info[@"request_date_time"]];
    } else {
        _iWorkDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"task"]))) {
        _TaskLabel.text = info[@"task"];
    } else {
        _TaskLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"office_address"]))) {
        _iWorkLocationLabel.text = info[@"office_address"];
    } else {
        _iWorkLocationLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"date_time"]))) {
        _requestDateLabel.text = [self dateToFormatedDate:info[@"date_time"]];
    } else {
        _requestDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"action_date"]))) {
        _actionDateLabel.text = info[@"action_date"];
    } else {
        _actionDateLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkin_date"]))) {
        NSArray *Split = [info[@"checkin_date"] componentsSeparatedByString:@" "];
        NSString *DateSplit = Split[1];
        _checkInLabel.text = [self GetDate:DateSplit];
    } else {
        _checkInLabel.text = @"";
    }
    if(IsSafeStringPlus(TrToString(info[@"checkout_date"]))) {
        NSArray *Split = [info[@"checkout_date"] componentsSeparatedByString:@" "];
        NSString *DateSplit = Split[1];
        _checkoutLabel.text = [self GetDate:DateSplit];
    } else {
        _checkoutLabel.text = @"";
    }
    _hoursLabel.text = @" ";
    if(IsSafeStringPlus(TrToString(info[@"checkout_status"]))) {
        
        if ([info[@"checkout_status"] integerValue]==1 ) {
            _hoursLabel.text = @"8:00";
        }else {
            _hoursLabel.text = @"";
        }
    }
    
//    if(IsSafeStringPlus(TrToString(info[@"max_checkin_time"]))) {
//        NSString *last = info[@"max_checkin_time"];
//        //        _MaxLabel.text = [NSString stringWithFormat:@"%@",last];
//        //        _MaxLabel.text = [self GetDate:_MaxLabel.text];
//        _MaxCheckInLabel.text = [self GetDate:last];
//        
//    } else {
//        _MaxCheckInLabel.text = @"";
//    }
    //    if(IsSafeStringPlus(TrToString(info[@"line_manager"]))) {
    //        _lineManagerLabel.text = info[@"line_manager"];
    //    } else {
    //        _lineManagerLabel.text = @" ";
    //    }
    
    if(IsSafeStringPlus(TrToString(info[@"max_checkin_time"]))) {
        NSString *last = [info[@"max_checkin_time"] substringToIndex:5];
        _MaxCheckInLabel.text = [NSString stringWithFormat:@"%@",last];
        _MaxCheckInLabel.text = [self GetDate:_MaxCheckInLabel.text];
    }else{
        _MaxCheckInLabel.text = @"";
    }
    if(IsSafeStringPlus(TrToString(info[@"attendance"]))) {
        _AttandenceStatusLabel.text = info[@"attendance"];
        if([_AttandenceStatusLabel.text isEqualToString:@"-"]){
            _AttandenceStatusLabel.text = @"";
        }
    }else{
        _AttandenceStatusLabel.text = @"";
    }
    if(IsSafeStringPlus(TrToString(info[@"status"]))) {
        
        if ([info[@"status"] integerValue] ==0 ) {
            _StatusLabel.textColor = [UIColor colorWithRed:1.0 green:0.83 blue:00.0 alpha:1.0];
            _StatusLabel.text = @"Pending";
            _StatusImageView.image = [UIImage imageNamed:@"my_request_pending"];
            
        } else if ([info[@"status"] integerValue] ==1 ) {
            
            _StatusLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:88.0/255.0 blue:45.0/255.0 alpha:1.0];
            _StatusLabel.text = @"Approved";
            _StatusImageView.image = [UIImage imageNamed:@"Approved"];
        } else {
            _StatusLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            _StatusLabel.text = @"Rejected";
            _StatusImageView.image = [UIImage imageNamed:@"Rejected"];
        }
    }
        NSString * checkn_btn = info[@"checkn_btn"];
        if([checkn_btn integerValue]==1){
            [_CheckInButton setTitle:NSLocalizedString(@"CHECK_IN_BUTTON", nil) forState:UIControlStateNormal];
            [_CheckInButton setBackgroundColor:[UIColor colorWithRed:1.0 green:0.83 blue:00.0 alpha:1.0]];
             _CheckInButton.hidden = NO;
        }
        else{
            
            NSString * checkin_status = info[@"checkin_status"];
            NSString * checkin_date = info[@"checkin_date"];
            NSString * checkout_status = info[@"checkout_status"];
            NSString * checkout_date;
            
            if( [info[@"checkout_date"] isEqual:[NSNull null]]){
                checkout_date = @"";
            }
            else{
                checkout_date = @"checkout_date";
            }
            
            if([checkin_status integerValue]==1 &&  ![checkin_date isEqual: [NSNull null]] && [checkout_status integerValue]==0 &&  [checkout_date isEqualToString:@""])
            {
                 [_CheckInButton setTitle:NSLocalizedString(@"CHECK_OUT_BUTTON", nil) forState:UIControlStateNormal];
                [_CheckInButton setBackgroundColor:[UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0]];
                _CheckInButton.hidden = NO;

            }
            else{
                _CheckInButton.hidden = YES;
            }
            
        }
}

-(NSString*)GetDate:(NSString*)Time{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:Time];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return  formattedDate;

}
-(NSString *)dateToFormatedDate:(NSString *)dateStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    [dateFormatter setDateFormat:@"dd MMM YYYY"];
    return [dateFormatter stringFromDate:date];
}
@end
