//
//  PopOverView.m
//  Created by Fourbrick on 17/03/16.
//
//
#import <UIKit/UIKit.h>
#import "PopOverView.h"

@interface PopOverView() <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *menuItems;
@property (assign, nonatomic) NSInteger  selectedIndex;
@end

@implementation PopOverView

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    return self;
}

+ (PopOverView *)popOverView {
    PopOverView *alertView = [[[NSBundle mainBundle] loadNibNamed:@"PopOverView" owner:self options:nil] lastObject];
    return alertView;
}

- (void)createOptionTableView:(NSArray *)menuItems selectedIndex:(NSInteger)selIndex {
    
    _menuItems = [[NSMutableArray alloc]init];
    [_menuItems addObjectsFromArray:menuItems];
    _selectedIndex = selIndex;
    
    _tableView.tableFooterView = [[UIView alloc]init];
    _tableView.scrollEnabled=false;
    
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CELL"];
    self.tableView.backgroundColor = [UIColor whiteColor];
   
    self.tableView.rowHeight = 50;
    _tableView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.width)  ;
    
    [self.tableView reloadData];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    cell.textLabel.text = _menuItems[indexPath.row];
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([self.delegate respondsToSelector:@selector(popUpOptionSelected:)]) {
       [_delegate popUpOptionSelected:indexPath.row];
    }
}

@end
