//
//  LocationModel.h
//  iWork
//
//  Created by Fourbrick on 17/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationModel : NSObject
@property(readwrite)BOOL isSelected;
@property(strong)NSString *officeAddress;
@property(assign)NSInteger AbilableSlot;
@property(assign)NSInteger workLocationId;
@property(strong)NSString *imageName;
@end
