  //
//  AppDelegate.m
//  iWork
//
//  Created by Fourbrick on 04/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
// Next

#import <UserNotifications/UserNotifications.h>
#import "Header.h"

NSString* const NotificationCategoryIdent1 = @"ACTIONABLE";
NSString* const NotificationActionOneIdent1 = @"ACTION_CANCEL";
NSString* const NotificationActionTwoIdent1 = @"ACTION_OK";

@import Firebase;

@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate,CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
}

@end

@implementation AppDelegate
@synthesize devicewidth,deviceheight,headerheight,barheight,nav,alertflag,boldFont,normalFont,normalFont1,smallFont,mediumFont,imsi,btnCollor,btnbgCollor,margin,dataDict,headerbgColler,borderColor,blackcolor,iscorner,areastr, bgColour, redColor,normalBold,dimColor,dataFull,count,buttonFont,dimBlack,smallFont2,tabheight,largeFont,headFont,contentFont,yellowColor,contentBigFont,confColor,susColor,inprogColor,pendingColor,failedColor,cancelColor,submittedColor,rejectedColor,discardedColor,contentSmallFont,assignColor,contentSmallFont1,userLat,userLong,viewtask,ProgressColor,BackgroudColor;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
     [self DidfinishData];
    
    
    return YES;
}

-(void)DidfinishData{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil];
    [self SetGoogleMapService];
    [self SetProperty];
    [self GetVersionNumber];
    _userRequestsArray = [[NSMutableArray alloc]init];
    [UIStoryboard setMainStoryboardName:@"Main"];
    [AppearanceManager applyGlobalAppearance];
    
    if ([ApplicationState hasUserIsLoggedIn]) {
        [[AppDelegate appDelegate] ContainerView];
    } else {
        [[AppDelegate appDelegate] signedOut];
        
    }
    [self GetCurrentLocation];
    // Use Firebase library to configure APIs
    [FIRApp configure];
    
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [FIRMessaging messaging].remoteMessageDelegate = self;
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        // For iOS 10 data message (sent via FCM)
        // [FIRMessaging messaging].remoteMessageDelegate = self;
#endif
    }
    
    
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    // Add observer to listen for the token refresh notification.
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(onTokenRefresh)
     name:kFIRInstanceIDTokenRefreshNotification object:nil];
}
- (void)onTokenRefresh {
    // Get the default token if the earlier default token was nil. If the we already
    // had a default token most likely this will be nil too. But that is OK we just
    // wait for another notification of this type.
     NSString *token = [[FIRInstanceID instanceID] token];
    // custom stuff as before.
     [self connectToFcm];
}

-(void)SetGoogleMapService{
    [GMSServices provideAPIKey:@"AIzaSyBn16sFlumU5BMaQwx0YQpaaHwkFiJ8AhU"];
    //[GMSServices provideAPIKey:@"AIzaSyC_pJvHtm6_uzj0zBUxMxwe-EWK6pKfqSg"];
    //[GMSServices provideAPIKey:@"AIzaSyCYRVoRVGq2pt7U04_0Q1mGazeQxgyEg6g"];
    
}
-(void)SetProperty{
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    
    self.alertflag=TRUE;
    self.margin=10;
    self.headerheight=60;
    self.barheight=0;
    iscorner=true;
    areastr=@"";
    
#if TARGET_IPHONE_SIMULATOR
    self.imsi=@"69becfaa6649969cf221fb21ea3b4f2c7879a474";
    
#else
    
    if([[UIDevice currentDevice].systemVersion floatValue]>=6.0)
        self.imsi=[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    else
    {
        long randNum = random(); //create the random number.
        self.imsi=[NSString stringWithFormat:@"%@gk%ld",[self getTid], randNum];
    }
    
#endif
    self.headerbgColler=[UIColor colorWithRed:237.0/255 green:28.0/255 blue:36.0/255 alpha:1.0];//#FFFFFF
    self.borderColor=[UIColor colorWithRed:200.0/255 green:199.0/255 blue:204.0/255 alpha:1.0];//#C8C7CC
    //self.bgColour=[UIColor colorWithRed:240.0/255 green:240.0/255 blue:238.0/255 alpha:1.0];//#F0F0EE
    self.bgColour = [UIColor colorWithRed:0.94 green:0.97 blue:1.00 alpha:1.0];
    self.btnCollor=[UIColor colorWithRed:239.0/255 green:239.0/255 blue:244.0/255 alpha:1.0];//#EFEFF4
    self.btnbgCollor=[UIColor colorWithRed:237.0/255 green:28.0/255 blue:36.0/255 alpha:1.0];//##c8c7cc
    
    self.blackcolor=[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0];
    
    //self.dimBlack=[UIColor colorWithRed:96.0/255 green:96.0/255 blue:96.0/255 alpha:1.0];
    self.dimBlack=[UIColor colorWithRed:116.0/255 green:116.0/255 blue:116.0/255 alpha:1.0];
   // self.dimColor=[UIColor colorWithRed:160.0/255 green:160.0/255 blue:160.0/255 alpha:1.0];
    self.dimColor = [UIColor darkGrayColor];
    
    self.yellowColor=[UIColor colorWithRed:255.0/255 green:212.0/255 blue:12.0/255 alpha:1.0];
    
    self.confColor = [UIColor colorWithRed:0.0/255 green:128.0/255 blue:0.0/255 alpha:1.0];
    self.susColor = [UIColor colorWithRed:237.0/255 green:28.0/255 blue:36.0/255 alpha:1.0];
    self.inprogColor = [UIColor colorWithRed:68.0/255 green:157.0/255 blue:239.0/255 alpha:1.0];
    self.pendingColor = [UIColor colorWithRed:255.0/255 green:152.0/255 blue:0.0/255 alpha:1.0];
    self.failedColor = [UIColor colorWithRed:134.0/255 green:34.0/255 blue:34.0/255 alpha:1.0];
    self.cancelColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:128.0/255 alpha:1.0];//#000080
    self.submittedColor = [UIColor colorWithRed:63.0/255 green:71.0/255 blue:82.0/255 alpha:1.0];//#000080
    self.rejectedColor = [UIColor colorWithRed:255.0/255 green:112.0/255 blue:67.0/255 alpha:1.0];//#FF7043
    self.discardedColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:128.0/255 alpha:1.0];//#000080
    
    self.assignColor = [UIColor colorWithRed:46.0/255 green:204.0/255 blue:113.0/255 alpha:1.0];
    self.viewtask =  [UIColor colorWithRed:0.27/255.0 green:0.62/255.0 blue:0.94/255.0 alpha:1.0];
    self.ProgressColor=  [UIColor colorWithRed:253/255.0 green:243/255.0 blue:230/255.0 alpha:1.0];
    self.BackgroudColor =[UIColor colorWithRed:241.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    self.boldFont=[UIFont fontWithName:@"Roboto-Bold" size:16];
    self.largeFont=[UIFont fontWithName:@"Roboto-Regular" size:16];
    self.normalFont=[UIFont fontWithName:@"Roboto-Regular" size:15]; //Emulator 13 // Phone 15
    self.normalFont1=[UIFont fontWithName:@"Roboto-Regular" size:13];//Emulator 12 // Phone 14
    self.smallFont=[UIFont fontWithName:@"Roboto-Regular" size:9];
    self.mediumFont=[UIFont fontWithName:@"Roboto-Regular" size:12]; //Emulator 10 //Phone 12
    self.normalBold=[UIFont fontWithName:@"Roboto-Bold" size:10];
    self.buttonFont=[UIFont fontWithName:@"Roboto-Bold" size:14];
    self.tabheight=37;
    self.smallFont2=[UIFont fontWithName:@"FuturaStd-Book" size:9];
    self.headFont=[UIFont fontWithName:@"Futura-Medium" size:17]; // ooredoo-Bold
    self.contentFont=[UIFont fontWithName:@"FuturaStd-Book" size:12];

    self.contentSmallFont=[UIFont fontWithName:@"FuturaStd-Book" size:12];
    self.contentSmallFont1=[UIFont fontWithName:@"FuturaStd-Book" size:10];
    self.contentBigFont=[UIFont fontWithName:@"FuturaStd-Book" size:16];
    self.redColor = [UIColor colorWithRed:237.0/255 green:28.0/255 blue:36.0/255 alpha:1.0];
    self.deviceheight=[[UIScreen mainScreen] bounds].size.height;
    self.devicewidth=[[UIScreen mainScreen] bounds].size.width;
    self.deviceheight=self.deviceheight-self.barheight;
    if ([@"6.9.9" compare:[[UIDevice currentDevice] systemVersion] options:NSNumericSearch] != NSOrderedAscending)
        self.barheight=0;
    
    self.ooredoo = [UIFont fontWithName:@"ooredoo-Bold" size:12];
    
    
}
#pragma mark - SignedIn
- (void)signedIn {
    
    [self changeRootViewController:[UIStoryboard instantiateViewControllerWithSegueIdentifier:@"NavDashBoardViewController"]];

    
}
-(void)ContainerView{
  
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    UINavigationController *Navigation = [[UINavigationController alloc] initWithRootViewController:ObjAppContainerViewController];
    self.window.rootViewController = Navigation;

}
-(void)LaunchView:(NSDictionary*)Dics{
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    LaunchViewController *ObjLaunchViewController = [storyboard instantiateViewControllerWithIdentifier:@"LaunchViewController"];
//    ObjLaunchViewController.Dic = Dics;
//    UINavigationController *Navigation = [[UINavigationController alloc] initWithRootViewController:ObjLaunchViewController];
//    Navigation.navigationBarHidden = YES;
//    self.window.rootViewController = Navigation;
    
}
- (void)signedOut {
    [self changeRootViewController:[UIStoryboard instantiateViewControllerWithSegueIdentifier:@"signInNavigationController"]];
}
+(AppDelegate *)appDelegate {
    return (AppDelegate *) [[UIApplication sharedApplication]delegate];
}

- (void)changeRootViewController:(UIViewController*)viewController {
    
    if (!self.window.rootViewController) {
        UINavigationController *Navigation = [[UINavigationController alloc] initWithRootViewController:viewController];
        self.window.rootViewController = Navigation;
        return ;
    }
    //UINavigationController *Navigation = [[UINavigationController alloc] initWithRootViewController:viewController];
    self.window.rootViewController = viewController;
}
-(NSString*)GetVersionNumber{
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSLog(@"build===%@",build);
    return build;
}
-(void)GetCurrentLocation{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
}
#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
   
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil) {
        userLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        userLong = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
     NSLog(@"userLat: %@", userLat);
     NSLog(@"userLong: %@", userLong);
}
- (void)applicationWillResignActive:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil];
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
     [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    //your app specific code here for handling the launch
    
    return YES;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    NSLog(@"url===%@",url);
    NSLog(@"sourceApplication===%@",sourceApplication);
   
    if ([application canOpenURL:url]) {
        [application openURL:url];
    }
    else{
        NSLog(@"NO");
    }

    
    return YES;
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil];
     [self connectToFcm];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
}


- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
   [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}

- (void) connectToFcm {
    
    [[FIRMessaging messaging]connectWithCompletion:^(NSError * _Nullable error) {
        
    }];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
     NSLog(@"userInfo===%@",userInfo);
    NSString *AppType = [userInfo valueForKey:@"app_type"];
    if([AppType isEqualToString:@"ipm"]){
       // [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil];
        NSMutableDictionary *Dic = [[NSMutableDictionary alloc] init];
        [Dic setValue:@"IPM" forKey:@"IPM"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil userInfo:Dic];
        [self launcDashBoadScreen:userInfo];
    }
    else{
//    if (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateActive) {
        [self SaveNotificationCounter:userInfo];
        
        NSMutableDictionary *Dic = [[NSMutableDictionary alloc] init];
        [Dic setValue:@"IWORK" forKey:@"IWORK"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil userInfo:Dic];
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil];
        if([userInfo valueForKey:@"statusCode"] == nil){
            if([[userInfo valueForKey:@"type"] integerValue] == 1){  //Get Line Manager notification for Employee
                [self GotoLineManagerDashBoard:userInfo];
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 2){  // Get Line Manager Notification for Employee Check in
                [self GotoLineManagerDashBoard:userInfo];
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 3){  // Get Line Manager Notification for Employee Check out
                [self GotoLineManagerDashBoard:userInfo];
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 4){  // Get Emaployee Notification accepted your request by manager
                [self GotoEmployeeDashBoard:userInfo];
                
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 5){  // Get Emaployee Notification accepted your reject by manager
                [self GotoEmployeeDashBoard:userInfo];
            }
        }
        else{
            [self launcDashBoadScreen:userInfo];
        }
        return;
   // }
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:
(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"userInfo1===%@",userInfo);
    NSString *AppType = [userInfo valueForKey:@"app_type"];
    if([AppType isEqualToString:@"ipm"]){
       [self SaveNotificationCounter:userInfo];
       // [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil];
        NSMutableDictionary *Dic = [[NSMutableDictionary alloc] init];
        [Dic setValue:@"IPM" forKey:@"IPM"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil userInfo:Dic];
        [self launcDashBoadScreen:userInfo];
    }
    else{
    
    
    NSDictionary *Dic = [ApplicationState getUserLoginData];
    NSString *user_type = [Dic valueForKey:@"user_type"];
    
        
//    if (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateActive) {
        [self SaveNotificationCounter:userInfo];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"Notification" object:nil];
        
        NSMutableDictionary *Dics = [[NSMutableDictionary alloc] init];
        [Dics setValue:@"IWORK" forKey:@"IWORK"];
        
        if([userInfo valueForKey:@"statusCode"] == nil){
            if([[userInfo valueForKey:@"type"] integerValue] == 1){  //Get Line Manager notification for Employee
                if([user_type integerValue]== 3){
                    [self GotoEmployeeDashBoard:userInfo];
                }
                else if([user_type integerValue]== 1){
                    [self GotoLineManagerDashBoard:userInfo];
                }
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 2){  // Get Line Manager Notification for Employee Check in
                if([user_type integerValue]== 3){
                    [self GotoEmployeeDashBoard:userInfo];
                }
                else if([user_type integerValue]== 1){
                    [self GotoLineManagerDashBoard:userInfo];
                }
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 3){  // Get Line Manager Notification for Employee Check out
                if([user_type integerValue]== 3){
                    [self GotoEmployeeDashBoard:userInfo];
                }
                else if([user_type integerValue]== 1){
                    [self GotoLineManagerDashBoard:userInfo];
                }
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 4){  // Get Emaployee Notification accepted your request by manager
                if([user_type integerValue]== 3){
                    [self GotoEmployeeDashBoard:userInfo];
                }
                else if([user_type integerValue]== 1){
                    [self GotoLineManagerDashBoard:userInfo];
                }
                
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 5){  // Get Emaployee Notification your reject  by manager
                if([user_type integerValue]== 3){
                    [self GotoEmployeeDashBoard:userInfo];
                }
                else if([user_type integerValue]== 1){
                    [self GotoLineManagerDashBoard:userInfo];
                }
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 6){  // Employee Can check in
                if([user_type integerValue]== 3){
                    [self GotoEmployeeDashBoard:userInfo];
                }
                else if([user_type integerValue]== 1){
                    [self GotoLineManagerDashBoard:userInfo];
                }
                
            }
            else if([[userInfo valueForKey:@"type"] integerValue] == 7){  // Employee Can check Out
                if([user_type integerValue]== 3){
                   [self GotoEmployeeDashBoard:userInfo];
                }
                else if([user_type integerValue]== 1){
                    [self GotoLineManagerDashBoard:userInfo];
                }
                
            }
        }
        else{
          [self launcDashBoadScreen:userInfo];
        }
        completionHandler(UIBackgroundFetchResultNoData);
        return;
   // }
    }

    
}
-(void)GotoLineManagerDashBoard:(NSDictionary*)Dic{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APNS_NOTIFICATION" object:nil userInfo:Dic];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ParentDashbordViewController *ObjParentDashbordViewController = [storyboard instantiateViewControllerWithIdentifier:@"ParentDashbordViewController"];
     ObjParentDashbordViewController.isComeFrom = @"NOTIFICATION";
    ObjParentDashbordViewController.NotiDic = Dic;
    UINavigationController *Navigation = [[UINavigationController alloc] initWithRootViewController:ObjParentDashbordViewController];
    Navigation.navigationBarHidden = YES;
    self.window.rootViewController = Navigation;
}
-(void)GotoEmployeeDashBoard:(NSDictionary*)Dic{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EmployeeDashBoardViewController *ObjEmployeeDashBoardViewController = [storyboard instantiateViewControllerWithIdentifier:@"EmployeeDashBoardViewController"];
    ObjEmployeeDashBoardViewController.isComeFrom = @"NOTIFICATION";
     ObjEmployeeDashBoardViewController.NoticationDic = Dic;
    UINavigationController *Navigation = [[UINavigationController alloc] initWithRootViewController:ObjEmployeeDashBoardViewController];
    Navigation.navigationBarHidden = YES;
    self.window.rootViewController = Navigation;
}
#pragma FIRMessagingDelegate
- (void)applicationReceivedRemoteMessage:(nonnull FIRMessagingRemoteMessage *)remoteMessage {
     [self launcDashBoadScreen:remoteMessage.appData];
}

- (void)launcDashBoadScreen:(NSDictionary *)dictonary {
    NSLog(@"dictonary===%@",dictonary);
    
    NSString *AppType = [dictonary valueForKey:@"app_type"];
    NSString *AppTypeStr  = @"";
    if([AppType isEqualToString:@"ipm"]){
        AppTypeStr = @"&#9432;PM";
    }
    else{
        AppTypeStr = @"&#9432;Work";
    }
    [UIApplication sharedApplication].applicationIconBadgeNumber =0;
    OpinionzAlertView *alert = [[OpinionzAlertView alloc] initWithTitle:[AppTypeStr stringByConvertingHTMLToPlainText]
                                                                message:dictonary[@"msg"]
                                                      cancelButtonTitle:NSLocalizedString(@"CLOSE", nil)
                                                      otherButtonTitles:nil];
    alert.iconType = OpinionzAlertIconSuccess;
    [alert show];
    //[[NSNotificationCenter defaultCenter]postNotificationName:kiWorkLocationCreatedNotification object:nil];

}
-(void)SaveNotificationCounter:(NSDictionary*)Dic{
    NSString *AppType = [Dic valueForKey:@"app_type"];
    NSString *AppTypeStr = @"";
    if([AppType isEqualToString:@"ipm"]){
        AppTypeStr = @"IPM";
    }
    else{
        AppTypeStr = @"IWORK";
    }
        
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSManagedObject *NoficationCounter = [NSEntityDescription insertNewObjectForEntityForName:AppTypeStr inManagedObjectContext:context];
    NSString *msg = [Dic valueForKey:@"msg"];
    [NoficationCounter setValue:msg forKey:@"noOfNoti"];
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    
}
-(int)FetchNotificationCounter:(NSString*)AppType{
    
    NSManagedObjectContext *managedObjectContext = self.persistentContainer.viewContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:AppType];
    NSMutableArray *CounterNoti  = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    return CounterNoti.count;
    
}
-(void)DeleteNotificationCounter:(NSString*)AppType{
    NSManagedObjectContext *managedObjectContext = self.persistentContainer.viewContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:AppType];
    NSMutableArray *CounterNoti  = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    for (NSManagedObject *managedObject in CounterNoti) {
        [managedObjectContext deleteObject:managedObject];
    }
    [self saveContext];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"iWork"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

-(void)AnimationStart{
    [UIView beginAnimations:@"Start" context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationsEnabled:YES];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
}
-(void)AnimationClose{
    [UIView commitAnimations];
}

-(NSString*)getTid
{
    NSDate *today = [NSDate dateWithTimeIntervalSinceNow:0];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddhhmmss"];
    return [dateFormat stringFromDate:today];
}




/// PMS ----
-(bool)isInternetConnected
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    bool isinternetavail=false;
    if (networkStatus == NotReachable) {
        [self showAlert:NSLocalizedString(@"CONNECTION_ERR_MSG",@"")];
        isinternetavail=false;
    } else {
        isinternetavail=true;
    }
    return isinternetavail;
}

-(void)showAlert:(NSString*)alertMessage{
    if(alertflag){
        alertflag=FALSE;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"APP_NAME",@"") message:alertMessage
                                                       delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] ;
        alert.tag=100;
        [alert show];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    alertflag=TRUE;
}

-(NSString*)SSOAuth: (NSMutableDictionary*) _params{
    //[self showAlert:NSLocalizedString(@"CONNECTION_ERR_MSG",@"")];
    NSLog(@"_params===>%@",_params);
    if([self isInternetConnected]){
        NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/appprofapi"];

        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:[self GetVersionNumber] forHTTPHeaderField: @"appversion"];
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        
        [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
        
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:body];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"responseString===>%@",responseString);
        return responseString;
    }
    else{
        NSString* msg = @"Please check your internet connection.";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
    }
    return @"";
}

-(NSString*)StackHolder:(NSMutableDictionary*) _params{
    if([self isInternetConnected]){
         NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/stakeholders/search"];
            
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        
        [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
        
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:body];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"responseString===>%@",responseString);
        return responseString;
    }
    else{
        NSString* msg = @"Please check your internet connection.";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
    }
    return @"";
}
-(NSString*)postRequestForLogin: (NSMutableDictionary*) _params{
    //[self showAlert:NSLocalizedString(@"CONNECTION_ERR_MSG",@"")];
    NSLog(@"_params===>%@",_params);
    if([self isInternetConnected]){
        NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/login"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        
        [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
        
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:body];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"responseString===>%@",responseString);
        return responseString;
    }
    else{
        NSString* msg = @"Please check your internet connection.";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
    }
    return @"";
}

-(NSString*)postRequestForLogin1: (NSMutableDictionary*) _params{
    //[self showAlert:NSLocalizedString(@"CONNECTION_ERR_MSG",@"")];
    NSLog(@"_params===>%@",_params);
    if([self isInternetConnected]){
         NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/login"];
        //NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
        
        //NSMutableData *body = [NSMutableData data];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSMutableDictionary *rus = [[NSMutableDictionary alloc] init];
        // add params (all params are strings)
        for (NSString *param in _params) {
            [rus setValue:[_params objectForKey:param] forKey:param];
        }
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:rus constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
        } error:nil];
        
        
        //manager.responseSerializer.acceptableContentTypes = nil;
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          // This is not called back on the main queue.
                          // You are responsible for dispatching to the main queue for UI updates
                          dispatch_async(dispatch_get_main_queue(), ^{
                              //Update the progress view
                              //[progressView setProgress:uploadProgress.fractionCompleted];
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          if (error) {
                              NSLog(@"Error: %@", error);
                          } else {
                              NSLog(@"%@ %@", response, responseObject);
                          }
                      }];
        
        [uploadTask resume];
        
        /*NSURLResponse *response = NULL;
         NSError *requestError = NULL;
         NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
         NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
         NSLog(@"responseString===>%@",responseString);
         return responseString;*/
    }
    else{
        NSString* msg = @"Please check your internet connection.";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
    }
    return @"";
}
-(NSString*)postRequestForGetNotifications:(NSMutableDictionary *)_params{
    //[self showAlert:NSLocalizedString(@"CONNECTION_ERR_MSG",@"")];
    NSLog(@"_params===>%@",_params);
    if([self isInternetConnected]){
        NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/getNotifications"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        
        [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
        
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:body];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"responseString===>%@",responseString);
        return responseString;
    }
    else{
        NSString* msg = @"Please check your internet connection.";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
    }
    return @"";
}


-(NSString*)postRequestForNewTask: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
    NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/mytaskcreate"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    return responseString;
}

-(NSString*)postRequestForMyTask: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
    NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/mytaskShow"];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    return responseString;
}

-(NSString*)postRequestForEmployeeDetails:(NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
    NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/employeeTasksL"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    return responseString;
}

-(NSString*)postRequestForMyReporties:(NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
     NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/employeePerformance"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    return responseString;
}

-(NSString*)postRequestForTaskDetails: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
     NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/viewDetail"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    return responseString;
}


-(NSString*)postRequestForSetStatus: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
    NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/manageTask"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    return responseString;
}

-(NSString*)postRequestForFollowOn: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
     NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/taskFollowOn"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    return responseString;
}

-(NSString*)postRequestForGetConversation: (NSMutableDictionary*) _params{
    NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/getConversation"];
    NSLog(@"_params===>%@",_params);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:
                                                url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    if ([responseString isEqualToString:@""])
    {
        NSString* msg = @"Failed to connect to server. Try again";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
        return @"";
    }
    
    return responseString;
}

-(NSString*)postRequestForFeedback: (NSMutableDictionary*) _params{
     NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/myTaskFeedback"];
    NSLog(@"_params===>%@",_params);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:
                                                 url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    if ([responseString isEqualToString:@""])
    {
        NSString* msg = @"Failed to connect to server. Try again";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
    }
    
    return responseString;
}

-(NSString*)postRequestForMyRequest: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
     NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/taskRequestQueue"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:
                                                url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    if ([responseString isEqualToString:@""])
    {
        NSString* msg = @"Failed to connect to server. Try again";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
    }
    //[self registerApplicationForPushNotifications];
    return responseString;
}

-(NSString*)postRequestForDashboard: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
     NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/myDash"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:
                                                 url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    if ([responseString isEqualToString:@""])
    {
        NSString* msg = @"Failed to connect to server. Try again";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
    }
    
    return responseString;
}

-(NSString*)postRequestForDashboardDetails: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
    NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/myDashApis"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:
                                                 url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    if ([responseString isEqualToString:@""])
    {
        NSString* msg = @"Failed to connect to server. Try again";
        SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
        [snackbar show];
    }
    
    return responseString;
}


-(NSString*)postRequestForRegID: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
    NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/gcmInfo"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:
                                                 url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    return responseString;
}

-(NSString*)postRequestForFilter: (NSMutableDictionary*) _params{
    
    NSLog(@"_params===>%@",_params);
    NSString*  url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/filter"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url]];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *postLength1 = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    [request setValue:postLength1 forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"responseString===>%@",responseString);
    return responseString;
}


#pragma mark - Push Notification Methods

- (void)registerApplicationForPushNotifications//:(UIApplication *)application
{
    UIMutableUserNotificationAction* action1;
    action1 = [[UIMutableUserNotificationAction alloc] init];
    [action1 setActivationMode:UIUserNotificationActivationModeBackground];
    [action1 setTitle:@"Cancel"];
    [action1 setIdentifier:NotificationActionOneIdent1];
    [action1 setDestructive:YES];
    [action1 setAuthenticationRequired:NO];
    
    UIMutableUserNotificationAction* action2;
    action2 = [[UIMutableUserNotificationAction alloc] init];
    [action2 setActivationMode:UIUserNotificationActivationModeBackground];
    [action2 setTitle:@"OK"];
    //[action2 setBehavior:UIUserNotificationActionBehaviorTextInput];
    [action2 setIdentifier:NotificationActionTwoIdent1];
    [action2 setDestructive:NO];
    [action2 setAuthenticationRequired:NO];
    
    UIMutableUserNotificationCategory* actionCategory;
    actionCategory = [[UIMutableUserNotificationCategory alloc] init];
    [actionCategory setIdentifier:NotificationCategoryIdent1];
    [actionCategory setActions:@[ action1, action2 ]
                    forContext:UIUserNotificationActionContextDefault];
    
    [actionCategory setActions:@[ action1, action2 ]
                    forContext:UIUserNotificationActionContextMinimal];
    
    NSSet* categories = [NSSet setWithObjects:actionCategory, nil];
    
    UIUserNotificationType types =
    (UIUserNotificationTypeAlert | UIUserNotificationTypeSound | UIUserNotificationTypeBadge);
    
    UIUserNotificationSettings* settings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    // Register device for iOS8
    /*UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
     [[UIApplication sharedApplication]  registerUserNotificationSettings:notificationSettings];
     [[UIApplication sharedApplication]  registerForRemoteNotifications];*/
}


- (void)application:(UIApplication*)application
handleActionWithIdentifier:(nullable NSString*)identifier
forRemoteNotification:(NSDictionary*)userInfo
   withResponseInfo:(NSDictionary*)responseInfo
  completionHandler:(void (^) ())completionHandler {
    if ([identifier isEqualToString:NotificationActionTwoIdent1]) {
        // Do something associated with OK button action
    } else {
        // Do something associated with Cancel button action
    }
    if (completionHandler) {
        completionHandler ();
    }
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//    // Get Message from Metadata
//    NSLog(@"Application did complete remote notification sync");
//    NSString* msg = @"Application did complete remote notification sync";
//    SSSnackbar *snackbar = [self snackbarForQuickRunningItem:msg];
//    [snackbar show];
//    //[self fireLocalNotification];
//}

/*Fire a local Notification: For testing purposes we are just going to fire this immediately after launch. But this will give you a general idea of how to create a UILocalNotification app-wide:*/
- (void)fireLocalNotification {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = @"We are just testing -";
    notification.category = NotificationCategoryIdent1;
    
    notification.fireDate = [NSDate dateWithTimeInterval:15 sinceDate:[NSDate date]];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

- (SSSnackbar *)snackbarForQuickRunningItem:(NSString *)msg {
    NSString *snackbarMessage = msg;
    
    SSSnackbar *snackbar = [SSSnackbar snackbarWithMessage:snackbarMessage
                                                actionText:NSLocalizedString(@"OKAY", nil)
                                                  duration:5
                                               actionBlock:nil                                            dismissalBlock:^(SSSnackbar *sender){
                                                   
                                               }];
    return snackbar;
}

@end
