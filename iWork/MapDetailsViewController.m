//
//  MapDetailsViewController.m
//  iWork
//
//  Created by Shailendra on 16/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MapDetailsViewController.h"
#import "Header.h"

@interface MapDetailsViewController (){
    AppDelegate *delegate;
    double Lat;
    double Long;
    double Lat1;
    double Long1;
}
@end

@implementation MapDetailsViewController
@synthesize MapDic,isComeFrom,RequestID;

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    RequestIDValueLabel.text = @" ";
    iWorkDateValueLabel.text = @" ";
    TaskValueLabel.text = @" ";
    RequestDateValueLabel.text = @" ";
    ActiontDateValueLabel.text = @" ";
    LocationValueLabel.text = @" ";
    LocationTypeValueLabel.text = @" ";
    CheckinValueLabel.text = @" ";
    CheckoutvalueLabel.text = @" ";
    HoursValueLabel.text = @" ";
    MaxTimeValueLabel.text = @" ";
    AttandenceValueLabel.text = @" ";
    StatusValueLabel.text = @" ";
    StatusImageView.image = [UIImage imageNamed:@""];
}
-(void)viewWillAppear:(BOOL)animated{
    [self GetData];
}
-(void)GetData{
    if(IsSafeStringPlus(TrToString(MapDic[@"locLatitude"]))) {
        Lat = [MapDic[@"locLatitude"] doubleValue];
    }
    else{
        Lat = 0.0;
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"locLongitude"]))) {
        Long = [MapDic[@"locLongitude"] doubleValue];
    }
    else{
        Long = 0.0;
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"checkInLatitude"]))) {
        Lat1 = [MapDic[@"checkInLatitude"] doubleValue];
    }
    else{
        Lat1 = 0.0;
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"checkInLongitude"]))) {
        Long1 = [MapDic[@"checkInLongitude"] doubleValue];
    }
    else{
        Long1 = 0.0;
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"location"]))){
        LocationValueLabel.text = MapDic[@"location"];
    }
    else if(IsSafeStringPlus(TrToString(MapDic[@"locType"]))){
        LocationValueLabel.text = MapDic[@"locType"];
    }
    else{
        LocationValueLabel.text = @" ";
    }
    //}
    NSString *HeaderStr = NSLocalizedString(@"IWORK_LOCATION", nil);
    HeaderStr = [HeaderStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    GMSMarker *marker1 = [[GMSMarker alloc] init];
    marker1.position = [[[CLLocation alloc]initWithLatitude:Lat longitude:Long] coordinate];
    if([isComeFrom isEqualToString:@"REPORT"]){
        marker1.title = @"Jakarta";
    }
    else{
        marker1.title = HeaderStr;
    }
    marker1.appearAnimation = kGMSMarkerAnimationPop;
    marker1.map = self.mapView;
    
    GMSMarker *marker2 = [[GMSMarker alloc] init];
    marker2.position =[[[CLLocation alloc]initWithLatitude:Long1 longitude:Long1] coordinate];
    marker2.title = @"Checkin Location";
    marker2.appearAnimation = kGMSMarkerAnimationPop;
    //if(![isComeFrom isEqualToString:@"REPORT"]){
    marker2.map = self.mapView;
    // }
    self.mapView.myLocationEnabled = NO;
    self.mapView.mapType = kGMSTypeNormal;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = NO;
    self.mapView.delegate = self;
    self.mapView.settings.zoomGestures = YES;
    
    if(IsSafeStringPlus(TrToString(MapDic[@"requestId"]))){
        RequestIDValueLabel.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"REQUEST_ID_WITH_COLON", nil),MapDic[@"requestId"]];
    }
    else{
         RequestIDValueLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"userName"]))){
        EmployeeNameValueLabel.text = MapDic[@"userName"];
    }
    else{
        EmployeeNameValueLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(MapDic[@"dateTime"]))){
        NSNumber *datenumber = MapDic[@"dateTime"];
        RequestDateValueLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        RequestDateValueLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(MapDic[@"task"]))){
        TaskValueLabel.text = MapDic[@"task"];
    }
    else{
        TaskValueLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(MapDic[@"locType"]))){
        LocationTypeValueLabel.text = MapDic[@"locType"];
    }
    else{
        LocationTypeValueLabel.text = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(MapDic[@"requestDate"]))) {
        NSNumber *datenumber = MapDic[@"requestDate"];
        iWorkDateValueLabel.text = [self DateFormateChange:datenumber];
        
    } else {
        iWorkDateValueLabel.text = @" ";
    }
   
    if(IsSafeStringPlus(TrToString(MapDic[@"actionDate"]))) {
        NSNumber *datenumber = MapDic[@"actionDate"];
        ActiontDateValueLabel.text = [self DateFormateChange:datenumber];
    } else {
        ActiontDateValueLabel.text = @" ";
    }

    if(IsSafeStringPlus(TrToString(MapDic[@"checkInTime"]))) {
        NSNumber *Timenumber = MapDic[@"checkInTime"];
        CheckinValueLabel.text = [self GetDate:Timenumber];
    } else {
        CheckinValueLabel.text = @"- -:- -";
    }
    
    if(IsSafeStringPlus(TrToString(MapDic[@"checkOutTime"]))) {
        NSNumber *Timenumber = MapDic[@"checkOutTime"];
        CheckoutvalueLabel.text = [self GetDate:Timenumber];
    } else {
        CheckoutvalueLabel.text = @"- -:- -";
    }

    if(IsSafeStringPlus(TrToString(MapDic[@"hour"]))){
        HoursValueLabel.text = MapDic[@"hour"];
    }
    else {
        HoursValueLabel.text = @" " ;
    }
    
    if(IsSafeStringPlus(TrToString(MapDic[@"attendanceStatus"]))){
        AttandenceValueLabel.text = MapDic[@"attendanceStatus"];
        if([AttandenceValueLabel.text isEqualToString:@"-"]){
            AttandenceValueLabel.text = @ "";
        }
    }
    else{
        AttandenceValueLabel.text = @" ";
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"maxCheckInTime"]))){
        MaxTimeValueLabel.text = [self GetDate:MapDic[@"maxCheckInTime"]];;
    }
    else{
        MaxTimeValueLabel.text = @" ";
        
    }
    if(IsSafeStringPlus(TrToString(MapDic[@"status"]))) {
        
        if ([MapDic[@"status"] isEqualToString:@"PENDING"]) {
            StatusValueLabel.textColor = [UIColor colorWithRed:1.0 green:0.83 blue:00.0 alpha:1.0];
            StatusValueLabel.text = @"New";
            StatusImageView.image = [UIImage imageNamed:@"NewStatus"];
            
        } else if ([MapDic[@"status"] isEqualToString:@"APPROVED"]) {
            
            StatusValueLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:88.0/255.0 blue:45.0/255.0 alpha:1.0];
            StatusValueLabel.text = @"Approved";
            StatusImageView.image = [UIImage imageNamed:@"Approved"];
            
        }
        else if ([MapDic[@"status"] isEqualToString:@"REJECTED"]) {
            StatusValueLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            StatusValueLabel.text = @"Rejected";
            StatusImageView.image = [UIImage imageNamed:@"Rejected"];
            
        }
        else if ([MapDic[@"status"] isEqualToString:@"CANCELLED"] ) {
            StatusValueLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            StatusValueLabel.text = @"Cancelled";
            StatusImageView.image = [UIImage imageNamed:@"CancelledStatus"];
            
        }
        else if ([MapDic[@"status"] isEqualToString:@"DISCARDED"]) {
            StatusValueLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            StatusValueLabel.text = @"Discarded";
            StatusImageView.image = [UIImage imageNamed:@"DiscardedStatus"];
            
        }
        else if ([MapDic[@"status"]isEqualToString:@"DECLINED"] ) {
            StatusValueLabel.textColor = [UIColor colorWithRed:0.93 green:0.11 blue:00.14 alpha:1.0];
            StatusValueLabel.text = @"Declined";
            StatusImageView.image = [UIImage imageNamed:@"DeclinedStatus"];
        }
    }
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)HomeAction:(id)sender{
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[AppContainerViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)GetDate:(NSNumber*)Timenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",Timenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH:mm:ss"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"hh:mm a"];
    return [dateformate stringFromDate:Newdate];
}

-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
