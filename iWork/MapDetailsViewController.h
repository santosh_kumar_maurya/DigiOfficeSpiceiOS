//
//  MapDetailsViewController.h
//  iWork
//
//  Created by Shailendra on 16/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface MapDetailsViewController : UIViewController<GMSMapViewDelegate>
{
    IBOutlet UILabel *RequestIDStaticLabel,*RequestIDValueLabel,*EmployeeNameStaticLabel,*EmployeeNameValueLabel,*iWorkDateStaticLabel,*iWorkDateValueLabel,*TaskStaticLabel,*TaskValueLabel,*LocationStaticLabel,*LocationValueLabel,*LocationTypeStaticLabel,*LocationTypeValueLabel,*RequestDateStaticLabel,*RequestDateValueLabel,*ActionDateStaticLabel,*ActiontDateValueLabel,*CheckinStaticLabel,*CheckinValueLabel,*CheckoutStaticLabel,*CheckoutvalueLabel,*HoursStaticLabel,*HoursValueLabel,*MaxTimeStaticLabel,*MaxTimeValueLabel,*AttandenceStaticLabel,*AttandenceValueLabel,*StatusValueLabel;

    IBOutlet UIImageView *StatusImageView;
}
@property(nonatomic,strong)IBOutlet GMSMapView *mapView;
@property(nonatomic,strong)NSDictionary *MapDic;
@property(nonatomic,strong)NSString *RequestID;
@property(nonatomic,strong)NSString *isComeFrom;
@end
