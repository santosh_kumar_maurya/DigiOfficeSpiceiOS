//
//  NetworkInterface.m
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import "NetworkInterface.h"
#import "AFNetworking/AFNetworking.h"

@implementation NetworkInterface

- (id)init {
    if (self = [super init]) {
        [self startMonitoring];
    }
    return self;
}

+ (instancetype)networkManager {
    return [[self alloc] init];
}

+ (instancetype)sharedNetworkManager {
    
    static NetworkInterface *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)startMonitoring {
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        //   NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

+ (BOOL)isNetworkReachible {
    return  [[AFNetworkReachabilityManager sharedManager] isReachable];
}

/**
 *  Get request to fetch the response from the server
 *
 *   urlString
 *   sucessBlock
 *   failure
 */

- (void)fetchDataWithRequestTypeGET:(NSString *)urlString successBlock:(NetworkInterfaceManagerSucessBlock)sucessBlock failure:(NetworkInterfaceManagerErrorBlock)failure {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            
            failure(error);
        } else {
            sucessBlock(responseObject);
        }
    }];
    [dataTask resume];
}

/**
 *  Post request to fetch the response from the server
 *
 *   urlString
 *   parms
 *   sucessBlock
 *   failure
 */

- (void)fetchDataWithRequestTypePOST:(NSString *)urlString parameters:(NSDictionary*)parms successBlock:(NetworkInterfaceManagerSucessBlock)sucessBlock failure:(NetworkInterfaceManagerErrorBlock)failure
{
    // NSLog(@"request Url = [%@] \n\n PostData = %@", urlString, parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    
    [manager POST:urlString parameters:parms progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        sucessBlock(responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(error);
    }];
}



/**
 *  Post request to fetch the response from the server
 *
 *   requestURL
 *   params
 *   sucessBlock
 *   failure
 */

- (void)postQuizData:(NSString *)urlString parameters:(NSArray *)parms successBlock:(NetworkInterfaceManagerSucessBlock)sucessBlock failure:(NetworkInterfaceManagerErrorBlock)failure
{
    //NSLog(@"request Url = [%@] \n\n PostData = %@", urlString, parms);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    
    [manager POST:urlString parameters:parms progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        sucessBlock(responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(error);
    }];
}

//- (NSString *)createRquestURL:(NSString *)urlPath {
//   NSURLComponents* components = [NSURLComponents componentsWithString:BASEURL];
//    components.path = urlPath;
//
//    return components.URL.absoluteString;
//}
/*
 / Create from base URL
 let components = NSURLComponents(string: "http://api.example.com")!
 // components.URL is http://api.example.com
 
 //  Add path
 components.path = "/widgets/details"
 // components.URL is http://api.example.com/widgets/details
 
 // Create query items
 let queryItem = NSURLQueryItem(name: "id", value: "123456")
 components.queryItems = [queryItem]
 // components.URL is http://api.example.com/widgets/details?id=123456
 
 let sillyItem = NSURLQueryItem(name: "escape", value: "?&-=")
 components.queryItems = [queryItem, sillyItem]
 //components.URL is http://api.example.com/widgets/details?id=123456&escape=?%26-%3D
 */
//http://commandshift.co.uk/blog/2015/09/23/dealing-with-web-services/



@end
