//
//  NSString+FormValidation.h
//  Alive
//
//  Created by Sanjay on 25/08/14.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FormValidation)

/**
 Verifies a valid email was entered
 
 @return Returns YES if valid, NO if not valid.
 **/
- (BOOL)isValidEmail;

/**
 Verifies a valid password was entered
 
 @return Returns YES if valid, NO if not valid.
 **/
- (BOOL)isValidPassword;

/**
 Verifies a valid name was entered
 
 @return Returns YES if valid, NO if not valid.
 **/
- (BOOL)isValidName;

-(BOOL) isValidMobileNumber;

-(BOOL) isValidPIN;

@end
