//
//  UITextField+textFiled.m
//  Created by Fourbrick on 25/05/15.
//
//

#import "UITextField+textFiled.h"

@implementation UITextField (textFiled)

- (void)setPlaceholder:(NSString *)placeholder withColor:(UIColor *)color {
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{ NSForegroundColorAttributeName: color }];
}

@end
