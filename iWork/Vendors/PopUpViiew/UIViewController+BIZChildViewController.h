//
//  UIViewController+BIZChildViewController.h
#import <UIKit/UIKit.h>


@interface UIViewController (BIZChildViewController)
- (void)containerAddChildViewController:(UIViewController *)childViewController;
- (void)containerAddChildViewController:(UIViewController *)childViewController withRootView:(UIView *)rootView;
- (void)containerRemoveChildViewController:(UIViewController *)childViewController;
@end
