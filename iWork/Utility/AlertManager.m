//
//  AlertManager.m
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import "AlertManager.h"
#import <ISMessages/ISMessages.h>
#import "UIView+Toast.h"


@implementation AlertManager

+ (void)showAlertView:(NSString *)title withMessage:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

+ (void)showTostAlert:(NSString *)message {
    
    UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
    [keyWindow makeToast:message
                duration:3.0
                position:[NSValue valueWithCGPoint:CGPointMake(keyWindow.bounds.size.width/2, 120)]];
    
}


+ (void)showPopupMessageAlert:(NSString *)message  withTitle:(NSString *) title {
    if ([message length] >2 && message !=nil) {
    
        [ISMessages showCardAlertWithTitle:title
                                   message:message
                                 iconImage:nil
                                  duration:3.f
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeError
                             alertPosition:ISAlertPositionTop];

    }
    
}



@end
