//
//  LoadingManager.m
//  MangoApp
//
//  Created by Fourbrick on 17/11/16.
//    Copyright © 2017 Fourbrick. All rights reserved.
//

#import "LoadingManager.h"
#import <SVProgressHUD/SVProgressHUD.h>
@interface LoadingManager()
@end

@implementation LoadingManager

- (id)init {
    if (self = [super init]) {
        
    }
    
    return self;
}

+ (instancetype)sharedLoadingManager {
    
    static LoadingManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        
        
        
    });
    return sharedMyManager;
}



+ (void)showLoadingView:(UIView *)view {

    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD show];
        
    });
    
}

+ (void)hideLoadingView:(UIView *)view {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
    
}

+ (void)showBlurLoadingView {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
}


@end
