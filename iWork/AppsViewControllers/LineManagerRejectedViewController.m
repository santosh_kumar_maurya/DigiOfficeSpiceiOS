//
//  LineManagerRejectedViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManagerRejectedViewController.h"
#import "RejectedCell.h"
#import "Header.h"

@interface LineManagerRejectedViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *RejectedTableView;
    NSMutableArray *RejectedArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *dalegate;
    IBOutlet UILabel *NoDataLabel;
    NSDictionary *params;
   
    APIService *Api;
    NSDictionary *ResponseDic;
    NSString *requestsId;
    int offset;
    int limit;
    int TagValue;
}


@end

@implementation LineManagerRejectedViewController

- (void)viewDidLoad {
    CheckValue = @"REJECTED";
    [self EmptyArray];
    TagValue = 0;
    Api = [[APIService alloc] init];
    dalegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = dalegate.BackgroudColor;
     RejectedTableView.backgroundColor = dalegate.BackgroudColor;
    RejectedTableView.estimatedRowHeight = 50;
    RejectedTableView.rowHeight = UITableViewAutomaticDimension;
    RejectedTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, RejectedTableView.bounds.size.width, 10.00f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [RejectedTableView addSubview:refreshControl];
    [self GetiWorkLineManagerRejected];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RejectedApi:) name:@"Rejected" object:nil];
    UINib *reportNib = [UINib nibWithNibName:@"ReportsTableViewCell" bundle:nil];
    [RejectedTableView registerNib:reportNib forCellReuseIdentifier:@"ReportsTableViewCell"];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    RejectedArray = [[NSMutableArray alloc]init];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self EmptyArray];
    [self GetiWorkLineManagerRejected];
}
#pragma mark  -UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [RejectedArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReportsTableViewCell * projectCell = (ReportsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ReportsTableViewCell" forIndexPath:indexPath];
    projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [projectCell.ViewDetailsBtn addTarget:self action:@selector(GotoMapView:) forControlEvents:UIControlEventTouchUpInside];
    projectCell.ViewDetailsBtn.tag = indexPath.section;
    
    projectCell.tag  = indexPath.section;
    UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
    [projectCell  setUserInteractionEnabled:YES];
    [projectCell addGestureRecognizer:RequestIWork];
    if(RejectedArray.count>0){
        [projectCell configureReportCell:RejectedArray[indexPath.section]];
    }
    return projectCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 8;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
    if(ArraysValue.count>0){
        offset = offset + 50;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkLineManagerRejected];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [RejectedArray addObject:BindDataDic];
    }
}
-(void)MapPage{
    NSDictionary *Dic = [RejectedArray objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GotoMapView:(UIButton*)Sender{
    TagValue = Sender.tag;
    [self MapPage];
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    TagValue = sender.view.tag;
    [self MapPage];
}
- (void)GetiWorkLineManagerRejected {
    if(dalegate.isInternetConnected){
        if([CheckValue isEqualToString:@"REJECTED"]||[CheckValue isEqualToString:@"Refersh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"status": @10,
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"viewTeamReport"];
            [refreshControl endRefreshing];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
                if(ResponseArrays.count>0){
                    NoDataLabel.hidden = YES;
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"]];
                    [RejectedTableView  reloadData];
                }
                else if(RejectedArray.count == 0){
                    [self NoIworkRequest];
                }
            }
            else{
                if(RejectedArray.count == 0){
                    [self NoIworkRequest];
                }
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkRequest{
    NoDataLabel.hidden = NO;
    NoDataLabel.text = @"No iWork request found";
    NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
}
-(void)RejectedApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [self GetiWorkLineManagerRejected];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}


@end
