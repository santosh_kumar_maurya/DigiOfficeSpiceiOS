//
//  MyiWorkViewController.m
//  iWork
//
//  Created by Shailendra on 16/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyiWorkViewController.h"
#import "Header.h"


@interface MyiWorkViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *MyiWorkTableView;
    AppDelegate *delegate;
    NSString *CheckValue;
    NSMutableArray *TemaiworkArray;
    NSDictionary *ResponseDic;
    BOOL useriWorkStatus;
  
    NSDictionary *params;
    NSString *iWorkIdStr;
    UIRefreshControl *refreshControl;
    
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *ApproveBtn;
    IBOutlet UIButton *RejectBtn;
   
    NSString *Type;
    NSString *user_requests_id;
    
    IBOutlet UILabel *ApproveRejactLabel;
    IBOutlet UIView *ApproveRejactView;
    IBOutlet UIImageView *ApproveImageView;
    IBOutlet UILabel *ApproveTitleLabel;
    IBOutlet UIButton *ApproveCloseBtn;
    
    IBOutlet UIView *CheckInOuterView;
    IBOutlet UIView *CheckInInnerView;
    IBOutlet UILabel *CheckInLabel;
    IBOutlet UIButton *DoneBtn;
    APIService *Api;
    NSString *empId;
    NSString *empName;
    NSString *location;
    
}
@end

@implementation MyiWorkViewController
@synthesize isComeFrom,NotificationDic;
- (void)viewDidLoad {
    [super viewDidLoad];
    Api = [[APIService alloc] init];
    empId = @"";
    empName = @"";
    location = @"";
    CheckValue = @"MY_IWORK";
    self.view.backgroundColor = delegate.BackgroudColor;
    MyiWorkTableView.backgroundColor = delegate.BackgroudColor;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    MyiWorkTableView.estimatedRowHeight = 200;
    [MyiWorkTableView setNeedsLayout];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [MyiWorkTableView addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
    [self CheckInCheckOutView];
    
    if([isComeFrom isEqualToString:@"NOTIFICATION"]){
        MsgLabel.text = [NotificationDic valueForKey:@"msg"];
        if([[NotificationDic valueForKey:@"type"]integerValue] ==6){
            CheckInOuterView.hidden = NO;
            [DoneBtn setTitle:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil) forState:UIControlStateNormal];
            [DoneBtn addTarget:self action:@selector(CheckInChekcOut:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if([[NotificationDic valueForKey:@"type"]integerValue] ==7){
            CheckInOuterView.hidden = NO;
            [DoneBtn setTitle:NSLocalizedString(@"CHECK_OUT_BTN_CAPS", nil) forState:UIControlStateNormal];
            [DoneBtn addTarget:self action:@selector(CheckInChekcOut:) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
           MsgOuterView.hidden = NO;
        }
    }
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iWorkFilterApi:) name:@"iWorkFilter" object:nil];
}
-(void)CheckInCheckOutView{
    CheckInInnerView.layer.cornerRadius = 5;
    [DoneBtn setBackgroundColor:[UIColor colorWithRed:0.85 green:0.69 blue:0.14 alpha:1.0]];
    DoneBtn.layer.cornerRadius = 5;
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self WebApiCell];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(TemaiworkArray.count>0){
        return TemaiworkArray.count + 3;
    }
    return  3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        RequestCell *Cell = (RequestCell*)[MyiWorkTableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        Cell.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToRequestIWork)];
        [Cell.iWorkView  setUserInteractionEnabled:YES];
        [Cell.iWorkView addGestureRecognizer:RequestIWork];
        
        //Cell.iWorkImageView.image = [UIImage imageNamed:@"create_request"];
        
        Cell.RequestIWorkLabel.text = NSLocalizedString(@"REQUEST_IWORK", nil);
        Cell.RequestIWorkLabel.text = [Cell.RequestIWorkLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
        Cell.iWorkImageView.image = [UIImage imageNamed:@""];
        UITapGestureRecognizer* ViewRequest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToViewRequest)];
        [Cell.RequestView  setUserInteractionEnabled:YES];
        [Cell.RequestView addGestureRecognizer:ViewRequest];
        
        return Cell;
    }
    else if(indexPath.row == 1){
        CheckInCell *Cell = (CheckInCell*)[MyiWorkTableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];\
         Cell.backgroundColor = [UIColor clearColor];
        
        Cell.CheckInButton.backgroundColor = delegate.redColor;
        [Cell.CheckInButton addTarget:self action:@selector(CheckInChekcOut:) forControlEvents:UIControlEventTouchUpInside];
        
        Cell.NoIWorkLabel.hidden = YES;
        Cell.DateLabel.text = [self GetDayWithDate];
        NSString *GetDateStr = [self GetCurrentDate];
        NSArray *Split = [GetDateStr componentsSeparatedByString:@" "];
        Cell.TimeLabel.text = Split[0];
        Cell.AM_PM_Label.text = Split[1];
        NSString *myRequestForToday = [ResponseDic[@"object"] valueForKey:@"myRequestForToday"];
        if([myRequestForToday integerValue] == 1){
            NSMutableDictionary *myRequestDic = [ResponseDic[@"object"] valueForKey:@"myRequest"];
            NSString *check_in = [myRequestDic valueForKey:@"checkIn"];
            NSString *check_out = [myRequestDic valueForKey:@"checkOut"];
            
            if([check_in integerValue] == 1){
                [Cell.CheckInButton setTitle:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil) forState:UIControlStateNormal];
                Cell.CheckInButton.hidden = NO;
                Cell.DateLabel.hidden = NO;
                Cell.TimeLabel.hidden = NO;
                Cell.AM_PM_Label.hidden = NO;
                Cell.CongratsLabel.hidden = NO;
                Cell.NoIWorkLabel.hidden = YES;
                useriWorkStatus =YES;
            }
            else if([check_out integerValue] == 1){
                [Cell.CheckInButton setTitle:NSLocalizedString(@"CHECK_OUT_BTN_CAPS", nil) forState:UIControlStateNormal];
                Cell.CheckInButton.hidden = NO;
                Cell.DateLabel.hidden = NO;
                Cell.TimeLabel.hidden = NO;
                Cell.AM_PM_Label.hidden = NO;
                Cell.CongratsLabel.hidden = NO;
                Cell.NoIWorkLabel.hidden = YES;
            }
            else{
                Cell.CheckInButton.hidden = YES;
                Cell.DateLabel.hidden = NO;
                Cell.AM_PM_Label.hidden = YES;
                Cell.CongratsLabel.hidden = YES;
                Cell.TimeLabel.hidden = YES;
                Cell.NoIWorkLabel.hidden = NO;
            }
            if ([myRequestDic valueForKey:@"checkIn"] == [NSNull null] && [myRequestDic valueForKey:@"checkOut"] == [NSNull null]) {
                Cell.CheckInButton.hidden = YES;
            }
        }
        else{
            Cell.TimeLabel.hidden = YES;
            Cell.AM_PM_Label.hidden = YES;
            Cell.CongratsLabel.hidden = YES;
            Cell.CheckInButton.hidden = YES;
            Cell.NoIWorkLabel.hidden = NO;
            Cell.DateLabel.hidden = NO;
            
        }
        return Cell;
        
    }
    else if(indexPath.row == 2){
        MyTeamCell *Cell = (MyTeamCell*)[MyiWorkTableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        Cell.MyTeamLabel.text = NSLocalizedString(@"MY_TEAM_IWORK", nil);
        Cell.MyTeamLabel.text = [Cell.MyTeamLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
        
        Cell.backgroundColor = [UIColor clearColor];
        [Cell.FilterButton addTarget:self action:@selector(FilterAction) forControlEvents:UIControlEventTouchUpInside];
        return Cell;
    }
    else{
        EmployeeCell *Cell = (EmployeeCell*)[MyiWorkTableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
         Cell.backgroundColor = [UIColor clearColor];
        Cell.CellBgView.tag = indexPath.row;
        UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
        [Cell.CellBgView  setUserInteractionEnabled:YES];
        [Cell.CellBgView addGestureRecognizer:RequestIWork];
        NSDictionary *Dic = [TemaiworkArray objectAtIndex:indexPath.row -3];
        if(IsSafeStringPlus(TrToString(Dic[@"userName"]))){
            Cell.UserNameLabel.text = Dic[@"userName"];
        }
        else{
            Cell.UserNameLabel.text = @"";
        }
        if(IsSafeStringPlus(TrToString(Dic[@"checkInTime"]))){
            NSNumber *Timenumber = Dic[@"checkInTime"];
            Cell.CheckInValueLabel.text = [self GetDate:Timenumber];
        }
        else{
            Cell.CheckInValueLabel.text = @"__:__";
        }
        if(IsSafeStringPlus(TrToString(Dic[@"checkOutTime"]))){
             NSNumber *Timenumber = Dic[@"checkOutTime"];
            Cell.CheckOutValueLabel.text =[self GetDate:Timenumber];
    
        }
        else{
            Cell.CheckOutValueLabel.text = @"__:__";
        }
        if(IsSafeStringPlus(TrToString(Dic[@"location"]))){
            Cell.LacationLabel.text = Dic[@"location"];
        }
        else{
            Cell.LacationLabel.text = @"";
        }
        if(IsSafeStringPlus(TrToString(Dic[@"dp"]))){
            UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:Dic[@"dp"]]]];
            if(images == nil){
                Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
            }
            else{
                NSString *URL = [NSString stringWithFormat:@"%@",Dic[@"dp"]];
                [Cell.ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
            }
        }
        else{
            Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        Cell.ProfileImageView.layer.cornerRadius = 22.5;
        Cell.ProfileImageView.clipsToBounds =YES;
        
        return Cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 1){
        return 105;
    }
    else if(indexPath.row == 2) {
        return 55;
    }
    return 92;
}
#pragma mark -
- (IBAction)ApproveBtnAction:(UIButton *)sender {
    CheckValue = @"APPROVE";
    Type = @"1";
    user_requests_id = [NotificationDic valueForKey:@"user_requests_id"];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
}
- (IBAction)RejectBtnAction:(UIButton *)sender {
    CheckValue = @"REJECT";
    Type = @"2";
    user_requests_id = [NotificationDic valueForKey:@"user_requests_id"];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
}
-(void)ShowErrorTitle:(NSString*)Title Message:(NSString*)Msg ImageName:(NSString*)ImageName BtnTitle:(NSString*)BtnTilte{
    ApproveRejactView.hidden = NO;
    ApproveTitleLabel.text = Title;
    ApproveRejactLabel.text = Msg;
    ApproveImageView.image = [UIImage imageNamed:ImageName];
    [ApproveCloseBtn setTitle:BtnTilte forState:UIControlStateNormal];
    
}
-(void)GoToRequestIWork{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateWorkRequestViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateWorkRequestViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GoToViewRequest{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EmployeeParentViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"EmployeeParentViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)CheckInChekcOut:(UIButton*)sender{
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil)]){
        CheckValue = @"CHECKIN";
    }
    else if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CHECK_OUT_BTN_CAPS", nil)]){
        CheckValue = @"CHECKOUT";
    }
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.4];
}
-(void)FilterAction{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyWorkFilterViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyWorkFilterViewController"];
    [self presentViewController:ObjViewController animated:YES completion:nil];
    
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    int TagValue = sender.view.tag;
    NSDictionary *Dic = [TemaiworkArray objectAtIndex:TagValue -3];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
- (void)WebApiCell {
    
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MY_IWORK"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"IWORKFILTER"]){
            if(![CheckValue isEqualToString:@"IWORKFILTER"]){
                params = @ {
                    @"empId": empId,
                    @"empName": empName,
                    @"location": location,
                    @"userId": [ApplicationState userId]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"myIwork"];
            [refreshControl endRefreshing];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                if(IsSafeStringPlus(TrToString(ResponseDic[@"object"]))){
                    iWorkIdStr = [[ResponseDic[@"object"]valueForKey:@"myRequest"] valueForKey:@"requestId"];
                    if(IsSafeStringPlus(TrToString([ResponseDic[@"object"]valueForKey:@"teamRequestList"]))){
                        TemaiworkArray = [ResponseDic[@"object"]valueForKey:@"teamRequestList"];
                    }
                }
                else{
                    iWorkIdStr = @"";
                }
                [MyiWorkTableView reloadData];
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
        else if ([CheckValue isEqualToString:@"CHECKIN"]||[CheckValue isEqualToString:@"CHECKOUT"]){
            if([delegate.userLat doubleValue] ==0.0 || [delegate.userLong doubleValue] == 0.0){
                [AlertManager showPopupMessageAlert:NSLocalizedString(@"TURN_ON_LOCATION", nil) withTitle:nil];
            }
            //else{
                delegate.userLat = @"";
                delegate.userLong = @"";
           // }
            NSString *Lat = [NSString stringWithFormat:@"%@",delegate.userLat];
            NSString *Logn = [NSString stringWithFormat:@"%@",delegate.userLong];
            if([Lat isEqualToString:@"(null)"]){
                Lat = @"";
                Logn = @"";
            }
            //            else{
            if([CheckValue isEqualToString:@"CHECKIN"]){
                params = @ {
                    @"mapping" : @"apis",
                    @"userId" : [ApplicationState userId],
                    @"latitude" : Lat,
                    @"longitude" : Logn
                };
                ResponseDic = [Api WebApi:params Url:@"checkIn"];
                NSLog(@"ResponseDic==%@",ResponseDic);
                
                if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                    [LoadingManager hideLoadingView:self.view];
                    MsgOuterView.hidden = YES;
                    [self ShowErrorTitle:NSLocalizedString(@"CHECK_IN_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"TITME_COMPLETE" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    CheckValue = @"MY_IWORK";
                    [MyiWorkTableView reloadData];
                    [self WebApiCell];
                }
                else{
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                }
            }
            else if([CheckValue isEqualToString:@"CHECKOUT"]){
                params = @ {
                    @"mapping" : @"apis",
                    @"userId" : [ApplicationState userId],
                    @"requestId" : iWorkIdStr,
                    @"latitude" : Lat,
                    @"longitude" : Logn
                };
                ResponseDic = [Api WebApi:params Url:@"checkOut"];
                if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                    [LoadingManager hideLoadingView:self.view];
                    MsgOuterView.hidden = YES;
                    [self ShowErrorTitle:NSLocalizedString(@"CHECK_OUT_TITME", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"TITME_COMPLETE" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
                else if([ResponseDic[@"statusCode"]intValue]==105) // Automatic Checkout
                {
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowErrorTitle:NSLocalizedString(@"HOURS_NOT_COMPLETE_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"CUT_OFF_TIME" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
                else if([ResponseDic[@"statusCode"]intValue]==106) // Automatic Checkout
                {
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowErrorTitle:NSLocalizedString(@"MAXIMUM_CHECKED_OUT_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"CHECKOUT_TIME" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
                else if([ResponseDic[@"statusCode"]intValue]==205)
                {
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowErrorTitle:NSLocalizedString(@"HOURS_NOT_COMPLETE_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"CUT_OFF_TIME" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
                else{
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                    [MyiWorkTableView reloadData];
                    CheckValue = @"MY_IWORK";
                    [self WebApiCell];
                }
            }
            // }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(NSString*)GetDayWithDate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM YYYY"];
    NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSString *day;
    switch ([component weekday]) {
        case 1:
            day = NSLocalizedString(@"SUNDAY", nil);
            break;
        case 2:
            day = NSLocalizedString(@"MONDAY", nil);
            break;
        case 3:
            day = NSLocalizedString(@"TUESDAY", nil);
            break;
        case 4:
            day = NSLocalizedString(@"WEDNESDAY", nil);
            break;
        case 5:
            day = NSLocalizedString(@"THERSDAY", nil);
            break;
        case 6:
            day = NSLocalizedString(@"FRIDAY", nil);
            break;
        case 7:
            day = NSLocalizedString(@"SATURDAY", nil);
            break;
        default:
            break;
    }
    return  [NSString stringWithFormat:@"%@, %@",day,[dateFormat stringFromDate:[NSDate date]]];
}
-(void)iWorkFilterApi:(NSNotification*)notification{
    TemaiworkArray = [[NSMutableArray alloc] init];
    params = notification.userInfo;
    CheckValue = @"IWORKFILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
}
-(NSString*)GetDate:(NSNumber*)Timenumber{
    NSString *DateStr = [NSString stringWithFormat:@"%@",Timenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH:mm:ss"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"hh:mm a"];
    return [dateformate stringFromDate:Newdate];
}
-(NSString*)GetCurrentDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [NSDate date];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return  formattedDate;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
