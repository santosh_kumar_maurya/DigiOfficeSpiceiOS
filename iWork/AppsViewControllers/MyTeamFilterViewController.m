//
//  MyTeamFilterViewController.m
//  iWork
//
//  Created by Shailendra on 20/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTeamFilterViewController.h"
#import "Header.h"

@interface MyTeamFilterViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSMutableArray *arrayForBool;
    NSMutableArray *FilterArray;
    IBOutlet UITableView *MyTeamTableView;
    NSString *EmployeeRadioSelect;
    NSString *DurationRadioSelect;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *CancelFilterBtn;
    IBOutlet UIButton *ApplyFilterBtn;
    AppDelegate *delegate;
    IBOutlet UIView *BottomView;
    NSMutableArray *PlusImageArray;
    NSMutableArray *MinusImageArray;
    
    NSIndexPath *indexPath3;
    MyTeamEmployeeCell *Cell3;
    NSIndexPath *indexPath4;
    MyTeamDurationCell *Cell4;
    NSString *SelectDuration;
    
    IBOutlet UIView *DatePickerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    
    NSString *DatePickerSelectionStr;
    
    NSString *StartDateStr;
    NSString *EndDateStr;
    
}


@end

@implementation MyTeamFilterViewController

- (void)viewDidLoad {
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    EmployeeRadioSelect = @"EMPLOYEE_NAME";
    DurationRadioSelect = @"CURRENT_WEEK";
    SelectDuration = @"week";
    DatePickerSelectionStr = @"";
    PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus",@"Plus", nil];
    MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus",@"Minus", nil];
    
    arrayForBool = [[NSMutableArray alloc] init];
    FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"EMPLOYEE_FILTER", nil),NSLocalizedString(@"DURATION_FILTER", nil), nil];
    
    arrayForBool=[[NSMutableArray alloc]init];
    for (int i=0; i<[FilterArray count]; i++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    MyTeamTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, MyTeamTableView.bounds.size.width, 0.01f)];
    
    UINib *SearchNib = [UINib nibWithNibName:@"MyTeamEmployeeCell" bundle:nil];
    [MyTeamTableView registerNib:SearchNib forCellReuseIdentifier:@"CELL"];
    
    UINib *SelectionNib = [UINib nibWithNibName:@"MyTeamDurationCell" bundle:nil];
    [MyTeamTableView registerNib:SelectionNib forCellReuseIdentifier:@"CELL1"];
    
    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    CancelBtn.layer.cornerRadius = 3.0;
    CancelBtn.clipsToBounds = YES;
    
    CancelFilterBtn.layer.borderWidth = 1.0;
    CancelFilterBtn.layer.borderColor = [ColorCategory PurperColor].CGColor;
    [CancelFilterBtn setTitleColor:[ColorCategory PurperColor] forState: UIControlStateNormal];
    CancelFilterBtn.layer.cornerRadius = 3.0;
    CancelFilterBtn.clipsToBounds = YES;
    CancelFilterBtn.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    ApplyFilterBtn.layer.cornerRadius = 3.0;
    ApplyFilterBtn.backgroundColor = [ColorCategory PurperColor];
    [ApplyFilterBtn setTitleColor:[UIColor whiteColor] forState: UIControlStateNormal];
    BottomView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    self.view.backgroundColor = [UIColor whiteColor];
    MyTeamTableView.backgroundColor = [UIColor whiteColor];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [FilterArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0 ){
        MyTeamEmployeeCell * Cell = (MyTeamEmployeeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
       
        if([EmployeeRadioSelect isEqualToString:@"EMPLOYEE_NAME"]){
            Cell.EmployeeNameRadioImage.image = [UIImage imageNamed:@"RadioOn"];
            Cell.EmployeeIDRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.ALLRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.EmployeeIDTextField.placeholder = NSLocalizedString(@"EMPLOYEE_NAME_FILTER", nil);
            if([DurationRadioSelect isEqualToString:@"CURRENT_WEEK"] || [DurationRadioSelect isEqualToString:@"CURRENT_MONTH"] || [DurationRadioSelect isEqualToString:@"CURRENT_YEAR"] || [DurationRadioSelect isEqualToString:@"SPECIFY_DATE"]){
               
            }
            Cell.EmployeeIDTextField.keyboardType = UIKeyboardTypeDefault;
            [Cell.EmployeeIDTextField becomeFirstResponder];
        }
        else if([EmployeeRadioSelect isEqualToString:@"EMPLOYEE_ID"]){
            Cell.EmployeeNameRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.EmployeeIDRadioImage.image = [UIImage imageNamed:@"RadioOn"];
            Cell.ALLRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.EmployeeIDTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ID_FILTER", nil);
            //Cell.EmployeeIDTextField.keyboardType = UIKeyboardTypeNumberPad;
            [Cell.EmployeeIDTextField becomeFirstResponder];
        }
        else if([EmployeeRadioSelect isEqualToString:@"ALL"]){
            Cell.EmployeeNameRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.EmployeeIDRadioImage.image = [UIImage imageNamed:@"RadioOff"];
            Cell.ALLRadioImage.image = [UIImage imageNamed:@"RadioOn"];
            Cell.EmployeeIDTextField.placeholder = NSLocalizedString(@"EMPLOYEE_ALL_FILTER", nil);
            //Cell.EmployeeIDTextField.keyboardType = UIKeyboardTypeDefault;
            [Cell.EmployeeIDTextField becomeFirstResponder];
        }
        
        [Cell.EmployeeNameBtn addTarget:self action:@selector(EmployeeRadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.EmployeeIDBtn addTarget:self action:@selector(EmployeeRadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
         [Cell.ALLBtn addTarget:self action:@selector(EmployeeRadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        return Cell;
    }
    else {
        MyTeamDurationCell * Cell = (MyTeamDurationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        
        if([DurationRadioSelect isEqualToString:@"CURRENT_WEEK"]){
            Cell.CurrentWeekRadioImage.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
            Cell.CurrentWeekRadioImage.image = [UIImage imageNamed:@"RadioOff"];
        }
        if([DurationRadioSelect isEqualToString:@"CURRENT_MONTH"]){
           
            Cell.CurrentMonthRadioImage.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
           Cell.CurrentMonthRadioImage.image = [UIImage imageNamed:@"RadioOff"];
        }
        if([DurationRadioSelect isEqualToString:@"CURRENT_YEAR"]){
            Cell.CurrentYearRadioImage.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
           Cell.CurrentYearRadioImage.image = [UIImage imageNamed:@"RadioOff"];
        }
        if([DurationRadioSelect isEqualToString:@"SPECIFY_DATE"]){
            Cell.SpecifyDateRadioImage.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
            Cell.SpecifyDateRadioImage.image = [UIImage imageNamed:@"RadioOff"];
        }
        
        if([DurationRadioSelect isEqualToString:@"CURRENT_WEEK"] || [DurationRadioSelect isEqualToString:@"CURRENT_MONTH"] || [DurationRadioSelect isEqualToString:@"CURRENT_YEAR"]){
            Cell.DateView.hidden = YES;
        }
        else{
            if([DatePickerSelectionStr isEqualToString:@"Start"] || [DatePickerSelectionStr isEqualToString:@"End"]){
                Cell.StartDateTextField.delegate = self;
                Cell.EndDateTextField.delegate = self;
                Cell.StartDateTextField.inputView = DatePickerViewBg;
                Cell.EndDateTextField.inputView = DatePickerViewBg;
                Cell.DateView.hidden = NO;
                if([DatePickerSelectionStr isEqualToString:@"Start"]){
                    Cell.StartDateTextField.text = StartDateStr;
                    Cell.EndDateTextField.text = EndDateStr;
                }
                else if ([DatePickerSelectionStr isEqualToString:@"End"]){
                    Cell.EndDateTextField.text = EndDateStr;
                    Cell.StartDateTextField.text = StartDateStr;
                }
            }
            else{
                Cell.StartDateTextField.delegate = self;
                Cell.EndDateTextField.delegate = self;
                Cell.StartDateTextField.inputView = DatePickerViewBg;
                Cell.EndDateTextField.inputView = DatePickerViewBg;
                Cell.DateView.hidden = NO;
            }
            
        }
        
        [Cell.CurrentWeekBtn addTarget:self action:@selector(DurationRadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.CurrentMonthBtn addTarget:self action:@selector(DurationRadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.CurrentYearBtn addTarget:self action:@selector(DurationRadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.SpecifyDateYearBtn addTarget:self action:@selector(DurationRadioBtnOnOff:) forControlEvents:UIControlEventTouchUpInside];
        
        return Cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return  163;
        }
        else{
            return 0;
        }
    }
    else if(indexPath.section == 1){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return 201;
        }
        else{
          return 0;
        }
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
#pragma mark - Creating View for TableView Section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *Views=[[UIView alloc]initWithFrame:CGRectMake(0, 0, MyTeamTableView.frame.size.width,0)];
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, MyTeamTableView.frame.size.width,44)];
    sectionView.tag=section;
    sectionView.backgroundColor  = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    UILabel *viewLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, MyTeamTableView.frame.size.width, 44)];
    viewLabel.backgroundColor=[UIColor clearColor];
    viewLabel.textAlignment = NSTextAlignmentLeft;
    viewLabel.textColor=[UIColor blackColor];
    viewLabel.font=[UIFont systemFontOfSize:14];
    viewLabel.text=[NSString stringWithFormat:@"%@",[FilterArray objectAtIndex:section]];
    [sectionView addSubview:viewLabel];
    
    //    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, MyWorkTableView.frame.size.width, 1)];
    //    separatorLineView.backgroundColor = [UIColor lightGrayColor];
    //    [sectionView addSubview:separatorLineView];
    
    UIImageView *ImageViews = [[UIImageView alloc] initWithFrame:CGRectMake(MyTeamTableView.frame.size.width-40, 17, 10, 10)];
    if ([[arrayForBool objectAtIndex:section] boolValue]){
        ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[MinusImageArray objectAtIndex:section]]];
    }
    else{
        ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[PlusImageArray objectAtIndex:section]]];
    }
    [sectionView addSubview:ImageViews];
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    return  sectionView;
    return Views;
}
#pragma mark - Table header gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[FilterArray count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [MyTeamTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)EmployeeRadioBtnOnOff:(UIButton*)Sender{
    switch (Sender.tag) {
        case 0:
            EmployeeRadioSelect = @"EMPLOYEE_NAME";
            [MyTeamTableView reloadData];
            break;
        case 1:
            EmployeeRadioSelect = @"EMPLOYEE_ID";
            [MyTeamTableView reloadData];
            break;
          case 2:
            EmployeeRadioSelect = @"ALL";
            [MyTeamTableView reloadData];
            break;
        default:
            break;
    }
}
-(void)DurationRadioBtnOnOff:(UIButton*)Sender{
    switch (Sender.tag) {
        case 0:
            DurationRadioSelect = @"CURRENT_WEEK";
            [MyTeamTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
            SelectDuration = @"week";
            break;
        case 1:
            DurationRadioSelect = @"CURRENT_MONTH";
            [MyTeamTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
            SelectDuration = @"month";
            break;
        case 2:
            DurationRadioSelect = @"CURRENT_YEAR";
            [MyTeamTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
            SelectDuration = @"year";
            break;
        case 3:
             SelectDuration = @"";
            DurationRadioSelect = @"SPECIFY_DATE";
             [MyTeamTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        default:
            break;
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    MyWorkEmployeeCell *Cell = [MyTeamTableView cellForRowAtIndexPath:indexPath];
    [Cell.EmployeeIDTextField resignFirstResponder];
}
-(IBAction)CancelAction:(id)sender{
    [self GetCellValue];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)MyTeamApplyFilter:(id)sender{
    [self GetCellValue];
    
    NSString *EmployeeID = @"";
    NSString *EmployeeName = @"";
    NSString *All = @"";
    
    NSString *StartDate = @"";
    NSString *EndDate = @"";
    
    if([EmployeeRadioSelect isEqualToString:@"EMPLOYEE_NAME"]){
        EmployeeName = Cell3.EmployeeIDTextField.text;
    }
    else if([EmployeeRadioSelect isEqualToString:@"EMPLOYEE_ID"]){
        EmployeeID = Cell3.EmployeeIDTextField.text;
    }
    else if([EmployeeRadioSelect isEqualToString:@"ALL"]){
        All = Cell3.EmployeeIDTextField.text;
    }
    
    StartDate  = Cell4.StartDateTextField.text;
    EndDate  = Cell4.EndDateTextField.text;
    
//    NSDictionary *UserDic = @ {
//        @"cortex" : @"MyTeam",
//        @"mapping" : @"apis",
//        @"user_id" : [ApplicationState userId],
//        @"emp_id" : EmployeeID,
//        @"emp_name" : EmployeeName,
//        @"duration": SelectDuration,
//        @"start_date":StartDate,
//        @"end_date":EndDate
//    };
   NSDictionary *params = @ {
        @"duration": SelectDuration,
        @"empId": EmployeeID,
        @"empName": EmployeeName,
        @"endDate": StartDate,
        @"startDate": EndDate,
        @"userId": [ApplicationState userId],
    };
    NSDictionary *Duration = @ {
        @"Duration" : SelectDuration
    };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MyTeamFilter" object:nil userInfo:params];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectDurations" object:nil userInfo:Duration];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
     DatePickerViewBg.hidden = NO;
    [self GetCellValue];
    if([textField isEqual:Cell4.StartDateTextField]){
        DatePickerSelectionStr = @"Start";
    }
    else if([textField isEqual:Cell4.EndDateTextField]){
        DatePickerSelectionStr = @"End";
    }
}
-(IBAction)DatePickerDoneAction{
    DatePickerViewBg.hidden = YES;
    NSDate *myDate = DatePicker.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if([DatePickerSelectionStr isEqualToString:@"Start"]){
        StartDateStr = [dateFormat stringFromDate:myDate];
    }
    else if([DatePickerSelectionStr isEqualToString:@"End"]){
        EndDateStr = [dateFormat stringFromDate:myDate];
    }
    [MyTeamTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}
-(IBAction)ValueChange{
    NSDate *myDate = DatePicker.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if([DatePickerSelectionStr isEqualToString:@"Start"]){
      StartDateStr = [dateFormat stringFromDate:myDate];
    }
    else if([DatePickerSelectionStr isEqualToString:@"End"]){
       EndDateStr = [dateFormat stringFromDate:myDate];
    }
  [MyTeamTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)GetCellValue{
    indexPath3 = [NSIndexPath indexPathForRow:0 inSection:0];
    Cell3 = [MyTeamTableView cellForRowAtIndexPath:indexPath3];
    [Cell3.EmployeeIDTextField resignFirstResponder];
    indexPath4 = [NSIndexPath indexPathForRow:0 inSection:1];
    Cell4 = [MyTeamTableView cellForRowAtIndexPath:indexPath4];
    [Cell4.StartDateTextField resignFirstResponder];
    [Cell4.EndDateTextField resignFirstResponder];
}

@end
