//
//  EmployeeMissedViewController.m
//  iWork
//
//  Created by Shailendra on 30/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeMissedViewController.h"
#import "RejectedCell.h"
#import "Header.h"

@interface EmployeeMissedViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *MissedTableView;
    NSMutableArray *MissedArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *dalegate;
    IBOutlet UILabel *NoDataLabel;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    APIService *Api;
    int offset;
    int limit;
}
@end

@implementation EmployeeMissedViewController

- (void)viewDidLoad {
    [self EmptyArray];
    MissedArray = [[NSMutableArray alloc] init];
    dalegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = dalegate.BackgroudColor;
    MissedTableView.backgroundColor = dalegate.BackgroudColor;
    MissedTableView.estimatedRowHeight = 50;
    MissedTableView.rowHeight = UITableViewAutomaticDimension;
    MissedTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, MissedTableView.bounds.size.width, 0.01f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [MissedTableView addSubview:refreshControl];
    
    [self GetiWorkUserRejected];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MissedApi:) name:@"Rejected" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self EmptyArray];
    [self GetiWorkUserRejected];
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    MissedArray = [[NSMutableArray alloc] init];
    [MissedTableView reloadData];
}
- (void)GetiWorkUserRejected {
    //@"locationId": @0,
    if(dalegate.isInternetConnected){
        if(![CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"attendanceStatus": @"",
                @"requestDate": @"",
                @"requestId": @0,
                @"status": @2,
                @"userId": [ApplicationState userId],
                @"workDate": @"",
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic = [Api WebApi:params Url:@"myIworkRequest"];
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
            if(ResponseArrays.count>0){
                [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                NoDataLabel.hidden = YES;
            }
            else if(ResponseArrays.count == 0 && MissedArray.count == 0){
                [self NoIworkRequest];
            }
            [MissedTableView reloadData];
        }
        else{
            if(MissedArray.count==0){
                [self NoIworkRequest];
            }
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
-(void)NoIworkRequest{
    [LoadingManager hideLoadingView:self.view];
    NoDataLabel.hidden = NO;
    NoDataLabel.text = @"No iWork request found";
    NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSLog(@"Params===%@",params);
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
                
            }
            params = [NewDic mutableCopy];
            NSLog(@"Params===%@",params);
        }
        [self GetiWorkUserRejected];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [MissedArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [MissedArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MissedCell *Cell = (MissedCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
    Cell.tag = indexPath.section;
    [Cell  setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:Gesture];
    if(MissedArray.count>0){
        NSDictionary * responseData = [MissedArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
    }
    return Cell;
}

-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    if(dalegate.isInternetConnected){
        int TagValue = sender.view.tag;
        NSDictionary *Dics = [MissedArray objectAtIndex:TagValue];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
        ObjViewController.MapDic = Dics;
        [[self navigationController] pushViewController:ObjViewController animated:YES];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)MissedApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserRejected) withObject:nil afterDelay:0.4];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
