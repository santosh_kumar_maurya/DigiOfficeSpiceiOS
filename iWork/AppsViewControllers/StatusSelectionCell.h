//
//  SelectionCell.h
//  iWork
//
//  Created by Shailendra on 25/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusSelectionCell : UITableViewCell{
    
}
@property(nonnull,retain)IBOutlet UILabel *SelectionLabel;
@end
