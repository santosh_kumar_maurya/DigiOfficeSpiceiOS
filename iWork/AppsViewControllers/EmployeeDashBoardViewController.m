//
//  EmployeeDashBoardViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeDashBoardViewController.h"
#import "Header.h"

@interface EmployeeDashBoardViewController ()<UITableViewDataSource,UITableViewDelegate,BIZPopupViewControllerDelegate>{
  
    
    IBOutlet UIImageView *ProfileImageView;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *UserNameLabel;
    IBOutlet UILabel *NatificationLabel;
    IBOutlet UITableView *EmployeeTableView;
   
    IBOutlet UILabel *ApproveRejactLabel;
    IBOutlet UIView *ApproveRejactView;
    IBOutlet UIImageView *ApproveImageView;
    IBOutlet UILabel *ApproveTitleLabel;
    IBOutlet UIButton *ApproveCloseBtn;

    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *DoneBtn;
    
    NSString *isGetNotification;
    APIService *Api;
    NSDictionary *params;
    NSString *empId;
    NSString *empName;
    NSString *location;
    NSString *iWorkIdStr;
    UIRefreshControl *refreshControl;
    NSString *CheckValue;
    NSMutableArray *TemaiworkArray;
    NSDictionary *ResponseDic;
    BOOL useriWorkStatus;
    AppDelegate *delegate;
    
}
@end

@implementation EmployeeDashBoardViewController
@synthesize isComeFrom,NoticationDic;

- (void)viewDidLoad {
    [super viewDidLoad];
    Api = [[APIService alloc] init];
    empId = @"";
    empName = @"";
    location = @"";
    UserNameLabel.text = @"";
    useriWorkStatus = NO;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    CheckValue = @"MY_IWORK";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCall) withObject:nil afterDelay:0.2];
    self.view.backgroundColor = delegate.BackgroudColor;
    EmployeeTableView.backgroundColor = delegate.BackgroudColor;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    EmployeeTableView.estimatedRowHeight = 200;
    [EmployeeTableView setNeedsLayout];
    
    NatificationLabel.layer.cornerRadius = 9.0;
    NatificationLabel.clipsToBounds = YES;
    ProfileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    ProfileImageView.layer.borderWidth = 2;
    ProfileImageView.layer.cornerRadius = 27;
    ProfileImageView.clipsToBounds = YES;
   
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [EmployeeTableView addSubview:refreshControl];
    
    NSString *MyIWorkStr = NSLocalizedString(@"MY_IWORK", nil);
    MyIWorkStr = [MyIWorkStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    NSString *HeaderStr = NSLocalizedString(@"IWORK", nil);
    HeaderStr = [HeaderStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    HeaderLabel.text = HeaderStr;
    [self MessageContentView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNotifications) name:@"Notification" object:nil];
    
    [ApproveCloseBtn setBackgroundColor:[UIColor colorWithRed:0.85 green:0.69 blue:0.14 alpha:1.0]];
    
    if([isComeFrom isEqualToString:@"NOTIFICATION"]){
        MsgOuterView.hidden = NO;
        MsgLabel.text =  [NoticationDic valueForKey:@"msg"];
        isGetNotification = @"YES";
        if([[NoticationDic valueForKey:@"type"]integerValue] ==6){
            [DoneBtn setTitle:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil) forState:UIControlStateNormal];
            [DoneBtn addTarget:self action:@selector(CheckInChekcOut:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if([[NoticationDic valueForKey:@"type"]integerValue] ==7){
            [DoneBtn setTitle:NSLocalizedString(@"CHECK_OUT_BTN_CAPS", nil) forState:UIControlStateNormal];
            [DoneBtn addTarget:self action:@selector(CheckInChekcOut:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    NSDictionary *Dic = [ApplicationState getUserLoginData];
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
        UserNameLabel.text = [NSString stringWithFormat:@"Hi, %@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]];
    }
    else{
        UserNameLabel.text = @" ";
    }
    
    NSString *URL = [NSString stringWithFormat:@"%@images/%@",BASE_URL,[[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]];
    URL = [URL stringByReplacingOccurrencesOfString:@"iworkapi" withString:@"iwork"];
    
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]))){
        NSURL *Url = [NSURL URLWithString:URL];
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL:Url]];
        if(images == nil){
            NSLog(@"image nil");
            ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            
            [ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
    }
}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
    [DoneBtn setTitle:NSLocalizedString(@"OKAY", nil) forState:UIControlStateNormal];
    [DoneBtn addTarget:self action:@selector(GoToViewRequest) forControlEvents:UIControlEventTouchUpInside];
    [DoneBtn setBackgroundColor:[UIColor colorWithRed:0.85 green:0.69 blue:0.14 alpha:1.0]];
    DoneBtn.layer.cornerRadius = 5;
}
-(void)viewWillAppear:(BOOL)animated{
    [self UpdateNotifications];
}
- (void)reloadData{
    CheckValue = @"Refersh";
    [self WebApiCall];
}
-(void)UpdateNotifications{
    NatificationLabel.alpha=0.0;
    NatificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
    if ([NatificationLabel.text intValue]>0) {
        NatificationLabel.alpha=1.0;
        NatificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        RequestCell *Cell = (RequestCell*)[EmployeeTableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        Cell.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToRequestIWork)];
        [Cell.iWorkView  setUserInteractionEnabled:YES];
        [Cell.iWorkView addGestureRecognizer:RequestIWork];
        
        Cell.RequestIWorkLabel.text = NSLocalizedString(@"REQUEST_IWORK", nil);
        Cell.RequestIWorkLabel.text = [Cell.RequestIWorkLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
        Cell.iWorkImageView.image = [UIImage imageNamed:@""];
        UITapGestureRecognizer* ViewRequest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToViewRequest)];
        [Cell.RequestView  setUserInteractionEnabled:YES];
        [Cell.RequestView addGestureRecognizer:ViewRequest];
        return Cell;
    }
    else {
        CheckInCell *Cell = (CheckInCell*)[EmployeeTableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];\
        Cell.backgroundColor = [UIColor clearColor];
        
        [Cell.CheckInButton setTitle:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil) forState:UIControlStateNormal];
        Cell.CheckInButton.backgroundColor = delegate.redColor;
        [Cell.CheckInButton addTarget:self action:@selector(CheckInChekcOut:) forControlEvents:UIControlEventTouchUpInside];
        Cell.NoIWorkLabel.hidden = YES;
        Cell.DateLabel.text = [self GetDayWithDate];
        NSString *GetDateStr = [self GetDate];
        NSArray *Split = [GetDateStr componentsSeparatedByString:@" "];
        Cell.TimeLabel.text = Split[0];
        Cell.AM_PM_Label.text = Split[1];

        NSString *myRequestForToday = [ResponseDic[@"object"] valueForKey:@"myRequestForToday"];
        if([myRequestForToday integerValue] == 1){
            NSMutableDictionary *myRequestDic = [ResponseDic[@"object"] valueForKey:@"myRequest"];
            NSString *check_in = [myRequestDic valueForKey:@"checkIn"];
            NSString *check_out = [myRequestDic valueForKey:@"checkOut"];
            
            if([check_in integerValue] == 1){
                [Cell.CheckInButton setTitle:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil) forState:UIControlStateNormal];
                Cell.CheckInButton.hidden = NO;
                Cell.DateLabel.hidden = NO;
                Cell.TimeLabel.hidden = NO;
                Cell.AM_PM_Label.hidden = NO;
                Cell.CongratsLabel.hidden = NO;
                Cell.NoIWorkLabel.hidden = YES;
                useriWorkStatus =YES;
            }
            else if([check_out integerValue] == 1){
                [Cell.CheckInButton setTitle:NSLocalizedString(@"CHECK_OUT_BTN_CAPS", nil) forState:UIControlStateNormal];
                Cell.CheckInButton.hidden = NO;
                Cell.DateLabel.hidden = NO;
                Cell.TimeLabel.hidden = NO;
                Cell.AM_PM_Label.hidden = NO;
                Cell.CongratsLabel.hidden = NO;
                Cell.NoIWorkLabel.hidden = YES;
            }
            else{
                Cell.CheckInButton.hidden = YES;
                Cell.DateLabel.hidden = NO;
                Cell.AM_PM_Label.hidden = YES;
                Cell.CongratsLabel.hidden = YES;
                Cell.TimeLabel.hidden = YES;
                Cell.NoIWorkLabel.hidden = NO;
            }
            if ([myRequestDic valueForKey:@"checkIn"] == [NSNull null] && [myRequestDic valueForKey:@"checkOut"] == [NSNull null]) {
                Cell.CheckInButton.hidden = YES;
            }
        }
        else{
            Cell.TimeLabel.hidden = YES;
            Cell.AM_PM_Label.hidden = YES;
            Cell.CongratsLabel.hidden = YES;
            Cell.CheckInButton.hidden = YES;
            Cell.NoIWorkLabel.hidden = NO;
            Cell.DateLabel.hidden = NO;
        }
        return Cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 1){
        return 105;
    }
    return 92;
}
- (void)WebApiCall {
    
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MY_IWORK"]||[CheckValue isEqualToString:@"Refersh"]){
            params = @ {
                @"empId": empId,
                @"empName": empName,
                @"location": location,
                @"userId": [ApplicationState userId]
            };
            ResponseDic = [Api WebApi:params Url:@"myIwork"];
            [refreshControl endRefreshing];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                if(IsSafeStringPlus(TrToString(ResponseDic[@"object"]))){
                    iWorkIdStr = [[ResponseDic[@"object"]valueForKey:@"myRequest"] valueForKey:@"requestId"];
                }
                else{
                    iWorkIdStr = @"";
                }
                [EmployeeTableView reloadData];
            }
            else{
               [LoadingManager hideLoadingView:self.view];
               [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
        else if ([CheckValue isEqualToString:@"CHECKIN"]||[CheckValue isEqualToString:@"CHECKOUT"]){
            if([delegate.userLat doubleValue] ==0.0 || [delegate.userLong doubleValue] == 0.0){
                [AlertManager showPopupMessageAlert:NSLocalizedString(@"TURN_ON_LOCATION", nil) withTitle:nil];
            }
            //else{
                delegate.userLat = @"";
                delegate.userLong = @"";
           // }
            NSString *Lat = [NSString stringWithFormat:@"%@",delegate.userLat];
            NSString *Logn = [NSString stringWithFormat:@"%@",delegate.userLong];
            if([Lat isEqualToString:@"(null)"]){
                Lat = @"";
                Logn = @"";
            }
//            else{
                if([CheckValue isEqualToString:@"CHECKIN"]){
                    params = @ {
                        @"mapping" : @"apis",
                        @"userId" : [ApplicationState userId],
                        @"latitude" : Lat,
                        @"longitude" : Logn
                    };
                    ResponseDic = [Api WebApi:params Url:@"checkIn"];
                    NSLog(@"ResponseDic==%@",ResponseDic);
                    
                    if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                        [LoadingManager hideLoadingView:self.view];
                        MsgOuterView.hidden = YES;
                        [self ShowErrorTitle:NSLocalizedString(@"CHECK_IN_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"TITME_COMPLETE" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                        CheckValue = @"MY_IWORK";
                        [EmployeeTableView reloadData];
                        [self WebApiCall];
                    }
                    else{
                        [LoadingManager hideLoadingView:self.view];
                        [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                    }
                }
                else if([CheckValue isEqualToString:@"CHECKOUT"]){
                    params = @ {
                        @"mapping" : @"apis",
                        @"userId" : [ApplicationState userId],
                        @"requestId" : iWorkIdStr,
                        @"latitude" : Lat,
                        @"longitude" : Logn
                    };
                    ResponseDic = [Api WebApi:params Url:@"checkOut"];
                    if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                        [LoadingManager hideLoadingView:self.view];
                        MsgOuterView.hidden = YES;
                        [self ShowErrorTitle:NSLocalizedString(@"CHECK_OUT_TITME", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"TITME_COMPLETE" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                        [EmployeeTableView reloadData];
                        CheckValue = @"MY_IWORK";
                        [self WebApiCall];
                    }
                    else if([ResponseDic[@"statusCode"]intValue]==105) // Automatic Checkout
                    {
                        [LoadingManager hideLoadingView:self.view];
                        [self ShowErrorTitle:NSLocalizedString(@"HOURS_NOT_COMPLETE_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"CUT_OFF_TIME" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                        [EmployeeTableView reloadData];
                        CheckValue = @"MY_IWORK";
                        [self WebApiCall];
                    }
                    else if([ResponseDic[@"statusCode"]intValue]==106) // Automatic Checkout
                    {
                        [LoadingManager hideLoadingView:self.view];
                        [self ShowErrorTitle:NSLocalizedString(@"MAXIMUM_CHECKED_OUT_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"CHECKOUT_TIME" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                        [EmployeeTableView reloadData];
                        CheckValue = @"MY_IWORK";
                        [self WebApiCall];
                    }
                    else if([ResponseDic[@"statusCode"]intValue]==205)
                    {
                        [LoadingManager hideLoadingView:self.view];
                        [self ShowErrorTitle:NSLocalizedString(@"HOURS_NOT_COMPLETE_TITLE", nil) Message:NSLocalizedString(ResponseDic[@"message"], nil) ImageName:@"CUT_OFF_TIME" BtnTitle:NSLocalizedString(@"CLOSE", nil)];
                        [EmployeeTableView reloadData];
                        CheckValue = @"MY_IWORK";
                        [self WebApiCall];
                    }
                    else{
                        [LoadingManager hideLoadingView:self.view];
                        [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                        [EmployeeTableView reloadData];
                        CheckValue = @"MY_IWORK";
                        [self WebApiCall];
                    }
                }
           // }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)ShowErrorTitle:(NSString*)Title Message:(NSString*)Msg ImageName:(NSString*)ImageName BtnTitle:(NSString*)BtnTilte{
    ApproveRejactView.hidden = NO;
    ApproveTitleLabel.text = Title;
    ApproveRejactLabel.text = Msg;
    ApproveImageView.image = [UIImage imageNamed:ImageName];
    [ApproveCloseBtn setTitle:BtnTilte forState:UIControlStateNormal];
}
-(IBAction)Close:(id)sender{
    ApproveRejactView.hidden = YES;
}
-(void)CheckInChekcOut:(UIButton*)sender{
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CHECK_IN_BTN_CAPS", nil)]){
        CheckValue = @"CHECKIN";
    }
    else if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CHECK_OUT_BTN_CAPS", nil)]){
         CheckValue = @"CHECKOUT";
    }
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCall) withObject:nil afterDelay:0.4];
}
-(void)GoToRequestIWork{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateWorkRequestViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateWorkRequestViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GoToViewRequest{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EmployeeParentViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"EmployeeParentViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
    MsgOuterView.hidden = YES;
}
-(IBAction)GotoProfilePage:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
    if([isGetNotification isEqualToString:@"YES"]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AppContainerViewController *newView = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else{
        [[self navigationController] popViewControllerAnimated:YES];
    }
}
-(NSString*)GetDayWithDate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM YYYY"];
    NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSString *day;
    switch ([component weekday]) {
        case 1:
            day = NSLocalizedString(@"SUNDAY", nil);
            break;
        case 2:
            day = NSLocalizedString(@"MONDAY", nil);
            break;
        case 3:
            day = NSLocalizedString(@"TUESDAY", nil);
            break;
        case 4:
            day = NSLocalizedString(@"WEDNESDAY", nil);
            break;
        case 5:
            day = NSLocalizedString(@"THERSDAY", nil);
            break;
        case 6:
            day = NSLocalizedString(@"FRIDAY", nil);
            break;
        case 7:
            day = NSLocalizedString(@"SATURDAY", nil);
            break;
        default:
            break;
    }
    return  [NSString stringWithFormat:@"%@, %@",day,[dateFormat stringFromDate:[NSDate date]]];
}
- (IBAction)notificationTapped:(id)sender {
   [delegate DeleteNotificationCounter:@"IWORK"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotificationViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(NSString*)GetDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [NSDate date];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return  formattedDate;
}
-(NSString*)GetDate:(NSString*)Time{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-mm-ss HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:Time];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return  formattedDate;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
- (IBAction)HomeAction:(id)sender {
    if([isGetNotification isEqualToString:@"YES"]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AppContainerViewController *newView = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else{
        NSArray *array = [self.navigationController viewControllers];
        for (int i = 0 ; i < array.count; i++) {
            UIViewController *ViewController = [array objectAtIndex:i];
            if([ViewController isKindOfClass:[AppContainerViewController class]]){
                [self.navigationController popToViewController:ViewController animated:YES];
            }
        }
    }
}
@end
