//
//  EmployeeNewViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeNewViewController.h"
#import "NewCell.h"
#import "Header.h"

@interface EmployeeNewViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *NewTableView;
    NSMutableArray *NewArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *dalegate;
    NSString *user_requests_id;
    IBOutlet UILabel *NoDataLabel;
    NSDictionary *params;
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *YesBtn;
    IBOutlet UIButton *NoBtn;
    int TagValue;
    UIWindow *Window;
    NSDictionary *ResponseDic;
    APIService *Api;
    int offset;
    int limit;
}
@end

@implementation EmployeeNewViewController

- (void)viewDidLoad {
    offset = 0;
    limit = 10;
    CheckValue = @"NEW";
     NewArray = [[NSMutableArray alloc] init];
    Api = [[APIService alloc] init];
    dalegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = dalegate.BackgroudColor;
    NewTableView.backgroundColor = dalegate.BackgroudColor;
    NewTableView.estimatedRowHeight = 50;
    NewTableView.rowHeight = UITableViewAutomaticDimension;
    NewTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, NewTableView.bounds.size.width, 10.0f)];
    
    NewTableView.delegate = self;
    NewTableView.dataSource = self;
    
    [self MessageContentView];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [NewTableView addSubview:refreshControl];
    
    [self GetiWorkUserNew];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NewApi:) name:@"New" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    NewArray = [[NSMutableArray alloc] init];
    [NewTableView reloadData];
}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
    [YesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [YesBtn setBackgroundColor:dalegate.redColor];
    [NoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
    [NoBtn setBackgroundColor:[UIColor lightGrayColor]];
   
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: YesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    YesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: NoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    NoBtn.layer.mask = maskLayer2;
}
- (void)reloadData{
    CheckValue = @"Refresh";
    offset = 0;
    limit = 10;
     NewArray = [[NSMutableArray alloc] init];
    [self GetiWorkUserNew];
}

- (void)GetiWorkUserNew {
   // @"locationId": @0,
    if(dalegate.isInternetConnected){
        if([CheckValue isEqualToString:@"NEW"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"attendanceStatus": @"",
                    @"requestDate": @"",
                    @"requestId": @0,
                    @"status": @0,
                    @"userId": [ApplicationState userId],
                    @"workDate": @"",
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"myIworkRequest"];
            [refreshControl endRefreshing];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                NoDataLabel.hidden = YES;
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    NoDataLabel.hidden = YES;
                }
                else if(ResponseArrays.count == 0 && NewArray.count == 0){
                    [self NoIworkRequest];
                }
                   [NewTableView reloadData];
            }
            else{
                if(NewArray.count == 0){
                    [self NoIworkRequest];
                }
            }
        }
        else if([CheckValue isEqualToString:@"CANCEL"]){
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"updateStatusOfRequest?status=3&requestId=%@",user_requests_id]];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                 [LoadingManager hideLoadingView:self.view];
                [self EmptyArray];
                MsgOuterView.hidden = YES;
                CheckValue = @"NEW";
                [self GetiWorkUserNew];
            }
            else{
                 [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkRequest{
    [LoadingManager hideLoadingView:self.view];
    NoDataLabel.hidden = NO;
    NoDataLabel.text = @"No iWork request found";;
    NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkUserNew];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
   
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [NewArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [NewArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NewCell *Cell = (NewCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
     Cell.CancelBtn.tag = indexPath.section;
    [Cell.CancelBtn addTarget:self action:@selector(CancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    Cell.CancelBtn.tag = indexPath.section;
    
    UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
    Cell.tag = indexPath.section;
    [Cell  setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:Gesture];
    
    if(NewArray.count>0){
        NSDictionary * responseData = [NewArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
    }
    return Cell;
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    if(dalegate.isInternetConnected){
         TagValue = sender.view.tag;
        NSDictionary *Dics = [NewArray objectAtIndex:TagValue];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
        ObjViewController.MapDic = Dics;
        [[self navigationController] pushViewController:ObjViewController animated:YES];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NewApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserNew) withObject:nil afterDelay:0.4];
}
-(IBAction)YESBtnAction:(id)sender{
    CheckValue =@"CANCEL";
    NSDictionary * responseData = NewArray[TagValue];
    if (responseData[@"requestId"] !=nil) {
        user_requests_id = responseData[@"requestId"];
    }
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserNew) withObject:nil afterDelay:0.4];
}
-(IBAction)NoBtnAction:(id)sender{
    MsgOuterView.hidden =YES;
}
-(void)CancelBtnAction:(UIButton*)Sender{
    MsgOuterView.hidden =NO;
    TagValue = Sender.tag;
    MsgLabel.text = NSLocalizedString(@"CACNEL_MSG", nil);
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
