//
//  ApproveRejectCell.h
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApproveRejectCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *CellBgView;
@property (strong, nonatomic) IBOutlet UIImageView *ProfileImageView;
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *ApprovedStaticLabel;
@property (strong, nonatomic) IBOutlet UILabel *ApproveValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *RejectStaticLabel;
@property (strong, nonatomic) IBOutlet UILabel *RejectValueLabel;
@property (strong, nonatomic) IBOutlet UIView *YellowView;
@end
