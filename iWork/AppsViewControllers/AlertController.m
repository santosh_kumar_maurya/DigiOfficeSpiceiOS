//
//  AlertController.m
//  iWork
//
//  Created by Shailendra on 27/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AlertController.h"

@interface AlertController (){
    IBOutlet UILabel *MessageLabel;
    IBOutlet UIButton *MessageBtn;
}
@end

@implementation AlertController
@synthesize MessageTitleStr,MessageBtnStr;


- (void)viewDidLoad {
    MessageLabel.text =  MessageTitleStr;
    [MessageBtn setTitle:MessageBtnStr forState:UIControlStateNormal];
    [super viewDidLoad];
}
- (IBAction)BackAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
