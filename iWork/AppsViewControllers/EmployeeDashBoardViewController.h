//
//  EmployeeDashBoardViewController.h
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeeDashBoardViewController : UIViewController

@property(nonatomic,strong)NSString *isComeFrom;
@property(nonatomic,strong)NSDictionary *NoticationDic;
@end
