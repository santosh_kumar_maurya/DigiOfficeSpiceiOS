//
//  EmployeeApprovedViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeApprovedViewController.h"
#import "ApprovedCell.h"
#import "Header.h"

@interface EmployeeApprovedViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *ApprovedTableView;
    NSMutableArray *ApprovedArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    NSString *cortex;
    NSString *user_requestsId;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *YesBtn;
    IBOutlet UIButton *NoBtn;
    int TagValue;
    UIButton *btn;
    NSDictionary *ResponseDic;
    APIService *Api;
    int offset;
    int limit;
}
@end

@implementation EmployeeApprovedViewController

- (void)viewDidLoad {
    offset = 0;
    limit = 10;
    Api = [[APIService alloc] init];
     ApprovedArray = [[NSMutableArray alloc] init];
    CheckValue = @"APPROVED";
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    ApprovedTableView.estimatedRowHeight = 50;
    ApprovedTableView.backgroundColor = delegate.BackgroudColor;
    ApprovedTableView.rowHeight = UITableViewAutomaticDimension;
    ApprovedTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, ApprovedTableView.bounds.size.width, 10.0f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [ApprovedTableView addSubview:refreshControl];
    [self MessageContentView];
    [self GetiWorkUserApproved];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ApprovedApi:) name:@"Approved" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
    [YesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [YesBtn setBackgroundColor:delegate.redColor];
    [NoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
    [NoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: YesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    YesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: NoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    NoBtn.layer.mask = maskLayer2;
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self EmpltyArray];
    [self GetiWorkUserApproved];
}
-(void)EmpltyArray{
    offset = 0;
    limit = 10;
    ApprovedArray = [[NSMutableArray alloc] init];
    [ApprovedTableView reloadData];
}
- (void)GetiWorkUserApproved {
    //@"locationId": @0,
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"APPROVED"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"status": @1,
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"myIworkRequest"];
            [refreshControl endRefreshing];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    NoDataLabel.hidden = YES;
                }
                else if(ResponseArrays.count == 0 && ApprovedArray.count == 0){
                    [self NoIworkRequest];
                }
                [ApprovedTableView reloadData];
            }
            else{
                
                if([ApprovedArray count]==0){
                    [self NoIworkRequest];
                }
            }
        }
        else if ([CheckValue isEqualToString:@"CHECKIN"]||[CheckValue isEqualToString:@"CHECKOUT"]){
            if([delegate.userLat doubleValue] ==0.0 || [delegate.userLong doubleValue] == 0.0){
                [AlertManager showPopupMessageAlert:NSLocalizedString(@"TURN_ON_LOCATION", nil) withTitle:nil];
            }
//            else{
                delegate.userLat = @"";
                delegate.userLong = @"";
            //}
            NSString *Lat = [NSString stringWithFormat:@"%@",delegate.userLat];
            NSString *Logn = [NSString stringWithFormat:@"%@",delegate.userLong];
            if([Lat isEqualToString:@"(null)"]){
                Lat = @"";
                Logn = @"";
            }
           // else{
                if([CheckValue isEqualToString:@"CHECKIN"]){
                    params = @ {
                        @"mapping" : @"apis",
                        @"userId" : [ApplicationState userId],
                        @"latitude" : Lat,
                        @"longitude" : Logn
                    };
                    ResponseDic = [Api WebApi:params Url:@"checkIn"];
                    NSLog(@"ResponseDic==%@",ResponseDic);
                    
                    if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                        [LoadingManager hideLoadingView:self.view];
                        MsgOuterView.hidden = YES;
                        CheckValue = @"APPROVED";
                        [self EmpltyArray];
                        [self GetiWorkUserApproved];
                    }
                    
                    else{
                        [LoadingManager hideLoadingView:self.view];
                        [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                    }
                }
                else if([CheckValue isEqualToString:@"CHECKOUT"]){
                    params = @ {
                        @"mapping" : @"apis",
                        @"userId" : [ApplicationState userId],
                        @"requestId" : user_requestsId,
                        @"latitude" : Lat,
                        @"longitude" : Logn
                    };
                    ResponseDic = [Api WebApi:params Url:@"checkOut"];
                    NSLog(@"ResponseDic==%@",ResponseDic);
                    if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                        MsgOuterView.hidden = YES;
                        CheckValue = @"APPROVED";
                       [self EmpltyArray];
                        [self GetiWorkUserApproved];
                    }
                    else{
                        [LoadingManager hideLoadingView:self.view];
                        [self ShowAlert:NSLocalizedString(ResponseDic[@"message"], nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                    }
                }
            //}
        }
     }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkRequest{
    [LoadingManager hideLoadingView:self.view];
    NoDataLabel.hidden = NO;
    NoDataLabel.text = @"No iWork request found";
    NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 10;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkUserApproved];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [ApprovedArray addObject:BindDataDic];
    }
}
- (void)checkInCheckOutClicked:(UIButton *)sender {
    TagValue = sender.tag;
    btn = sender;
    MsgOuterView.hidden =NO;
    if([sender.titleLabel.text isEqualToString:@"Check-In"]){
        MsgLabel.text = NSLocalizedString(@"CHECKIN_MSG", nil);
        CheckValue = @"CHECKIN";
    }
    else{
        MsgLabel.text = NSLocalizedString(@"CHECKOUT_MSG", nil);
        CheckValue = @"CHECKOUT";
    }
}
-(IBAction)YESBtnAction:(id)sender{
    user_requestsId = @" ";
    NSDictionary * responseData = ApprovedArray[TagValue];
    if (responseData[@"requestId"] !=nil) {
        user_requestsId = responseData[@"requestId"];
    }
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserApproved) withObject:nil afterDelay:0.4];
}
-(IBAction)NoBtnAction:(id)sender{
    MsgOuterView.hidden =YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [ApprovedArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ApprovedCell *Cell = (ApprovedCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.CheckInButton.tag = indexPath.section;
    [Cell.CheckInButton addTarget:self action:@selector(checkInCheckOutClicked:) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
    Cell.tag = indexPath.section;
    [Cell  setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:Gesture];
   
    if(ApprovedArray.count>0){
        NSDictionary * responseData = [ApprovedArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
    }
    
    return Cell;
}
-(void)ApprovedApi:(NSNotification*)notification{
    params = notification.userInfo;
    [self EmpltyArray];
    CheckValue = @"FILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetiWorkUserApproved) withObject:nil afterDelay:0.4];
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    if(delegate.isInternetConnected){
        TagValue = sender.view.tag;
        NSDictionary *Dics = [ApprovedArray objectAtIndex:TagValue];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
        ObjViewController.MapDic = Dics;
        [[self navigationController] pushViewController:ObjViewController animated:YES];
    }
    else{
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
