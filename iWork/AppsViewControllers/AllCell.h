//
//  AllCell.h
//  iWork
//
//  Created by Shailendra on 18/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@interface AllCell : UITableViewCell
{
     AppDelegate *delegate;
}
@property (weak, nonatomic) IBOutlet UILabel *RequestIdStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *RequestIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *RequestDateStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *RequestDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkLocationStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkLocationTypeStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *iWorkLocationTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *StatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *StatusImageView;
@property (weak, nonatomic) IBOutlet UILabel *ActionDateStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *ActionDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *HoursStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *HoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *CheckInStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *CheckInLabel;
@property (weak, nonatomic) IBOutlet UILabel *CheckoutStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *CheckoutLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskLabel;
@property (weak, nonatomic) IBOutlet UILabel *AttandenceStatusStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *AttandenceStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *MaxCheckInStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *MaxCheckInLabel;
@property (weak, nonatomic) IBOutlet UIButton *CheckInButton;
@property (weak, nonatomic) IBOutlet UIButton *CancelButton;
- (void)configureCell:(NSDictionary *)info;
@end
