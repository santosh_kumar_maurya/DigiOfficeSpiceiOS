//
//  ProfileViewController.m
//  iWork
//
//  Created by Fourbrick on 05/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "ProfileViewController.h"
#import "Header.h"

@interface ProfileViewController (){
    IBOutlet UIView *ProfileView;
    IBOutlet UIView *EmailView;
    IBOutlet UIView *DepartmentView;
    IBOutlet UIView *ReportingView;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *EmailIDStaticLabel;
    IBOutlet UILabel *DepartmentStaticLabel;
    IBOutlet UILabel *ReportingManagerStaticLabel;
    IBOutlet UILabel *WelcomeStaticLabel;
    IBOutlet UIImageView *profileImageView;
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *emailLabel;
    IBOutlet UILabel *departmentLabel;
    IBOutlet UILabel *reportingManagerLabel;
    IBOutlet UILabel *workLimitLabel;
    IBOutlet UILabel *consumedLabel;
    AppDelegate *delegate;
    NSDictionary *params;
    APIService *Api;
    NSDictionary *ResponseDic;
}
@end

@implementation ProfileViewController
@synthesize isComeFrom;
- (void)viewDidLoad {
    [super viewDidLoad];
    Api = [[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    HeaderLabel.text = NSLocalizedString(@"MY_PROFILE", nil);
    EmailIDStaticLabel.text = NSLocalizedString(@"EMAIL_ID", nil);
    DepartmentStaticLabel.text = NSLocalizedString(@"DEPARTMENT", nil);
    ReportingManagerStaticLabel.text = NSLocalizedString(@"REPORTING_MANAGER", nil);

    profileImageView.layer.cornerRadius = 30.0;
    profileImageView.layer.masksToBounds=YES;
    emailLabel.text = @"";
    departmentLabel.text = @"";
    reportingManagerLabel.text = @"";
    workLimitLabel.text = @"";
    consumedLabel.text = @"";
    nameLabel.text = @"";
    
    ProfileView.layer.cornerRadius = 5;
    ProfileView.layer.masksToBounds = YES;
    ProfileView.backgroundColor = [UIColor whiteColor];
    ProfileView.layer.borderWidth = 0.5;
    ProfileView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    EmailView.layer.cornerRadius = 5;
    EmailView.layer.masksToBounds = YES;
    EmailView.layer.borderWidth = 0.5;
    EmailView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    DepartmentView.layer.cornerRadius = 5;
    DepartmentView.layer.masksToBounds = YES;
    DepartmentView.layer.borderWidth = 0.5;
    DepartmentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    ReportingView.layer.cornerRadius = 5;
    ReportingView.layer.masksToBounds = YES;
    ReportingView.layer.borderWidth = 0.5;
    ReportingView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    if([isComeFrom isEqualToString:@"PMS"]||[isComeFrom isEqualToString:@"CONTAINER"]){
        WelcomeStaticLabel.text = NSLocalizedString(@"WELCOME_TO_INDOSAT", nil);
        workLimitLabel.hidden = YES;
        consumedLabel.hidden = YES;
    }
    else{
        WelcomeStaticLabel.text = NSLocalizedString(@"WELCOME_TO_I_WORK", nil);
        WelcomeStaticLabel.text = [WelcomeStaticLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    }
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(performUserRequestData) withObject:nil afterDelay:1.0];
    self.view.backgroundColor = delegate.BackgroudColor;
}
- (void)performUserRequestData {
   
    if(delegate.isInternetConnected){
        if([isComeFrom isEqualToString:@"PMS"]||[isComeFrom isEqualToString:@"CONTAINER"]){
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"userProfileAuth?userId=%@",[ApplicationState userId]]];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                NSDictionary *responseObject = [[ResponseDic valueForKey:@"object"] valueForKey:@"employeeDetail"];
                if(IsSafeStringPlus(TrToString(responseObject[@"name"]))){
                    nameLabel.text = [NSString stringWithFormat:@"%@", responseObject[@"name"]];
                }
                if(IsSafeStringPlus(TrToString(responseObject[@"email_id"]))){
                    emailLabel.text = [NSString stringWithFormat:@"%@", responseObject[@"email_id"]];
                }
                if(IsSafeStringPlus(TrToString(responseObject[@"department"]))) {
                    if ([responseObject[@"department"] isEqualToString:@"0"]) {
                        departmentLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                    }
                    else if ([responseObject[@"department"] isEqualToString:@"1"]) {
                        departmentLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                    }
                    else {
                        departmentLabel.text = [NSString stringWithFormat:@"%@",responseObject[@"department"]] ;
                    }
                } else {
                    departmentLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                }
                if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"lineManagerDetail"]))) {
                    reportingManagerLabel.text = [NSString stringWithFormat:@"%@", [[[ResponseDic valueForKey:@"object"] valueForKey:@"lineManagerDetail"] valueForKey:@"name"]];
                    ReportingView.hidden = NO;
                    
                } else {
                    reportingManagerLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                    ReportingView.hidden = YES;
                }
                if(IsSafeStringPlus(TrToString(responseObject[@"dp"]))){
                    UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:responseObject[@"dp"]]]];
                    if(images == nil){
                        profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                    }
                    else{
                        NSString *URL = [NSString stringWithFormat:@"%@",responseObject[@"dp"]];
                        [profileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
                    }
                }
                else{
                    profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                }
            }
            else {
                [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:ResponseDic[@"message"] ButtonTitle:NSLocalizedString(@"OKAY", nil)];
            }
        }
        else{
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"userProfileData?user_id=%@",[ApplicationState userId]]];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                NSDictionary *responseObject = [ResponseDic valueForKey:@"object"];
                if(IsSafeStringPlus(TrToString(responseObject[@"name"]))){
                    nameLabel.text = [NSString stringWithFormat:@"%@", responseObject[@"name"]];
                }
                if(IsSafeStringPlus(TrToString(responseObject[@"email_id"]))){
                    emailLabel.text = [NSString stringWithFormat:@"%@", responseObject[@"email_id"]];
                }
                if(IsSafeStringPlus(TrToString(responseObject[@"department"]))) {
                    if ([responseObject[@"department"] isEqualToString:@"0"]) {
                        departmentLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                    }
                    else if ([responseObject[@"department"] isEqualToString:@"1"]) {
                        departmentLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                    }
                    else {
                        departmentLabel.text = [NSString stringWithFormat:@"%@",responseObject[@"department"]] ;
                    }
                } else {
                    departmentLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                }
                
                if(IsSafeStringPlus(TrToString(responseObject[@"reporting_manager"]))) {
                    if (![responseObject[@"reporting_manager"] isEqualToString:@"0"]) {
                        reportingManagerLabel.text = [NSString stringWithFormat:@"%@", responseObject[@"reporting_manager"]];
                        ReportingView.hidden = NO;
                    } else {
                        reportingManagerLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                        ReportingView.hidden = YES;
                    }
                } else {
                    reportingManagerLabel.text = [NSString stringWithFormat:@"%@", @""] ;
                    ReportingView.hidden = YES;
                }
                
                if(IsSafeStringPlus(TrToString(responseObject[@"iwork_limit"]))) {
                    NSString *Str = NSLocalizedString(@"I_WORK_LIMIT", nil);
                    Str = [Str stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
                    workLimitLabel.text = [NSString stringWithFormat:@"%@ %d",Str, [responseObject[@"iwork_limit"]intValue]];
                } else {
                    workLimitLabel.text = @"" ;
                }
                
                if(IsSafeStringPlus(TrToString(responseObject[@"comsumed"]))){
                    consumedLabel.text = [NSString stringWithFormat:@"Used : %@", responseObject[@"comsumed"]];
                }
                else {
                    consumedLabel.text = @" ";
                }
                if(IsSafeStringPlus(TrToString(responseObject[@"dp"]))){
                    UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:responseObject[@"dp"]]]];
                    if(images == nil){
                        profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                    }
                    else{
                        NSString *URL = [NSString stringWithFormat:@"%@",responseObject[@"dp"]];
                        [profileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
                    }
                }
                else{
                    profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                }
            }
            else {
                [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:ResponseDic[@"message"] ButtonTitle:NSLocalizedString(@"OKAY", nil)];
            }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (IBAction)HomeAction:(id)sender {
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[AppContainerViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
-(IBAction)backButtonDidClicked:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
