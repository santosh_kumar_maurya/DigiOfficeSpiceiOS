//
//  MyTeamCell.h
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTeamCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *CellBgView;
@property (strong, nonatomic) IBOutlet UILabel *MyTeamLabel;
@property (strong, nonatomic) IBOutlet UIImageView *FilterImageView;
@property (strong, nonatomic) IBOutlet UIButton *FilterButton;
@end
