//
//  EmployeeCell.h
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeeCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *CellBgView;
@property (strong, nonatomic) IBOutlet UIImageView *ProfileImageView;
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *CheckInStaticLabel;
@property (strong, nonatomic) IBOutlet UILabel *CheckInValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *CheckOutStaticLabel;
@property (strong, nonatomic) IBOutlet UILabel *CheckOutValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *FramStaticLabel;
@property (strong, nonatomic) IBOutlet UILabel *LacationLabel;
@property (strong, nonatomic) IBOutlet UIView *YellowView;

@end
