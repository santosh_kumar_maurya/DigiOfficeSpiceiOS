//
//  AlertController.h
//  iWork
//
//  Created by Shailendra on 27/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertController : UIViewController{
}
@property (weak, nonatomic)NSString *MessageTitleStr;
@property (weak, nonatomic)NSString *MessageBtnStr;
@end
