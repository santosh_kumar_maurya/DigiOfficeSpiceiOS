//
//  NotificationViewController.m
//  iWork
//
//  Created by Fourbrick on 05/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "NotificationViewController.h"
#import "Header.h"

@interface NotificationViewController (){
    IBOutlet UITableView *NotificationtableView;
    IBOutlet UILabel *NoDataLabel;
    AppDelegate *delegate;
    APIService *Api;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSMutableArray *requestArray;
}
@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Api  =[[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NotificationtableView.estimatedRowHeight = 500.0;
    NotificationtableView.rowHeight = UITableViewAutomaticDimension;
    [NotificationtableView setNeedsLayout];
    NotificationtableView.backgroundColor = [UIColor clearColor];
    NotificationtableView.backgroundView.backgroundColor = [UIColor clearColor];
    NotificationtableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,NotificationtableView.bounds.size.width, 0.01f)];
    self.view.backgroundColor = delegate.BackgroudColor;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(performUserRequestData) withObject:nil afterDelay:1.0];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [requestArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
     return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NotificationCell * projectCell = (NotificationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(requestArray.count>0){
        NSDictionary * responseData = requestArray[indexPath.section];
        if(IsSafeStringPlus(TrToString(responseData[@"message"]))) {
            NSString *msg =  [responseData valueForKey:@"message"];
            msg = [msg stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
            projectCell.NameLabel.text = msg;
            
        } else {
            projectCell.NameLabel.text = @" ";
        }
        [projectCell configureCell:requestArray[indexPath.section]];
    }
    return projectCell;
}
- (void)performUserRequestData {
    
    if(delegate.isInternetConnected){
       ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getNotifications?userId=%@",[ApplicationState userId]]];
        requestArray = [[NSMutableArray alloc] init];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            requestArray = [ResponseDic valueForKey:@"object"];
            NoDataLabel.hidden = YES;
            if([requestArray count]==0){
                [self NoIworkNotification];
            }
            [NotificationtableView reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
             [self NoIworkNotification];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkNotification{
    NoDataLabel.hidden = NO;
    NoDataLabel.text = @"No iWork Nofification found";
    NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
}
- (IBAction)backDidClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)HomeAction:(id)sender {
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[AppContainerViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
