//
//  NotificationCell.m
//  iWork
//
//  Created by Shailendra on 19/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "NotificationCell.h"
#import "Shared.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)configureCell:(NSDictionary *)info{

    if(IsSafeStringPlus(TrToString(info[@"date_time"]))) {
        NSNumber *datenumber = info[@"date_time"];
        NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
        double miliSec = [DateStr doubleValue];
        NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"YYY-MM-dd"]; // Date formater
        NSString *date = [dateformate stringFromDate:takeOffDate]; // Convert date to string
        _DateLabel.text =  [self dateToFormatedDate:date];
        
    } else {
        _DateLabel.text = @" ";
    }
}
-(NSString *)dateToFormatedDate:(NSString *)dateStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYY-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    [dateFormatter setDateFormat:@"dd MMM YYYY"];
    return [dateFormatter stringFromDate:date];
}

@end
