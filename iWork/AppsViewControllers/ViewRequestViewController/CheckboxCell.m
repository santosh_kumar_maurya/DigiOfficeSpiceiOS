//
//  CheckboxCell.m
//  iWork
//
//  Created by Shailendra on 22/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "CheckboxCell.h"

@implementation CheckboxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
