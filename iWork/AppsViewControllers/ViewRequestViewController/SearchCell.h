//
//  SearchCell.h
//  iWork
//
//  Created by Shailendra on 17/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell
{
    
}
@property(nonnull,retain)IBOutlet UIButton *SearchBtn;
@property(nonnull,retain)IBOutlet UITextField *SearchTextField;
@end
