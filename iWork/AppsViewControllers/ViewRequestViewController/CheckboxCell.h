//
//  CheckboxCell.h
//  iWork
//
//  Created by Shailendra on 22/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckboxCell : UITableViewCell{
    
}
@property (nonatomic,retain) IBOutlet UIButton *CheckBoxBtn;
@property (nonatomic,retain) IBOutlet UIButton *BlanckBtn;
@property (nonatomic,retain) IBOutlet UILabel *LocationNameLabel;
@end
