//
//  MyTeamViewController.m
//  iWork
//
//  Created by Shailendra on 16/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTeamViewController.h"
#import "Header.h"


@interface MyTeamViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *MyTeamTableView;
    AppDelegate *delegate;
    NSString *CheckValue;
    NSMutableArray *TemaiworkArray;
    NSDictionary *ResponseDic;
    NSDictionary *params;
    NSString *SelectDuration;
    UIRefreshControl *refreshControl;
    APIService *Api;
}
@end

@implementation MyTeamViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    Api = [[APIService alloc] init];
    CheckValue = @"MY_TEAM";
    SelectDuration = @"";
    self.view.backgroundColor = delegate.BackgroudColor;
    MyTeamTableView.backgroundColor = delegate.BackgroudColor;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    MyTeamTableView.estimatedRowHeight = 200;
    [MyTeamTableView setNeedsLayout];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [MyTeamTableView addSubview:refreshControl];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MyTeamFilterApi:) name:@"MyTeamFilter" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetDuration:) name:@"SelectDurations" object:nil];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self WebApiCell];
}
- (void)WebApiCell {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MY_TEAM"]||[CheckValue isEqualToString:@"Refresh"]){
            params = @ {
                @"duration": @"month",
                @"empId": @"",
                @"empName": @"",
                @"endDate": @"",
                @"startDate": @"",
                @"userId": [ApplicationState userId],
            };
        }
        ResponseDic = [Api WebApi:params Url:@"myTeam"];
        TemaiworkArray = [[NSMutableArray alloc] init];
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            TemaiworkArray = [ResponseDic valueForKey:@"object"];
            [MyTeamTableView reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [TemaiworkArray count] +2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        RequestCell *Cell = (RequestCell*)[MyTeamTableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        Cell.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToRequestIWork)];
        [Cell.iWorkView  setUserInteractionEnabled:YES];
        [Cell.iWorkView addGestureRecognizer:RequestIWork];
        
        Cell.RequestIWorkLabel.text = NSLocalizedString(@"APPROVE_IWORK", nil);
        Cell.RequestIWorkLabel.text = [Cell.RequestIWorkLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
        
        UITapGestureRecognizer* ViewRequest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToViewRequest)];
        [Cell.RequestView  setUserInteractionEnabled:YES];
        [Cell.RequestView addGestureRecognizer:ViewRequest];
        
        return Cell;
    }
    else if(indexPath.row == 1){
        MyTeamCell *Cell = (MyTeamCell*)[MyTeamTableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        
        if([SelectDuration isEqualToString:@""]){
            SelectDuration = @"Month";
        }
        NSString *Duration = NSLocalizedString(@"IWORK_THIS", nil);
        Duration = [Duration stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
        
        Cell.MyTeamLabel.text = [NSString stringWithFormat:@"%@ %@",Duration,SelectDuration];
        
        Cell.backgroundColor = [UIColor clearColor];
        [Cell.FilterButton addTarget:self action:@selector(FilterAction) forControlEvents:UIControlEventTouchUpInside];
        return Cell;
    }
    else{
        ApproveRejectCell *Cell = (ApproveRejectCell*)[MyTeamTableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        Cell.backgroundColor = [UIColor clearColor];
        Cell.CellBgView.tag = indexPath.row;
        UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToEmpDetails:)];
        [Cell.CellBgView  setUserInteractionEnabled:YES];
        [Cell.CellBgView addGestureRecognizer:RequestIWork];
        NSDictionary *Dic;
        if(TemaiworkArray.count>0){
            Dic = [TemaiworkArray objectAtIndex:indexPath.row -2];
            if(IsSafeStringPlus(TrToString(Dic[@"dayApproved"]))){
                Cell.ApproveValueLabel.text = [NSString stringWithFormat:@"%d",[Dic[@"dayApproved"] intValue]];
            }
            else{
                Cell.ApproveValueLabel.text = @"";
            }
            if(IsSafeStringPlus(TrToString(Dic[@"dayRejected"]))){
                Cell.RejectValueLabel.text =  [NSString stringWithFormat:@"%d",[Dic[@"dayRejected"] intValue]];
            }
            else{
                Cell.RejectValueLabel.text = @"";
            }
            if(IsSafeStringPlus(TrToString(Dic[@"uName"]))){
                Cell.UserNameLabel.text = Dic[@"uName"];
            }
            else{
                Cell.UserNameLabel.text = @"";
            }
            if(IsSafeStringPlus(TrToString(Dic[@"dp"]))){
                UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:Dic[@"dp"]]]];
                if(images == nil){
                    NSLog(@"image nil");
                    Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
                }
                else{
                    NSString *URL = [NSString stringWithFormat:@"%@",Dic[@"dp"]];
                    [Cell.ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
                }
            }
            else{
                Cell.ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
            }
        }
        Cell.ProfileImageView.layer.cornerRadius = 22.5;
        Cell.ProfileImageView.clipsToBounds =YES;
        return Cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 1){
        return 55;
    }
    return 92;
}
-(void)GoToRequestIWork{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LineManagerRequestViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerRequestViewController"];
     ObjViewController.isComeFrom = @"Approvel";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GoToViewRequest{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LineManagerRequestViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerRequestViewController"];
    ObjViewController.isComeFrom = @"Report";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    LineManagerParentViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerParentViewController"];
//    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)FilterAction{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyTeamFilterViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTeamFilterViewController"];
   [self presentViewController:ObjViewController animated:YES completion:nil];
}
-(void)MyTeamFilterApi:(NSNotification*)notification{
    TemaiworkArray = [[NSMutableArray alloc] init];
    params = notification.userInfo;
    CheckValue = @"MYTEAMFILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(WebApiCell) withObject:nil afterDelay:0.5];
}
-(void)GetDuration:(NSNotification*)notification{
    NSDictionary *Dic = notification.userInfo;
    SelectDuration = [Dic valueForKey:@"Duration"];
}
-(void)GoToEmpDetails:(UIGestureRecognizer*)sender{
    int TagValue = sender.view.tag;
    NSDictionary *Dic = TemaiworkArray[TagValue - 2];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EmployeeDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"EmployeeDetailsViewController"];
    ObjViewController.Dic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
