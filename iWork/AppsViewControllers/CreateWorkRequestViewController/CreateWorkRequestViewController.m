//
//  CreateWorkRequestViewController.m
//  iWork
//
//  Created by Fourbrick on 04/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "CreateWorkRequestViewController.h"
#import "Header.h"


@interface CreateWorkRequestViewController () <PopOverViewDelegate, LocationDataDelegate, BIZPopupViewControllerDelegate,UIScrollViewDelegate>{
    
    IBOutlet UIView *ProfileView;
    IBOutlet UIView *TaskView;
    IBOutlet UIView *WeeklyView;
    IBOutlet UIView *MonthlyView;
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UIScrollView *ProfileScrollView;
    IBOutlet UIButton *InsosatLocationButton;
    IBOutlet UIButton *OtherLocationButton;
    IBOutlet UIButton *CreateIWorkRequest;
    IBOutlet UIButton *DoneBtn;
    
    IBOutlet UITextField *TaskTextField;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *MontlyQuotaUsedLabel;
    IBOutlet UILabel *MontlyQuotaLabel;
    IBOutlet UILabel *WeeklyQuotaUsedLabel;
    IBOutlet UILabel *WeeklyQuotaLabel;
    IBOutlet UILabel *iWorkMonthlyLimitLabel;
    IBOutlet UILabel *iWorkWeeklyLimitLabel;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UILabel *TitleLabel;
    IBOutlet UIImageView *RequestImageView;
    
    IBOutlet UILabel *userNameLabel;
    IBOutlet UIButton *calenderButton;
    IBOutlet UITextField *dateTewxtField;
    IBOutlet UITextField *locationLabel;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIView *containerBaseView;
    IBOutlet UIImageView *profileImageView;
    
    UIDatePicker *datePicker;
    BOOL datehasEnterd;
    NSInteger  workLocationId;
    APIService *Api;
    NSDictionary *ResponseDic;
    NSDictionary *Dic;
    int isOpenLaction;
    AppDelegate *delegate;
    NSDictionary *QuotaResponseDic;
    NSString *TitleString;
    NSString *MessageString;
    
    IBOutlet UIView *CancelMsgOuterView;
    IBOutlet UIView *CancelMsgInnerView;
    IBOutlet UILabel *CancelMsgLabel;
    IBOutlet UIButton *YesBtn;
    IBOutlet UIButton *NoBtn;
    
    
}
@end

@implementation CreateWorkRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Api = [[APIService alloc] init];
    isOpenLaction  = 1 ;
    userNameLabel.text = @"";
    [self EmptyData];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.view.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    dateTewxtField.placeholder = @"YYYY-MM-DD";
    locationLabel.placeholder = NSLocalizedString(@"SELECT_LOCATION_VALIDATION", nil);
    
    HeaderLabel.text = NSLocalizedString(@"CREATE_IWORK", nil);
    HeaderLabel.text = [HeaderLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    iWorkWeeklyLimitLabel.text = NSLocalizedString(@"IWORK_LIMIT", nil);
    iWorkWeeklyLimitLabel.text = [iWorkWeeklyLimitLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    iWorkMonthlyLimitLabel.text = NSLocalizedString(@"IWORK_LIMIT", nil);
    iWorkMonthlyLimitLabel.text = [iWorkMonthlyLimitLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    NSString *BtnStr = NSLocalizedString(@"CREATE_REQUEST", nil);
    BtnStr = [BtnStr uppercaseString];
    BtnStr = [BtnStr stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    [CreateIWorkRequest setTitle:BtnStr forState:UIControlStateNormal];
    CreateIWorkRequest.layer.cornerRadius = 3.0;
    CreateIWorkRequest.clipsToBounds = YES;
    
    [cancelButton setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    cancelButton.layer.cornerRadius = 3.0;
    cancelButton.layer.borderWidth = 1.0;
    cancelButton.clipsToBounds = YES;
    cancelButton.layer.borderColor = [ColorCategory PurperColor].CGColor;
    
    
    containerBaseView.layer.cornerRadius = 5;
    UIBezierPath *shadowPath1 = [UIBezierPath bezierPathWithRect:containerBaseView.bounds];
    containerBaseView.layer.masksToBounds = NO;
    containerBaseView.layer.shadowColor = [UIColor blackColor].CGColor;
    containerBaseView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    containerBaseView.layer.shadowOpacity = 0.3f;
    containerBaseView.layer.shadowPath = shadowPath1.CGPath;
    
    WeeklyView.layer.cornerRadius = 5;
    UIBezierPath *shadowPath2 = [UIBezierPath bezierPathWithRect:WeeklyView.bounds];
    WeeklyView.layer.masksToBounds = NO;
    WeeklyView.layer.shadowColor = [UIColor blackColor].CGColor;
    WeeklyView.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    WeeklyView.layer.shadowOpacity = 0.3f;
    WeeklyView.layer.shadowPath = shadowPath2.CGPath;
    
    MonthlyView.layer.cornerRadius = 5;
    UIBezierPath *shadowPath3 = [UIBezierPath bezierPathWithRect:MonthlyView.bounds];
    MonthlyView.layer.masksToBounds = NO;
    MonthlyView.layer.shadowColor = [UIColor blackColor].CGColor;
    MonthlyView.layer.shadowOffset = CGSizeMake(1.0f, 2.0f);
    MonthlyView.layer.shadowOpacity = 0.3f;
    MonthlyView.layer.shadowPath = shadowPath3.CGPath;
    
    profileImageView.layer.cornerRadius =30;
    profileImageView.layer.masksToBounds =YES;
    profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    profileImageView.layer.borderWidth = 2;
    profileImageView.clipsToBounds = YES;
    
    datePicker=[[UIDatePicker alloc]init];
    datePicker.datePickerMode=UIDatePickerModeDate;
    datePicker.minimumDate = [NSDate date];
    [dateTewxtField setInputView:datePicker];
   
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    [dateTewxtField setInputAccessoryView:toolBar];
    [InsosatLocationButton setImage:[UIImage imageNamed:@"RadioOn"] forState:UIControlStateNormal];
    Dic = [ApplicationState getUserLoginData];
    
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
        userNameLabel.text = [NSString stringWithFormat:@"Hi, %@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]];
    }
    else{
        userNameLabel.text = @" ";
    }
    NSString *URL = [NSString stringWithFormat:@"%@images/%@",BASE_URL,[[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]];
    URL = [URL stringByReplacingOccurrencesOfString:@"iworkapi" withString:@"iwork"];
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]))){
        NSURL *Url = [NSURL URLWithString:URL];
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL:Url]];
        if(images == nil){
            profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            [profileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        profileImageView.image = [UIImage imageNamed:@"profile_image_default"];
    }
    [self MessageContentView];
    [self CancelContentView];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(QuotaLimitWebApi) withObject:nil afterDelay:0.5];
}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
    [DoneBtn setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [DoneBtn setBackgroundColor:[UIColor colorWithRed:0.85 green:0.69 blue:0.14 alpha:1.0]];
    DoneBtn.layer.cornerRadius = 5;
    RequestImageView.layer.cornerRadius = 25;
    RequestImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    RequestImageView.layer.borderWidth = 1.0;
}
-(void)CancelContentView{
    CancelMsgInnerView.layer.cornerRadius = 6;
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: NoBtn.bounds byRoundingCorners:UIRectCornerBottomLeft cornerRadii: (CGSize){6.0}].CGPath;
    NoBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: YesBtn.bounds byRoundingCorners:UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
   YesBtn.layer.mask = maskLayer2;
}
-(IBAction)CacnelNoAction:(id)sender{
    CancelMsgOuterView.hidden =YES;
}
-(IBAction)CacnelYESAction:(id)sender{
    CancelMsgOuterView.hidden =YES;
    [[self navigationController] popViewControllerAnimated:YES];
}
- (void)QuotaLimitWebApi {
    
    if(delegate.isInternetConnected){
        ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"quotaLimitData?userId=%@",[ApplicationState userId]]];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(IsSafeStringPlus(TrToString(ResponseDic[@"object"]))) {
                    
                    NSNumber *available = [ResponseDic[@"object"] valueForKey:@"quota"];
                    MontlyQuotaLabel.text = [NSString stringWithFormat:@"%@",available];
                    
                    NSNumber *consumed = [ResponseDic[@"object"] valueForKey:@"consumed"];
                    MontlyQuotaUsedLabel.text = [NSString stringWithFormat:@"%@",consumed];;
                    
                    NSNumber *quota_weekly = [ResponseDic[@"object"] valueForKey:@"quota_weekly"];
                    WeeklyQuotaLabel.text = [NSString stringWithFormat:@"%@",quota_weekly];;
                    
                    NSNumber *available_weekly = [ResponseDic[@"object"] valueForKey:@"used_quota_week"];
                    WeeklyQuotaUsedLabel.text = [NSString stringWithFormat:@"%@",available_weekly];;
                }
                else {
                    [self EmptyData];
                }
            });
        }
        else{
            [LoadingManager hideLoadingView:self.view];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)EmptyData{
    [LoadingManager hideLoadingView:self.view];
    MontlyQuotaLabel.text = @"";
    MontlyQuotaUsedLabel.text = @"";
    WeeklyQuotaUsedLabel.text = @"";
    WeeklyQuotaUsedLabel.text = @"";
}
-(void)ShowSelectedDate{
    [dateTewxtField resignFirstResponder];
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    dateTewxtField.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    [dateTewxtField resignFirstResponder];
    datehasEnterd=YES;
    locationLabel.text = @"";
 }
- (IBAction)dateButtonDidClicked:(id)sender {
    [dateTewxtField becomeFirstResponder];
}
- (IBAction)CancelButtonAction{
     CancelMsgOuterView.hidden = NO;
}
- (IBAction)BackButtonAction{
    [[self navigationController] popViewControllerAnimated:YES];
}
- (IBAction)locationButtonDidClicked:(id)sender {
    if([dateTewxtField.text isEqualToString:@""]){
        NSString *iWorkDate = NSLocalizedString(@"SELECT_DATE_VALIDATION", nil);
        iWorkDate = [iWorkDate stringByReplacingOccurrencesOfString:@"iWork" withString:[@"&#9432;Work" stringByConvertingHTMLToPlainText]];
        [self ShowAlert:iWorkDate ButtonTitle:NSLocalizedString(@"OKAY", nil)];
    }
    else{
        [self OpenIndosatLocation];
    }
}
-(IBAction)DoneBtnAction:(id)sender{
    MsgOuterView.hidden = YES;
    [[self navigationController] popViewControllerAnimated:YES];
}
#pragma mark - LocationDataDelegate
- (void)locationDataSelected:(LocationModel *)locData {
    locationLabel.text = locData.officeAddress;
    workLocationId = locData.workLocationId;
     [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)SearchData:(NSString*)Str{
    locationLabel.text = Str;
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Create Request
- (void)performWorkRequest {
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString =  [formatter stringFromDate:datePicker.date];
    NSString *NameStr;
    NSDictionary *userInfo = [ApplicationState getUserLoginData];
    if(IsSafeStringPlus(TrToString([[[userInfo valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
        NameStr = [[[userInfo valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"];
    }
    NSDictionary *params;
    if(delegate.isInternetConnected){
        if(isOpenLaction == 1){
            params = @ {
                @"user_id" : [ApplicationState userId],
                @"name" : NameStr,
                @"loc_type" : @"0",
                @"location_id": @(workLocationId),
                @"location": locationLabel.text,
                @"new_loc":@"",
                @"request_date" : dateString,
                @"task" : TaskTextField.text
            };
        }
        else if(isOpenLaction == 2){
            params = @ {
                @"user_id" : [ApplicationState userId],
                @"name" : NameStr,
                @"loc_type" : @"1",
                @"location_id":@"",
                @"location": @"",
                @"new_loc":locationLabel.text,
                @"request_date" : dateString,
                @"task" : TaskTextField.text
            };
        }
        ResponseDic = [Api WebApi:params Url:@"createRequest"];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==100){
            [LoadingManager hideLoadingView:self.view];
            TitleString = NSLocalizedString(@"REQUEST_SENT", nil);
            [self ShowMessageCustomView:@"SUCCESS"];
            MsgOuterView.hidden = NO;
        }
        else if([ResponseDic[@"statusCode"]integerValue]==101){
            [LoadingManager hideLoadingView:self.view];
            TitleString = NSLocalizedString(@"DUPLICATE_REQUEST", nil);
            [self ShowMessageCustomView:@"DUPLICATE"];
            MsgOuterView.hidden = NO;
        }
        else if([ResponseDic[@"statusCode"]integerValue]==102){
            [LoadingManager hideLoadingView:self.view];
            TitleString = NSLocalizedString(@"IWORK_LIMIT_CONSUMED", nil);
            [self ShowMessageCustomView:@"LIMIT"];
            MsgOuterView.hidden = NO;
        }
        else if([ResponseDic[@"statusCode"]integerValue]==103){
            [LoadingManager hideLoadingView:self.view];
            TitleString = NSLocalizedString(@"CUT_OFF_TIME_OVER", nil);
            [self ShowMessageCustomView:@"CUT_OFF_TIME"];
            MsgOuterView.hidden = NO;
        }
        else if([ResponseDic[@"statusCode"]integerValue]==104){
            [LoadingManager hideLoadingView:self.view];
            TitleString = NSLocalizedString(@"CUT_OFF_DAYS", nil);
            [self ShowMessageCustomView:@"CUT_OFF_DAYS"];
            MsgOuterView.hidden = NO;
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
         [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)ShowMessageCustomView:(NSString*)Image{
    TitleString = [TitleString stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    MessageString = ResponseDic[@"message"];
    MessageString = [MessageString stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    RequestImageView.image = [UIImage imageNamed:Image];
    TitleLabel.text = TitleString;
    MsgLabel.text = MessageString;
}
-(IBAction)RadioSelect:(UIButton*)sender {
    int TagValue = sender.tag;
    if(TagValue == 1){
        [InsosatLocationButton setImage:[UIImage imageNamed:@"RadioOn"] forState:UIControlStateNormal];
        [OtherLocationButton setImage:[UIImage imageNamed:@"RadioOff"] forState:UIControlStateNormal];
        locationLabel.text = @"";
        isOpenLaction = 1;
    }
    else if(TagValue == 2){
        locationLabel.text = @"";
        isOpenLaction = 2;
        [InsosatLocationButton setImage:[UIImage imageNamed:@"RadioOff"] forState:UIControlStateNormal];
        [OtherLocationButton setImage:[UIImage imageNamed:@"RadioOn"] forState:UIControlStateNormal];
    }
}
-(void)OpenIndosatLocation{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LocPickupViewController *smallViewController = [storyboard instantiateViewControllerWithIdentifier:@"LocPickupViewController"];
    smallViewController.delegate=self;
    smallViewController.SelectDateStr = [NSString stringWithFormat:@"%@",dateTewxtField.text];
    if(isOpenLaction ==2){
       smallViewController.isComeFrom = @"OTHER";
    }
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:smallViewController contentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    [self presentViewController:popupViewController animated:NO completion:nil];
}
- (IBAction)createRequestButtonDidCLicked:(id)sender {
    NSString *iWorkDate = NSLocalizedString(@"SELECT_DATE__LOCATION_VALIDATION", nil);
    iWorkDate = [iWorkDate stringByReplacingOccurrencesOfString:@"iWork" withString:[@"&#9432;Work" stringByConvertingHTMLToPlainText]];
    if (!datehasEnterd) {
        [self ShowAlert:iWorkDate ButtonTitle:NSLocalizedString(@"COFIRM", nil)];
    }
    else  if ([locationLabel.text isEqualToString:@""]) {
        [self ShowAlert:iWorkDate ButtonTitle:NSLocalizedString(@"COFIRM", nil)];
    }
    else{
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(performWorkRequest) withObject:nil afterDelay:0.2];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (IBAction)HomeAction:(id)sender {
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[AppContainerViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    ProfileScrollView.contentSize = CGSizeMake(ProfileScrollView.frame.size.width * 2 - 40, ProfileScrollView.frame.size.height);
    
}
@end
