//
//  ApproveRejectCell.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "ApproveRejectCell.h"

@implementation ApproveRejectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.CellBgView.layer.cornerRadius = 5.0;
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.YellowView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii: (CGSize){10.0}].CGPath;
    self.YellowView.layer.mask = maskLayer1;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.CellBgView.bounds];
    self.CellBgView.layer.masksToBounds = NO;
    self.CellBgView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.CellBgView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.CellBgView.layer.shadowOpacity = 0.3f;
    self.CellBgView.layer.shadowPath = shadowPath.CGPath;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
