//
//  EmployeeDetailsViewController.m
//  iWork
//
//  Created by Shailendra on 18/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeDetailsViewController.h"
#import "Header.h"


@interface EmployeeDetailsViewController ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>{
    
    IBOutlet UITableView *EmployeeTableView;
    NSMutableArray *EmployeeDetailsArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    int TagValue;
    NSString *EmployeeID;
    IBOutlet UILabel *iWorkUsedStaticLabel;
    IBOutlet UILabel *iWorkUsedValueLabel;
    
    IBOutlet UILabel *iWorkAvaliableStaticLabel;
    IBOutlet UILabel *iWorkAvaliableValueLabel;
    NSDictionary   *params;

    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *NoDataLabel;
    APIService *Api;
    NSDictionary *ResponseDic;
    int offset;
    int limit;
    
    
}

@end

@implementation EmployeeDetailsViewController
@synthesize Dic;
- (void)viewDidLoad {
    NSLog(@"Dic==%@",Dic);
    [self EmptyArray];
    Api = [[APIService alloc] init];
    CheckValue = @"EMPLOYEE";
    HeaderLabel.text = @"";
    HeaderLabel.text = [Dic valueForKey:@"uName"];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    EmployeeID = [Dic valueForKey:@"userId"];
    self.view.backgroundColor = delegate.BackgroudColor;
    EmployeeTableView.backgroundColor = delegate.BackgroudColor;
    EmployeeTableView.estimatedRowHeight = 50;
    EmployeeTableView.rowHeight = UITableViewAutomaticDimension;
    EmployeeTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, EmployeeTableView.bounds.size.width, 0.01f)];
    
    iWorkUsedStaticLabel.text = NSLocalizedString(@"IWORK_USED", nil);
    iWorkUsedStaticLabel.text = [iWorkUsedStaticLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    iWorkAvaliableStaticLabel.text = NSLocalizedString(@"IWORK_AVALIABLE", nil);
    iWorkAvaliableStaticLabel.text = [iWorkAvaliableStaticLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
      
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [EmployeeTableView addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(EmployeeApiCall) withObject:nil afterDelay:0.5];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EmployeeDetailsApi:) name:@"EmployeeDetails" object:nil];
    [super viewDidLoad];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self EmptyArray];
    [self EmployeeApiCall];
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    EmployeeDetailsArray = [[NSMutableArray alloc] init];
    [EmployeeTableView reloadData];
}
-(void)EmployeeApiCall{
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"EMPLOYEE"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"empId" : [Dic valueForKey:@"userId"],
                    @"status": @10,
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
        }
        ResponseDic = [Api WebApi:params Url:@"viewTeamReport"];
        [refreshControl endRefreshing];
        NoDataLabel.hidden = YES;
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
            if(ResponseArrays.count>0){
                iWorkAvaliableValueLabel.text = [NSString stringWithFormat:@"%@",[[ResponseDic valueForKey:@"object"] valueForKey:@"iwork_available"]];
                iWorkUsedValueLabel.text = [NSString stringWithFormat:@"%@",[[ResponseDic valueForKey:@"object"] valueForKey:@"iwork_used"]];
                NSArray *Request_ListArray = [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
                if(Request_ListArray.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"]];
                }
                else{
                   [self NoDataFound];
                }
                [EmployeeTableView reloadData];
            }
            else if(EmployeeDetailsArray.count == 0){
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
      [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(EmployeeDetailsArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No Employee Details found";
    }
    [EmployeeTableView reloadData];
}
-(void)UpdateWorkLimit:(NSDictionary *)Dics{
    iWorkUsedValueLabel.text = [[Dics valueForKey:@"monthly"] valueForKey:@"monthly_quota_used"];
    iWorkAvaliableValueLabel.text = [[Dics valueForKey:@"monthly"] valueForKey:@"monthly_quota"];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [EmployeeDetailsArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeDetailsCell *Cell = (EmployeeDetailsCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.ViewDetailsButton.tag = indexPath.section;
    [Cell.ViewDetailsButton addTarget:self action:@selector(ViewDetailAction:) forControlEvents:UIControlEventTouchUpInside];
    if(EmployeeDetailsArray.count>0){
        [Cell configureCell:[EmployeeDetailsArray objectAtIndex:indexPath.section]];
    }
    Cell.tag  = indexPath.section;
    UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
    [Cell  setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:RequestIWork];
    return Cell;
}

-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    TagValue = sender.view.tag;
    [self MapPage];
}
-(void)MapPage{
    NSDictionary *Dics = [EmployeeDetailsArray objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.isComeFrom = @"REPORT";
    ObjViewController.MapDic = Dics;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)GotoFilter:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LineManagerFilterViewController *ObjLineManagerFilterViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerFilterViewController"];
    ObjLineManagerFilterViewController.isComeFrom =@"EmployeeDetail";
    ObjLineManagerFilterViewController.EmployeeID =[Dic valueForKey:@"userId"];
    [self presentViewController:ObjLineManagerFilterViewController animated:YES completion:nil];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 50;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self EmployeeApiCall];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dics = [Array objectAtIndex:i];
        for (NSString *key in [Dics allKeys]){
            [BindDataDic setObject:[Dics objectForKey:key] forKey:key];
        }
        [EmployeeDetailsArray addObject:BindDataDic];
    }
}
-(IBAction)ViewDetailAction:(UIButton*)sender {
    TagValue = sender.tag;
    [self MapPage];
}
-(void)EmployeeDetailsApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(EmployeeApiCall) withObject:nil afterDelay:0.5];
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)HomeAction:(id)sender{
    NSArray *array = [self.navigationController viewControllers];
    for (int i = 0 ; i < array.count; i++) {
        UIViewController *ViewController = [array objectAtIndex:i];
        if([ViewController isKindOfClass:[AppContainerViewController class]]){
            [self.navigationController popToViewController:ViewController animated:YES];
        }
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}


@end
