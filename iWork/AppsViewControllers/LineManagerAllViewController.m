//
//  LineManagerAllViewController.m
//  iWork
//
//  Created by Shailendra on 17/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManagerAllViewController.h"
#import "AllCell.h"
#import "Header.h"

@interface LineManagerAllViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *ALLTableView;
    NSMutableArray *AllArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    NSString *cortex;
    NSString *user_requests_id;
    IBOutlet UILabel *NoDataLabel;
    
    IBOutlet UIView *RejectionOuterView;
    IBOutlet UIView *RejectionInnerView;
    IBOutlet UITextView *RejectionTextView;
    IBOutlet UIButton *RejectionYesBtn;
    IBOutlet UIButton *RejectionNoBtn;
    IBOutlet UIImageView *RequireImageView;
    
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UIButton *YesBtn;
    IBOutlet UIButton *NoBtn;
    APIService *Api;
    NSDictionary *ResponseDic;
    NSString *requestsId;
    int offset;
    int limit;
    int TagValue;
}


@end

@implementation LineManagerAllViewController

- (void)viewDidLoad {
    CheckValue = @"ALL";
    offset = 0;
    limit = 10;
    TagValue = 0;
    Api = [[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    ALLTableView.backgroundColor = delegate.BackgroudColor;
    AllArray = [[NSMutableArray alloc] init];
    ALLTableView.estimatedRowHeight = 50;
    ALLTableView.rowHeight = UITableViewAutomaticDimension;
     ALLTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, ALLTableView.bounds.size.width, 10.00f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [ALLTableView addSubview:refreshControl];
    
    UINib *approvalNib = [UINib nibWithNibName:@"ApprovalsTableViewCell" bundle:nil];
    UINib *reportNib = [UINib nibWithNibName:@"ReportsTableViewCell" bundle:nil];
    [ALLTableView registerNib:approvalNib forCellReuseIdentifier:@"ApprovalsTableViewCell"];
    [ALLTableView registerNib:reportNib forCellReuseIdentifier:@"ReportsTableViewCell"];
    
    [self GetiWorkLineManagerAll];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AllApi:) name:@"All" object:nil];
    [self MessageContentView];
    [self RejectionContentView];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self EmptyArray];
    [self GetiWorkLineManagerAll];
}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
    [YesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [YesBtn setBackgroundColor:delegate.redColor];
    [NoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
    [NoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: YesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    YesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: NoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    NoBtn.layer.mask = maskLayer2;
}
-(void)RejectionContentView{
    RejectionInnerView.layer.cornerRadius = 5;
    RejectionInnerView.clipsToBounds =YES;
    [RejectionYesBtn setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    [RejectionYesBtn setBackgroundColor:delegate.redColor];
    [RejectionNoBtn setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
    [RejectionNoBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: RejectionYesBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){5.0}].CGPath;
    RejectionYesBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: RejectionNoBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    RejectionNoBtn.layer.mask = maskLayer2;
    RejectionTextView.layer.cornerRadius = 5.0;
    RejectionTextView.layer.borderWidth = 1.0;
    RejectionTextView.layer.borderColor = delegate.borderColor.CGColor;
    RejectionTextView.textColor = delegate.borderColor;
}
-(void)EmptyArray{
    offset = 0;
    limit = 10;
    AllArray = [[NSMutableArray alloc]init];
}
#pragma mark  -UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [AllArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *Cell;
    if(AllArray.count>0){
       NSDictionary *Dic = AllArray[indexPath.section];
        if ([Dic[@"status"]  isEqualToString:@"Approved"]){
            ApprovalsTableViewCell * projectCell = (ApprovalsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ApprovalsTableViewCell" forIndexPath:indexPath];
            projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            NSDictionary *responseData = AllArray [indexPath.section];
            [projectCell configureApprovalCell:responseData];
            projectCell.approveButton.tag = indexPath.section;
            projectCell.rejectButton.tag = indexPath.section;
            [projectCell.approveButton addTarget:self action:@selector(approveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [projectCell.rejectButton addTarget:self action:@selector(rejectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            return projectCell;
        }
        else{
            ReportsTableViewCell * projectCell = (ReportsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ReportsTableViewCell" forIndexPath:indexPath];
            projectCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [projectCell.ViewDetailsBtn addTarget:self action:@selector(GotoMapView:) forControlEvents:UIControlEventTouchUpInside];
            projectCell.ViewDetailsBtn.tag = indexPath.section;
            
            projectCell.tag  = indexPath.section;
            UITapGestureRecognizer* RequestIWork = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToMapPage:)];
            [projectCell  setUserInteractionEnabled:YES];
            [projectCell addGestureRecognizer:RequestIWork];
            if(AllArray.count>0){
                [projectCell configureReportCell:AllArray[indexPath.section]];
            }
            return projectCell;
        }
    }
    return Cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 8;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
    if(ArraysValue.count>0){
        offset = offset + 50;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self GetiWorkLineManagerAll];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [AllArray addObject:BindDataDic];
    }
}
-(void)MapPage{
    NSDictionary *Dic = [AllArray objectAtIndex:TagValue];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailsViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailsViewController"];
    ObjViewController.MapDic = Dic;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)GotoMapView:(UIButton*)Sender{
    TagValue = Sender.tag;
    [self MapPage];
}
-(void)GoToMapPage:(UIGestureRecognizer*)sender{
    TagValue = sender.view.tag;
    [self MapPage];
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView{
    RejectionTextView.text = @"";
    RejectionTextView.textColor = [UIColor blackColor];
    RejectionTextView.layer.borderColor = delegate.borderColor.CGColor;
    RequireImageView.hidden =YES;
    return YES;
}
-(void) textViewDidChange:(UITextView *)textView{
    if(RejectionTextView.text.length == 0){
        RejectionTextView.textColor = [UIColor lightGrayColor];
        RejectionTextView.text = NSLocalizedString(@"STATE_THE_REASON", nil);
        [RejectionTextView resignFirstResponder];
    }
}
#pragma mark -
- (void)approveButtonClicked:(UIButton *)sender {
    MsgLabel.text = NSLocalizedString(@"APPROVE_MSG", nil);
    TagValue = sender.tag;
    CheckValue = @"APPROVE";
    MsgOuterView.hidden =NO;
}
- (void)rejectButtonClicked:(UIButton *)sender {
    TagValue = sender.tag;
    CheckValue = @"REJECT";
    RejectionOuterView.hidden =NO;
}
- (void)GetiWorkLineManagerAll{
    if(delegate.isInternetConnected){
         if([CheckValue isEqualToString:@"ALL"]||[CheckValue isEqualToString:@"Refersh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"status": @10,
                    @"userId": [ApplicationState userId],
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"viewTeamReport"];
            [refreshControl endRefreshing];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"];
                if(ResponseArrays.count>0){
                     NoDataLabel.hidden = YES;
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"request_list"]];
                    [ALLTableView  reloadData];
                }
                else if(AllArray.count == 0){
                   [self NoIworkRequest];
                }
            }
            else{
                if(AllArray.count == 0){
                    [self NoIworkRequest];
                }
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoIworkRequest{
    NoDataLabel.hidden = NO;
    NoDataLabel.text = @"No iWork request found";
    NoDataLabel.text = [NoDataLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
}
-(IBAction)YESBtnAction:(id)sender{
    NSDictionary * responseData = AllArray [TagValue];
    requestsId = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"requestId"]];
    NSLog(@"requestsId==%@",requestsId);
    [self ApproveRejectWebApi];
}
-(void)ApproveRejectWebApi{
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"REJECT"]){
            NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
            NSString *RemarkStr = [RejectionTextView.text stringByAddingPercentEncodingWithAllowedCharacters:set];
            NSString *UrlStr = [NSString stringWithFormat:@"updateStatusOfRequest?status=2&requestId=%@&rejectionRemark=%@",requestsId,RemarkStr];
            NSLog(@"UrlStr==%@",UrlStr);
            ResponseDic = [Api WebApi:nil Url:UrlStr];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                MsgOuterView.hidden =YES;
                RejectionOuterView.hidden =YES;
                [self EmptyArray];
                [self GetiWorkLineManagerAll];
                
            }
            else{
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
        else if([CheckValue isEqualToString:@"APPROVE"]){
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"updateStatusOfRequest?status=1&requestId=%@",requestsId]];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                MsgOuterView.hidden =YES;
                [self EmptyArray];
                [self GetiWorkLineManagerAll];
            }
            else{
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
-(IBAction)NoBtnAction:(id)sender{
    MsgOuterView.hidden =YES;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)RejectionYesAction:(id)sender{
    [RejectionTextView resignFirstResponder];
    NSDictionary * responseData = AllArray [TagValue];
    requestsId = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"requestId"]];
    if([RejectionTextView.text isEqualToString:NSLocalizedString(@"STATE_THE_REASON", nil)]){
        RejectionTextView.layer.borderColor = delegate.redColor.CGColor;
        RequireImageView.hidden =NO;
    }
    else if([RejectionTextView.text isEqualToString:@""]){
        RejectionTextView.layer.borderColor = delegate.redColor.CGColor;
        RequireImageView.hidden =NO;
    }
    else{
        [self ApproveRejectWebApi];
    }
}
-(IBAction)RejectionNoCrossAction:(id)sender{
    RejectionTextView.text =NSLocalizedString(@"STATE_THE_REASON", nil);
    RejectionTextView.textColor = delegate.borderColor;
    [RejectionTextView resignFirstResponder];
    CheckValue = @"";
    RejectionOuterView.hidden = YES;
    RejectionTextView.layer.borderColor = delegate.borderColor.CGColor;
    RequireImageView.hidden =YES;
}
-(void)LineManageFilterApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [self GetiWorkLineManagerAll];
}
- (IBAction)FilterAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LineManagerFilterViewController *ObjLineManagerFilterViewController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerFilterViewController"];
    [self presentViewController:ObjLineManagerFilterViewController animated:YES completion:nil];
}
-(void)AllApi:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"Notification";
    [self GetiWorkLineManagerAll];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
