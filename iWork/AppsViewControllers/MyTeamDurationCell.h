//
//  MyTeamDurationCell.h
//  iWork
//
//  Created by Shailendra on 21/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTeamDurationCell : UITableViewCell


@property(nonatomic,strong)IBOutlet UIImageView *CurrentWeekRadioImage;
@property(nonatomic,strong)IBOutlet UIImageView *CurrentMonthRadioImage;
@property(nonatomic,strong)IBOutlet UIImageView *CurrentYearRadioImage;
@property(nonatomic,strong)IBOutlet UIImageView *SpecifyDateRadioImage;
@property(nonatomic,strong)IBOutlet UILabel *CurrentWeekLabel;
@property(nonatomic,strong)IBOutlet UILabel *CurrentMonthLabel;
@property(nonatomic,strong)IBOutlet UILabel *CurrentYearLabel;
@property(nonatomic,strong)IBOutlet UILabel *SpecifyDateYearLabel;
@property(nonatomic,strong)IBOutlet UIButton *CurrentWeekBtn;
@property(nonatomic,strong)IBOutlet UIButton *CurrentMonthBtn;
@property(nonatomic,strong)IBOutlet UIButton *CurrentYearBtn;
@property(nonatomic,strong)IBOutlet UIButton *SpecifyDateYearBtn;
@property(nonatomic,strong)IBOutlet UITextField *StartDateTextField;
@property(nonatomic,strong)IBOutlet UITextField *EndDateTextField;
@property(nonatomic,strong)IBOutlet UIView *DateView;



@end
