//
//  APIService.h
//  iWork
//
//  Created by Shailendra on 20/07/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"
@interface APIService : NSObject{
    AppDelegate *delegate;
}
-(NSDictionary*)WebApi: (NSDictionary*)_params Url:(NSString*)Url;
@end
