//
//  LocPickupViewController.m
//  iWork
//
//  Created by Fourbrick on 04/02/17.
//  Copyright © 2017 Fourbrick. All rights reserved.
//

#import "LocPickupViewController.h"
#import "Header.h"

@interface LocPickupViewController ()<UITextFieldDelegate>{
    
    APIService *Api;
    AppDelegate *delegate;
    NSString *Str;
    NSString *SearchDataStr;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSMutableArray *filteredTableData;
    NSMutableArray *locationsArray;
    bool isFiltered;
    IBOutlet UITableView *LocationtableView;
    IBOutlet UISearchBar *CustomsearchBar;
    IBOutlet UIButton *confirmButton;
    IBOutlet UITextField *SearchTextField;
    IBOutlet UILabel *LocationStaticLabel;
    IBOutlet UILabel *AvailabilityStaticLabel;
}
@end

@implementation LocPickupViewController
@synthesize SelectDateStr,isComeFrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    Api = [[APIService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    locationsArray = [[NSMutableArray alloc]init];
    filteredTableData = [[NSMutableArray alloc]init];
    LocationtableView.estimatedRowHeight = 50;;
    LocationtableView.tableFooterView = [[UIView alloc]init];
    LocationtableView.rowHeight = UITableViewAutomaticDimension;
    LocationtableView.estimatedRowHeight = 40.0;
    [SearchTextField setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
}
-(void)viewDidLayoutSubviews{
   
    if([isComeFrom isEqualToString:@"OTHER"]){
        AvailabilityStaticLabel.hidden = YES;
        LocationStaticLabel.hidden = YES;
        SearchTextField.placeholder = @"Enter Location";
        LocationtableView.frame = CGRectMake(LocationtableView.frame.origin.x, LocationtableView.frame.origin.y-10, LocationtableView.frame.size.width, LocationtableView.frame.size.height+10);
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetLocation) withObject:nil afterDelay:0];
}
#pragma mark  -UItableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(locationsArray.count>0 || filteredTableData.count>0){
        return  isFiltered ?[filteredTableData count] : [locationsArray count];
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LocationCell * projectCell = (LocationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    LocationModel* item = isFiltered ?filteredTableData[indexPath.row] : locationsArray[indexPath.row];
    if (item.isSelected) {
        projectCell.CheckUnCheckImageView.image = [UIImage imageNamed:@"check"];
    } else {
        projectCell.CheckUnCheckImageView.image = [UIImage imageNamed:@"un_check"];
    }
    projectCell.LocationNameLabel.text = item.officeAddress;
    if([isComeFrom isEqualToString:@"OTHER"]){
        projectCell.AvibilityLabel.text = @"";
        projectCell.AvibilityLabel.hidden = YES;
    }
    else{
        projectCell.AvibilityLabel.text = [NSString stringWithFormat:@"%ld",(long)item.AbilableSlot];
        if(item.AbilableSlot == 0){
            projectCell.AvibilityLabel.textColor = [UIColor darkGrayColor];
            projectCell.LocationNameLabel.textColor = [UIColor darkGrayColor];
        }
        else{
            projectCell.AvibilityLabel.textColor = [UIColor blackColor];
            projectCell.LocationNameLabel.textColor = [UIColor blackColor];
        }
        projectCell.AvibilityLabel.hidden = NO;
    }
    return projectCell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    for ( LocationModel *items in locationsArray) {
        items.isSelected = FALSE;
    }
    LocationModel* item = isFiltered ?filteredTableData[indexPath.row] :locationsArray[indexPath.row];
    NSUInteger AbilableSlotIntValue = item.AbilableSlot;
    if(AbilableSlotIntValue != 0){
         item.isSelected = YES;
    }
    if([isComeFrom isEqualToString:@"OTHER"]){
        item.isSelected = YES;
    }
    [tableView reloadData];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    SearchDataStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(SearchDataStr.length == 0) {
        isFiltered = FALSE;
    }
    else {
        isFiltered  = true;
        [filteredTableData removeAllObjects];
        for ( LocationModel *progects in locationsArray) {
            NSRange nameRange = [progects.officeAddress rangeOfString:SearchDataStr options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound ) {
                [filteredTableData addObject:progects];
            }
        }
    }
    [LocationtableView reloadData];
    return YES;
}
- (IBAction)confirmButtonDidClicked:(id)sender {
    if([isComeFrom isEqualToString:@"OTHER"]){
        LocationModel* item;
        if(item.isSelected==YES){
            [self SelectedItem];
        }
        else{
          [self.delegate SearchData:SearchDataStr];
        }
//        if(filteredTableData.count==0){
//            [self.delegate SearchData:SearchDataStr];
//        }
//        else{
//            [self SelectedItem];
//        }
    }
    else{
         [self SelectedItem];
    }
}
-(void)SelectedItem{
    for ( LocationModel *items in locationsArray) {
        if (items.isSelected) {
            [self.delegate locationDataSelected:items];
        }
    }
}
- (void)GetLocation {
    if(delegate.isInternetConnected){
        [LoadingManager showLoadingView:self.view];
        if([isComeFrom isEqualToString:@"OTHER"]){
            params = @ {
                @"userId" : [ApplicationState userId],
                @"date" : SelectDateStr,
                @"locType" : @"1",
            };
        }
        else{
            params = @ {
                @"userId" : [ApplicationState userId],
                @"date" : SelectDateStr,
                @"locType" : @"0",
            };
        }
        ResponseDic = [Api WebApi:params Url:@"availableLocationsList"];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if([ResponseDic[@"object"] isKindOfClass:[NSArray class]]){
                NSArray * locations = ResponseDic[@"object"];
                if(locations.count>0){
                    for (NSDictionary * dict in locations) {
                        LocationModel* loc = [[LocationModel alloc]init];
                        NSString *Name = @"";
                        int TotalStot;
                        int UsedStot;
                        loc.imageName = @"un_check";
                        loc.isSelected = FALSE;
                        loc.workLocationId = [dict[@"locationId"] integerValue];
                        if(IsSafeStringPlus(TrToString(dict[@"locationName"]))){
                            Name = [dict valueForKey:@"locationName"];
                        }
                        else{
                            Name = @"";
                        }
                        loc.officeAddress = Name;
                        if(IsSafeStringPlus(TrToString(dict[@"totalSlot"]))){
                            TotalStot = [dict[@"totalSlot"] intValue];
                        }
                        else{
                            TotalStot = 0;
                        }
                        if(IsSafeStringPlus(TrToString(dict[@"usedSlot"]))){
                            UsedStot = [dict[@"usedSlot"] intValue];
                        }
                        else{
                            UsedStot = 0;
                        }
                        int AviableSlot = TotalStot - UsedStot;
                        loc.AbilableSlot = AviableSlot;
                        [locationsArray addObject:loc];
                    }
                    [LocationtableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
                }
            }
        }
        else{
            [LoadingManager hideLoadingView:self.view];
           [self ShowAlert:NSLocalizedString(@"NO_LOCATION_FROM_SERVER", nil) ButtonTitle:NSLocalizedString(@"OK", nil)];
        }
    }
    else{
       [LoadingManager hideLoadingView:self.view];
      [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (IBAction)concelButtonDidClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
