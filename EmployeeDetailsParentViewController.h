//
//  EmployeeDetailsParentViewController.h
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeeDetailsParentViewController : UIViewController

@property(nonatomic,strong)NSDictionary *Dics;
@end
