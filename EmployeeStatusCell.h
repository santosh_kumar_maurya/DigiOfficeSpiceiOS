//
//  EmployeeStatusCell.h
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "RateView.h"
@interface EmployeeStatusCell : UITableViewCell
{
    AppDelegate *delegate;
    
}
@property (weak, nonatomic) IBOutlet UIView *TaskView;
@property (weak, nonatomic) IBOutlet UILabel *TaskIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *DateLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *CreatedByStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *CreatedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *DurationStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *DurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *KPIStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *KPILabel;
@property (weak, nonatomic) IBOutlet UIView *DetailsView;
@property (weak, nonatomic) IBOutlet UILabel *StatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *StausImageView;
@property (weak, nonatomic) IBOutlet UIButton *DetailsBtn;
@property (weak, nonatomic) IBOutlet RateView *RatingView;


- (void)configureCell:(NSDictionary *)info;

@end
