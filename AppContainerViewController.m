//
//  AppContainerViewController.m
//  iWork
//
//  Created by Shailendra on 14/04/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AppContainerViewController.h"
#import "Header.h"

@interface AppContainerViewController ()<NSURLSessionDelegate>{
    
    NSString *CheckValue;
    UIWindow *window;
    NSNumber *noti_count;
    NSNumber *PMSnoti_count;
    UINavigationController *nav;
    NSString *isClickPMS;
    UIView *snapShot;
    APIService *Api;
    WebApiService *API;
    NSDictionary *ResponseDic;
    NSDictionary *params;
    NSDictionary *AppLunchDic;
    
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *EmployeeAppLabel;
    IBOutlet UILabel *iWorkLabel;
    IBOutlet UILabel *IPMLabel;
    IBOutlet UILabel *LogoutTitleLable;
    IBOutlet UILabel *LineLable;
    IBOutlet UILabel *iConnectLable;
    IBOutlet UILabel *iConnectNotificationLable;
    IBOutlet UILabel *iWorkNotificationLabel;
    IBOutlet UILabel *PMSNotificationLabel;
    IBOutlet UIButton *NoButton;
    IBOutlet UIButton *YesButton;
    IBOutlet UIView *ContainerView;
    IBOutlet UIView *LogoutView;
    IBOutlet UIImageView *ContainImageView;
    
    IBOutlet UIView *MsgOuterView;
    IBOutlet UIView *MsgInnerView;
    IBOutlet UILabel *MsgLabel;
    IBOutlet UILabel *TitleLabel;
    IBOutlet UIButton *DoneBtn;
    
    IBOutlet UIView *OuterView;
    IBOutlet UIButton *UpdateBtn;
    IBOutlet UIButton *CancelExitBtn;
    IBOutlet UILabel *MsgsLabel;
    NSString *refreshedToken;
    
}
@end

@implementation AppContainerViewController
@synthesize isComefrom;

- (void)viewDidLoad {
    Api = [[APIService alloc] init];
    API = [[WebApiService alloc] init];
    isClickPMS = @"";
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    HeaderLabel.text = NSLocalizedString(@"DIGI_OFFICE", nil);
    EmployeeAppLabel.text = NSLocalizedString(@"EMPLOYEE_APP", nil);
    LogoutTitleLable.text = NSLocalizedString(@"LOGOUT_CONFIRM", nil);
    [NoButton setTitle:NSLocalizedString(@"NO", nil) forState:UIControlStateNormal];
    [YesButton setTitle:NSLocalizedString(@"YES", nil) forState:UIControlStateNormal];
    
    iWorkLabel.text =  NSLocalizedString(@"IWORK", nil);
    iWorkLabel.text = [iWorkLabel.text stringByReplacingOccurrencesOfString:@"iWork" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Work" stringByConvertingHTMLToPlainText]]];
    
    IPMLabel.text =  NSLocalizedString(@"IPM", nil);
    IPMLabel.text = [IPMLabel.text stringByReplacingOccurrencesOfString:@"iPM" withString:[NSString stringWithFormat:@"%@",[@"&#9432;PM" stringByConvertingHTMLToPlainText]]];
    
    iConnectLable.text =  NSLocalizedString(@"ICONNECT", nil);
    iConnectLable.text = [iConnectLable.text stringByReplacingOccurrencesOfString:@"iConnect" withString:[NSString stringWithFormat:@"%@",[@"&#9432;Connect" stringByConvertingHTMLToPlainText]]];
    
    LineLable.layer.cornerRadius = 4.0;
    iWorkNotificationLabel.layer.cornerRadius = 11.0;
    PMSNotificationLabel.layer.cornerRadius = 11.0;
    iConnectNotificationLable.layer.cornerRadius = 11.0;
    
    PMSNotificationLabel.layer.masksToBounds = YES;
    iWorkNotificationLabel.layer.masksToBounds = YES;
    PMSNotificationLabel.layer.masksToBounds = YES;
    LineLable.layer.masksToBounds = YES;
    PMSNotificationLabel.textColor = [UIColor whiteColor];
    iWorkNotificationLabel.textColor = [UIColor whiteColor];
    PMSNotificationLabel.textColor = [UIColor whiteColor];
    
    refreshedToken =  [ApplicationState currentPushToken];
    if (refreshedToken==nil) {
        refreshedToken =  @"";
    }
    
    self.navigationController.navigationBarHidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNotification:) name:@"Notification" object:nil];
    [self MessageContentView];
    [LoadingManager showLoadingView:self.view];
    [self LaunchWebApi];
    [super viewDidLoad];
}
-(void)MessageContentView{
    MsgInnerView.layer.cornerRadius = 5;
    [DoneBtn setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [DoneBtn setBackgroundColor:[UIColor colorWithRed:0.85 green:0.69 blue:0.14 alpha:1.0]];
    DoneBtn.layer.cornerRadius = 5;
}
-(void)viewWillAppear:(BOOL)animated{
    [self performSelector:@selector(UpdateNotification:) withObject:nil afterDelay:1.5];
}
-(void)UpdateNotification:(NSNotification*)notification{
    NSString *AppType = [notification.userInfo valueForKey:@"IWORK"];
    if([AppType isEqualToString:@"IWORK"]){
        iWorkNotificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
        if([iWorkNotificationLabel.text intValue]==0){
            iWorkNotificationLabel.hidden = YES;
        }
        else{
            iWorkNotificationLabel.hidden = NO;
        }
    }
    else {
        PMSNotificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
        if([PMSNotificationLabel.text integerValue]== 0){
            PMSNotificationLabel.hidden = YES;
        }
        else{
            PMSNotificationLabel.hidden =  NO;
        }
    }
}
- (IBAction)iWorkAction {
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(iWorkApi) withObject:nil afterDelay:1.0];
}
-(void)iWorkApi{
    
    if(delegate.isInternetConnected){
        NSString *UrlStr = [NSString stringWithFormat:@"%@ssoAuth?userId=%@",BASE_URL,[ApplicationState userId]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:UrlStr]];
        [request setHTTPMethod:@"POST"];
        [request addValue:[ApplicationState GetToken] forHTTPHeaderField: @"Authorization"];
//        [request addValue:@"ios" forHTTPHeaderField: @"platform"];
//        [request addValue:[delegate GetVersionNumber] forHTTPHeaderField: @"appversion"];
//        [request addValue:refreshedToken forHTTPHeaderField: @"fcm_id"];
        
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSData *responseData1=[responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *jsonParsingError = nil;
        NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:responseData1
                                                                    options:0 error:&jsonParsingError];
        NSLog(@"responseObject---%@",responseObject);
        [LoadingManager hideLoadingView:self.view];
        if ([responseObject[@"statusCode"]intValue]==5) {

            iWorkNotificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IWORK"]];
            if([iWorkNotificationLabel.text intValue]==0){
                iWorkNotificationLabel.hidden = YES;
            }
            else{
                iWorkNotificationLabel.hidden = NO;
            }
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            NSString *user_type = [[responseObject valueForKey:@"object"] valueForKey:@"userType"];
            if([user_type integerValue]== 3){
                EmployeeDashBoardViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"EmployeeDashBoardViewController"];
                [[self navigationController] pushViewController:ObjViewController animated:YES];
            }
            else if([user_type integerValue]== 1){
                ParentDashbordViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ParentDashbordViewController"];
                [[self navigationController] pushViewController:ObjViewController animated:YES];
            }
            
        }else {
            [self ShowAlert:responseObject[@"msg"] ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}

- (IBAction)PmsAction {
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(PMS_API) withObject:nil afterDelay:2];
}
-(void)PMS_API{
    if(delegate.isInternetConnected){
        params = @ {
            @"userId": [ApplicationState userId],
            @"fcm_id": refreshedToken,
        };
        ResponseDic = [API WebApi:params Url:@"ssoAuth"];
         [LoadingManager hideLoadingView:self.view];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            NSString *HandSetID =[[ResponseDic valueForKey:@"object"] valueForKey:@"handset_id"];
             [ApplicationState setHandsetID:HandSetID];
            
            if([[ResponseDic[@"object"] valueForKey:@"userType"]integerValue]==2){
                MsgLabel.text = NSLocalizedString(@"ADMIN_NOT_ALLOW", nil);
                MsgOuterView.hidden = YES;
            }
            else{
                PMSNotificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
                if([PMSNotificationLabel.text integerValue]== 0){
                    PMSNotificationLabel.hidden = YES;
                }
                else{
                    PMSNotificationLabel.hidden =  NO;
                }
                if([[[ResponseDic valueForKey:@"object"] valueForKey:@"userType"]integerValue] == 3){
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
                    EmplpoyeeViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"EmplpoyeeViewController"];
                    [[self navigationController] pushViewController:ObjController animated:YES];
                }
                else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
                    LineManagerParantViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"LineManagerParantViewController"];
                    [[self navigationController] pushViewController:ObjController animated:YES];
                }
            }
        }
        else{
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
    else{
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)saveData{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:delegate.dataFull];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"Key"];
    NSString* path = [NSString stringWithFormat:@"%@%@",
                      [[NSBundle mainBundle] resourcePath],
                      @"myfilename.plist"];
    NSMutableArray* myArray = [[NSMutableArray alloc]init];
    [myArray insertObject:@"OK" atIndex:0];
    [myArray insertObject:[[ResponseDic objectForKey:@"object"] valueForKey:@"handset_id"] atIndex:1];
    [myArray writeToFile:path atomically:YES];
}
- (IBAction)IConnectAction{
    
    Class LSApplicationWorkspace_class = objc_getClass("LSApplicationWorkspace");
    SEL selector=NSSelectorFromString(@"defaultWorkspace");
    NSObject* workspace = [LSApplicationWorkspace_class performSelector:selector];
    SEL selectorALL = NSSelectorFromString(@"allApplications");
    //    NSLog(@"apps: %@", [workspace performSelector:selectorALL]);
    NSMutableArray *AllApp = [workspace performSelector:selectorALL];
    NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
    for (id lsApplicationProxy in AllApp) {
        [mutableArray addObject:[lsApplicationProxy performSelector:NSSelectorFromString(@"applicationIdentifier")]];
    }
    if ([[[AppLunchDic valueForKey:@"iconnect"] valueForKey:@"enabled"] boolValue]==YES) {
        NSString *Token = [ApplicationState GetToken];
        NSString *UserKey = [ApplicationState userId];
        if([mutableArray containsObject:[[AppLunchDic valueForKey:@"iconnect"] valueForKey:@"bundleId"]]){
            NSString *appStoreUrl = [NSString stringWithFormat:@"iconnect://?Token=%@&UserKey=%@",Token,UserKey];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreUrl] options:@{} completionHandler:nil];
        }
        else {
            if ([[[AppLunchDic valueForKey:@"iconnect"] valueForKey:@"live"] boolValue]){
                NSString *ios_prodlink = [[AppLunchDic valueForKey:@"iconnect"] valueForKey:@"ios_prodlink"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ios_prodlink] options:@{} completionHandler:nil];
            }
            else{
                NSString *ios_testlink = [[AppLunchDic valueForKey:@"iconnect"] valueForKey:@"ios_testlink"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ios_testlink] options:@{} completionHandler:nil];
            }
        }
    }
    else{
        MsgLabel.text = NSLocalizedString(@"COMING_SOON", nil);
        MsgOuterView.hidden = NO;
    }
}
- (IBAction)LogoutAction {
    LogoutView.hidden = NO;
}
- (IBAction)YesAction {
    [delegate DeleteNotificationCounter:@"IWORK"];
    [delegate DeleteNotificationCounter:@"IPM"];
    [ApplicationState setUserIsLoggedOut];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLoggedIn"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Key"];
    SignInViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}
- (void)LaunchWebApi {
    if([delegate isInternetConnected]){
        AppLunchDic = [Api WebApi:nil Url:@"appLaunch"];
        if([[AppLunchDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            if ([[AppLunchDic[@"object"] valueForKey:@"status"]boolValue]) {
                OuterView.alpha = 0.0;
            }else {
                OuterView.alpha = 1.0;
                [self ChangeBtn:AppLunchDic];
            }
        }
        else{
             [LoadingManager hideLoadingView:self.view];
        }
    }
}
-(void)ChangeBtn:(NSDictionary*)Dic{
    MsgsLabel.text = [Dic valueForKey:@"message"];
    if([[[Dic valueForKey:@"object"] valueForKey:@"forceupdate"] boolValue] == YES){
         [CancelExitBtn setTitle:@"EXIT" forState:UIControlStateNormal];
    }
    else{
         [CancelExitBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
    }
}
-(IBAction)ExitExitApp:(id)sender{
    if([CancelExitBtn.titleLabel.text isEqualToString:@"EXIT"]){
        exit(0);
    }
    else{
        [UIView animateWithDuration:3.0 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            OuterView.alpha = 1.0;
        } completion:^(BOOL finished) {
            OuterView.alpha = 0.0;
        }];
    }
}
-(IBAction)UpdateAction:(id)sender{
    [[UIApplication sharedApplication]
     openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/mastercook/id999514024?mt=8"]];  /// Url will change
}
- (IBAction)CloseAction{
    MsgOuterView.hidden = YES;
}
- (IBAction)CancelAction {
    LogoutView.hidden = YES;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (IBAction)GotoProfilePageAction {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"CONTAINER";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
