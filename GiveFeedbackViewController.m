//
//  GiveFeedbackViewController.m
//  iWork
//
//  Created by Shailendra on 01/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "GiveFeedbackViewController.h"
#import "Header.h"


@interface GiveFeedbackViewController ()<UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>{
    IBOutlet UITableView *FeedbackTableView;
    NSMutableArray *FeedbackArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UIView *GiveFeedBackOuterView;
    IBOutlet UIView *GiveFeedBackInnerView;
    IBOutlet UILabel *GiveFeedbackLabel;
    IBOutlet UILabel *DemonstratedStaticLabel;
    IBOutlet UILabel *SelectEffectiveLabel;
    IBOutlet UIView *SelectEffectiveView;
    IBOutlet UILabel *NeedToBeDevelopedStaticLabel;
    IBOutlet UILabel *SelectLackLabel;
    IBOutlet UIView *SelectLackView;
    IBOutlet UILabel *FeedbackStaticLabel;
    IBOutlet UITextView *FeedbackTextView;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *SubmitBtn;
    WebApiService *Api;
    
    NSMutableArray *CompentencyNameArray;
    NSMutableArray *CompentencyIDArray;
    NSString *SelectOption;
    NSString *SelectEffectiveIDStr;
    NSString *SelectLackIDStr;
    
    IBOutlet UIView *PickerViewBg;
    IBOutlet UIPickerView *PickerView;
    NSString *TaskIDStr;
    NSDictionary *ResponseDic;
    int offset;
    int limit;
}
@end

@implementation GiveFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    offset = 0;
    limit = 50;
    CheckValue = @"GETLIST";
    TaskIDStr = @"";
    FeedbackArray = [[NSMutableArray alloc] init];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    FeedbackTableView.estimatedRowHeight = 50;
    FeedbackTableView.backgroundColor = delegate.BackgroudColor;
    FeedbackTableView.rowHeight = UITableViewAutomaticDimension;
    FeedbackTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, FeedbackTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [FeedbackTableView addSubview:refreshControl];
    FeedbackTextView.textColor = [UIColor lightGrayColor];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GiveFeedbackApi) withObject:nil afterDelay:0.5];
    [self GiveFeedbackViewMethod];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"GIVEFEEDBACK" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    FeedbackArray = [[NSMutableArray alloc] init];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GiveFeedbackApi) withObject:nil afterDelay:0.5];
}
-(void)GiveFeedbackViewMethod{
    
    GiveFeedBackInnerView.layer.cornerRadius = 6.0;
    SelectEffectiveView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1];
    SelectEffectiveView.layer.borderColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1].CGColor;
    SelectEffectiveView.layer.borderWidth = 1.0;
    SelectLackView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1];
    SelectLackView.layer.borderColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1].CGColor;
    SelectLackView.layer.borderWidth = 1.0;
    SelectEffectiveView.layer.cornerRadius = 6.0;
    SelectLackView.layer.cornerRadius = 6.0;
    FeedbackTextView.layer.cornerRadius = 6.0;
    FeedbackTextView.layer.borderColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1].CGColor;
    FeedbackTextView.layer.borderWidth =1.0;
    
    CancelBtn.layer.cornerRadius = 6.0;
    CancelBtn.layer.borderColor = delegate.redColor.CGColor;
    [CancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    CancelBtn.layer.borderWidth = 1.0;
    SubmitBtn.layer.cornerRadius = 6.0;
    [SubmitBtn setTitle:NSLocalizedString(@"SUBMIT_BTN_CAPS", nil) forState:UIControlStateNormal];
    SelectEffectiveLabel.text = @"Select Effective Competency";
    SelectLackLabel.text = @"Select Lacking Competency";
    
}
- (void)reloadData{
    CheckValue = @"Refresh";
    offset = 0;
    limit = 50;
    FeedbackArray = [[NSMutableArray alloc] init];
    [self GiveFeedbackApi];
}
-(IBAction)CancelCloseAction{
    GiveFeedBackOuterView.hidden = YES;
    if([SelectOption isEqualToString:@"Effective"]){
        SelectEffectiveLabel.text = @"Select Effective Competency";
    }
    else if([SelectOption isEqualToString:@"Lacking"]){
        SelectLackLabel.text  = @"Select Lacking Competency";
    }
}
-(IBAction)SubmitAction:(id)sender{
    CheckValue = @"POSTFEEDBACK";
    if([SelectEffectiveLabel.text isEqualToString:@"Select Effective Competency"]){
        [self ShowAlert:@"Please enter some feedback & try again" ButtonTitle:NSLocalizedString(@"CANCEL", nil)];
    }
    else if([SelectLackLabel.text isEqualToString:@"Select Effective Competency"]){
       [self ShowAlert:@"Please enter some feedback & try again" ButtonTitle:NSLocalizedString(@"CANCEL", nil)];
    }
    else if ([FeedbackTextView.text isEqualToString: @"Write your Feedback"]){
       [self ShowAlert:@"Please enter some feedback & try again" ButtonTitle:NSLocalizedString(@"CANCEL", nil)];
    }
    else{
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(GiveFeedbackApi) withObject:nil afterDelay:0.5];
    }
}
-(IBAction)SelectEffectiveAction:(id)sender{
    [FeedbackTextView resignFirstResponder];
     SelectOption = @"Effective";
    [PickerView selectRow:0 inComponent:0 animated:NO];
    PickerViewBg.hidden = NO;
}
-(IBAction)SelectLackingAction:(id)sender{
     [FeedbackTextView resignFirstResponder];
    SelectOption = @"Lacking";
     [PickerView selectRow:0 inComponent:0 animated:NO];
     PickerViewBg.hidden = NO;
}
-(IBAction)PickerDone:(id)sender{
    PickerViewBg.hidden = YES;
}
- (void)GiveFeedbackApi {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"GETLIST"]){
            ResponseDic =  [Api WebApi:nil Url:@"giveFeedback/List"];
            if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
                [LoadingManager hideLoadingView:self.view];
                if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"name"]))){
                    CompentencyNameArray = [[ResponseDic valueForKey:@"object"] valueForKey:@"name"];
                    CompentencyIDArray = [[ResponseDic valueForKey:@"object"]valueForKey:@"id"];
                }
            }
            CheckValue = @"GETFEEDBACK";
            [self GiveFeedbackApi];
        }
        else if([CheckValue isEqualToString:@"GETFEEDBACK"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"employeeName": @"",
                    @"ennDate": @"",
                    @"startDate": @"",
                    @"offset" : [NSNumber numberWithInt:offset],
                    @"limit" : [NSNumber numberWithInt:limit]
                };
            }
            ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"giveFeedbackTask?employeeId=%@",[ApplicationState userId]]];
            [refreshControl endRefreshing];
            
            if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
                [LoadingManager hideLoadingView:self.view];
                NSLog(@"CompleteResponseDic==%@",ResponseDic);
                NoDataLabel.hidden = YES;
                if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                    
                    NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                    if(ResponseArrays.count>0){
                        [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                        NoDataLabel.hidden = YES;
                    }
                    else if(ResponseArrays.count == 0 && FeedbackArray.count == 0){
                        [self NoDataFound];
                    }
                    [FeedbackTableView reloadData];
                    
                }
                else{
                   [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else if([CheckValue isEqualToString:@"POSTFEEDBACK"]){
            params = @ {
                @"effective_competency": SelectEffectiveIDStr,
                @"employeeId": [ApplicationState userId],
                @"feedback": FeedbackTextView.text,
                @"lacking_competency": SelectLackIDStr,
                @"taskId": TaskIDStr
            };
            ResponseDic =  [Api WebApi:params Url:@"giveFeedback"];
            if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
                [LoadingManager hideLoadingView:self.view];
                GiveFeedBackOuterView.hidden = YES;
                 CheckValue = @"GETFEEDBACK";
                 [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                 [self GiveFeedbackApi];
            }
            else{
                [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(FeedbackArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No record found";
    }
    [FeedbackTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if([CheckValue isEqualToString:@"Refresh"]){
         CheckValue = @"GETFEEDBACK";
    }
    else{
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            if([CheckValue isEqualToString:@"FILTER"]){
                NSMutableDictionary *NewDic = [params mutableCopy];
                for (int i = 0 ; i< [NewDic count];i++){
                    if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                        [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                    }
                    else{
                        [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                    }
                }
                params = [NewDic mutableCopy];
            }
            [self GiveFeedbackApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [FeedbackArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [FeedbackArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FeedbackCell *Cell = (FeedbackCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.GiveFeedbackBtn.tag = indexPath.section;
    [Cell.GiveFeedbackBtn addTarget:self action:@selector(GiveFeedbackAction:) forControlEvents:UIControlEventTouchUpInside];
    if(FeedbackArray.count>0){
        NSDictionary * responseData = [FeedbackArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
    }
    return Cell;
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// Total rows in our component.
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [CompentencyIDArray count];
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}
-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *TitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    if(component == 0){
        UIView *Views = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 216)];
        Views.backgroundColor = [UIColor clearColor];
        TitleLabel.backgroundColor = [UIColor clearColor];
        TitleLabel.textAlignment = NSTextAlignmentCenter;
        TitleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        TitleLabel.numberOfLines= 3;
        TitleLabel.font = [UIFont systemFontOfSize:13];
        TitleLabel.text = [CompentencyNameArray objectAtIndex:row];
        [Views addSubview:TitleLabel];
    }
    return TitleLabel;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if([SelectOption isEqualToString:@"Effective"]){
        SelectEffectiveLabel.text = [CompentencyNameArray objectAtIndex:row];
        SelectEffectiveIDStr =  [CompentencyIDArray objectAtIndex:row];
    }
    else if([SelectOption isEqualToString:@"Lacking"]){
        SelectLackLabel.text  = [CompentencyNameArray objectAtIndex:row];
        SelectLackIDStr =  [CompentencyIDArray objectAtIndex:row];
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Write your Feedback"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write your Feedback";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}
-(void)GiveFeedbackAction:(UIButton*)sender{
    GiveFeedBackOuterView.hidden = NO;
    NSDictionary * responseData = [FeedbackArray objectAtIndex:sender.tag];
    TaskIDStr = [responseData valueForKey:@"taskId"];
}
-(IBAction)FilterAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbacFilterViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbacFilterViewController"];
    ObjController.isComeFrom =@"GIVEFEEDBACK";
    [[self navigationController] presentViewController:ObjController animated:YES completion:nil];
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    ObjAppContainerViewController.isComefrom = @"PMS";
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
