#import "MyKPIScreen.h"
#import "Header.h"

@interface MyKPIScreen ()<UITableViewDelegate,UITableViewDataSource>{
    WebApiService *Api;
    NSDictionary *ResponseDic;
    int offset;
    int limit;
    NSMutableArray *KpiIDArray;
    NSMutableArray *KpiDiscArray;
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
}
@end

@implementation MyKPIScreen
@synthesize isComeFrom;
- (void)viewDidLoad{
    [super viewDidLoad];
    offset = 0;
    limit = 50;
    Api = [[WebApiService alloc] init];
    KpiIDArray = [[NSMutableArray alloc] init];
    KpiDiscArray = [[NSMutableArray alloc] init];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    tab.estimatedRowHeight = 200;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 5.0f)];
    tab.backgroundColor = delegate.BackgroudColor;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(KPIWebApiService) withObject:nil afterDelay:0.5];
}
-(void)KPIWebApiService{
    if(delegate.isInternetConnected){
        ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getKPI?user_id=%@",[ApplicationState userId]]];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            KpiDiscArray = [[[ResponseDic valueForKey:@"object"] valueForKey:@"kpi"] valueForKey:@"kpiDesc"];
            KpiIDArray = [[[ResponseDic valueForKey:@"object"] valueForKey:@"kpi"] valueForKey:@"kpiId"];
            [tab reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:ResponseDic[@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return [KpiIDArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    KPICell *Cell = (KPICell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.KPILabel.text= KpiDiscArray[indexPath.section];
    UITapGestureRecognizer* Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToKPIDetails:)];
    Cell.tag = indexPath.section;
    [Cell  setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:Gesture];
    return Cell;
}

-(void)GoToKPIDetails:(UIGestureRecognizer*)sender{
    int TagValue = sender.view.tag;
    if([isComeFrom isEqualToString:@"Task"]){
        NSDictionary *Dic =
        Dic = @ {
            @"CODE" : KpiIDArray[TagValue],
            @"DESC" :  KpiDiscArray[TagValue]
        };
        [[NSNotificationCenter defaultCenter] postNotificationName:@"KPINOTI" object:nil userInfo:Dic];
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else{
        if(delegate.isInternetConnected){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
            MyKPIDetailsScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyKPIDetailsScreen"];
            ObjViewController.kpiId = KpiIDArray[TagValue];
            ObjViewController.KPIName = KpiDiscArray[TagValue];
            [[self navigationController] pushViewController:ObjViewController animated:YES];
        }
        else{
            [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
  
    }
}
-(IBAction)btnfun:(id)sender{
  [[self navigationController] popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    ObjAppContainerViewController.isComefrom = @"PMS";
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
