//
//  AssignmentCell.m
//  iWork
//
//  Created by Shailendra on 04/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AssignmentCell.h"

@implementation AssignmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    _AssignmentView.layer.cornerRadius = 6.0;
    _AssignmentView.maskView.layer.cornerRadius = 7.0f;
    _AssignmentView.layer.shadowRadius = 3.0f;
    _AssignmentView.layer.shadowColor = [UIColor blackColor].CGColor;
    _AssignmentView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _AssignmentView.layer.shadowOpacity = 0.7f;
    _AssignmentView.layer.masksToBounds = NO;
    
    _AskProgressView.layer.cornerRadius = 6.0;
    _AskProgressView.maskView.layer.cornerRadius = 7.0f;
    _AskProgressView.layer.shadowRadius = 3.0f;
    _AskProgressView.layer.shadowColor = [UIColor blackColor].CGColor;
    _AskProgressView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _AskProgressView.layer.shadowOpacity = 0.7f;
    _AskProgressView.layer.masksToBounds = NO;
}

@end
