#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DownPicker.h"
#import "DQAlertView.h"
#import "NYAlertViewController.h"
#import "VCFloatingActionButton.h"
#import "FCAlertView.h"
#import "AFURLSessionManager.h"

@interface TaskDetailScreen : UIViewController <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UINavigationControllerDelegate,floatMenuDelegate, FCAlertViewDelegate,UITextViewDelegate>
{
    AppDelegate *delegate;
    UITableView *tab;
    NSMutableArray *tabdataarray1,*tabdataarray2,*dataArray,*dataConv;
    NSMutableDictionary *dataDict;
    UILabel *flatButton_lab,*flatButton1_lab,*sel_lab,*lab1,*datelab,*flatButton2_lab;
    UITextField *searchtxt,*searchtxt1,*renttext,*phonetext,*desctext;
    BOOL isShowDetail, isMe, isServerResponded;
    int taskType;
    NSString *taskID, *userComment;
    int count;
    UIDatePicker *dateTimePicker;
    UIView *mainScreenView, *msgvw;//, *screenView;
    UIAlertView *alertView;
    UITextField *alertTextField, *alertTextField1;
    VCFloatingActionButton *addButton;
    FCAlertView *alert;
    UIView *CoomentView;
    NSString *SelectBtn;
    UITextView *CommentTextView;
}
@property (nonatomic) BOOL isShowDetail,isMe;
@property (nonatomic) int count, taskType;
@property (nonatomic, retain) NSMutableDictionary *dataDict;
@property (nonatomic, retain) NSString *taskID;
@property (strong, nonatomic) DownPicker *downPicker;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
- (void) ScreenDesign;
- (void)showCommentAlert;
@end
