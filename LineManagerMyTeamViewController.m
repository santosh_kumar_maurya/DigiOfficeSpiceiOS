//
//  LineManagerMyTeamViewController.m
//  iWork
//
//  Created by Shailendra on 31/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManagerMyTeamViewController.h"
#import "Header.h"

@interface LineManagerMyTeamViewController ()<UITableViewDelegate,UITextViewDelegate>{
    IBOutlet UITableView * MyTeamTableView;
    NSMutableArray *TeamArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    int IndexNumber;
    WebApiService *Api;
    NSDictionary *MyTaskDic;
    NSDictionary *ResponseDic;
}

@end

@implementation LineManagerMyTeamViewController

- (void)viewDidLoad {
    CheckValue = @"MYTEAM";
    Api = [[WebApiService alloc] init];
    IndexNumber = 2;
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    MyTeamTableView.estimatedRowHeight = 200;
    MyTeamTableView.backgroundColor = delegate.BackgroudColor;
    MyTeamTableView.rowHeight = UITableViewAutomaticDimension;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [MyTeamTableView addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TeamWebApi) withObject:nil afterDelay:0.1];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"MYTEAM" object:nil];
    [super viewDidLoad];
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    TeamArray = [[NSMutableArray alloc] init];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TeamWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    TeamArray = [[NSMutableArray alloc] init];
    [self TeamWebApi];
}
- (void)TeamWebApi {
    
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MYTEAM"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"]){
                params = @ {
                    @"empId" : @"",
                    @"empName" : @"",
                    @"handset_id" :@"",
                    @"user_id" :[ApplicationState userId]
                };
            }
            ResponseDic = [Api WebApi:params Url:@"getTeamDashData"];
        }
        NSLog(@"ResponseDic===%@",ResponseDic);
        [refreshControl endRefreshing];
        if([[ResponseDic objectForKey:@"statusCode"]intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            MyTaskDic = ResponseDic;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"empTaskDetails"]))){
               TeamArray = [[ResponseDic valueForKey:@"object"] valueForKey:@"empTaskDetails"];
            }
            [MyTeamTableView reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([TeamArray count]>0){
        return [TeamArray count] + 2;
    }
    else if([CheckValue isEqualToString:@"FILTER"]){
        if(TeamArray.count == 0){
            return 3;
        }
        else{
            return [TeamArray count] + 2;
        }
    }
    return  2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        AssignmentCell *Cell = (AssignmentCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        return Cell;
    }
    else if(indexPath.row == 1){
        MyTaskCell *Cell = (MyTaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        if(IsSafeStringPlus(TrToString([MyTaskDic[@"object"] valueForKey:@"taskCreationCount"]))){
            Cell.TaskCreationLabel.text = [NSString stringWithFormat:@"%@", [MyTaskDic[@"object"] valueForKey:@"taskCreationCount"]];
        }
        else{
            Cell.TaskCreationLabel.text = @"0";
        }
        if(IsSafeStringPlus(TrToString([MyTaskDic[@"object"] valueForKey:@"taskCompletionCount"]))){
            Cell.TaskCompletionLabel.text = [NSString stringWithFormat:@"%@", [MyTaskDic[@"object"] valueForKey:@"taskCompletionCount"]];
        }
        else{
            Cell.TaskCompletionLabel.text = @"0";
        }
        return Cell;
    }
    else if([CheckValue isEqualToString:@"FILTER"]){
        if(TeamArray.count == 0){
            MyTeamEmptyCell *Cell = (MyTeamEmptyCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3"];
            Cell = nil;
            if(Cell == nil){
                Cell = (MyTeamEmptyCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3"];
            }
            return Cell;
        }
        else{
            TeamCell *Cell = (TeamCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2"];
            Cell = nil;
            if(Cell == nil){
                Cell = (TeamCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2"];
            }
            if(IndexNumber == 2){
            }
            if(indexPath.row > IndexNumber){
                Cell.hight.constant = 0;
                Cell.MyTeamStaticLabel.hidden = YES;
                Cell.FilterBtn.hidden = YES;
                
            }
            Cell.tag = indexPath.row;
            UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoEmployeeDetails:)];
            [Cell setUserInteractionEnabled:YES];
            [Cell addGestureRecognizer:gesture];
            [Cell configureCell:[TeamArray objectAtIndex:indexPath.row - 2]];
            return Cell;
        }
    }
    else{
        TeamCell *Cell = (TeamCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2"];
        Cell = nil;
        if(Cell == nil){
            Cell = (TeamCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2"];
        }
        if(IndexNumber == 2){
        }
        if(indexPath.row > IndexNumber){
            Cell.hight.constant = 0;
            Cell.MyTeamStaticLabel.hidden = YES;
            Cell.FilterBtn.hidden = YES;
            
        }
        Cell.tag = indexPath.row;
        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoEmployeeDetails:)];
        [Cell setUserInteractionEnabled:YES];
        [Cell addGestureRecognizer:gesture];
        [Cell configureCell:[TeamArray objectAtIndex:indexPath.row - 2]];
        return Cell;
    }
}
-(IBAction)ViewAllTask:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskRequestScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
    ObjController.isNewReq = TRUE;
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(void)GotoEmployeeDetails:(UIGestureRecognizer *)sender{
    int TagValue = sender.view.tag;
    NSDictionary *Dic = [TeamArray objectAtIndex:TagValue - 2];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EMPLOYEEINFO" object:nil userInfo:Dic];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    EmployeeDetailsParentViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"EmployeeDetailsParentViewController"];
    ObjController.Dics = Dic;
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GoToAssignment:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    CreateTaskScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateTaskScreen"];
    ObjViewController.isAssignTask = YES;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)GoToAskForProgress:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    AskForProgressViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"AskForProgressViewController"];
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GotoTaskCreationAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskRequestScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
    ObjController.isNewReq = TRUE;
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GotoTaskCompletionAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskRequestScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
    ObjController.isNewReq = FALSE;
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GotoFilterControllerAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbacFilterViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbacFilterViewController"];
    ObjController.isComeFrom =@"MYTEAM";
    [[self navigationController] presentViewController:ObjController animated:YES completion:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
