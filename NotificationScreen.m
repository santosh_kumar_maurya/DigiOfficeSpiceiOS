#import "NotificationScreen.h"
#import "Header.h"
@interface NotificationScreen ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource, FCAlertViewDelegate>
{
    UIRefreshControl *refreshControl;
    WebApiService *Api;
    NSDictionary *ResponseDic;
    int offset;
    int limit;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    IBOutlet UILabel *headerlab;
    
    int tagValue;
    NSString *taskType;
    NSString *SelectBtnString;
  
    UIView *CoomentView;
    NSString *SelectBtn;
    UITextView *CommentTextView;
    IBOutlet UIView *OuterView;
    IBOutlet UIView *InterView;
    IBOutlet UIView *ButtonView;
    IBOutlet HCSStarRatingView *RatingView;
    IBOutlet UILabel *ApproveTaskStaticLabel;
    IBOutlet UILabel *RatingLabel;
    IBOutlet UIButton *SubmitBtn;
    IBOutlet UIButton *CloseBtn;
    IBOutlet UITextView *FeedTextView;
    float RateValue;
    
    IBOutlet UITableView *tab;
    
    AppDelegate *delegate;
    NSMutableArray *dataArray;
    NSString *taskID,*taskFeedback;
    FCAlertView* alert;
    int taskRating;
}
@end

@implementation NotificationScreen

- (void)viewDidLoad{
    [super viewDidLoad];
    offset = 0;
    limit = 50;
    taskType = @"";
    SelectBtnString = @"";
    dataArray = [[NSMutableArray alloc] init];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    headerlab.text=NSLocalizedString(@"NOTIFICATIONS", nil);
    taskRating = 0;
    taskFeedback = @"";
    tab.estimatedRowHeight = 50;
    tab.backgroundColor = delegate.BackgroudColor;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 8.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    [self FeedbackLayout];
    self.view.backgroundColor = delegate.BackgroudColor;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(NotificationWebApi) withObject:nil afterDelay:0.5];
}
-(void) NotificationWebApi{
    if(delegate.isInternetConnected){
        params = @ {
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit],
            @"userId":[ApplicationState userId]
        };
        NSLog(@"params==%@",params);
        ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"getNotification?userId=%@",[ApplicationState userId]]];
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"NotificationResponseDic==%@",ResponseDic);
            NoDataLabel.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"notification"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"notification"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"notification"]];
                    [tab reloadData];
                }
                else if(ResponseArrays.count == 0 && dataArray.count == 0){
                    [self NoDataFound];
                }
            }
            else{
                 [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(dataArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No record found";
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"notification"];
    if(ArraysValue.count>0){
        offset = offset + 50;
        [self NotificationWebApi];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [dataArray addObject:BindDataDic];
    }
}
- (void)reloadData{
    offset = 0;
    limit = 50;
    dataArray = [[NSMutableArray alloc] init];
    [self NotificationWebApi];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return [dataArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
      return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[[dataArray objectAtIndex:indexPath.section] objectForKey:@"notiStatus"] isEqualToString:@"UNREAD"]){
        NSLog(@"UNREAD");
        NeedRevisionCell *Cell = (NeedRevisionCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        Cell.RjectBtn.tag = indexPath.section;
        Cell.ApproveBtn.tag = indexPath.section;
        Cell.NeedRevisionBtn.tag = indexPath.section;
        Cell.CompleteApproveBtn.tag = indexPath.section;
        Cell.StartTaskBtn.tag = indexPath.section;
        [Cell.RjectBtn addTarget:self action:@selector(RejectBtnAction:) forControlEvents:UIControlEventTouchUpInside];
         [Cell.ApproveBtn addTarget:self action:@selector(ApproveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
         [Cell.NeedRevisionBtn addTarget:self action:@selector(NeedRevisionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
         [Cell.CompleteApproveBtn addTarget:self action:@selector(CompleteApproveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
         [Cell.StartTaskBtn addTarget:self action:@selector(StartTaskBtnAction:) forControlEvents:UIControlEventTouchUpInside];
       
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
        Cell.tag = indexPath.section;
        [Cell addGestureRecognizer:gestureRecognizer];
        
        if(dataArray.count>0){
            NSDictionary * responseData = [dataArray objectAtIndex:indexPath.section];
            [Cell configureCell:responseData];
            
            if ([[[dataArray objectAtIndex:indexPath.section] objectForKey:@"type"] isEqualToString:@"APPROVE"]){
                Cell.StartTaskBtn.hidden = NO;
                Cell.ApproveBtn.hidden = YES;
                Cell.RjectBtn.hidden = YES;
                Cell.NeedRevisionBtn.hidden = YES;
                Cell.CompleteApproveBtn.hidden = YES;
                
                [Cell.StartTaskBtn setTitle:[NSLocalizedString(@"START_TASK", nil) uppercaseString] forState:UIControlStateNormal];
            }
            else if ([[[dataArray objectAtIndex:indexPath.section] objectForKey:@"type"] isEqualToString:@"NEWTASK"]){
                
                Cell.StartTaskBtn.hidden = YES;
                Cell.ApproveBtn.hidden = NO;
                Cell.RjectBtn.hidden = NO;
                Cell.NeedRevisionBtn.hidden = YES;
                Cell.CompleteApproveBtn.hidden = YES;
                [Cell.ApproveBtn setTitle:[NSLocalizedString(@"APPROVE_BTN_CAPS", nil) uppercaseString] forState:UIControlStateNormal];
                [Cell.RjectBtn setTitle:[NSLocalizedString(@"REJECT_BTN_CAPS", nil) uppercaseString] forState:UIControlStateNormal];
               
            }
            else if ([[[dataArray objectAtIndex:indexPath.section] objectForKey:@"type"] isEqualToString:@"COMPLETE"]){
                
                Cell.StartTaskBtn.hidden = YES;
                Cell.ApproveBtn.hidden = YES;
                Cell.RjectBtn.hidden = YES;
                Cell.NeedRevisionBtn.hidden = NO;
                Cell.CompleteApproveBtn.hidden = NO;
                [Cell.NeedRevisionBtn setTitle:[NSLocalizedString(@"NEED_REVISION", nil) uppercaseString] forState:UIControlStateNormal];
                [Cell.CompleteApproveBtn setTitle:[NSLocalizedString(@"APPROVE_BTN_CAPS", nil) uppercaseString] forState:UIControlStateNormal];
            }
            else{
                Cell.StartTaskBtn.hidden = YES;
                Cell.ApproveBtn.hidden = YES;
                Cell.RjectBtn.hidden = YES;
                Cell.NeedRevisionBtn.hidden = NO;
                Cell.CompleteApproveBtn.hidden = NO;
                
                NotiCell *Cell = (NotiCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                NSLog(@"READ");
                UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
                Cell.tag = indexPath.section;
                [Cell addGestureRecognizer:gestureRecognizer];
                
                if(dataArray.count>0){
                    NSDictionary * responseData = [dataArray objectAtIndex:indexPath.section];
                    [Cell configureCell:responseData];
                }
                return Cell;
            }
        }
         return Cell;
    }
    else{
       NotiCell *Cell = (NotiCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
         NSLog(@"READ");
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
        Cell.tag = indexPath.section;
        [Cell addGestureRecognizer:gestureRecognizer];
        
        if(dataArray.count>0){
            NSDictionary * responseData = [dataArray objectAtIndex:indexPath.section];
            [Cell configureCell:responseData];
        }
        return Cell;
    }
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
        tagValue = recognizer.view.tag;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
        if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"LM_NEWCOMMENT"]){
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Approved"]
                || [[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]
                ){
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                viewController.taskID = str;
                viewController.isManager = TRUE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            else{
                TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
                viewController.taskID = str;
                viewController.isManager = TRUE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"EMP_NEWCOMMENT"]){
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Approved"]){
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                viewController.taskID = str;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            else{
                TaskDetailScreen *viewController = [[TaskDetailScreen alloc] init];
                viewController.taskID = str;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"S_CLOSE"]){
            
            //            TaskDetailStakeHolderScreen *viewController = [[TaskDetailStakeHolderScreen alloc] init];
            //            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            //            viewController.taskID = str;
            //            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"STACKFEED"]){
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            viewController.taskID = str;
            //viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"LM_STACKFEED"]){
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            viewController.taskID = str;
            viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"START"]){
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
          if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Approved"]
                || [[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]
                ){
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                viewController.taskID = str;
                viewController.isManager = TRUE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            else{
                TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
                viewController.taskID = str;
                viewController.isManager = TRUE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
        }
    
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"CLOSE"] ||
                 [[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"REJECT"]){
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
            viewController.taskID = str;
            viewController.isManager = false;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"COMPLETE"]){
           
            TaskRequestScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
            ObjViewController.isNewReq = FALSE;
            [[self navigationController] pushViewController:ObjViewController animated:YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"NEWTASK"]){
            TaskRequestScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskRequestScreen"];
            ObjViewController.isNewReq = YES;
            [[self navigationController] pushViewController:ObjViewController animated:YES];
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"APPROVE"]){
            if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
                MyTaskScreenParentViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskScreenParentViewController"];
                [[self navigationController] pushViewController:ObjViewController animated:YES];
            }
            else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
                NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
                TaskDetailScreen *viewController = [[TaskDetailScreen alloc] init];
                viewController.taskID = str;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
            else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Submitted"] ||
                     [[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Approved"]||
                     [[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]||
                     [[[dataArray objectAtIndex:tagValue] objectForKey:@"taskStatus"] isEqualToString:@"Cancelled"]){
                NSString* str = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
                HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
                viewController.taskID = str;
                viewController.isManager = FALSE;
                [[self navigationController] pushViewController:viewController animated: YES];
            }
        }
        else if ([[[dataArray objectAtIndex:tagValue] objectForKey:@"type"] isEqualToString:@"NEED REVISION"]){
            MyTaskScreen *viewController= [[MyTaskScreen alloc] init];
            viewController.taskType = 2;
            viewController.isComeFrom = @"PROGRESS";
            [[self navigationController] pushViewController:viewController animated: YES];
        }


}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)showAlertStartTask{
    alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeSuccess];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"START_TASK", nil) withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_START", nil) withCustomImage:nil
          withDoneButtonTitle:NSLocalizedString(@"OKAY", nil) andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]];
}
-(IBAction)RejectBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    taskType = @"2";
    SelectBtnString = @"REJECT";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    [self showRejectAlertForNew];
    
}
-(IBAction)ApproveBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    taskType = @"1";
    SelectBtnString = @"APPROVE";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    [self showApproveAlertForNew];
}
-(IBAction)NeedRevisionBtnAction:(UIButton*)sender{
    SelectBtnString = @"";
    tagValue = sender.tag;
    taskType = @"3";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    [self CreateCommentView];
}
-(IBAction)CompleteApproveBtnAction:(UIButton*)sender{
    SelectBtnString = @"";
    tagValue = sender.tag;
    taskType = @"1";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    OuterView.hidden = NO;
}
-(IBAction)StartTaskBtnAction:(UIButton*)sender{
    SelectBtnString = @"STARTTASK";
    tagValue = sender.tag;
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    [self showAlertStartTask];
}
-(void) startTheBgForStatus{
    if(delegate.isInternetConnected){
        if(OuterView.hidden == NO){
            int ConvertRating = (int)RateValue;
            params = @ {
                @"status": taskType,
                @"taskId": taskID,
                @"employeeId": [ApplicationState userId],
                @"rating" : [NSNumber numberWithInt:ConvertRating],
                @"feedback" : FeedTextView.text
            };
        }
        else{
            NSString *Comment = @"";
            Comment = CommentTextView.text;
            if(Comment == nil){
                Comment = @"";
            }
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"user_id": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
            else if([SelectBtnString isEqualToString:@"STARTTASK"]){
                
            }
            else{
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"userId": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
        }
        NSLog(@"params==%@",params);
        if(OuterView.hidden == NO){
            ResponseDic = [Api WebApi:params Url:@"taskApproval"];
        }
        else{
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                ResponseDic = [Api WebApi:params Url:@"updateTaskStatus"];
            }
            else if([SelectBtnString isEqualToString:@"STARTTASK"]){
                ResponseDic =  [Api WebApi:nil Url:[NSString stringWithFormat:@"startButton?employeeId=%@&taskId=%@",[ApplicationState userId],taskID]];
            }
            else{
                ResponseDic = [Api WebApi:params Url:@"needRevision"];
            }
        }
        if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert: [[ResponseDic valueForKey:@"object"] valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            CoomentView.hidden = YES;
            OuterView.hidden = YES;
            [self ClearData];
            [self NotificationWebApi];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self ShowAlert: [ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)ClearData{
    offset = 0;
    limit = 50;
    dataArray = [[NSMutableArray alloc] init];
}
-(void)CreateCommentView{
    CoomentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    CoomentView.backgroundColor = [UIColor clearColor];
    UIImageView *ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    ImageView.backgroundColor = [UIColor blackColor];
    ImageView.alpha = 0.3;
    UIView *InnerView = [[UIView alloc] initWithFrame:CGRectMake(10, delegate.deviceheight/2 - 100, delegate.devicewidth -20, 200)];
    CGPoint center = InnerView.center;
    center.x = CoomentView.center.x;
    InnerView.center = center;
    InnerView.layer.cornerRadius = 5.0;
    InnerView.backgroundColor = [UIColor whiteColor];
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(0, InnerView.frame.size.height-45, InnerView.frame.size.width, 45)];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: BtnView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    BtnView.layer.mask = maskLayer1;
    
    UILabel *AddCommentLabel;
    if([SelectBtn isEqualToString:@"CANCEL"]){
        AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 0)];
    }
    else{
        AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 45)];
    }
    AddCommentLabel.backgroundColor = delegate.yellowColor;
    AddCommentLabel.textColor = [UIColor whiteColor];
    AddCommentLabel.font = delegate.headFont;
    AddCommentLabel.textAlignment = NSTextAlignmentCenter;
    AddCommentLabel.text = NSLocalizedString(@"NEED_REVISION", nil);
    
    CAShapeLayer * AddCommentmaskLayer1 = [CAShapeLayer layer];
    AddCommentmaskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: AddCommentLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){5.0}].CGPath;
    AddCommentLabel.layer.mask = AddCommentmaskLayer1;
    
    UILabel *AddCommentStaticLabel;
    if([SelectBtn isEqualToString:@"CANCEL"]){
        AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 35, 150, 15)];
    }
    else{
        AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, AddCommentLabel.frame.origin.y+AddCommentLabel.frame.size.height + 10, 150, 15)];
    }
    
    AddCommentStaticLabel.textColor = [UIColor blackColor];
    AddCommentStaticLabel.font = delegate.contentFont;
    AddCommentStaticLabel.textAlignment = NSTextAlignmentLeft;
    AddCommentStaticLabel.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    
    CommentTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, AddCommentStaticLabel.frame.origin.y+AddCommentStaticLabel.frame.size.height + 10, AddCommentLabel.frame.size.width -20, BtnView.frame.origin.y - 90)];
    CommentTextView.font = delegate.contentFont;
    CommentTextView.delegate = self;
    CommentTextView.layer.cornerRadius = 5.0;
    CommentTextView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.layer.borderWidth = 0.5;
    CommentTextView.textColor = [UIColor lightGrayColor]; //optional
    CommentTextView.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    CommentTextView.backgroundColor = [UIColor clearColor];
    
    SubmitBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, BtnView.frame.size.width/2, 45)];
    [SubmitBtn addTarget:self action:@selector(SubmitAction) forControlEvents:UIControlEventTouchUpInside];
    SubmitBtn.backgroundColor = delegate.yellowColor;
    SubmitBtn.titleLabel.font = delegate.normalFont;
    [SubmitBtn setTitle:NSLocalizedString(@"SUBMIT_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    UIButton *CancelBtn=[[UIButton alloc] initWithFrame:CGRectMake(BtnView.frame.size.width/2, 0, BtnView.frame.size.width/2, 45)];
    [CancelBtn addTarget:self action:@selector(CancelAction) forControlEvents:UIControlEventTouchUpInside];
    CancelBtn.backgroundColor = delegate.redColor;
    CancelBtn.titleLabel.font = delegate.normalFont;
    [CancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    [BtnView addSubview:SubmitBtn];
    [BtnView addSubview:CancelBtn];
    [InnerView addSubview:BtnView];
    [InnerView addSubview:CommentTextView];
    [InnerView addSubview:AddCommentLabel];
    [InnerView addSubview:AddCommentStaticLabel];
    [CoomentView addSubview:ImageView];
    [CoomentView addSubview:InnerView];
    
    [self.view addSubview:CoomentView];
}
-(void)SubmitAction{
    if([CommentTextView.text isEqualToString:NSLocalizedString(@"ADD_COMMENT_SMALL", nil)] ||[CommentTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_ADD_COMMENT", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        taskFeedback = CommentTextView.text;
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
        [CommentTextView resignFirstResponder];
    }
}
-(void)CancelAction{
    CoomentView.hidden = YES;
}
- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView {
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
}
- (void) FCAlertView:(FCAlertView *)alertView clickedButtonIndex:(NSInteger)index buttonTitle:(NSString *)title {
}
- (void)showApproveAlertForNew {
    alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeSuccess];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.tag = 11;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"APPROVE_TASK", nil)
                 withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_APPROVE", nil)
              withCustomImage:nil
          withDoneButtonTitle:NSLocalizedString(@"OKAY", nil)
                   andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]];
}

- (void)showRejectAlertForNew {
    alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeWarning];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.tag = 10;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"REJECT_TASK", nil)
                 withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_REJECT", nil)
              withCustomImage:nil
          withDoneButtonTitle:NSLocalizedString(@"OKAY", nil)
                   andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView isEqual:FeedTextView]){
        if ([textView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor]; //optional
        }
    }
    else{
        if ([textView.text isEqualToString:@"Add Comment"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor]; //optional
        }
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if([textView isEqual:FeedTextView]){
        if ([textView.text isEqualToString:@""]) {
            textView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
            textView.textColor = [UIColor lightGrayColor]; //optional
        }
    }
    else{
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Add Comment";
            textView.textColor = [UIColor lightGrayColor]; //optional
        }
    }
    [textView resignFirstResponder];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)FeedbackCancelAction:(id)sender{
    OuterView.hidden = YES;
    FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    RatingView.value = 0.0;
    RatingLabel.text = @"";
    FeedTextView.textColor = [UIColor lightGrayColor];
    [FeedTextView resignFirstResponder];
}
-(IBAction)FeedbackSubmitAction:(id)sender{
    
    if (RateValue == 0.0){
        [self ShowAlert:NSLocalizedString(@"PLEASE_FILL_THE_STAR", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([FeedTextView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)] ||[FeedTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_GIVE_THE_FEEDBACK", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        taskFeedback = FeedTextView.text;
        taskRating = RateValue;
        [FeedTextView resignFirstResponder];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
    }
}
- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    RateValue = sender.value;
    if(RateValue == 0.0){
        RatingLabel.hidden = YES;
    }
    else if(RateValue == 1.0){
        RatingLabel.text = @"Below Expectation";
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 2.0){
        RatingLabel.text = @"Meet Expectation";
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 3.0){
        RatingLabel.text = @"Above Expectation";
        RatingLabel.hidden = NO;
    }
}
-(void)FeedbackLayout{
    
    InterView.layer.cornerRadius = 6;
    InterView.clipsToBounds = YES;
    
    FeedTextView.layer.cornerRadius = 6;
    FeedTextView.layer.borderWidth = 1;
    FeedTextView.layer.borderColor = delegate.borderColor.CGColor;
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: ApproveTaskStaticLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    ApproveTaskStaticLabel.layer.mask = maskLayer;
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: SubmitBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){6.0}].CGPath;
    SubmitBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: CloseBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    CloseBtn.layer.mask = maskLayer2;
    
    CAShapeLayer * maskLayer3 = [CAShapeLayer layer];
    maskLayer3.path = [UIBezierPath bezierPathWithRoundedRect: ButtonView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    ButtonView.layer.mask = maskLayer3;

}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
