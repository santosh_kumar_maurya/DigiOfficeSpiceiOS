#import "CreateTaskScreen.h"
#import "Header.h"

@interface CreateTaskScreen ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,FCAlertViewDelegate>{
   
    NSString *StackHolderID;
    IBOutlet UIView *DatePickerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    NSString *DatePickerSelectionStr;
    NSString *StartDateStr;
    NSString *EndDateStr;
    NSString *NumberOfDaysStr;
    NSString *KPIStr;
    NSString *StakeHolderStr;
    NSDateFormatter *dateFormat;
    NSMutableArray *EMPIDArray;
    NSString *EMPIDStr;
    NSDictionary *ResponseDic;
    NSDictionary *param;
    WebApiService *Api;
    NSString *HandsetID;
    NSString *CheckValue;
    NSDictionary *params;
    NSString *SubTaskStr;
    
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    NSMutableDictionary *dataDict;
    UITextField *taskNameTxt, *taskDetailTxt, *kpiTxt, *empTxt,*stakeHolderTxt,*StartDate,*EndDate;
    int subTaskCount;
    IBOutlet UIPickerView *areaSelect;
    IBOutlet UIView *CustomView;
    UIDatePicker *dateTimePicker;
    UITextField *subTaskTxt0, *subTaskTxt1, *subTaskTxt2, *subTaskTxt3, *subTaskTxt4;
    NSString *SelectTextField;
    NSMutableArray *EMPArray;
    NSString *EMPStr;
    NSString *KMPCodeStr;
    
    IBOutlet UILabel *headerlab;
}
@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation CreateTaskScreen
@synthesize isAssignTask;

- (void)viewDidLoad{
    [super viewDidLoad];
    EMPStr = @"";
    EMPIDStr = @"";
    SelectTextField = @"";
    DatePickerSelectionStr = @"";
    Api = [[WebApiService alloc] init];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    dataDict=[[NSMutableDictionary alloc] init];
    EMPArray = [[NSMutableArray alloc] init];
    EMPIDArray = [[NSMutableArray alloc] init];
    [DatePicker setMinimumDate:[NSDate date]];
    if(isAssignTask ==YES){
        CheckValue = @"Employee";
        headerlab.text=NSLocalizedString(@"ASSING_TASK", nil);
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(EmployeeListAndAssignTaskApi) withObject:nil afterDelay:0.5];
    }
    else{
       headerlab.text=NSLocalizedString(@"TASK_CREATION", nil);
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetStackHolderData:) name:@"StackHolder" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetkpiData:) name:@"KPINOTI" object:nil];
    self.view.backgroundColor = delegate.BackgroudColor;
}
-(void)EmployeeListAndAssignTaskApi{
    
    if(delegate.isInternetConnected==YES){
        if(isAssignTask==YES){
            if([CheckValue isEqualToString:@"Employee"]){
                ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getEmployeeList?user_id=%@",[ApplicationState userId]]];
                if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                    [LoadingManager hideLoadingView:self.view];
                    EMPArray = [[[ResponseDic valueForKey:@"object"] valueForKey:@"empData"] valueForKey:@"empName"];
                    NSLog(@"EMPArray==%@",EMPArray);
                    EMPIDArray =  [[[ResponseDic valueForKey:@"object"] valueForKey:@"empData"] valueForKey:@"empId"];
                    [areaSelect reloadAllComponents];
                }
            }
            else if([CheckValue isEqualToString:@"Assignment"]){
                params = @ {
                    @"assignedEmployeeId": EMPIDStr,
                    @"employeeName": EMPStr,
                    @"endDate": EndDateStr,
                    @"kpi": KPIStr,
                    @"stakeholderId": StackHolderID,
                    @"startDate": StartDateStr,
                    @"status": @"New",
                    @"subtask": SubTaskStr,
                    @"taskDetail": taskDetailTxt.text,
                    @"taskName": taskNameTxt.text,
                    @"user_id": [ApplicationState userId]
                };
                NSLog(@"params==%@",params);
                ResponseDic =  [Api WebApi:params Url:@"giveAssignment"];
                if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
                    [LoadingManager hideLoadingView:self.view];
                    [self alertViewFC:[ResponseDic objectForKey:@"message"]];
                }
                else{
                    [LoadingManager hideLoadingView:self.view];
                  [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                }
            }
        }
        else {
            if([CheckValue isEqualToString:@"Create"]){
                params = @ {
                    @"eEndDate": EndDateStr,
                    @"eStartDate": StartDateStr,
                    @"kpi": KPIStr,
                    @"stackholderId": StackHolderID,
                    @"subtask": SubTaskStr,
                    @"taskDetail": taskDetailTxt.text,
                    @"taskName": taskNameTxt.text,
                    @"userId": [ApplicationState userId]
                };
                NSLog(@"params==%@",params);
                ResponseDic =  [Api WebApi:params Url:@"taskCreation"];
                if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
                    [LoadingManager hideLoadingView:self.view];
                    [self alertViewFC:[ResponseDic objectForKey:@"message"]];
                }
                else{
                    [LoadingManager hideLoadingView:self.view];
                    [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                }
            }
        }
    }
    else {
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(subTaskCount>1){
        if(isAssignTask == YES){
            tab.contentSize = CGSizeMake(tab.frame.size.width, tab.frame.size.height + subTaskCount*25 + 40);
        }
        else{
           tab.contentSize = CGSizeMake(tab.frame.size.width, tab.frame.size.height + subTaskCount*25);
        }
    }
}
-(void)ClearBtn:(UIButton*)sender{
    int TagValue = sender.tag;
    [dataDict removeObjectForKey:[NSString stringWithFormat:@"subTask%d", TagValue]];
    for(int i = 0; i <dataDict.count;i++){
        NSString *key =  [NSString stringWithFormat:@"subTask%d", i];
        if(key !=nil){
            if([dataDict objectForKey:key]){
            }
            else{
                NSString *Value =  [dataDict objectForKey:[NSString stringWithFormat:@"subTask%d", i+1]];
                if(Value !=nil){
                    [dataDict removeObjectForKey:[NSString stringWithFormat:@"subTask%d", i+1]];
                    [dataDict setObject:Value forKey:[NSString stringWithFormat:@"subTask%d", i]];
                }
            }
        }
    }
    subTaskCount = subTaskCount - 1;
    [tab reloadData];
    
}
-(IBAction)btnfun:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==20 || btn.tag == 101){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else if(btn.tag==100){
        if([taskNameTxt.text length] < 1){
            [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else if([taskDetailTxt.text length] < 1){
           [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else if ([stakeHolderTxt.text length] < 1){
          [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else if ([kpiTxt.text length] < 1){
            [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else if([empTxt.text length] < 1 && TRUE == isAssignTask){
           [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else if([StartDate.text isEqualToString:@""]){
            [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else if([EndDate.text isEqualToString:@""]){
            [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else if ([NumberOfDaysStr containsString:@"-"]){
           [self ShowAlert:@"Number of days should be in postive value." ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else if ([NumberOfDaysStr containsString:@""]){
           [self ShowAlert:NSLocalizedString(@"CREATE_TASK_VALIDATION", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
            [self getSubTaskData];
            if(isAssignTask == YES){
                CheckValue = @"Assignment";
            }
            else{
                CheckValue = @"Create";
            }
            [LoadingManager showLoadingView:self.view];
            [self performSelector:@selector(EmployeeListAndAssignTaskApi) withObject:nil afterDelay:0.5];
        }
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (FALSE == isAssignTask){
        return 7;
    }
    else{
        return 8;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (2 == indexPath.row)
        return 44+(subTaskCount*40);
    else if((4 == indexPath.row  && FALSE == isAssignTask) ||
            (5 == indexPath.row  && TRUE == isAssignTask))
        return 80;
    else if (5 == indexPath.row && FALSE ==isAssignTask)
        return 70;
    else if (6 == indexPath.row && TRUE ==isAssignTask)
        return 70;
    else if ((6 == indexPath.row && FALSE ==isAssignTask) || (7 == indexPath.row && TRUE == isAssignTask))
        return 60;
    else
        return 44;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
    }
    {
        if(indexPath.row==0){
            taskNameTxt=[[UITextField alloc] initWithFrame:CGRectMake(delegate.margin, 10,delegate.devicewidth-4*delegate.margin, 30)];
            taskNameTxt.keyboardType=UIKeyboardTypeDefault;
            taskNameTxt.placeholder=NSLocalizedString(@"TASK_NAME_WITH_ASTRIC", nil);
            taskNameTxt.font = delegate.contentFont;
            taskNameTxt.textColor = delegate.dimColor;
            taskNameTxt.borderStyle=UITextBorderStyleRoundedRect;
            taskNameTxt.delegate=self;
            [taskNameTxt setReturnKeyType:UIReturnKeyDone];
            taskNameTxt.autocorrectionType = UITextAutocorrectionTypeNo;
            taskNameTxt.text = [dataDict objectForKey:@"taskName"];
            [cell addSubview:taskNameTxt];
        }
        else if(indexPath.row==1){
            taskDetailTxt=[[UITextField alloc] initWithFrame:CGRectMake(delegate.margin, 7,delegate.devicewidth-4*delegate.margin, 30)];
            taskDetailTxt.keyboardType=UIKeyboardTypeDefault;
            taskDetailTxt.placeholder=NSLocalizedString(@"TASK_DETAILS_WITH_ASTRIC", nil);
            taskDetailTxt.font=delegate.contentFont;
            taskDetailTxt.textColor = delegate.dimColor;
            taskDetailTxt.borderStyle=UITextBorderStyleRoundedRect;
            taskDetailTxt.delegate=self;
            [taskDetailTxt setReturnKeyType:UIReturnKeyDone];
            taskDetailTxt.autocorrectionType = UITextAutocorrectionTypeNo;
            taskDetailTxt.text = [dataDict objectForKey:@"taskDetail"];
            [cell addSubview:taskDetailTxt];
        }
        else if(indexPath.row==2){
            UILabel *celllab=[[UILabel alloc] initWithFrame:CGRectMake(delegate.margin, 7,100, 30)];
            celllab.font=delegate.contentFont;
            celllab.textColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:207/255.0 alpha:1];
            celllab.layer.borderColor = [UIColor colorWithRed:230/255.0 green:229/255.0 blue:230/255.0 alpha:1].CGColor;
            celllab.layer.borderWidth = 1.0;
            celllab.layer.cornerRadius = 4.0;
            celllab.text=[NSString stringWithFormat:@"  %@",NSLocalizedString(@"Add_SUB_TASK", nil)];
            [cell addSubview:celllab];
            
            UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapGusture:)];

            [celllab setUserInteractionEnabled:YES];
            [celllab addGestureRecognizer:gesture];
            UIButton *addButton=[[UIButton alloc] initWithFrame:CGRectMake(celllab.frame.origin.x + celllab.frame.size.width - 22, 12, 20, 20)];
            [addButton addTarget:self action:@selector(AddSubtask:) forControlEvents:UIControlEventTouchUpInside];
             [addButton setImage:[UIImage imageNamed:@"btn_add1"] forState:UIControlStateNormal];
            addButton.tag=150;
            [cell addSubview:addButton];
            for (int i = 0; i < subTaskCount; i++){
                UITextField *subTaskTxt;
                subTaskTxt = [[UITextField alloc] initWithFrame:CGRectMake(delegate.margin, addButton.frame.size.height*(2*(i+1))+2,delegate.devicewidth-4*delegate.margin, 30)];
                subTaskTxt.tag = i;
                if (0 == i)
                    subTaskTxt0 = subTaskTxt;
                else if (1 == i)
                    subTaskTxt1 = subTaskTxt;
                else if (2 == i)
                    subTaskTxt2 = subTaskTxt;
                else if (3 == i)
                    subTaskTxt3 = subTaskTxt;
                else if (4 == i)
                    subTaskTxt4 = subTaskTxt;
                [cell addSubview:subTaskTxt];
                subTaskTxt.keyboardType=UIKeyboardTypeDefault;
                subTaskTxt.placeholder=NSLocalizedString(@"Add_SUB_TASK", nil);
                subTaskTxt.font = delegate.contentFont;
                subTaskTxt.borderStyle=UITextBorderStyleRoundedRect;
                subTaskTxt.delegate=self;
                subTaskTxt.autocorrectionType = UITextAutocorrectionTypeNo;
                subTaskTxt.text = [dataDict objectForKey:[NSString stringWithFormat:@"subTask%d", i]];
                subTaskTxt.textColor = delegate.dimColor;
                [subTaskTxt setReturnKeyType:UIReturnKeyDone];
                UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [clearButton setImage:[UIImage imageNamed:@"clear"] forState:UIControlStateNormal];
                [clearButton setFrame:CGRectMake(0,0, 45, 45)];
                [clearButton addTarget:self action:@selector(ClearBtn:) forControlEvents:UIControlEventTouchUpInside];
                clearButton.tag = i;
                subTaskTxt.rightViewMode = UITextFieldViewModeAlways;
                [subTaskTxt setRightView:clearButton];
                if (5 == i)
                    break;
            }
        }
        else if(indexPath.row==3){
            kpiTxt=[[UITextField alloc] initWithFrame:CGRectMake(delegate.margin, 7, delegate.devicewidth-4*delegate.margin, 30)];
            kpiTxt.keyboardType=UIKeyboardTypeDefault;
            kpiTxt.font=delegate.contentFont;
            kpiTxt.borderStyle=UITextBorderStyleRoundedRect;
            kpiTxt.delegate=self;
            kpiTxt.textColor = delegate.dimColor;
            kpiTxt.autocorrectionType = UITextAutocorrectionTypeNo;
            kpiTxt.inputView = areaSelect;
            kpiTxt.placeholder=NSLocalizedString(@"SELECT_KPI", nil);
            kpiTxt.text = KPIStr;
            [cell addSubview:kpiTxt];
            
            UIButton *Button=[[UIButton alloc] initWithFrame:CGRectMake(delegate.margin, kpiTxt.frame.origin.y-3, kpiTxt.frame.size.width-10, kpiTxt.frame.size.height)];
            [Button addTarget:self action:@selector(OpenKpiAction) forControlEvents:UIControlEventTouchUpInside];
            [kpiTxt addSubview:Button];
            
        }
        else if(4 == indexPath.row && TRUE == isAssignTask){
            empTxt=[[UITextField alloc] initWithFrame:CGRectMake(delegate.margin, 7, delegate.devicewidth-4*delegate.margin, 30)];
            empTxt.keyboardType=UIKeyboardTypeDefault;
            empTxt.font=delegate.contentFont;
            empTxt.textColor = delegate.dimColor;
            empTxt.borderStyle=UITextBorderStyleRoundedRect;
            empTxt.delegate=self;
            empTxt.autocorrectionType = UITextAutocorrectionTypeNo;
            empTxt.placeholder=NSLocalizedString(@"SELECT_EMPLOYEE", nil);
            empTxt.inputView = areaSelect;
            empTxt.text = EMPStr;
            [cell addSubview:empTxt];
            
            UIButton *Button=[[UIButton alloc] initWithFrame:CGRectMake(delegate.margin, empTxt.frame.origin.y-3, empTxt.frame.size.width-10, empTxt.frame.size.height)];
            [Button addTarget:self action:@selector(OpenEmpPickerView) forControlEvents:UIControlEventTouchUpInside];
            [empTxt addSubview:Button];
            //
        }
        else if((4 == indexPath.row  && FALSE == isAssignTask)||(5 == indexPath.row  && TRUE == isAssignTask)){
            UILabel *DurationLabel = [[UILabel alloc] initWithFrame:CGRectMake(delegate.margin, 7,100, 17)];
            DurationLabel.font = delegate.contentFont;
            DurationLabel.textColor = delegate.dimColor;
            DurationLabel.text = @"Duration";
            [cell addSubview:DurationLabel];
            
            UIImageView *ImageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(delegate.margin+3, DurationLabel.frame.origin.y+ DurationLabel.frame.size.height +5,18, 18)];
            ImageView1.image = [UIImage imageNamed:@"calendar"];
            [cell addSubview:ImageView1];
            
            UIImageView *ImageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(delegate.devicewidth/2, DurationLabel.frame.origin.y+ DurationLabel.frame.size.height +5,18, 18)];
            ImageView2.image = [UIImage imageNamed:@"calendar"];
            [cell addSubview:ImageView2];
            
            StartDate=[[UITextField alloc] init];
            StartDate.frame = CGRectMake(ImageView1.frame.origin.x + ImageView1.frame.size.width + 5, DurationLabel.frame.origin.y+ DurationLabel.frame.size.height, 100, 30);
            StartDate.placeholder= @"YYYY-MM-DD";
            StartDate.font=delegate.contentFont;
            StartDate.textColor = delegate.dimColor;
            StartDate.borderStyle=UITextBorderStyleNone;
            StartDate.delegate=self;
            StartDate.text = StartDateStr;
            [cell addSubview:StartDate];
            
            UIButton *Button=[[UIButton alloc] initWithFrame:CGRectMake(StartDate.frame.origin.x, StartDate.frame.origin.y, StartDate.frame.size.width, StartDate.frame.size.height)];
            [Button addTarget:self action:@selector(OpenDatePicker:) forControlEvents:UIControlEventTouchUpInside];
            Button.tag =1;
            [cell addSubview:Button];
            
            
            UILabel *line1 = [[UILabel alloc] initWithFrame:CGRectMake(ImageView1.frame.origin.x + ImageView1.frame.size.width + 5, StartDate.frame.origin.y+StartDate.frame.size.height-5,100, 1)];
            line1.backgroundColor = delegate.dimColor;
            [cell addSubview:line1];
            
            EndDate=[[UITextField alloc] init];
            EndDate.frame=CGRectMake(ImageView2.frame.origin.x + ImageView2.frame.size.width + 5, DurationLabel.frame.origin.y+ DurationLabel.frame.size.height, 100, 30);
            EndDate.placeholder= @"YYYY-MM-DD";
            EndDate.font=delegate.contentFont;
            EndDate.textColor = delegate.dimColor;
            EndDate.borderStyle=UITextBorderStyleNone;
            EndDate.delegate=self;
            EndDate.text = EndDateStr;
            [cell addSubview:EndDate];
            
            UIButton *Button1=[[UIButton alloc] initWithFrame:CGRectMake(EndDate.frame.origin.x, EndDate.frame.origin.y, EndDate.frame.size.width, EndDate.frame.size.height)];
            [Button1 addTarget:self action:@selector(OpenDatePicker:) forControlEvents:UIControlEventTouchUpInside];
            Button1.tag = 2;
          [cell addSubview:Button1];
            
            UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(ImageView2.frame.origin.x + ImageView2.frame.size.width + 5, EndDate.frame.origin.y+EndDate.frame.size.height-5,100, 1)];
            line2.backgroundColor = delegate.dimColor;
            [cell addSubview:line2];
            
            UILabel *Days = [[UILabel alloc] initWithFrame:CGRectMake(delegate.margin, StartDate.frame.origin.y+StartDate.frame.size.height+5,45, 17)];
            Days.textColor = delegate.dimColor;
            Days.font = delegate.contentFont;
            Days.text = @"Days :";
            [cell addSubview:Days];
            
            UILabel *DaysValue = [[UILabel alloc] initWithFrame:CGRectMake(Days.frame.size.width, StartDate.frame.origin.y+StartDate.frame.size.height+5,100, 17)];
            DaysValue.textColor = delegate.dimColor;
            DaysValue.font = delegate.contentFont;
            DaysValue.text = NumberOfDaysStr;
            [cell addSubview:DaysValue];
        }
        else if((5 == indexPath.row  && FALSE == isAssignTask) ||
                (6 == indexPath.row  && TRUE == isAssignTask)){
            stakeHolderTxt=[[UITextField alloc] initWithFrame:CGRectMake(delegate.margin, 7, delegate.devicewidth-4*delegate.margin, 30)];
            stakeHolderTxt.keyboardType=UIKeyboardTypeDefault;
            stakeHolderTxt.placeholder= NSLocalizedString(@"ADD_STAKE_HOLDER", nil);
            stakeHolderTxt.font=delegate.contentFont;
            stakeHolderTxt.textColor = delegate.dimColor;
            stakeHolderTxt.borderStyle=UITextBorderStyleRoundedRect;
            stakeHolderTxt.delegate=self;
            stakeHolderTxt.autocorrectionType = UITextAutocorrectionTypeNo;
            stakeHolderTxt.text = StakeHolderStr;
            [stakeHolderTxt setReturnKeyType:UIReturnKeyDone];
            [cell addSubview:stakeHolderTxt];
            
            UIButton *Button=[[UIButton alloc] initWithFrame:CGRectMake(delegate.margin, stakeHolderTxt.frame.origin.y-3, stakeHolderTxt.frame.size.width-10, stakeHolderTxt.frame.size.height)];
            [Button addTarget:self action:@selector(OpenStackHolderAction) forControlEvents:UIControlEventTouchUpInside];
            [stakeHolderTxt addSubview:Button];
        }
        else if ((6 == indexPath.row && FALSE == isAssignTask) ||
                 (7 == indexPath.row && TRUE == isAssignTask)){
            UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 7, delegate.devicewidth-delegate.margin*2, 40)];
            CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
            maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: footerView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
            [cell addSubview:footerView];
            
            UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2, 40)];
            submitButton.backgroundColor = delegate.yellowColor;
            submitButton.titleLabel.font = delegate.normalFont;
            [submitButton setTitle:NSLocalizedString(@"SUBMIT_BTN_CAPS", nil) forState:UIControlStateNormal];
            [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
            submitButton.tag=100;
            [footerView addSubview:submitButton];
            
            UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2, 0, footerView.frame.size.width/2, 40)];
            cancelButton.backgroundColor = delegate.redColor;
            cancelButton.titleLabel.font = delegate.normalFont;
            [cancelButton setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
            cancelButton.tag=101;
            [footerView addSubview:cancelButton];
            UILabel* msg = [[UILabel alloc] initWithFrame:CGRectMake(10, cancelButton.frame.size.height+15, delegate.devicewidth, 20)];
            msg.textColor = delegate.dimColor;
            msg.text =NSLocalizedString(@"MANDATORY_FIELD", nil);
            msg.font = delegate.contentFont;
            [cell addSubview:msg];
        }
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(delegate.margin, 5, tableView.frame.size.width-2*delegate.margin, 20);
    myLabel.font = delegate.boldFont;
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    return headerView;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    if([EMPArray count]>0){
        return [EMPArray count];
    }
    return 0;
}
-(NSString*) pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if([EMPArray count]>0){
        return [EMPArray objectAtIndex:row];
    }
    return @"";
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;{
    if([EMPArray count]>0){
        EMPStr = [EMPArray objectAtIndex:row];
        EMPIDStr = [EMPIDArray objectAtIndex:row];
    }
    else{
        EMPStr = @"";
        EMPIDStr = @"";
    }
}
-(void)OpenEmpPickerView{
    CustomView.hidden = NO;
    [areaSelect reloadAllComponents];
}
-(IBAction)PickerDoneAction:(id)sender{
    CustomView.hidden = YES;
    if([EMPStr isEqualToString:@""]){
      EMPStr = [EMPArray objectAtIndex:0];
        EMPIDStr = [EMPIDArray objectAtIndex:0];
    }
    [tab reloadData];
}
-(void)OpenKpiAction{
    [self KeyBoardHide];
    SelectTextField = @"KPI";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyKPIScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyKPIScreen"];
    ObjViewController.isComeFrom = @"Task";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == kpiTxt){
        [kpiTxt resignFirstResponder];
    }
    else if(textField== empTxt){
         [empTxt resignFirstResponder];
         SelectTextField = @"EMP";
         areaSelect.hidden = NO;
         [tab reloadData];
    }
    else if (textField == stakeHolderTxt){
        [stakeHolderTxt resignFirstResponder];
    }
}
-(void)OpenDatePicker:(UIButton*)sender{
    switch (sender.tag) {
        case 1:
            EndDateStr = @"";
            DatePickerViewBg.hidden = NO;
            DatePickerSelectionStr = @"Start";
            break;
        case 2:
            DatePickerViewBg.hidden = NO;
            DatePickerSelectionStr = @"End";
            break;
            
        default:
            break;
    }
}
-(IBAction)DatePickerDoneAction:(id)sender{
    
    DatePickerViewBg.hidden = YES;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if([DatePickerSelectionStr isEqualToString:@"Start"]){
        StartDateStr = [dateFormat stringFromDate: DatePicker.date];
        EndDateStr = @"";
        NumberOfDaysStr = @"";
        
    }
    else if([DatePickerSelectionStr isEqualToString:@"End"]){
        EndDateStr = [dateFormat stringFromDate: DatePicker.date];
    }
    
    [tab reloadData];
    if(![EndDateStr isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:StartDateStr];
        NSDate *endDate = [dateFormat dateFromString:EndDateStr];
        result = [startDate compare:endDate];
        
        if(result==NSOrderedAscending){
            [self DaysCount];
        }
        else if(result==NSOrderedDescending){
            EndDateStr = @"";
            NumberOfDaysStr = @"";
            [self ShowAlert:@"You can not select End Date earlier or equal to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
        else{
            EndDateStr = @"";
            NumberOfDaysStr = @"";
            [self ShowAlert:@"You can not select End Date earlier or equal to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
}
-(void)DaysCount{
    NSString *start = StartDateStr;
    NSString *end = EndDateStr;
    NSDateComponents *components;
    if([StartDateStr isEqualToString:EndDateStr]){
        NSDate *startDate = [dateFormat dateFromString:start];
        NSDate *endDate = [dateFormat dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        components = [gregorianCalendar components:NSCalendarUnitDay
                                          fromDate:startDate
                                            toDate:endDate
                                           options:0];
        components.day = 1;
        NSDate *nextDate = [gregorianCalendar dateByAddingComponents:components toDate:[NSDate date] options:0];
        EndDateStr = [dateFormat stringFromDate: nextDate];
    }
    else{
        NSDate *startDate = [dateFormat dateFromString:start];
        NSDate *endDate = [dateFormat dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        components = [gregorianCalendar components:NSCalendarUnitDay
                                          fromDate:startDate
                                            toDate:endDate
                                           options:0];
    }
    NumberOfDaysStr = [NSString stringWithFormat:@"%ld",(long)[components day]];
    [tab reloadData];
}
-(void)OpenStackHolderAction{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    StackHolderViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"StackHolderViewController"];
    [self presentViewController:ObjViewController animated:YES completion:nil];
}
-(void)viewWillLayoutSubviews{
    [self KeyBoardHide];
    [self.view endEditing:YES];
    [super viewWillLayoutSubviews];
}
-(void)viewDidAppear:(BOOL)animated{
    [self KeyBoardHide];
}
-(void)KeyBoardHide{
    [taskNameTxt resignFirstResponder];
    [taskDetailTxt resignFirstResponder];
    [StartDate resignFirstResponder];
    [EndDate resignFirstResponder];
    [subTaskTxt0 resignFirstResponder];
    [subTaskTxt1 resignFirstResponder];
    [subTaskTxt2 resignFirstResponder];
    [subTaskTxt3 resignFirstResponder];
    [subTaskTxt4 resignFirstResponder];
    [kpiTxt resignFirstResponder];
    [empTxt resignFirstResponder];
    [stakeHolderTxt resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return TRUE;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
        if(textField == taskNameTxt)
            [dataDict setObject:taskNameTxt.text forKey:@"taskName"];
        else if(textField == taskDetailTxt)
            [dataDict setObject:taskDetailTxt.text forKey:@"taskDetail"];
        else if(textField == kpiTxt)
            [dataDict setObject:kpiTxt.text forKey:@"kpiTxt"];
        else if(textField==subTaskTxt0)
            [dataDict setObject:subTaskTxt0.text forKey:@"subTask0"];
        else if(textField==subTaskTxt1)
            [dataDict setObject:subTaskTxt1.text forKey:@"subTask1"];
        else if(textField==subTaskTxt2)
            [dataDict setObject:subTaskTxt2.text forKey:@"subTask2"];
        else if(textField==subTaskTxt3)
            [dataDict setObject:subTaskTxt3.text forKey:@"subTask3"];
        else if(textField==subTaskTxt4)
            [dataDict setObject:subTaskTxt4.text forKey:@"subTask4"];
        else if(textField==stakeHolderTxt)
            [dataDict setObject:stakeHolderTxt.text forKey:@"stakeHolders"];
}
-(void)TapGusture:(UIGestureRecognizer*)sender{
    [self SubTaskAdd];
}
-(void)AddSubtask:(UIButton*)sender{
    [self SubTaskAdd];
}
-(void)SubTaskAdd {
    if (5 <= subTaskCount){
        [self ShowAlert:NSLocalizedString(@"MAX_LIMIT", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        subTaskCount = subTaskCount + 1;
        [tab reloadData];
    }
}
-(void)GetStackHolderData:(NSNotification*)notification{
    NSDictionary *Dic = notification.userInfo;
    StakeHolderStr = [Dic valueForKey:@"StackHolderName"];
    StackHolderID = [Dic valueForKey:@"StackHolderID"];
    [tab reloadData];
}
-(void)GetkpiData:(NSNotification*)notification;{
    KPIStr = [notification.userInfo valueForKey:@"DESC"];
    KMPCodeStr = [notification.userInfo valueForKey:@"CODE"];
    [tab reloadData];
}
-(void) alertViewFC:(NSString*) msg{
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.titleColor = delegate.yellowColor;
    alert.titleFont = delegate.headFont;
    alert.subtitleFont = delegate.contentFont;
    [alert makeAlertTypeSuccess];
    alert.colorScheme = alert.flatRed;
    alert.delegate = self;
    [alert showAlertWithTitle:NSLocalizedString(@"SUBMITTED", nil) withSubtitle:msg withCustomImage:nil withDoneButtonTitle:NSLocalizedString(@"OKAY", nil) andButtons:nil];
}
- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView {
    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)getSubTaskData{
    NSMutableArray *SubTaskArray = [[NSMutableArray alloc] initWithObjects:subTaskTxt0.text,subTaskTxt1.text,subTaskTxt2.text,subTaskTxt3.text,subTaskTxt4.text, nil];
    SubTaskStr = [SubTaskArray componentsJoinedByString:@","];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
