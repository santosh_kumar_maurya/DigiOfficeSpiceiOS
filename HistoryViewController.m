//
//  HistoryViewController.m
//  iWork
//
//  Created by Shailendra on 08/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "HistoryViewController.h"
#import "Header.h"
@interface HistoryViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *HistoryTableView;
    NSMutableArray *HistoryArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
}

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [self EmptyArray];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    HistoryTableView.estimatedRowHeight = 50;
    HistoryTableView.backgroundColor = delegate.BackgroudColor;
    HistoryTableView.rowHeight = UITableViewAutomaticDimension;
    HistoryTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, HistoryTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [HistoryTableView addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(HistoryWebApi) withObject:nil afterDelay:0.5];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"HISTORYTASK" object:nil];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    HistoryArray = [[NSMutableArray alloc] init];
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(HistoryWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self EmptyArray];
    [self HistoryWebApi];
}
- (void)HistoryWebApi {
    
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"FILTER"]){
            ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"viewAllHistoryDashTask?employeeId=%@",[ApplicationState userId]]];
        }
        else{
            params = @ {
                @"end_date" : @"",
                @"start_date" :@"",
                @"taskId" :@"",
                @"task_type": @"",
                @"task_request_status" : @"",
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
            ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"viewAllHistoryDashTask?employeeId=%@",[ApplicationState userId]]];
        }
        [refreshControl endRefreshing];
        
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            NSLog(@"HistoryResponseDic==%@",ResponseDic);
            [LoadingManager hideLoadingView:self.view];
            NoDataLabel.hidden = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                     [HistoryTableView reloadData];
                }
                else if(ResponseArrays.count == 0 && HistoryArray.count == 0){
                    [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(HistoryArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No record found";
    }
    [HistoryTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self HistoryWebApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [HistoryArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [HistoryArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FeedbackScreenCell *Cell = (FeedbackScreenCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoEmployeeDetails:)];
    [Cell setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:gesture];
    Cell.tag = indexPath.section;
    Cell.DetailsBtn.tag = indexPath.section;
    [Cell.DetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    Cell.RatingView.hidden = NO;
    Cell.StausImageView.hidden = YES;
    Cell.StatusLabel.hidden = YES;
    Cell.RatingView.hidden = NO;
    
    if(HistoryArray.count>0){
        NSDictionary * responseData = [HistoryArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
        
        if(IsSafeStringPlus(TrToString(responseData[@"task_type"]))){
            Cell.FeedbackLabel.text  = responseData[@"task_type"];
        }
        else{
            Cell.FeedbackLabel.text  = @" ";
        }
        if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"New"]){
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
        }
        else if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"]  isEqualToString:@"Assigned"]){
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"START", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
        }
        else if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Declined"] ||([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Discarded"])){
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =YES;
        }
        else if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Approved"] ||([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Rejected"])){
            Cell.RatingView.hidden = NO;
            Cell.StausImageView.hidden = YES;
            Cell.StatusLabel.hidden = YES;
            Cell.DetailsBtn.hidden =NO;
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
        }
        else{
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
        }
        NSString *statusType;
        NSString *imageType;
        UIColor *colorType;
        if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Cancelled"]){
            statusType = @"Cancelled";
            imageType = @"cancelled_new";
            colorType = delegate.redColor;
        }
        else if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Discarded"]){
            statusType = @"Discarded";
            imageType = @"discarded_new";
            colorType = delegate.discardedColor;
        }
        else if([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Declined"]){
            statusType = @"Declined";
            imageType = @"declined_new";
            colorType = delegate.failedColor;
        }
        else if([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"FAILED"]){
            statusType = @"Declined";
            imageType = @"Declined_new";
            colorType = delegate.failedColor;
        }
        else if([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"In-Progress"]){
            statusType = @"In-Progress";
            imageType = @"inprogressblue";
            colorType = delegate.inprogColor;
        }
        else if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Submitted"]){
            statusType = @"Submitted";
            imageType = @"submitted_new";
            colorType = delegate.submittedColor;
        }
        else if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"CLOSED"]){
            statusType = @"Assigned";
            imageType = @"confirmed";
            colorType = delegate.confColor;
        }
        else if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Assigned"]){
            statusType = @"Assigned";
            imageType = @"assigned";
            colorType = delegate.assignColor;
        }
        else if ([[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"New"]||[[[HistoryArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"new"]){
            statusType = @"New";
            imageType = @"pending_new";
            colorType = delegate.pendingColor;
        }
        Cell.StatusLabel.text = statusType;
        Cell.StatusLabel.textColor = colorType;
        Cell.StausImageView.image = [UIImage imageNamed:imageType];
    }
    return Cell;
}
-(void)GotoEmployeeDetails:(UIGestureRecognizer*)Gesture{
    TagValue = Gesture.view.tag;
    [self gotoDetails];
}
-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    [self gotoDetails];
    
}
-(void)gotoDetails{
    NSString* str = [[HistoryArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    if ([[[HistoryArray objectAtIndex:TagValue] objectForKey:@"status"] isEqualToString:@"In-Progress"]){
        TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
    else{
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
