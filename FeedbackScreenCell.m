//
//  FeedbackScreenCell.m
//  iWork
//
//  Created by Shailendra on 18/07/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "FeedbackScreenCell.h"
#import "Header.h"
@implementation FeedbackScreenCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    self.layer.shadowRadius = 3.0f;
    self.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
    self.layer.shadowOpacity = 0.7f;
    self.layer.masksToBounds = NO;
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.TaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.TaskView.layer.mask = maskLayer;
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.DetailsView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    self.DetailsView.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.DetailsBtn.bounds byRoundingCorners:UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    self.DetailsBtn.layer.mask = maskLayer2;
}
- (void)configureCell:(NSDictionary *)info{
    
    if(IsSafeStringPlus(TrToString(info[@"taskId"]))){
        _TaskIdLabel.text  = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),info[@"taskId"]];
    }
    else{
        _TaskIdLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        NSNumber *datenumber = info[@"creationDate"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"creationOn"]))){
        NSNumber *datenumber = info[@"creationOn"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"createdOn"]))){
        NSNumber *datenumber = info[@"createdOn"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"created_on"]))){
        NSNumber *datenumber = info[@"created_on"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskName"]))){
        _TaskNameLabel.text  = info[@"taskName"];
    }
    else if(IsSafeStringPlus(TrToString(info[@"taskNAme"]))){
        _TaskNameLabel.text  = info[@"taskNAme"];
    }
    else if(IsSafeStringPlus(TrToString(info[@"task_name"]))){
        _TaskNameLabel.text  = info[@"task_name"];
    }
    else{
        _TaskNameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"kpi"]))){
        _KPIDetailsLabel.text  = info[@"kpi"];
    }
    else{
        _KPIDetailsLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"rating"]))){
        
        _RatingView.rating  = [info[@"rating"] floatValue];
        _RatingView.starFillColor = delegate.redColor;
        _RatingView.starSize = 13.0;
    }
    else{
        _RatingView.rating  = 0;
        _RatingView.starSize = 13.0;
    }
    if(IsSafeStringPlus(TrToString(info[@"feedback"]))){
        _FeedbackLabel.text  = info[@"feedback"];
    }
    else{
        _FeedbackLabel.text  = @" ";
    }
//    
//    if ([[info objectForKey:@"taskStatus"] isEqualToString:@"Cancelled"]){
//        statusType = @"Cancelled";
//        imageType = @"cancelled_new";
//        colorType = delegate.redColor;
//    }
//    else if ([[info objectForKey:@"taskStatus"] isEqualToString:@"Discarded"]){
//        statusType = @"Discarded";
//        imageType = @"discarded_new";
//        colorType = delegate.discardedColor;
//    }
//    else if([[info objectForKey:@"taskStatus"] isEqualToString:@"Declined"]){
//        statusType = @"Declined";
//        imageType = @"declined_new";
//        colorType = delegate.failedColor;
//    }
//    else if([[info objectForKey:@"taskStatus"] isEqualToString:@"FAILED"]){
//        statusType = @"Declined";
//        imageType = @"Declined_new";
//        colorType = delegate.failedColor;
//    }
//    else if([[info objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
//        statusType = @"In-Progress";
//        imageType = @"inprogressblue";
//        colorType = delegate.inprogColor;
//    }
//    else if ([[info objectForKey:@"taskStatus"] isEqualToString:@"Submitted"]){
//        statusType = @"Submitted";
//        imageType = @"submitted_new";
//        colorType = delegate.submittedColor;
//    }
//    else if ([[info objectForKey:@"taskStatus"] isEqualToString:@"CLOSED"]){
//        statusType = @"Assigned";
//        imageType = @"confirmed";
//        colorType = delegate.confColor;
//    }
//    else if ([[info objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
//        statusType = @"Assigned";
//        imageType = @"assigned";
//        colorType = delegate.assignColor;
//    }
//    
//    _StatusLabel.text = statusType;
//    _StatusLabel.textColor = colorType;
//    _StausImageView.image = [UIImage imageNamed:imageType];
    
    
    
    
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm:aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
