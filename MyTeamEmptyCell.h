//
//  MyTeamEmptyCell.h
//  iWork
//
//  Created by Shailendra on 26/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTeamEmptyCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UIView *FilterView;
@property(nonatomic,strong)IBOutlet UILabel *MyTeamStaticLabel;
@property(nonatomic,strong)IBOutlet UIButton *FilterBtn;
@end
