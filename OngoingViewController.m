//
//  OngoingViewController.m
//  iWork
//
//  Created by Shailendra on 08/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "OngoingViewController.h"
#import "Header.h"
@interface OngoingViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *OngoingTableView;
    NSMutableArray *OngoingArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
}

@end

@implementation OngoingViewController

- (void)viewDidLoad {
    [self EmptyArray];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    OngoingTableView.estimatedRowHeight = 50;
    OngoingTableView.backgroundColor = delegate.BackgroudColor;
    OngoingTableView.rowHeight = UITableViewAutomaticDimension;
    OngoingTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, OngoingTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [OngoingTableView addSubview:refreshControl];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GoingTaskWebApi) withObject:nil afterDelay:0.5];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"ONGOINGTASK" object:nil];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    OngoingArray = [[NSMutableArray alloc] init];
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self EmptyArray];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GoingTaskWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self EmptyArray];
    [self GoingTaskWebApi];
}
- (void)GoingTaskWebApi {
    
    if(delegate.isInternetConnected){
        
        if([CheckValue isEqualToString:@"FILTER"]){
            ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"viewAllOngoingDashTask?employeeId=%@",[ApplicationState userId]]];
        }
        else{
            params = @ {
                @"end_date" : @"",
                @"start_date" :@"",
                @"taskId" :@"",
                @"task_type": @"",
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
            ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"viewAllOngoingDashTask?employeeId=%@",[ApplicationState userId]]];
        }
        [refreshControl endRefreshing];
        
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            NSLog(@"OngoingTaskResponseDic==%@",ResponseDic);
            [LoadingManager hideLoadingView:self.view];
            NoDataLabel.hidden = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    [OngoingTableView reloadData];
                }
                else if(ResponseArrays.count == 0 && OngoingArray.count == 0){
                   [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(OngoingArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No record found";
    }
    [OngoingTableView reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self GoingTaskWebApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [OngoingArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [OngoingArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AskForProgressCell *Cell = (AskForProgressCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoEmployeeDetails:)];
    [Cell setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:gesture];
    Cell.tag = indexPath.section;
    Cell.DetailsBtn.tag = indexPath.section;
    [Cell.DetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    if(OngoingArray.count>0){
        NSDictionary * responseData = [OngoingArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
    }
    return Cell;
}
-(void)GotoEmployeeDetails:(UIGestureRecognizer*)Gesture{
    TagValue = Gesture.view.tag;
    [self gotoDetails];
}
-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    [self gotoDetails];
}
-(void)gotoDetails{
    NSString* str = [[OngoingArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    if ([[[OngoingArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
        TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
    else{
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}


@end
