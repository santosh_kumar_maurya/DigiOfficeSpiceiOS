//
//  AskForProgressCell.h
//  iWork
//
//  Created by Shailendra on 08/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AskForProgressCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *TaskView;
@property (weak, nonatomic) IBOutlet UILabel *TaskIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *DateLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *TypeStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *TypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *TimeLeftStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *TimeLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *KPIStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *KPILabel;
@property (weak, nonatomic) IBOutlet UIView *DetailsView;
@property (weak, nonatomic) IBOutlet UILabel *DetailsStaticLabel;

@property (weak, nonatomic) IBOutlet UIButton *DetailsBtn;

- (void)configureCell:(NSDictionary *)info;

@end
