//
//  SubmitCell.h
//  iWork
//
//  Created by Shailendra on 05/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UIView *SubmitTaskView;
@property(nonatomic,strong)IBOutlet UIView *SubmitProgressView;
@property(nonatomic,strong)IBOutlet UIView *GiveFeedBackView;
@property(nonatomic,strong)IBOutlet UILabel *SubmitTaskStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *SubmitProgressStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *GiveFeedbackStaticLabel;
@end
