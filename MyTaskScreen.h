#import <UIKit/UIKit.h>
#import "Header.h"

#import "AppDelegate.h"
#import "CustomIOSAlertView.h"
#import "DQAlertView.h"
#import "FCAlertView.h"
#import "SSSnackbar.h"
#import "AFURLSessionManager.h"
#import "TNRadioButtonGroup.h"


@interface MyTaskScreen : UIViewController <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,FCAlertViewDelegate>
{
   
    NSMutableArray *ongoingArray,*historyArray,*newArray,*pickerDataArray;
    NSMutableDictionary *dataDict;
    UILabel *flatButton_lab,*flatButton1_lab,*sel_lab,*lab1,*datelab,*flatButton2_lab;
    UITextField *fromDate,*toDate,*filterTaskId;
    int taskType;
    int count;
    NSString* taskID;
    UIView *msgvw,*screenView,*filterView;
    UIAlertView *alertView;
    CustomIOSAlertView *alertView1;
    NSString *actionType;
    FCAlertView *alert;
    BOOL isServerResponded,isFromDate;
    UIDatePicker *dateTimePicker;
    TNRadioButtonGroup* typeGroup, *ratingGroup, *statusGroup;
    NSString* filterTaskStatus,*filterRating,*filterTaskID,*filterTaskType;
    NSMutableDictionary *filterData;
    UIButton *resetButton;
}
@property (nonatomic) int count, taskType;
@property (nonatomic)NSString *isComeFrom;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@property (strong, nonatomic) FCAlertView *alert;
@property (strong, nonatomic) CustomIOSAlertView *alertView1;
- (void) ScreenDesign;
@end
