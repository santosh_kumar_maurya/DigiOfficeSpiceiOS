//
//  FeedbackStackholderCell.m
//  iWork
//
//  Created by Shailendra on 01/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "FeedbackStackholderCell.h"
#import "Header.h"
@implementation FeedbackStackholderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    
    self.layer.cornerRadius = 7.0f;
    self.layer.shadowRadius = 3.0f;
    self.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
    self.layer.shadowOpacity = 0.7f;
    self.layer.masksToBounds = NO;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.TaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.TaskView.layer.mask = maskLayer2;
    
}
- (void)configureCell:(NSDictionary *)info{
  
    if(IsSafeStringPlus(TrToString(info[@"taskId"]))){
        _TaskIdLabel.text  = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),info[@"taskId"]];
    }
    else{
        _TaskIdLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        NSNumber *datenumber = info[@"creationDate"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskName"]))){
        _TaskNameLabel.text  = info[@"taskName"];
    }
    else{
        _TaskNameLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"employeeName"]))){
        _EmployeeNameLabel.text  = info[@"employeeName"];
    }
    else{
        _EmployeeNameLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"effectiveCompetency"]))){
        _EffectiveLabel.text  = info[@"effectiveCompetency"];
    }
    else{
        _EffectiveLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"lackingCompetency"]))){
        _LackingLabel.text  = info[@"lackingCompetency"];
    }
    else{
        _LackingLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"feedbackDate"]))){
        NSNumber *datenumber = info[@"feedbackDate"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _FeedBackDateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"feedback"]))){
        _FeedBackLabel.text  = info[@"feedback"];
    }
    else{
        _FeedBackLabel.text  = @" ";
    }
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm:aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
