#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DownPicker.h"
#import "AFURLSessionManager.h"

@interface HistoryDetailScreen : UIViewController <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    AppDelegate *delegate;
    UITableView *tab;
    NSMutableArray *tabdataarray1,*tabdataarray2,*dataArray,*dataConv;
    NSMutableDictionary *dataDict;
    UILabel *flatButton_lab,*flatButton1_lab,*sel_lab,*lab1,*datelab,*flatButton2_lab;
    UITextField *searchtxt,*searchtxt1,*renttext,*phonetext,*desctext;
    BOOL isShowDetail, isMe, isManager, isServerResponded;
    int taskType;
    NSString *taskID;
    int count;
    UIPickerView *areaSelect;
    UIDatePicker *dateTimePicker;
    UIView *mainScreenView, *msgvw;
    UIAlertView *alertView;
    UITextField *alertTextField, *alertTextField1;
}
@property (nonatomic) BOOL isShowDetail,isMe, isManager;
@property (nonatomic) int count, taskType;
@property (nonatomic, retain) NSMutableDictionary *dataDict;
@property (nonatomic, retain) NSString *taskID;
@property (strong, nonatomic) DownPicker *downPicker;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
- (void) ScreenDesign;
@end
