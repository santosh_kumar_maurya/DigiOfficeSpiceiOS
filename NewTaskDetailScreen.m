#import "NewTaskDetailScreen.h"
#import "Header.h"

const UIEdgeInsets textInsetsMine10 = {5, 10, 11, 17};
const UIEdgeInsets texttextInsetsSomeone10 = {5, 15, 11, 10};

@interface NewTaskDetailScreen ()<UITextViewDelegate>{
   
    UIButton *screenView;
    WebApiService *Api;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    UIView *CoomentView;
    NSString *SelectBtn;
    UITextView *CommentTextView;
    UIRefreshControl *refreshControl;
    NSString *SubTask;
    NSArray* subTaskData;
    NSMutableArray *stakeHolderData;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UITextView *FeedTextView;
    IBOutlet UIView *OuterView;
    IBOutlet UIView *InterView;
    IBOutlet UIView *ButtonView;
    IBOutlet HCSStarRatingView *RatingView;
    IBOutlet UIButton *SubmitBtn;
    float RateValue;
    IBOutlet UILabel *RatingLabel;
    IBOutlet UILabel *ApproveTaskStaticLabel;
    IBOutlet UIButton *CloseBtn;
}

@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation NewTaskDetailScreen
@synthesize isShowDetail, count, taskID, taskType, dataDict, isMe, isNewReq;

- (void)viewDidLoad
{
    RateValue = 0.0;
    Api = [[WebApiService alloc] init];
    [super viewDidLoad];
    self.view.backgroundColor = delegate.BackgroudColor;
    HeaderLabel.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),taskID];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    isServerResponded = FALSE;
    isShowDetail= TRUE;
    isMe = TRUE;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskDetailsWebApi) withObject:nil afterDelay:0.5];
    [self.view addSubview:OuterView];
    OuterView.hidden = YES;
    RatingView.tintColor = delegate.redColor;
    [self FeedbackLayout];
}
-(void)TaskDetailsWebApi{
    if(delegate.isInternetConnected){
        if([SelectBtn isEqualToString:@"REJECT"]){
            params = @ {
                @"comment": CommentTextView.text,
                @"status":@"4",
                @"taskId": taskID,
                @"user_Id"  : [ApplicationState userId],
            };
            ResponseDic = [Api WebApi:params Url:[NSString stringWithFormat:@"markCompleted?employeeId=%@&taskId=%@",[ApplicationState userId],taskID]];
        }
        else if([SelectBtn isEqualToString:@"APPROVE"]){
            params = @ {
                @"comment": CommentTextView.text,
                @"subTask":SubTask,
                @"taskId": taskID,
                @"user_id"  : [ApplicationState userId],
            };
            ResponseDic = [Api WebApi:params Url:@"addComment"];
        }
        else{
            ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getTaskDetail?taskId=%@&userId=%@",taskID,[ApplicationState userId]]];
        }
        isServerResponded = YES;
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                if([SelectBtn isEqualToString:@"MARK"]){
                    [[self navigationController] popViewControllerAnimated:YES];
                }
                else if([SelectBtn isEqualToString:@"COMMENT"]){
                    [[self navigationController] popViewControllerAnimated:YES];
                }
                else{
                    dataArray = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                }
            }
            else{
                [LoadingManager hideLoadingView:self.view];
                [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
            [tab reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)FeedbackLayout{
    
    InterView.layer.cornerRadius = 6;
    InterView.clipsToBounds = YES;
    
    FeedTextView.layer.cornerRadius = 6;
    FeedTextView.layer.borderWidth = 1;
    FeedTextView.layer.borderColor = delegate.borderColor.CGColor;
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: ApproveTaskStaticLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    ApproveTaskStaticLabel.layer.mask = maskLayer;
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: SubmitBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){6.0}].CGPath;
    SubmitBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: CloseBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    CloseBtn.layer.mask = maskLayer2;
    
    CAShapeLayer * maskLayer3 = [CAShapeLayer layer];
    maskLayer3.path = [UIBezierPath bezierPathWithRoundedRect: ButtonView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    ButtonView.layer.mask = maskLayer3;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView isEqual:FeedTextView]){
        if ([textView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor]; //optional
        }
    }
    else{
        if ([textView.text isEqualToString:@"Add Comment"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor]; //optional
        }
    }
    [textView becomeFirstResponder];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView isEqual:FeedTextView]){
        if ([textView.text isEqualToString:@""]) {
            textView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
            textView.textColor = [UIColor lightGrayColor]; //optional
        }
    }
    else{
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Add Comment";
            textView.textColor = [UIColor lightGrayColor]; //optional
        }
    }
    [textView resignFirstResponder];
}
-(void)screenTaskDetail:(UITableViewCell *)cell{
    
    int rightPad = delegate.devicewidth-delegate.margin*2;
    UIButton *mainView=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    mainView.backgroundColor = delegate.BackgroudColor;
    [cell addSubview:mainView];
    
    int ypos = 15;
    UILabel *createdOnLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/3, 15)];
    createdOnLabel.text = NSLocalizedString(@"CREATED_ON", nil);
    createdOnLabel.font = delegate.contentSmallFont;
    createdOnLabel.textColor = [UIColor blackColor];
    createdOnLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOnLabel];
    
    UILabel *createdOn =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos+17, delegate.devicewidth/3, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"creationDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"creationDate"];
        createdOn.text = [self DateFormateChange:datenumber];
    }
    else{
        createdOn.text = @"";
    }
    createdOn.font = delegate.contentSmallFont;
    createdOn.textColor = delegate.dimBlack;
    createdOn.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOn];
    
    createdOnLabel.frame = CGRectMake(10, ypos, createdOn.intrinsicContentSize.width, 15);
    int xpos = delegate.devicewidth/2-((delegate.devicewidth/3)/2);
    
    if (FALSE == isNewReq){
        UILabel *startDateLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/3, 15)];
        startDateLabel.text = NSLocalizedString(@"ACTUAL_START_DATE", nil);
        startDateLabel.textColor = delegate.dimColor;
        startDateLabel.font = delegate.contentSmallFont;
        startDateLabel.textAlignment = ALIGN_LEFT;
        startDateLabel.textColor = [UIColor blackColor];
        [mainView addSubview:startDateLabel];
        
        UILabel *startDate =
        [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, delegate.devicewidth/3, 15)];
        if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"]))){
            NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"];
            startDate.text = [self DateFormateChange:datenumber];
        }
        else{
            startDate.text = @"";
        }
        startDate.font = delegate.contentSmallFont;
        startDate.textAlignment = ALIGN_LEFT;
        startDate.textColor = delegate.dimBlack;
        [mainView addSubview:startDate];
        
        xpos = delegate.devicewidth/2 - (startDate.intrinsicContentSize.width/2);
        startDateLabel.frame = CGRectMake(xpos-20, ypos, startDate.intrinsicContentSize.width+30, 15);
        startDate.frame = CGRectMake(xpos-20, ypos+17, startDate.intrinsicContentSize.width, 15);
    }
    xpos = delegate.devicewidth-(delegate.devicewidth/3);
    UILabel *dayLeftLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/3, 15)];
    if (FALSE == isNewReq){
        dayLeftLabel.text = NSLocalizedString(@"COMPLETION_DATE", nil);
    }
    else{
        dayLeftLabel.text = NSLocalizedString(@"DURATION", nil);
    }
   
    dayLeftLabel.textColor = [UIColor blackColor];
    dayLeftLabel.font = delegate.contentSmallFont;
    dayLeftLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:dayLeftLabel];
    
    xpos = delegate.devicewidth - dayLeftLabel.intrinsicContentSize.width - 15;
    dayLeftLabel.frame = CGRectMake(xpos, ypos, dayLeftLabel.intrinsicContentSize.width, 15);
    
    UILabel *dayLeft =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, dayLeftLabel.intrinsicContentSize.width, 15)];
    if (FALSE == isNewReq){
        if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"completionDate"]))){
            NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"completionDate"];
            dayLeft.text = [self DateFormateChange:datenumber];
        }
        else{
            dayLeft.text = @"";
        }
    }
     else{
         dayLeft.text  =[[dataArray objectAtIndex:0] objectForKey:@"duration"];
         
     }
    dayLeft.font = delegate.contentSmallFont;
    dayLeft.textAlignment = ALIGN_LEFT;
    dayLeft.textColor = delegate.dimBlack;
    [mainView addSubview:dayLeft];
    
    if (dayLeftLabel.intrinsicContentSize.width > dayLeft.intrinsicContentSize.width)
        dayLeft.frame = CGRectMake(xpos, ypos+17, dayLeftLabel.intrinsicContentSize.width, 15);
    else
        dayLeft.frame = CGRectMake(xpos, ypos+17, dayLeft.intrinsicContentSize.width, 15);

    int saveYPos = ypos = ypos + 30 + 15;
    screenView=[[UIButton alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, delegate.deviceheight)];
    screenView.backgroundColor=[UIColor whiteColor];
    [mainView addSubview:screenView];
    cell.backgroundColor=[UIColor clearColor];

    ypos = 15;
    UILabel *status1 =
    [[UILabel alloc] initWithFrame:CGRectMake(rightPad/2-10, ypos, rightPad/2-delegate.margin, 20)];
    status1.font = delegate.headFont;
    status1.textAlignment = ALIGN_CENTER;
    status1.text =NSLocalizedString(@"NEW", nil);
    status1.textColor = delegate.pendingColor;
    CGPoint center = status1.center;
    center.x = screenView.frame.size.width / 2 + 20;
    [status1 setCenter:center];
    [screenView addSubview:status1];
    
    UIImageView *statusLogo;
    statusLogo=[[UIImageView alloc] initWithFrame:CGRectMake(rightPad/2, ypos, 20, 20)];
    statusLogo.image=[UIImage imageNamed:@"pending_new"];
    center = statusLogo.center;
    center.x = screenView.frame.size.width / 2 - 30;
    [statusLogo setCenter:center];
    [screenView addSubview:statusLogo];
    
    if (FALSE == isNewReq){
        status1.text = @"Submitted";
        status1.textColor = delegate.submittedColor;
        statusLogo.image=[UIImage imageNamed:@"submitted_new"];
    }
    ypos = ypos + statusLogo.frame.size.height + 15;
    UIView *lineView0 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView0.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView0];
    
    ypos = ypos + 15;
    UILabel *taskNameLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, rightPad-delegate.margin*2, 15)];
    taskNameLabel.text = NSLocalizedString(@"TASK_NAME_WITH_COLON", nil);
    taskNameLabel.font = delegate.contentFont;
    taskNameLabel.textColor=[UIColor blackColor];
    taskNameLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:taskNameLabel];
    
    NSString *taskInfo = [[dataArray objectAtIndex:0] objectForKey:@"taskName"];
    if (taskInfo.length > 0){
        taskInfo = [NSString stringWithFormat:@"%@%@",[[taskInfo substringToIndex:1] uppercaseString],[taskInfo substringFromIndex:1] ];
    }
    xpos = taskNameLabel.intrinsicContentSize.width + 10;
    UILabel *taskName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 50)];
    taskName.text = taskInfo;
    taskName.font = delegate.contentFont;
    taskName.textColor=delegate.dimBlack;
    taskName.textAlignment = ALIGN_LEFT;
    taskName.numberOfLines = 0;
    [taskName sizeToFit];
    [screenView addSubview:taskName];
    
    ypos = ypos + taskName.frame.size.height + 15;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView];
    
    ypos = ypos + 15;
    UILabel *StartDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    StartDateStaticLabel.text = NSLocalizedString(@"START_DATE_WITH_COLON", nil);
    StartDateStaticLabel.font = delegate.contentFont;
    StartDateStaticLabel.textColor = [UIColor blackColor];
    StartDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateStaticLabel];
    
    xpos = StartDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *StartDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"startDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"startDate"];
        StartDateValueLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        StartDateValueLabel.text = @"";
    }
    StartDateValueLabel.font = delegate.contentFont;
    StartDateValueLabel.textColor = delegate.dimBlack;
    StartDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateValueLabel];
    
    ypos = ypos + StartDateStaticLabel.frame.size.height + 15;
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView1.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView1];
    
    ypos = ypos + 15;
    UILabel *EndDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    EndDateStaticLabel.text = NSLocalizedString(@"END_DATE_WITH_COLON", nil);
    EndDateStaticLabel.font = delegate.contentFont;
    EndDateStaticLabel.textColor = [UIColor blackColor];
    EndDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateStaticLabel];
    
    xpos = EndDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *EndDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"endDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"endDate"];
        EndDateValueLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        EndDateValueLabel.text = @"";
    }
    EndDateValueLabel.font = delegate.contentFont;
    EndDateValueLabel.textColor = delegate.dimBlack;
    EndDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateValueLabel];
    
    ypos = ypos + EndDateValueLabel.frame.size.height + 15;
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView2.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView2];
    if (FALSE == isNewReq){
        ypos = ypos + 15;
        UILabel *DurationStaticLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
        DurationStaticLabel.text = NSLocalizedString(@"DURATION_WITH_COLON", nil);
        DurationStaticLabel.font = delegate.contentFont;
        DurationStaticLabel.textColor = [UIColor blackColor];
        DurationStaticLabel.textAlignment = ALIGN_LEFT;
        [screenView addSubview:DurationStaticLabel];
        
        xpos = EndDateStaticLabel.intrinsicContentSize.width + 10;
        UILabel *DurationValueLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
        DurationValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"duration"];
        DurationValueLabel.font = delegate.contentFont;
        DurationValueLabel.textColor = delegate.dimBlack;
        DurationValueLabel.textAlignment = ALIGN_LEFT;
        [screenView addSubview:DurationValueLabel];
        ypos = ypos + DurationValueLabel.frame.size.height + 15;
        
        UIView *lineView3 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
        lineView3.backgroundColor = delegate.borderColor;
        [screenView addSubview:lineView3];

    }
    ypos = ypos + 15;
    UILabel *createdByLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    createdByLabel.text = NSLocalizedString(@"TYPE_WITH_COLON", nil);
    createdByLabel.font = delegate.contentFont;
    createdByLabel.textColor = [UIColor blackColor];
    createdByLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdByLabel];
    
    xpos = createdByLabel.intrinsicContentSize.width + 10;
    UILabel *createdBy =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, delegate.devicewidth/2, 15)];
    createdBy.text = [[dataArray objectAtIndex:0] objectForKey:@"type"];
    createdBy.font = delegate.contentFont;
    createdBy.textColor = delegate.dimBlack;
    createdBy.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdBy];
    
    createdBy.frame = CGRectMake(createdByLabel.intrinsicContentSize.width+10, ypos, rightPad, 15);
    createdByLabel.frame = CGRectMake(10, ypos, createdByLabel.intrinsicContentSize.width, 15);
    ypos = ypos + createdByLabel.frame.size.height + 15;
    
    UIView *lineView10 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView10.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView10];
    
    ypos = ypos + 15;
//    if (FALSE == isNewReq){
//        
//        UILabel *taskDurationLabel =
//        [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
//        taskDurationLabel.text = NSLocalizedString(@"SUBMITTED_ON_WITH_COLON", nil);
//        taskDurationLabel.textColor = [UIColor blackColor];
//        taskDurationLabel.font = delegate.contentFont;
//        taskDurationLabel.textAlignment = ALIGN_LEFT;
//        taskDurationLabel.textColor = delegate.dimColor;
//        [screenView addSubview:taskDurationLabel];
//        
//        UILabel *taskDuration =
//        [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
//        taskDuration.text = [[dataArray objectAtIndex:0] objectForKey:@"completeDate"];
//        taskDuration.font = delegate.contentFont;
//        taskDuration.textAlignment = ALIGN_LEFT;
//        taskDuration.textColor = delegate.dimBlack;
//        [screenView addSubview:taskDuration];
//        
//        taskDuration.frame = CGRectMake(taskDurationLabel.intrinsicContentSize.width + 10, ypos, rightPad, 15);
//        ypos = ypos + taskDuration.frame.size.height + 15;
//        UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
//        lineView1.backgroundColor = delegate.borderColor;
//        [screenView addSubview:lineView1];
//        ypos = ypos + 15;
//    }
    
    UILabel *kpiLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
    kpiLabel.text = NSLocalizedString(@"KPI_WITH_COLON", nil);;
    kpiLabel.textColor = [UIColor blackColor];
    kpiLabel.font = delegate.contentFont;
    createdBy.numberOfLines = 0;
    kpiLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiLabel];
    
    xpos = kpiLabel.intrinsicContentSize.width + 10;
    UILabel *kpiName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-30, 50)];
    kpiName.text = [NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:0]  objectForKey:@"kpi"]];
    kpiName.textColor = delegate.dimBlack;
    kpiName.font = delegate.contentFont;
    kpiName.numberOfLines = 0;
    [kpiName sizeToFit];
    kpiName.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiName];
    
    if (kpiName.text.length > 0)
        ypos = ypos + kpiName.frame.size.height + 15;
    else
        ypos = ypos + kpiLabel.frame.size.height + 15;
    int subTaskHeight = 0;
    subTaskData = [[dataArray objectAtIndex:0] objectForKey:@"subTask"];
    if (((NSNull*)subTaskData != [NSNull null]) && subTaskData.count > 0 && [subTaskData[0] length] > 0)
    {
        for (int i=0; i < subTaskData.count; i++){
            UIView *lineView4 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView4.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView4];
            
            ypos = ypos + 15;
            UILabel *subtaskLabel =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            subtaskLabel.text = NSLocalizedString(@"SUBTASK_WITH_COLON", nil);
            subtaskLabel.font = delegate.contentFont;
            subtaskLabel.textColor = [UIColor blackColor];;
            subtaskLabel.textAlignment = ALIGN_LEFT;
            [screenView addSubview:subtaskLabel];
            
            xpos = subtaskLabel.intrinsicContentSize.width + 10;
            UILabel *subtask =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 500)];
            subtask.text = subTaskData[i];
            subtask.font = delegate.contentFont;
            subtask.textColor = delegate.dimBlack;
            subtask.numberOfLines=0;
            subtask.textAlignment = ALIGN_LEFT;
            [subtask sizeToFit];
            [screenView addSubview:subtask];
            subTaskHeight = subTaskHeight + subtask.frame.size.width;
            ypos = ypos + subtask.frame.size.height + 15;
        }
    }
    
    UIView *lineView5 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView5.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView5];
    
    ypos = ypos + 15;
    UILabel *taskDetailLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth-delegate.margin*2-5, 15)];
    taskDetailLabel.text =NSLocalizedString(@"TASK_DETAILS_WITH_COLON", nil);
    taskDetailLabel.font = delegate.contentFont;
    taskDetailLabel.textColor = [UIColor blackColor];;
    taskDetailLabel.textAlignment = ALIGN_LEFT;
    taskDetailLabel.numberOfLines = 0;
    [taskDetailLabel sizeToFit];
    [screenView addSubview:taskDetailLabel];
    
    xpos = taskDetailLabel.intrinsicContentSize.width + 10;
    UILabel *taskDetail =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
    taskDetail.text = [[dataArray objectAtIndex:0] objectForKey:@"taskDetail"];
    taskDetail.font = delegate.contentFont;
    taskDetail.textColor = delegate.dimBlack;;
    taskDetail.textAlignment = ALIGN_LEFT;
    taskDetail.numberOfLines = 0;
    [taskDetail sizeToFit];
    [screenView addSubview:taskDetail];
    
    if (taskDetail.text.length > 0)
        ypos = ypos + taskDetail.frame.size.height + 15;
    else
        ypos = ypos + taskDetailLabel.frame.size.height + 15;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, ypos, delegate.devicewidth-delegate.margin*2, 40)];
    [screenView addSubview:footerView];
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2, 0, footerView.frame.size.width/2, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.contentBigFont;
    [submitButton setTitle:NSLocalizedString(@"APPROVE_BTN_CAPS", nil) forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submitButton.tag = 1000;
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:submitButton];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2, 40)];
    cancelButton.backgroundColor = delegate.yellowColor;
    cancelButton.clipsToBounds = YES;
    cancelButton.titleLabel.font = delegate.contentBigFont;
    cancelButton.tag = 2000;
    if(isNewReq == YES){
    [cancelButton setTitle:NSLocalizedString(@"REJECT_BTN_CAPS", nil) forState:UIControlStateNormal];
    }
    else{
        [cancelButton setTitle:NSLocalizedString(@"NEED_REVISION", nil) forState:UIControlStateNormal];
    }
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    
    [footerView addSubview:cancelButton];
    int height = ypos + footerView.frame.size.height;
    screenView.frame = CGRectMake(delegate.margin, saveYPos, delegate.devicewidth-delegate.margin*2, height);
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}

-(IBAction)HomeAction:(id)sender{
    [self GotobackContainer];
}
-(IBAction)btnfun:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==20){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else if(btn.tag==1000){
        taskID = [[dataArray objectAtIndex:0] objectForKey:@"taskId"];
        if (TRUE == isNewReq){
            taskType = @"1";
            [self showApproveAlertForNew];
        }
        else{
            taskType = @"1";
            OuterView.hidden = NO;
        }
    }
    else if(btn.tag==2000){
        if (TRUE == isNewReq){
            taskType = @"2";
            [self showRejectAlertForNew];
        }
        else{
            taskType = @"3";
            [self CreateCommentView];
        }
    }
}
-(void)CreateCommentView{
    CoomentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    CoomentView.backgroundColor = [UIColor clearColor];
    UIImageView *ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    ImageView.backgroundColor = [UIColor blackColor];
    ImageView.alpha = 0.3;
    UIView *InnerView = [[UIView alloc] initWithFrame:CGRectMake(10, delegate.deviceheight/2 - 100, delegate.devicewidth -20, 200)];
    CGPoint center = InnerView.center;
    center.x = CoomentView.center.x;
    InnerView.center = center;
    InnerView.layer.cornerRadius = 5.0;
    InnerView.backgroundColor = [UIColor whiteColor];
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(0, InnerView.frame.size.height-45, InnerView.frame.size.width, 45)];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: BtnView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    BtnView.layer.mask = maskLayer1;
    
    UILabel *AddCommentLabel;
    AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 45)];
    AddCommentLabel.backgroundColor = delegate.yellowColor;
    AddCommentLabel.textColor = [UIColor whiteColor];
    AddCommentLabel.font = delegate.headFont;
    AddCommentLabel.textAlignment = NSTextAlignmentCenter;
    AddCommentLabel.text = NSLocalizedString(@"NEED_REVISION", nil);
    
    CAShapeLayer * AddCommentmaskLayer1 = [CAShapeLayer layer];
    AddCommentmaskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: AddCommentLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){5.0}].CGPath;
    AddCommentLabel.layer.mask = AddCommentmaskLayer1;
    
    UILabel *AddCommentStaticLabel;
    if([SelectBtn isEqualToString:@"CANCEL"]){
        AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 35, 150, 15)];
    }
    else{
        AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, AddCommentLabel.frame.origin.y+AddCommentLabel.frame.size.height + 10, 150, 15)];
    }
    
    AddCommentStaticLabel.textColor = [UIColor blackColor];
    AddCommentStaticLabel.font = delegate.contentFont;
    AddCommentStaticLabel.textAlignment = NSTextAlignmentLeft;
    AddCommentStaticLabel.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    
    CommentTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, AddCommentStaticLabel.frame.origin.y+AddCommentStaticLabel.frame.size.height + 10, AddCommentLabel.frame.size.width -20, BtnView.frame.origin.y - 90)];
    CommentTextView.font = delegate.contentFont;
    CommentTextView.delegate = self;
    CommentTextView.layer.cornerRadius = 5.0;
    CommentTextView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.layer.borderWidth = 0.5;
    CommentTextView.textColor = [UIColor lightGrayColor]; //optional
    CommentTextView.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    CommentTextView.backgroundColor = [UIColor clearColor];
    
    SubmitBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, BtnView.frame.size.width/2, 45)];
    [SubmitBtn addTarget:self action:@selector(SubmitAction) forControlEvents:UIControlEventTouchUpInside];
    SubmitBtn.backgroundColor = delegate.yellowColor;
    SubmitBtn.titleLabel.font = delegate.normalFont;
    [SubmitBtn setTitle:NSLocalizedString(@"SUBMIT_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    UIButton *CancelBtn=[[UIButton alloc] initWithFrame:CGRectMake(BtnView.frame.size.width/2, 0, BtnView.frame.size.width/2, 45)];
    [CancelBtn addTarget:self action:@selector(CancelAction) forControlEvents:UIControlEventTouchUpInside];
    CancelBtn.backgroundColor = delegate.redColor;
    CancelBtn.titleLabel.font = delegate.normalFont;
    [CancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    [BtnView addSubview:SubmitBtn];
    [BtnView addSubview:CancelBtn];
    [InnerView addSubview:BtnView];
    [InnerView addSubview:CommentTextView];
    [InnerView addSubview:AddCommentLabel];
    [InnerView addSubview:AddCommentStaticLabel];
    [CoomentView addSubview:ImageView];
    [CoomentView addSubview:InnerView];
    
    [self.view addSubview:CoomentView];
}
-(void)SubmitAction{
    
    if([CommentTextView.text isEqualToString:NSLocalizedString(@"ADD_COMMENT_SMALL", nil)] ||[CommentTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_ADD_COMMENT", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        taskFeedback = CommentTextView.text;
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
        [CommentTextView resignFirstResponder];
    }
    
}
-(void) startTheBgForStatus{
    
    if(delegate.isInternetConnected){
        if(OuterView.hidden == NO){
            int ConvertRating = (int)RateValue;
            params = @ {
                @"status": taskType,
                @"taskId": taskID,
                @"employeeId": [ApplicationState userId],
                @"rating" : [NSNumber numberWithInt:ConvertRating],
                @"feedback" : FeedTextView.text
            };
        }
        else{
            NSString *Comment = @"";
            Comment = CommentTextView.text;
            if(Comment == nil){
                Comment = @"";
            }
            params = @ {
                @"status": taskType,
                @"taskId": taskID,
                @"userId": [ApplicationState userId],
                @"comment" : Comment
            };
        }
        NSLog(@"params==%@",params);
        if(OuterView.hidden == NO){
          ResponseDic = [Api WebApi:params Url:@"taskApproval"];
        }
        else{
           ResponseDic = [Api WebApi:params Url:@"needRevision"];
        }
        
        isServerResponded = TRUE;
        if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert: [[ResponseDic valueForKey:@"object"] valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
             CoomentView.hidden = YES;
             OuterView.hidden = YES;
            [NSThread sleepForTimeInterval:2];
            [[self navigationController] popViewControllerAnimated:YES];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert: [ResponseDic valueForKey:@"status"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    RateValue = sender.value;
    if(RateValue == 0.0){
        RatingLabel.hidden = YES;
    }
    else if(RateValue == 1.0){
        RatingLabel.text = @"Below Expectation";
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 2.0){
        RatingLabel.text = @"Meet Expectation";
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 3.0){
        RatingLabel.text = @"Above Expectation";
        RatingLabel.hidden = NO;
    }
}
-(void)CancelAction{
    CoomentView.hidden = YES;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (TRUE == isShowDetail){
        return screenView.frame.size.height + 280;
    }
    else{
        int indx=(int)indexPath.row;
        NSString * str = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
        UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        CGSize size1 = CGSizeMake(230,9999);
        CGRect textRect = [str boundingRectWithSize:size1 options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        CGSize size = textRect.size;
        int temp = size.height+textInsetsMine10.bottom+textInsetsMine10.top;
        return temp + 40;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(delegate.margin, 5, delegate.devicewidth-delegate.margin, 50);
    myLabel.font = delegate.boldFont;
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    return headerView;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
    }
    [self screenTaskDetail:cell];
    return cell;
}

- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView {
    [self startTheBgForStatus];
}
-(IBAction)FeedbackSubmitAction:(id)sender{
    if (RateValue == 0.0){
        [self ShowAlert:NSLocalizedString(@"PLEASE_FILL_THE_STAR", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([FeedTextView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)] ||[FeedTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_GIVE_THE_FEEDBACK", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        taskFeedback = FeedTextView.text;
        taskRating = RateValue;
        [FeedTextView resignFirstResponder];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.5];
    }
}
-(IBAction)FeedbackCancelAction:(id)sender{
    OuterView.hidden = YES;
    FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    RatingView.value = 0.0;
    RatingLabel.text = @"";
    FeedTextView.textColor = [UIColor lightGrayColor];
    [FeedTextView resignFirstResponder];
}
- (void)showApproveAlertForNew {
    
    alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeSuccess];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.tag = 11;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"APPROVE_TASK", nil)
                 withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_APPROVE", nil)
              withCustomImage:nil
          withDoneButtonTitle:NSLocalizedString(@"OKAY", nil)
                   andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]];
}
- (void)showRejectAlertForNew {
    
    alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeWarning];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.tag = 10;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"REJECT_TASK", nil)
                 withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_REJECT", nil)
              withCustomImage:nil
          withDoneButtonTitle:NSLocalizedString(@"OKAY", nil)
                   andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}

@end
