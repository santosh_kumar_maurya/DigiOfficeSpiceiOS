#import "MyTaskScreen.h"
#import "Header.h"


@interface MyTaskScreen ()
{
    UINavigationBar *NavBar;
    UIRefreshControl *refreshControl;
    NSDateFormatter *dateFormat;

    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    IBOutlet UILabel *NoDataLabel;
    WebApiService *Api;
    NSString *handelId;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSString *CheckValue;
    
}
@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation MyTaskScreen
@synthesize count,taskType,alertView1, alert,isComeFrom;

- (void)viewDidLoad{
    
    [super viewDidLoad];
    self.view.backgroundColor = delegate.BackgroudColor;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    dataDict=[[NSMutableDictionary alloc] init];
    filterTaskStatus = filterRating = filterTaskType = @"";
    handelId = [NSString stringWithFormat:@"%@",[delegate.dataFull objectForKey:@"handsetId"]];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [tab registerNib:Nib forCellReuseIdentifier:@"CELL"];
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 50;
    tab.backgroundColor = delegate.BackgroudColor;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    self.view.backgroundColor = delegate.BackgroudColor;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NewMyTask) name:@"NEW" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(OnGoingMyTask) name:@"ONGOING" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(BehindMyTask) name:@"BEHIND" object:nil];
    if([isComeFrom isEqualToString:@"PROGRESS"]){
       taskType = 2;
        CheckValue = @"ONGOING";
        [self fetchDetail];
    }
    else{
        CheckValue = @"NEW";
        taskType = 1;
    }
    
    //[self ScreenDesign];
}
-(void)NewMyTask{
    taskType = 1;
    [tab reloadData];
}
-(void)OnGoingMyTask{
    taskType = 2;
    [tab reloadData];
}
-(void)BehindMyTask{
    taskType = 3;
    [tab reloadData];
}
-(void)viewWillAppear:(BOOL)animated{
//    [self fetchDetail];
}
-(void) fetchDetail{
    if(delegate.isInternetConnected){
        NSLog(@"CheckValue==%@",CheckValue);
        if([CheckValue isEqualToString:@"NEW"]){
            params = @ {
                @"eEndDate" : @"",
                @"eStartDate" :@"",
                @"taskId" :@0,
                @"task_request_status": @"",
                @"task_type": @""
            };
            ResponseDic = [Api WebApi:params Url:@"viewAllNewDashTask"];
            if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
                if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                    newArray = [ResponseDic valueForKey:@"object"];
                    NoDataLabel.hidden = YES;
                    if([newArray count]==0){
                        NoDataLabel.text = @"No record found";
                        NoDataLabel.hidden = NO;
                    }
                }
                else{
                    if([newArray count]==0){
                        NoDataLabel.text = @"No record found";
                        NoDataLabel.hidden = NO;
                    }
                }
                [tab reloadData];
//                if([isComeFrom isEqualToString:@"PROGRESS"]){
//                    CheckValue = nil;
//                }
//                else{
//                    CheckValue = @"ONGOING";
//                    [self fetchDetail];
//                }
            }
            else{
                NoDataLabel.text = @"No record found";
                NoDataLabel.hidden = NO;
            }
        }
        else if ([CheckValue isEqualToString:@"ONGOING"]){
            
            params = @ {
                @"end_date" : @"",
                @"start_date" :@"",
                @"taskId" :@0,
                @"task_type": @""
            };
            ResponseDic = [Api WebApi:params Url:[NSString stringWithFormat:@"viewAllOngoingDashTask?employeeId=%@",[ApplicationState userId]]];
            if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
                if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                    ongoingArray = [ResponseDic valueForKey:@"object"];
                    NoDataLabel.hidden = YES;
                    if([ongoingArray count]==0){
                        NoDataLabel.text = @"No record found";
                        NoDataLabel.hidden = NO;
                    }
                }
                else{
                    if([ongoingArray count]==0){
                        NoDataLabel.text = @"No record found";
                        NoDataLabel.hidden = NO;
                    }
                }
                [tab reloadData];
                if([isComeFrom isEqualToString:@"PROGRESS"]){
                     CheckValue = nil;
                    NSLog(@"CheckValue==%@",CheckValue);
                }
                else{
                    CheckValue = @"HISTORY";
                    [self fetchDetail];
                   
                }
            }
            else{
                NoDataLabel.text = @"No record found";
                NoDataLabel.hidden = NO;
            }
            
            
        }
        else if ([CheckValue isEqualToString:@"HISTORY"]){
            params = @ {
                @"end_date" : @"",
                @"start_date" :@"",
                @"taskId" :@0,
                @"task_type": @"",
                @"task_request_status" : @""
            };
            ResponseDic = [Api WebApi:params Url:[NSString stringWithFormat:@"viewAllHistoryDashTask?employeeId=%@",[ApplicationState userId]]];
            if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
                historyArray = [ResponseDic valueForKey:@"object"];
                NoDataLabel.hidden = YES;
                if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                    historyArray = [ResponseDic valueForKey:@"object"];
                    NoDataLabel.hidden = YES;
                    if([historyArray count]==0){
                        NoDataLabel.text = @"No record found";
                        NoDataLabel.hidden = NO;
                    }
                }
                else{
                    if([historyArray count]==0){
                        NoDataLabel.text = @"No record found";
                        NoDataLabel.hidden = NO;
                    }
                }
                CheckValue = @"";
                [tab reloadData];
            }
            else{
                NoDataLabel.text = @"No record found";
                NoDataLabel.hidden = NO;
                
            }
            
            
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
//    if(delegate.isInternetConnected){
//        params = @ {
//            @"post" : @"action",
//            @"handsetId" : handelId,
//        };
//        ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/mytaskShow"]];
//        [refreshControl endRefreshing];
//        newArray = [[NSMutableArray alloc] init];
//        ongoingArray = [[NSMutableArray alloc] init];
//        historyArray = [[NSMutableArray alloc] init];
//        if([[ResponseDic objectForKey:@"status"] isEqualToString:@"OK"]){
//            NoDataLabel.hidden = NO;
//            if ([ResponseDic objectForKey:@"data1"] != [NSNull null]){
//                newArray = [ResponseDic objectForKey:@"data1"];
//            }
//            if ([ResponseDic objectForKey:@"data2"] != [NSNull null]){
//                ongoingArray = [ResponseDic objectForKey:@"data2"];
//            }
//            if ([ResponseDic objectForKey:@"data3"] != [NSNull null]){
//                historyArray = [ResponseDic objectForKey:@"data3"];
//            }
//            if([isComeFrom isEqualToString:@"PROGRESS"]){
//                taskType = 2;
//            }
//            else{
//                taskType = 1;
//            }
//            [tab reloadData];
//        }
//        else {
//            NoDataLabel.text = [ResponseDic objectForKey:@"msg"];
//            NoDataLabel.hidden = NO;
//            [tab reloadData];
//        }
//    }
//    else{
//       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
//    }
}

- (void)reloadData{
    [self fetchDetail];
}

-(void) resetFilterButton{
    resetButton= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 90, 40)];
    resetButton.backgroundColor = delegate.borderColor;
    resetButton.titleLabel.font = delegate.contentBigFont;
    [resetButton setTitle:NSLocalizedString(@"RESET_BTN", nil) forState:UIControlStateNormal];
    [resetButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    resetButton.tag = 20000;
    float y = self.view.bounds.size.height - resetButton.frame.size.height - 40;
    [resetButton setFrame:CGRectMake(0, y, resetButton.frame.size.width, resetButton.frame.size.height)];
    resetButton.layer.zPosition++;
    CGPoint center = resetButton.center;
    center.x = delegate.devicewidth / 2;
    [resetButton setCenter:center];
    [resetButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
}
- (void) ScreenDesign{
    
    int width = delegate.devicewidth;
    screenView = [[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    screenView.backgroundColor = delegate.BackgroudColor;
    
    filterData = [[NSMutableDictionary alloc] init];
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor=delegate.headerbgColler;
    [screenView addSubview:headerview];
    
    UIButton *backButton=[[UIButton alloc] initWithFrame:CGRectMake(5, 35, 30, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    backButton.tag=20;
    CGPoint center = backButton.center;
    center.y = headerview.frame.size.height / 2 +3;
    [backButton setCenter:center];
    [headerview addSubview:backButton];
    
    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(45, backButton.frame.origin.y+12, 200, 20)];
    headerlab.textColor=delegate.yellowColor;
    headerlab.font=delegate.headFont;
    headerlab.text=NSLocalizedString(@"MY_TASK", nil);
    headerlab.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab];
    
    UIButton* HomeButton=[UIButton buttonWithType:UIButtonTypeCustom];
    HomeButton.frame=CGRectMake(delegate.devicewidth-60, 10, 18, 18);
    HomeButton.tag=20;
    center = HomeButton.center;
    center.y = headerview.frame.size.height / 2 + 3;
    [HomeButton setCenter:center];
    [HomeButton addTarget:self action:@selector(GotobackContainer) forControlEvents:UIControlEventTouchUpInside];
    [HomeButton setImage:[UIImage imageNamed:@"HomeImage"]  forState:UIControlStateNormal];
    [headerview addSubview:HomeButton];
    
    UIButton *filter=[[UIButton alloc] initWithFrame:CGRectMake(delegate.devicewidth-30 , 10, 20, 20)];
    [filter setImage:[UIImage imageNamed:@"filter_white"] forState:UIControlStateNormal];
    [filter addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    filter.tag=30;
    center = filter.center;
    center.y = headerview.frame.size.height / 2 +3;
    [filter setCenter:center];
    [headerview addSubview:filter];
    
    UIButton *flatButton=[[UIButton alloc] initWithFrame:CGRectMake(0, delegate.headerheight, width/3, delegate.tabheight)];
    flatButton.backgroundColor=[UIColor whiteColor];
    flatButton_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, width/3, delegate.tabheight)];
    flatButton_lab.text=NSLocalizedString(@"NEW", nil);
    flatButton_lab.backgroundColor = delegate.headerbgColler;
    flatButton_lab.textAlignment = ALIGN_CENTER;
    flatButton_lab.font = delegate.normalBold;
    flatButton_lab.textColor = [UIColor whiteColor];
    [flatButton addSubview:flatButton_lab];
    [flatButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton.tag=300;
    [screenView addSubview:flatButton];
    
    UIButton *flatButton1=[[UIButton alloc] initWithFrame:CGRectMake(width/3, delegate.headerheight,width/3, delegate.tabheight)];
    flatButton1_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, width/3, delegate.tabheight)];
    flatButton1_lab.text=NSLocalizedString(@"ONGOING", nil);
    flatButton1_lab.textAlignment = ALIGN_CENTER;
    flatButton1_lab.backgroundColor = delegate.headerbgColler;
    flatButton1_lab.font = delegate.normalBold;
    flatButton1_lab.textColor = [UIColor whiteColor];
    [flatButton1 addSubview:flatButton1_lab];
    [flatButton1 addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton1.tag=301;
    [screenView addSubview:flatButton1];
    
    UIButton *flatButton2=[[UIButton alloc] initWithFrame:CGRectMake((width/3)+(width/3),delegate.headerheight,width-(width/3), delegate.tabheight)];
    flatButton2_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, width/3+2, delegate.tabheight)];
    flatButton2_lab.text = NSLocalizedString(@"HISTORY", nil);
    flatButton2_lab.textAlignment = ALIGN_CENTER;
    flatButton2_lab.font = delegate.normalBold;
    flatButton2_lab.backgroundColor = delegate.headerbgColler;
    flatButton2_lab.textColor = [UIColor whiteColor];
    [flatButton2 addSubview:flatButton2_lab];
    [flatButton2 addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton2.tag=302;
    [screenView addSubview:flatButton2];
    
    sel_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight-2, screenView.frame.size.width/3, 2)];
    sel_lab.backgroundColor = [UIColor whiteColor];
    [screenView addSubview:sel_lab];
    
    tab=[[UITableView alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight, screenView.frame.size.width,screenView.frame.size.height)];
    tab.delegate=self;
    tab.dataSource=self;
    tab.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab.backgroundColor=[UIColor clearColor];
    tab.allowsSelection = YES;
    tab.scrollEnabled = YES;
    tab.estimatedRowHeight = 200.0;
    tab.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    [self resetFilterButton];
    [screenView addSubview:tab];
    [self.view addSubview:screenView];
    
    if([isComeFrom isEqualToString:@"PROGRESS"]){
        taskType = 2;
        sel_lab.frame = CGRectMake(delegate.devicewidth/3, delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/3, 2);
    }
    else{
        taskType = 1;
    }
}
-(IBAction)btnfun:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if(20 == btn.tag){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else if(30 == btn.tag){
        if (1 == taskType)
            [self filterView];
        else if (2 == taskType)
            [self filterViewOutgoing];
        else
            [self filterViewHistory];
    }
    else if(btn.tag==300){
        taskType = 1;
        filterTaskStatus = filterRating = filterTaskType = @"";
        [filterData setObject:@"" forKey:@"fromDate"];
        [filterData setObject:@"" forKey:@"toDate"];
        [filterData setObject:@"" forKey:@"taskStatus"];
        [filterData setObject:@"" forKey:@"taskType"];
        [filterData setObject:@"" forKey:@"taskId"];
        [filterData setObject:@"" forKey:@"taskRating"];
        dateTimePicker.hidden = TRUE;
        NavBar.hidden = TRUE;
        sel_lab.frame=CGRectMake(0, delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/3, 2);
        [tab reloadData];
    }
    else if(btn.tag==301){
        taskType = 2;
        filterTaskStatus = filterRating = filterTaskType = @"";
        [filterData setObject:@"" forKey:@"fromDate"];
        [filterData setObject:@"" forKey:@"toDate"];
        [filterData setObject:@"" forKey:@"taskStatus"];
        [filterData setObject:@"" forKey:@"taskType"];
        [filterData setObject:@"" forKey:@"taskId"];
        [filterData setObject:@"" forKey:@"taskRating"];
        dateTimePicker.hidden = TRUE;
        NavBar.hidden = TRUE;
        sel_lab.frame = CGRectMake(delegate.devicewidth/3, delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/3, 2);
        [tab reloadData];
    }
    else if(302 == btn.tag){
        taskType = 3;
        filterTaskStatus = filterRating = filterTaskType = @"";
        [filterData setObject:@"" forKey:@"fromDate"];
        [filterData setObject:@"" forKey:@"toDate"];
        [filterData setObject:@"" forKey:@"taskStatus"];
        [filterData setObject:@"" forKey:@"taskType"];
        [filterData setObject:@"" forKey:@"taskId"];
        [filterData setObject:@"" forKey:@"taskRating"];
        dateTimePicker.hidden = TRUE;
        NavBar.hidden = TRUE;
        sel_lab.frame = CGRectMake(delegate.devicewidth-(delegate.devicewidth/3), delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/3, 2);
        [tab reloadData];
    }
    else if(1000 == btn.tag){
        dateTimePicker.hidden = TRUE;
        NavBar.hidden = TRUE;
        if (![fromDate.text isEqualToString:@""] || ![toDate.text isEqualToString:@""]){
            if ([toDate.text isEqualToString:@""] || [fromDate.text isEqualToString:@""]){
                 [self ShowAlert:NSLocalizedString(@"ENTER_DATE", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                return;
            }
        }
        [screenView addSubview:resetButton];
        [filterView removeFromSuperview];
        [self startTheBgForFilter];
    }
    else if(1001 == btn.tag){
        filterTaskStatus = filterRating = filterTaskType = @"";
        [filterData setObject:@"" forKey:@"fromDate"];
        [filterData setObject:@"" forKey:@"toDate"];
        [filterData setObject:@"" forKey:@"taskStatus"];
        [filterData setObject:@"" forKey:@"taskType"];
        [filterData setObject:@"" forKey:@"taskId"];
        [filterData setObject:@"" forKey:@"taskRating"];
        dateTimePicker.hidden = TRUE;
        NavBar.hidden = TRUE;
        [filterView removeFromSuperview];
    }
    else if (20000 == btn.tag){
        [self fetchDetail];
        filterTaskStatus = filterRating = filterTaskType = @"";
        [filterData setObject:@"" forKey:@"fromDate"];
        [filterData setObject:@"" forKey:@"toDate"];
        [filterData setObject:@"" forKey:@"taskStatus"];
        [filterData setObject:@"" forKey:@"taskType"];
        [filterData setObject:@"" forKey:@"taskId"];
        [filterData setObject:@"" forKey:@"taskRating"];
        [resetButton removeFromSuperview];
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    if (taskType == 1){
       return [newArray count];
    }
    else if (taskType == 2){
       return [ongoingArray count];
    }
    else{
       return [historyArray count];
    }
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (1 == taskType){
        EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        Cell.DetailsBtn.tag = indexPath.section;
        [Cell.DetailsBtn addTarget:self action:@selector(StartCancelAction:) forControlEvents:UIControlEventTouchUpInside];
        Cell.RatingView.hidden = YES;
        Cell.StausImageView.hidden = NO;
        Cell.StatusLabel.hidden = NO;
        if(newArray.count>0){
            NSDictionary * responseData = [newArray objectAtIndex:indexPath.section];
            [Cell configureCell:responseData];
        }
        if ([[[newArray objectAtIndex:indexPath.section] objectForKey:@"taskStatus"] isEqualToString:@"New"]){
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
        }
        else{
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"START", nil) forState:UIControlStateNormal];
        }
        return Cell;
    }
    else if (2 == taskType){
        AskForProgressCell *Cell = (AskForProgressCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        Cell.DetailsBtn.tag = indexPath.section;
        [Cell.DetailsBtn addTarget:self action:@selector(OngoingDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
        if(ongoingArray.count>0){
            NSDictionary * responseData = [ongoingArray objectAtIndex:indexPath.section];
            [Cell configureCell:responseData];
        }
        return Cell;

    }
    else 
    {
        FeedbackScreenCell *Cell = (FeedbackScreenCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        Cell.DetailsBtn.tag = indexPath.section;
        [Cell.DetailsBtn addTarget:self action:@selector(HistoryDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
        Cell.RatingView.hidden = NO;
        
        if(historyArray.count>0){
            NSDictionary * responseData = [historyArray objectAtIndex:indexPath.section];
            [Cell configureCell:responseData];
           
            if(IsSafeStringPlus(TrToString(responseData[@"type"]))){
                Cell.FeedbackLabel.text  = responseData[@"type"];
            }
            else{
                Cell.FeedbackLabel.text  = @" ";
            }
            if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"New"]){
                [Cell.DetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
                Cell.RatingView.hidden = YES;
                Cell.StausImageView.hidden = NO;
                Cell.StatusLabel.hidden = NO;
                Cell.DetailsBtn.hidden =NO;
            }
            else if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"]  isEqualToString:@"Assigned"]){
                [Cell.DetailsBtn setTitle:NSLocalizedString(@"START", nil) forState:UIControlStateNormal];
                Cell.RatingView.hidden = YES;
                Cell.StausImageView.hidden = NO;
                Cell.StatusLabel.hidden = NO;
                Cell.DetailsBtn.hidden =NO;
            }
            else if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Declined"] ||([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Discarded"])){
                Cell.RatingView.hidden = YES;
                Cell.StausImageView.hidden = NO;
                Cell.StatusLabel.hidden = NO;
                Cell.DetailsBtn.hidden =YES;
            }
            else if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Approved"] ||([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Rejected"])){
                Cell.RatingView.hidden = NO;
                Cell.StausImageView.hidden = YES;
                Cell.StatusLabel.hidden = YES;
                Cell.DetailsBtn.hidden =NO;
                [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
            }
            else{
                Cell.RatingView.hidden = YES;
                Cell.StausImageView.hidden = NO;
                Cell.StatusLabel.hidden = NO;
                Cell.DetailsBtn.hidden =NO;
                [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
            }
            NSString *statusType;
            NSString *imageType;
            UIColor *colorType;
            if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Cancelled"]){
                statusType = @"Cancelled";
                imageType = @"cancelled_new";
                colorType = delegate.redColor;
            }
            else if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Discarded"]){
                statusType = @"Discarded";
                imageType = @"discarded_new";
                colorType = delegate.discardedColor;
            }
            else if([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Declined"]){
                statusType = @"Declined";
                imageType = @"declined_new";
                colorType = delegate.failedColor;
            }
            else if([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"FAILED"]){
                statusType = @"Declined";
                imageType = @"Declined_new";
                colorType = delegate.failedColor;
            }
            else if([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"In-Progress"]){
                statusType = @"In-Progress";
                imageType = @"inprogressblue";
                colorType = delegate.inprogColor;
            }
            else if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Submitted"]){
                statusType = @"Submitted";
                imageType = @"submitted_new";
                colorType = delegate.submittedColor;
            }
            else if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"CLOSED"]){
                statusType = @"Assigned";
                imageType = @"confirmed";
                colorType = delegate.confColor;
            }
            else if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"Assigned"]){
                statusType = @"Assigned";
                imageType = @"assigned";
                colorType = delegate.assignColor;
            }
            else if ([[[historyArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"New"]){
                statusType = @"New";
                imageType = @"pending_new";
                colorType = delegate.pendingColor;
            }
            Cell.StatusLabel.text = statusType;
            Cell.StatusLabel.textColor = colorType;
            Cell.StausImageView.image = [UIImage imageNamed:imageType];
        }
        return Cell;
    }
}
-(void) startTheBgSetStatus:(void (^)(id responseObject, NSError *error))completionHandler
{
    if([delegate isInternetConnected]){
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        [_params setObject:@"post" forKey:@"action"];
        [_params setObject:[NSString stringWithFormat:@"%@", [delegate.dataFull objectForKey:@"handsetId"]] forKey:@"handsetId"];
        [_params setObject:[NSString stringWithFormat:@"%@", actionType] forKey:@"opType"];
        [_params setObject:[NSString stringWithFormat:@"%@", taskID] forKey:@"taskId"];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSString* url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/manageTask"];
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:_params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        } error:nil];
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self initProgBar];
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, NSData *data, NSError * _Nullable error) {
                          if (error) {
                             [self ShowAlert:[error description] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                          } else {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  if (completionHandler) {
                                      completionHandler(data, nil);
                                  }
                              });
                          }
                      }];
        [uploadTask resume];
    }
    else{
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void) startTheBgSetStatus_Delete:(NSString*)status
{
    if(delegate.isInternetConnected){
        NSString *responseString = @"";
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        delegate.count = 2;
        [_params setObject:@"post" forKey:@"action"];
        [_params setObject:[NSString stringWithFormat:@"%@", [delegate.dataFull objectForKey:@"handsetId"]] forKey:@"handsetId"];
        [_params setObject:[NSString stringWithFormat:@"%@", status] forKey:@"opType"];
        [_params setObject:[NSString stringWithFormat:@"%@", taskID] forKey:@"taskId"];
        responseString = [delegate postRequestForSetStatus:_params];
        NSData *responseData=[responseString dataUsingEncoding:NSUTF8StringEncoding];
        [self initProgBar];
        NSError *jsonParsingError = nil;
        NSDictionary *reqdataDict = [NSJSONSerialization JSONObjectWithData:responseData
                                                                    options:0 error:&jsonParsingError];
        if([[reqdataDict objectForKey:@"status"] isEqualToString:@"OK"]){
            [self ShowAlert:[reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            NSArray *array = [[NSArray alloc] init];
            if ([reqdataDict objectForKey:@"data1"] != [NSNull null]){
                array = [reqdataDict objectForKey:@"data1"];
                newArray = [array mutableCopy];
            }
            if ([reqdataDict objectForKey:@"data2"] != [NSNull null]){
                array = [[NSArray alloc] init];
                array = [reqdataDict objectForKey:@"data2"];
                ongoingArray = [array mutableCopy];
            }
            if ([reqdataDict objectForKey:@"data3"] != [NSNull null]){
                array = [[NSArray alloc] init];
                array = [reqdataDict objectForKey:@"data3"];
                historyArray = [array mutableCopy];
            }
            [tab reloadData];
        }
        else {
             [self ShowAlert:[reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)DonePicker{
    NavBar.hidden = YES;
    dateTimePicker.hidden = TRUE;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    if (TRUE ==  isFromDate){
        fromDate.text=[dateFormat stringFromDate:dateTimePicker.date];
    }
    else{
        toDate.text=[dateFormat stringFromDate:dateTimePicker.date];
    }
    
    if(![toDate.text isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:fromDate.text];
        NSDate *endDate = [dateFormat dateFromString:toDate.text];
        result = [startDate compare:endDate];
        
        if(result==NSOrderedAscending){
            NSLog(@"today is less");
        }
        else if(result==NSOrderedDescending){
            NSLog(@"newDate is less");
            toDate.text = @"";
            [self ShowAlert:@"You cannot select date earlier to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
}
-(void) startTheBg_Delete{
    if(delegate.isInternetConnected){
        NSString *responseString = @"";
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        delegate.count = 2;
        [_params setObject:@"post" forKey:@"action"];
        [_params setObject:[NSString stringWithFormat:@"%@", [delegate.dataFull objectForKey:@"handsetId"]] forKey:@"handsetId"];
        responseString = [delegate postRequestForMyTask:_params];
        NSData *responseData=[responseString dataUsingEncoding:NSUTF8StringEncoding];
        [self initProgBar];
        NSError *jsonParsingError = nil;
        NSDictionary *reqdataDict = [NSJSONSerialization JSONObjectWithData:responseData
                                                                    options:0 error:&jsonParsingError];
        [self stopProgBar];
        if([[reqdataDict objectForKey:@"status"] isEqualToString:@"OK"]){
            NSArray *array = [[NSArray alloc] init];
            if ([reqdataDict objectForKey:@"data1"] != [NSNull null]){
                array = [reqdataDict objectForKey:@"data1"];
                newArray = [array mutableCopy];
            }
            if ([reqdataDict objectForKey:@"data2"] != [NSNull null]){
                array = [[NSArray alloc] init];
                array = [reqdataDict objectForKey:@"data2"];
                ongoingArray = [array mutableCopy];
            }
            if ([reqdataDict objectForKey:@"data3"] != [NSNull null]){
                array = [[NSArray alloc] init];
                array = [reqdataDict objectForKey:@"data3"];
                historyArray = [array mutableCopy];
            }
            [tab reloadData];
        }
        else {
            [self ShowAlert: [reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    
}
- (void) setStatus{
    if(delegate.isInternetConnected){
        [self startTheBgSetStatus:^(id data, NSError *error) {
            [self stopProgBar];
            NSDictionary *reqdataDict = (NSDictionary *)  data;
            if([[reqdataDict objectForKey:@"status"] isEqualToString:@"OK"]){
                [self ShowAlert:[reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                NSArray *array = [[NSArray alloc] init];
                if ([reqdataDict objectForKey:@"data1"] != [NSNull null])
                {
                    array = [reqdataDict objectForKey:@"data1"];
                    newArray = [array mutableCopy];
                }
                else
                    newArray = nil;
                if ([reqdataDict objectForKey:@"data2"] != [NSNull null]){
                    array = [[NSArray alloc] init];
                    array = [reqdataDict objectForKey:@"data2"];
                    ongoingArray = [array mutableCopy];
                }
                else
                    ongoingArray = nil;
                if ([reqdataDict objectForKey:@"data3"] != [NSNull null]){
                    array = [[NSArray alloc] init];
                    array = [reqdataDict objectForKey:@"data3"];
                    historyArray = [array mutableCopy];
                }
                else
                    historyArray = nil;
                [tab reloadData];
            }
            else {
               [self ShowAlert: [reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }];
        
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void) startTheBg_API:(NSMutableDictionary*) params  url:(NSString*) url completionHandler:(void (^)(id responseObject, NSError *error))completionHandler
{
    if([delegate isInternetConnected]){
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        } error:nil];
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self initProgBar];
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, NSData *data, NSError * _Nullable error) {
                          
                          if (error) {
                               [self ShowAlert:[error debugDescription] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                          } else {
                              NSLog(@"Success: %@", data);
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  if (completionHandler) {
                                      completionHandler(data, nil);
                                  }
                              });
                          }
                      }];
        [uploadTask resume];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)initProgBar{
    self.activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    self.activityIndicatorView.frame = CGRectMake(delegate.devicewidth/2-25, delegate.deviceheight-200, 50, 50);
    self.activityIndicatorView.tag=1;
    [self.view addSubview:self.activityIndicatorView];
    self.activityIndicatorView.color = delegate.redColor;
    [self.view setUserInteractionEnabled:NO];
    [self.activityIndicatorView startAnimating];
}
-(void)stopProgBar{
    [self.view setUserInteractionEnabled:YES];
    [self.activityIndicatorView stopAnimating];
}
-(void)noMessageScreen:(NSString*)message{
    msgvw=[[UIView alloc] initWithFrame:tab.frame];
    msgvw.backgroundColor=delegate.BackgroudColor;
    tab.backgroundColor=delegate.BackgroudColor;
    UIImageView *nomsgimg;
    UILabel *tab_lab;
    if (delegate.deviceheight <= 460){
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-100)/2,
                                                               0, 100,100)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    else{
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-100)/2,
                                                               20, 100,100)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    nomsgimg.image=[UIImage imageNamed:@"no_data"];
    [msgvw addSubview:nomsgimg];
    tab_lab.text = message;
    tab_lab.font=delegate.ooredoo;
    tab_lab.textColor=delegate.dimBlack;
    tab_lab.lineBreakMode = NSLineBreakByWordWrapping;
    tab_lab.numberOfLines = 0;
    tab_lab.textAlignment=ALIGN_CENTER;
    [msgvw addSubview:tab_lab];
    [tab addSubview:msgvw];
}
-(int)getHeight:(NSString*)str
{
    UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    CGSize size1 = CGSizeMake(230,9999);
    CGRect textRect = [str boundingRectWithSize:size1 options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:font}
                                        context:nil];
    CGSize size = textRect.size;
    return size.height;
}
- (UIView *)createDemoView{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    UILabel* label =[[UILabel alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
    //UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
    label.text = @"Are you sure to want to cancel this task";
    //[imageView setImage:[UIImage imageNamed:@"demo"]];
    [demoView addSubview:label];
    return demoView;
}
-(void)showAlertCancelTask
{
    alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeWarning];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"CANCEL_TASK", nil) withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_CANCEL", nil) withCustomImage:nil
          withDoneButtonTitle:NSLocalizedString(@"OKAY", nil) andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil) ]];
}
-(void)showAlertStartTask
{
    alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeSuccess];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"START_TASK", nil) withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_START", nil) withCustomImage:nil
          withDoneButtonTitle:NSLocalizedString(@"OKAY", nil) andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]];
}
- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView {
    [alert dismissAlertView];
    [self setStatus];
}
- (void) FCAlertView:(FCAlertView *)alertView clickedButtonIndex:(NSInteger)index buttonTitle:(NSString *)title {
    [alert dismissAlertView];
}
-(void)StartCancelAction:(UIButton*)sender{
    int TagValue = sender.tag;
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]){
        actionType = @"cancel";
        taskID = [[newArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        [self showAlertCancelTask];
    }
    else if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"START", nil)]){
        actionType = @"start";
        taskID = [[newArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        [self showAlertStartTask];
    }
    
}
-(void)OngoingDetailsAction:(UIButton*)sender{
    int TagValue = sender.tag;
    TaskDetailScreen *viewController = [[TaskDetailScreen alloc] init];
    NSString* str = [[ongoingArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    viewController.taskID = str;
    [[self navigationController] pushViewController:viewController animated: YES];
}

-(void)HistoryDetailsAction:(UIButton *)sender{
    int TagValue = sender.tag;
    HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
    NSString* str = [[historyArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    viewController.taskID = str;
    viewController.isManager = FALSE;
    [[self navigationController] pushViewController:viewController animated: YES];
}

-(void)filterView{
    //filterTaskStatus = filterRating = filterTaskType = @"";
    filterView = [[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, 120)];
    filterView.backgroundColor = [UIColor whiteColor];
    filterView.layer.borderWidth = 0.5;
    filterView.layer.borderColor = delegate.borderColor.CGColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor = delegate.headerbgColler;
    [filterView addSubview:headerview];
    
    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(2*delegate.margin, 0,delegate.devicewidth, delegate.headerheight)];
    headerlab.textColor=[UIColor whiteColor];
    headerlab.font=delegate.headFont;
    headerlab.text=@"Filters";
    headerlab.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab];
    
    dateTimePicker = [[UIDatePicker alloc] init];
    dateTimePicker.datePickerMode = UIDatePickerModeDate;
    dateTimePicker.date = [NSDate date];
    [dateTimePicker addTarget:self
                       action:@selector(dateTimeChange:)
             forControlEvents:UIControlEventValueChanged];
    
    //dateTimePicker.minimumDate=[NSDate date];
    [dateTimePicker setFrame: CGRectMake(0,(screenView.frame.size.height-162), screenView.frame.size.width, 162)];
    dateTimePicker.backgroundColor = [UIColor whiteColor];
    dateTimePicker.layer.zPosition = 1;
    [filterView addSubview:dateTimePicker];
    dateTimePicker.hidden = TRUE;
    
    
    int ypos = delegate.headerheight + 20;
    UILabel *fromLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.font = delegate.ooredoo;
    fromLabel.text = @"From";
    [filterView addSubview:fromLabel];
    
    fromDate=[[UITextField alloc] initWithFrame:CGRectMake(30, ypos+20, 120, 25)];
    fromDate.keyboardType=UIKeyboardTypeDefault;
    fromDate.font=delegate.contentFont;
    fromDate.borderStyle=UITextBorderStyleRoundedRect;
    fromDate.delegate=self;
    fromDate.tag = 100;
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    image.image = [UIImage imageNamed:@"calendar"] ;
    fromDate.leftViewMode = UITextFieldViewModeAlways;
    [fromDate setLeftView:image];
    fromDate.placeholder = @"Start Date";
    fromDate.textColor = delegate.dimColor;
    fromDate.borderStyle = UITextBorderStyleNone;
    [fromDate setBackgroundColor:[UIColor clearColor]];
    fromDate.text = [filterData objectForKey:@"fromDate"];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, fromDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [fromDate.layer addSublayer:bottomBorder];
    
    fromDate.autocorrectionType = UITextAutocorrectionTypeNo;
    [filterView addSubview:fromDate];
    
    int xpos = delegate.devicewidth - 140;
    UILabel *toLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, 80, 15)];
    toLabel.textColor = [UIColor blackColor];
    toLabel.font = delegate.ooredoo;
    toLabel.text = @"To";
    [filterView addSubview:toLabel];
    
    toDate=[[UITextField alloc] initWithFrame:CGRectMake(xpos, ypos+20, 120, 25)];
    toDate.keyboardType=UIKeyboardTypeDefault;
    toDate.font=delegate.contentFont;
    toDate.borderStyle=UITextBorderStyleRoundedRect;
    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    image1.image = [UIImage imageNamed:@"calendar"] ;
    toDate.leftViewMode = UITextFieldViewModeAlways;
    [toDate setLeftView:image1];
    toDate.delegate=self;
    toDate.tag = 101;
    toDate.placeholder = @"End Date";
    toDate.textColor = delegate.dimColor;
    toDate.autocorrectionType = UITextAutocorrectionTypeNo;
    toDate.text = [filterData objectForKey:@"toDate"];
    toDate.borderStyle = UITextBorderStyleNone;
    [toDate setBackgroundColor:[UIColor clearColor]];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, toDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [toDate.layer addSublayer:bottomBorder];
    
    [filterView addSubview:toDate];
    
    ypos = ypos + fromLabel.frame.size.height + fromDate.frame.size.height + 20;
    
    UILabel *taskLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 300, 15)];
    taskLabel.textColor = [UIColor blackColor];
    taskLabel.font = delegate.ooredoo;
    taskLabel.text = @"Task ID";
    [filterView addSubview:taskLabel];
    
    ypos = ypos + taskLabel.frame.size.height + 20;
    
    filterTaskId = [[UITextField alloc] initWithFrame:CGRectMake(30, ypos, 300, 25)];
    filterTaskId.keyboardType=UIKeyboardTypeNumberPad;
    filterTaskId.font=delegate.contentFont;
    filterTaskId.borderStyle=UITextBorderStyleRoundedRect;
    filterTaskId.delegate=self;
    filterTaskId.placeholder = @"Task Id";
    filterTaskId.textColor = delegate.dimColor;
    filterTaskId.autocorrectionType = UITextAutocorrectionTypeNo;
    filterTaskId.borderStyle = UITextBorderStyleNone;
    [filterTaskId setBackgroundColor:[UIColor clearColor]];
    filterTaskId.text = [filterData objectForKey:@"taskId"];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, filterTaskId.frame.size.height - 1, filterTaskId.frame.size.width-50, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [filterTaskId.layer addSublayer:bottomBorder];
    
    [filterView addSubview:filterTaskId];
    
    ypos = ypos + filterTaskId.frame.size.height + 20;
    
    UILabel *typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    typeLabel.textColor = [UIColor blackColor];
    typeLabel.font = delegate.ooredoo;
    typeLabel.text = @"Task Type";
    [filterView addSubview:typeLabel];
    
    ypos = ypos + typeLabel.frame.size.height + 20;
    
    TNCircularRadioButtonData *type1 = [TNCircularRadioButtonData new];
    type1.labelText = @"Tasks";
    type1.identifier = @"MyTasks";
    type1.circleActiveColor = type1.labelActiveColor = delegate.dimBlack;
    type1.labelPassiveColor = delegate.dimColor;
    //type1.circlePassiveColor = delegate.dimColor;
    //type1.borderRadius = 12;
    
    //type1.circleRadius = 5;
    
    TNCircularRadioButtonData *type2 = [TNCircularRadioButtonData new];
    type2.labelText = @"Assignments";
    type2.identifier = @"Assignments";
    type2.circleActiveColor = type2.labelActiveColor = delegate.dimBlack;
    type2.circlePassiveColor = type2.labelPassiveColor = delegate.dimColor;
    type2.borderRadius = 12;
    type2.circleRadius = 5;
    
    TNCircularRadioButtonData *type3 = [TNCircularRadioButtonData new];
    type3.labelText = @"Both";
    //type3.labelFont = delegate.contentFont;
    type3.circleActiveColor = type3.labelActiveColor = delegate.dimBlack;
    type3.circlePassiveColor = type3.labelPassiveColor = delegate.dimColor;
    type3.identifier = @"Both";
    //type3.selected = YES;
    type3.borderRadius = 12;
    type3.circleRadius = 5;
    
    if ([[filterData objectForKey:@"taskType"] isEqualToString:@"MyTasks"])
    {
        type1.selected = YES;
    }
    else if ([[filterData objectForKey:@"taskType"] isEqualToString:@"Assignments"])
    {
        type2.selected = YES;
    }
    else
        type3.selected = YES;

    
    typeGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[type1, type2, type3] layout:TNRadioButtonGroupLayoutHorizontal];
    typeGroup.identifier = @"My group";
    [typeGroup create];
    //typeGroup.position = CGPointMake(30, ypos);
    typeGroup.labelFont = delegate.contentFont;
    typeGroup.labelColor = delegate.dimBlack;
    typeGroup.frame = CGRectMake(30, ypos, 300, 20);
    [filterView addSubview:typeGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(typeGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:typeGroup];
    
    ypos = ypos + typeGroup.frame.size.height + 20;
    
    
    UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 120, 15)];
    statusLabel.textColor = [UIColor blackColor];
    statusLabel.font = delegate.ooredoo;
    statusLabel.text = @"Request Status";
    [filterView addSubview:statusLabel];
    
    ypos = ypos + typeLabel.frame.size.height + 20;
    
    TNCircularRadioButtonData *status1 = [TNCircularRadioButtonData new];
    status1.labelText = @"Assigned";
    status1.identifier = @"Assigned";
    status1.circleActiveColor = status1.labelActiveColor = delegate.dimBlack;
    status1.circlePassiveColor = status1.labelPassiveColor = delegate.dimColor;
    status1.borderRadius = 12;
    
    status1.circleRadius = 5;
    
    TNCircularRadioButtonData *status2 = [TNCircularRadioButtonData new];
    status2.labelText = @"New";
    status2.identifier = @"New";
    status2.circleActiveColor = status2.labelActiveColor = delegate.dimBlack;
    status2.circlePassiveColor = status2.labelPassiveColor = delegate.dimColor;
    status2.borderRadius = 12;
    status2.circleRadius = 5;
    
    TNCircularRadioButtonData *status3 = [TNCircularRadioButtonData new];
    status3.labelText = @"Both";
    //status3.labelFont = delegate.contentFont;
    status3.circleActiveColor = status3.labelActiveColor = delegate.dimBlack;
    status3.circlePassiveColor = status3.labelPassiveColor = delegate.dimColor;
    status3.identifier = @"Both";
    //status3.selected = YES;
    status3.borderRadius = 12;
    status3.circleRadius = 5;
    
    if ([[filterData objectForKey:@"taskStatus"] isEqualToString:@"Assigned"])
    {
        status1.selected = YES;
    }
    else if ([[filterData objectForKey:@"taskStatus"] isEqualToString:@"New"])
    {
        status2.selected = YES;
    }
    else
        status3.selected = YES;

    
    statusGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[status1, status2, status3] layout:TNRadioButtonGroupLayoutHorizontal];
    statusGroup.identifier = @"Status group";
    [statusGroup create];
    statusGroup.position = CGPointMake(30, ypos);
    statusGroup.labelFont = delegate.contentFont;
    statusGroup.labelColor = delegate.dimBlack;
    //statusGroup = CGRectMake(30, ypos, 300, 20);
    [filterView addSubview:statusGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:statusGroup];
    
    ypos = ypos + statusGroup.frame.size.height + 30;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, 40)];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: footerView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    //footerView.layer.mask = maskLayer1;
    [filterView addSubview:footerView];
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2-10, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.normalFont;
    [submitButton setTitle:NSLocalizedString(@"APPLY_FILTER", nil) forState:UIControlStateNormal];
    submitButton.layer.cornerRadius = 5.0;
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag=1000;
    [footerView addSubview:submitButton];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2+10, 0, footerView.frame.size.width/2-10, 40)];
    cancelButton.backgroundColor = [UIColor whiteColor];
    cancelButton.clipsToBounds = YES;
    cancelButton.titleLabel.font = delegate.normalFont;
    [cancelButton setTitleColor: delegate.redColor forState:UIControlStateNormal];
    cancelButton.layer.cornerRadius = 5.0;
    cancelButton.layer.borderColor = delegate.redColor.CGColor;
    cancelButton.layer.borderWidth =  1.0;
    [cancelButton setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.tag=1001;
    [footerView addSubview:cancelButton];
    
    
    ypos = ypos + submitButton.frame.size.height + 20;
    
    filterView.frame = CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight);
    
    NavBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(dateTimePicker.frame.origin.x, dateTimePicker.frame.origin.y-44, dateTimePicker.frame.size.width, 44)];
    NavBar.barStyle = UIBarStyleDefault;
    NavBar.backgroundColor = [UIColor grayColor];
    [filterView addSubview:NavBar];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(DonePicker)];
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = backButton;
    NavBar.items = @[ navItem ];
    NavBar.hidden = TRUE;

    
    [self.view addSubview:filterView];
    //[screenView bringSubviewToFront:filterView];
}

- (void)typeGroupUpdated:(NSNotification *)notification {
    filterTaskType = typeGroup.selectedRadioButton.data.identifier;
}
- (void)ratingGroupUpdated:(NSNotification *)notification {
    filterRating = ratingGroup.selectedRadioButton.data.identifier;
}
- (void)statusGroupUpdated:(NSNotification *)notification {
    filterTaskStatus = statusGroup.selectedRadioButton.data.identifier;
}
-(void) startTheBgForFilter
{
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:@"post" forKey:@"action"];
    [_params setObject:[NSString stringWithFormat:@"%@", [delegate.dataFull objectForKey:@"handsetId"]] forKey:@"handsetId"];
    if (1 == taskType)
        [_params setObject:@"myTaskShow_new" forKey:@"filterType"];
    else if (2 == taskType)
        [_params setObject:@"myTaskShow_ongoing" forKey:@"filterType"];
    else if (3 == taskType)
        [_params setObject:@"myTaskShow_history" forKey:@"filterType"];
            
    [_params setObject:[NSString stringWithFormat:@"%@", fromDate.text] forKey:@"fromDate"];
    [_params setObject:[NSString stringWithFormat:@"%@", toDate.text] forKey:@"toDate"];
    
    [filterData setObject:fromDate.text forKey:@"fromDate"];
    [filterData setObject:toDate.text forKey:@"toDate"];
    
    if (filterTaskId.text != nil)
    {
        [_params setObject:[NSString stringWithFormat:@"%@", filterTaskId.text] forKey:@"taskId"];
        [filterData setObject:filterTaskId.text forKey:@"taskId"];
    }
    if (filterTaskType != nil)
    {
        [_params setObject:[NSString stringWithFormat:@"%@", filterTaskType] forKey:@"taskType"];
        [filterData setObject:filterTaskType forKey:@"taskType"];
    }
    if (filterRating != nil)
    {
        [_params setObject:[NSString stringWithFormat:@"%@", filterRating] forKey:@"rating"];
        [filterData setObject:filterRating forKey:@"taskRating"];
    }
    if (filterTaskStatus != nil)
    {
        [_params setObject:[NSString stringWithFormat:@"%@", filterTaskStatus] forKey:@"taskStatus"];
        [filterData setObject:filterTaskStatus forKey:@"taskStatus"];
    }

     NSString* url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/filter"];
    
    [self startTheBg_API:_params url:url completionHandler:^(id data, NSError *error) {
        isServerResponded = TRUE;
        [self stopProgBar];
        NSDictionary *reqdataDict = data;
        if([[reqdataDict objectForKey:@"status"] isEqualToString:@"OK"])
        {
            NSArray *array = [[NSArray alloc] init];
            if (1 == taskType)
            {
                if ([reqdataDict objectForKey:@"data1"] != [NSNull null])
                {
                    array = [reqdataDict objectForKey:@"data1"];
                    newArray = [array mutableCopy];
                }
                else
                {
                    newArray = [array mutableCopy];
                }
                
            }
            if (2 == taskType)
            {
                if ([reqdataDict objectForKey:@"data2"] != [NSNull null])
                {
                    array = [[NSArray alloc] init];
                    array = [reqdataDict objectForKey:@"data2"];
                    ongoingArray = [array mutableCopy];
                }
                else
                {
                    ongoingArray = [array mutableCopy];
                }
            }
            if (3 == taskType)
            {
                if ([reqdataDict objectForKey:@"data3"] != [NSNull null])
                {
                    array = [[NSArray alloc] init];
                    array = [reqdataDict objectForKey:@"data3"];
                    historyArray = [array mutableCopy];
                }
                else
                {
                    historyArray = [array mutableCopy];
                }
            }
            [tab reloadData];

        }
        
        else {
            NSString* errorMsg = [reqdataDict objectForKey:@"msg"];
            SSSnackbar *snackbar = [delegate snackbarForQuickRunningItem:errorMsg];
            [snackbar show];
        }
        
    }];
}

-(void)filterViewHistory{
    filterView = [[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, 120)];
    filterView.backgroundColor = [UIColor whiteColor];
    filterView.layer.borderWidth = 0.5;
    filterView.layer.borderColor = delegate.borderColor.CGColor;
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor = delegate.headerbgColler;
    [filterView addSubview:headerview];
    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(2*delegate.margin, 0,delegate.devicewidth, delegate.headerheight)];
    headerlab.textColor=[UIColor whiteColor];
    headerlab.font=delegate.headFont;
    headerlab.text=@"Filters";
    headerlab.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab];
    
    dateTimePicker = [[UIDatePicker alloc] init];
    dateTimePicker.datePickerMode = UIDatePickerModeDate;
    dateTimePicker.date = [NSDate date];
    [dateTimePicker addTarget:self
                       action:@selector(dateTimeChange:)
             forControlEvents:UIControlEventValueChanged];
    
    //dateTimePicker.minimumDate=[NSDate date];
    [dateTimePicker setFrame: CGRectMake(0,(screenView.frame.size.height-162), screenView.frame.size.width, 162)];
    dateTimePicker.backgroundColor = [UIColor whiteColor];
    dateTimePicker.layer.zPosition = 1;
    [filterView addSubview:dateTimePicker];
    dateTimePicker.hidden = TRUE;
    
    int ypos = delegate.headerheight + 20;
    UILabel *fromLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.font = delegate.ooredoo;
    fromLabel.text = @"From";
    [filterView addSubview:fromLabel];
    
    fromDate=[[UITextField alloc] initWithFrame:CGRectMake(30, ypos+20, 120, 25)];
    fromDate.keyboardType=UIKeyboardTypeDefault;
    fromDate.font=delegate.contentFont;
    fromDate.borderStyle=UITextBorderStyleRoundedRect;
    fromDate.delegate=self;
    fromDate.tag = 100;
    fromDate.text = [filterData objectForKey:@"fromDate"];
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    image.image = [UIImage imageNamed:@"calendar"] ;
    fromDate.leftViewMode = UITextFieldViewModeAlways;
    [fromDate setLeftView:image];
    fromDate.placeholder = @"Start Date";
    fromDate.textColor = delegate.dimColor;
    fromDate.borderStyle = UITextBorderStyleNone;
    [fromDate setBackgroundColor:[UIColor clearColor]];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, fromDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [fromDate.layer addSublayer:bottomBorder];
    
    fromDate.autocorrectionType = UITextAutocorrectionTypeNo;
    [filterView addSubview:fromDate];
    
    int xpos = delegate.devicewidth - 140;
    UILabel *toLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, 80, 15)];
    toLabel.textColor = [UIColor blackColor];
    toLabel.font = delegate.ooredoo;
    toLabel.text = @"To";
    [filterView addSubview:toLabel];
    
    toDate=[[UITextField alloc] initWithFrame:CGRectMake(xpos, ypos+20, 120, 25)];
    toDate.keyboardType=UIKeyboardTypeDefault;
    toDate.font=delegate.contentFont;
    toDate.borderStyle=UITextBorderStyleRoundedRect;
    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    image1.image = [UIImage imageNamed:@"calendar"] ;
    toDate.leftViewMode = UITextFieldViewModeAlways;
    [toDate setLeftView:image1];
    toDate.delegate=self;
    toDate.tag = 101;
    toDate.placeholder = @"End Date";
    toDate.textColor = delegate.dimColor;
    toDate.autocorrectionType = UITextAutocorrectionTypeNo;
    
    toDate.borderStyle = UITextBorderStyleNone;
    toDate.text = [filterData objectForKey:@"toDate"];
    [toDate setBackgroundColor:[UIColor clearColor]];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, toDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [toDate.layer addSublayer:bottomBorder];
    
    [filterView addSubview:toDate];
    
    ypos = ypos + fromLabel.frame.size.height + fromDate.frame.size.height + 20;
    
    UILabel *taskLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 300, 15)];
    taskLabel.textColor = [UIColor blackColor];
    taskLabel.font = delegate.ooredoo;
    taskLabel.text = @"Task ID";
    [filterView addSubview:taskLabel];
    
    ypos = ypos + taskLabel.frame.size.height + 20;
    
    filterTaskId = [[UITextField alloc] initWithFrame:CGRectMake(30, ypos, 300, 25)];
    filterTaskId.keyboardType=UIKeyboardTypeNumberPad;
    filterTaskId.font=delegate.contentFont;
    filterTaskId.borderStyle=UITextBorderStyleRoundedRect;
    filterTaskId.delegate=self;
    filterTaskId.placeholder = @"Task Id";
    filterTaskId.textColor = delegate.dimColor;
    filterTaskId.autocorrectionType = UITextAutocorrectionTypeNo;
    
    filterTaskId.borderStyle = UITextBorderStyleNone;
    
    filterTaskId.text = [filterData objectForKey:@"taskId"];
    
    [filterTaskId setBackgroundColor:[UIColor clearColor]];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, filterTaskId.frame.size.height - 1, filterTaskId.frame.size.width-50, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [filterTaskId.layer addSublayer:bottomBorder];
    
    [filterView addSubview:filterTaskId];
    
    ypos = ypos + filterTaskId.frame.size.height + 20;
    
    UILabel *typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    typeLabel.textColor = [UIColor blackColor];
    typeLabel.font = delegate.ooredoo;
    typeLabel.text = @"Task Type";
    [filterView addSubview:typeLabel];
    
    ypos = ypos + typeLabel.frame.size.height + 20;
    
    TNCircularRadioButtonData *type1 = [TNCircularRadioButtonData new];
    type1.labelText = @"Task";
    type1.identifier = @"MyTasks";
    type1.circleActiveColor = type1.labelActiveColor = delegate.dimBlack;
    type1.circlePassiveColor = type1.labelPassiveColor = delegate.dimColor;
    type1.borderRadius = 12;
    
    type1.circleRadius = 5;
    
    TNCircularRadioButtonData *type2 = [TNCircularRadioButtonData new];
    type2.labelText = @"Assignments";
    type2.identifier = @"Assignments";
    type2.circleActiveColor = type2.labelActiveColor = delegate.dimBlack;
    type2.circlePassiveColor = type2.labelPassiveColor = delegate.dimColor;
    type2.borderRadius = 12;
    type2.circleRadius = 5;
    
    TNCircularRadioButtonData *type3 = [TNCircularRadioButtonData new];
    type3.labelText = @"Both";
    //type3.labelFont = delegate.contentFont;
    type3.circleActiveColor = type3.labelActiveColor = delegate.dimBlack;
    type3.circlePassiveColor = type3.labelPassiveColor = delegate.dimColor;
    type3.identifier = @"Both";
    //type3.selected = YES;
    type3.borderRadius = 12;
    type3.circleRadius = 5;
    
    if ([[filterData objectForKey:@"taskType"] isEqualToString:@"MyTasks"])
    {
        type1.selected = YES;
    }
    else if ([[filterData objectForKey:@"taskType"] isEqualToString:@"Assignments"])
    {
        type2.selected = YES;
    }
    else
        type3.selected = YES;

    
    typeGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[type1, type2, type3] layout:TNRadioButtonGroupLayoutHorizontal];
    typeGroup.identifier = @"My group";
    [typeGroup create];
    //typeGroup.position = CGPointMake(30, ypos);
    typeGroup.labelFont = delegate.contentFont;
    typeGroup.labelColor = delegate.dimBlack;
    typeGroup.frame = CGRectMake(30, ypos, 300, 20);
    [filterView addSubview:typeGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(typeGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:typeGroup];
    
    ypos = ypos + typeGroup.frame.size.height + 20;
    
    
    UILabel *ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    ratingLabel.textColor = [UIColor blackColor];
    ratingLabel.font = delegate.ooredoo;
    ratingLabel.text = @"Rating";
    [filterView addSubview:ratingLabel];
    
    ypos = ypos + typeLabel.frame.size.height + 20;
    
    TNCircularRadioButtonData *rating1 = [TNCircularRadioButtonData new];
    rating1.labelText = @"Lowest";
    rating1.identifier = @"Lowest";
    rating1.circleActiveColor = rating1.labelActiveColor = delegate.dimBlack;
    rating1.circlePassiveColor = rating1.labelPassiveColor = delegate.dimColor;
    rating1.borderRadius = 12;
    
    rating1.circleRadius = 5;
    
    TNCircularRadioButtonData *rating2 = [TNCircularRadioButtonData new];
    rating2.labelText = @"Highest";
    rating2.identifier = @"Highest";
    rating2.circleActiveColor = rating2.labelActiveColor = delegate.dimBlack;
    rating2.circlePassiveColor = rating2.labelPassiveColor = delegate.dimColor;
    rating2.borderRadius = 12;
    rating2.circleRadius = 5;
    
    TNCircularRadioButtonData *rating3 = [TNCircularRadioButtonData new];
    rating3.labelText = @"All";
    //rating3.labelFont = delegate.contentFont;
    rating3.circleActiveColor = rating3.labelActiveColor = delegate.dimBlack;
    rating3.circlePassiveColor = rating3.labelPassiveColor = delegate.dimColor;
    rating3.identifier = @"Both";
    //rating3.selected = YES;
    rating3.borderRadius = 12;
    rating3.circleRadius = 5;
    
    if ([[filterData objectForKey:@"taskRating"] isEqualToString:@"Lowest"])
    {
        rating1.selected = YES;
    }
    else if ([[filterData objectForKey:@"taskRating"] isEqualToString:@"Highest"])
    {
        rating2.selected = YES;
    }
    else
        rating3.selected = YES;

    
    ratingGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[rating1, rating2, rating3] layout:TNRadioButtonGroupLayoutHorizontal];
    ratingGroup.identifier = @"Rating group";
    [ratingGroup create];
    ratingGroup.position = CGPointMake(30, ypos);
    ratingGroup.labelFont = delegate.contentFont;
    ratingGroup.labelColor = delegate.dimBlack;
    //ratingGroup = CGRectMake(30, ypos, 300, 20);
    [filterView addSubview:ratingGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ratingGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:ratingGroup];
    
    ypos = ypos + ratingGroup.frame.size.height + 20;
    
    UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 120, 15)];
    statusLabel.textColor = [UIColor blackColor];
    statusLabel.font = delegate.ooredoo;
    statusLabel.text = @"Request Status";
    [filterView addSubview:statusLabel];
    
    ypos = ypos + typeLabel.frame.size.height + 20;
    
    TNCircularRadioButtonData *status1 = [TNCircularRadioButtonData new];
    status1.labelText = @"Approved";
    status1.identifier = @"Approved";
    status1.circleActiveColor = status1.labelActiveColor = delegate.dimBlack;
    status1.circlePassiveColor = status1.labelPassiveColor = delegate.dimColor;
    status1.borderRadius = 12;
    
    status1.circleRadius = 5;
    
    TNCircularRadioButtonData *status2 = [TNCircularRadioButtonData new];
    status2.labelText = @"Declined";
    status2.identifier = @"Declined";
    status2.circleActiveColor = status2.labelActiveColor = delegate.dimBlack;
    status2.circlePassiveColor = status2.labelPassiveColor = delegate.dimColor;
    status2.borderRadius = 12;
    status2.circleRadius = 5;
    
    TNCircularRadioButtonData *status3 = [TNCircularRadioButtonData new];
    status3.labelText = @"Both";
    //status3.labelFont = delegate.contentFont;
    status3.circleActiveColor = status3.labelActiveColor = delegate.dimBlack;
    status3.circlePassiveColor = status3.labelPassiveColor = delegate.dimColor;
    status3.identifier = @"Both";
    //status3.selected = YES;
    status3.borderRadius = 12;
    status3.circleRadius = 5;
    
    if ([[filterData objectForKey:@"taskStatus"] isEqualToString:@"Approved"])
    {
        status1.selected = YES;
    }
    else if ([[filterData objectForKey:@"taskStatus"] isEqualToString:@"Declined"])
    {
        status2.selected = YES;
    }
    else
        status3.selected = YES;

    
    statusGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[status1, status2, status3] layout:TNRadioButtonGroupLayoutHorizontal];
    statusGroup.identifier = @"Status group";
    [statusGroup create];
    statusGroup.position = CGPointMake(30, ypos);
    statusGroup.labelFont = delegate.contentFont;
    statusGroup.labelColor = delegate.dimBlack;
    //statusGroup = CGRectMake(30, ypos, 300, 20);
    [filterView addSubview:statusGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:statusGroup];
    
    ypos = ypos + statusGroup.frame.size.height + 30;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, 40)];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: footerView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    //footerView.layer.mask = maskLayer1;
    [filterView addSubview:footerView];
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2-10, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.normalFont;
    [submitButton setTitle:NSLocalizedString(@"APPLY_FILTER", nil) forState:UIControlStateNormal];
    submitButton.layer.cornerRadius = 5.0;
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag=1000;
    [footerView addSubview:submitButton];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2+10, 0, footerView.frame.size.width/2-10, 40)];
    cancelButton.backgroundColor = [UIColor whiteColor];
    cancelButton.clipsToBounds = YES;
    cancelButton.titleLabel.font = delegate.normalFont;
    [cancelButton setTitleColor: delegate.redColor forState:UIControlStateNormal];
    cancelButton.layer.cornerRadius = 5.0;
    cancelButton.layer.borderColor = delegate.redColor.CGColor;
    cancelButton.layer.borderWidth =  1.0;
    [cancelButton setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.tag=1001;
    [footerView addSubview:cancelButton];
    
    ypos = ypos + submitButton.frame.size.height + 20;
    
    filterView.frame = CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight);
    
    //tab.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    NavBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(dateTimePicker.frame.origin.x, dateTimePicker.frame.origin.y-44, dateTimePicker.frame.size.width, 44)];
    NavBar.barStyle = UIBarStyleDefault;
    NavBar.backgroundColor = [UIColor grayColor];
    [filterView addSubview:NavBar];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(DonePicker)];
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = backButton;
    NavBar.items = @[ navItem ];
    NavBar.hidden = TRUE;
    [self.view addSubview:filterView];
    //[screenView bringSubviewToFront:filterView];
}

-(void)filterViewOutgoing {
    
    //filterTaskStatus = filterRating = filterTaskType = @"";
    filterView = [[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, 120)];
    filterView.backgroundColor = [UIColor whiteColor];
    filterView.layer.borderWidth = 0.5;
    filterView.layer.borderColor = delegate.borderColor.CGColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor = delegate.headerbgColler;
    [filterView addSubview:headerview];
    
    
    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(2*delegate.margin, 0,delegate.devicewidth, delegate.headerheight)];
    headerlab.textColor=[UIColor whiteColor];
    headerlab.font=delegate.headFont;
    headerlab.text=@"Filters";
    headerlab.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab];

    
    dateTimePicker = [[UIDatePicker alloc] init];
    dateTimePicker.datePickerMode = UIDatePickerModeDate;
    dateTimePicker.date = [NSDate date];
    [dateTimePicker addTarget:self
                       action:@selector(dateTimeChange:)
             forControlEvents:UIControlEventValueChanged];
    
    //dateTimePicker.minimumDate=[NSDate date];
    [dateTimePicker setFrame: CGRectMake(0,(screenView.frame.size.height-162), screenView.frame.size.width, 162)];
    dateTimePicker.backgroundColor = [UIColor whiteColor];
    dateTimePicker.layer.zPosition = 1;
    [filterView addSubview:dateTimePicker];
    dateTimePicker.hidden = TRUE;
    
    int ypos = delegate.headerheight + 20;
    UILabel *fromLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.font = delegate.ooredoo;
    fromLabel.text = @"From";
    [filterView addSubview:fromLabel];
    
    fromDate=[[UITextField alloc] initWithFrame:CGRectMake(30, ypos+20, 120, 25)];
    fromDate.keyboardType=UIKeyboardTypeDefault;
    fromDate.font=delegate.contentFont;
    fromDate.borderStyle=UITextBorderStyleRoundedRect;
    fromDate.delegate=self;
    fromDate.tag = 100;
    fromDate.text = [filterData objectForKey:@"fromDate"];
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    image.image = [UIImage imageNamed:@"calendar"] ;
    fromDate.leftViewMode = UITextFieldViewModeAlways;
    [fromDate setLeftView:image];
    fromDate.placeholder = @"Start Date";
    fromDate.textColor = delegate.dimColor;
    fromDate.borderStyle = UITextBorderStyleNone;
    [fromDate setBackgroundColor:[UIColor clearColor]];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, fromDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [fromDate.layer addSublayer:bottomBorder];
    
    fromDate.autocorrectionType = UITextAutocorrectionTypeNo;
    [filterView addSubview:fromDate];
    
    int xpos = delegate.devicewidth - 140;
    UILabel *toLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, 80, 15)];
    toLabel.textColor = [UIColor blackColor];
    toLabel.font = delegate.ooredoo;
    toLabel.text = @"To";
    [filterView addSubview:toLabel];
    
    toDate=[[UITextField alloc] initWithFrame:CGRectMake(xpos, ypos+20, 120, 25)];
    toDate.keyboardType=UIKeyboardTypeDefault;
    toDate.font=delegate.contentFont;
    toDate.borderStyle=UITextBorderStyleRoundedRect;
    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    image1.image = [UIImage imageNamed:@"calendar"] ;
    toDate.leftViewMode = UITextFieldViewModeAlways;
    [toDate setLeftView:image1];
    toDate.delegate=self;
    toDate.tag = 101;
    toDate.placeholder = @"End Date";
    toDate.textColor = delegate.dimColor;
    toDate.autocorrectionType = UITextAutocorrectionTypeNo;
    
    toDate.borderStyle = UITextBorderStyleNone;
    toDate.text = [filterData objectForKey:@"toDate"];
    [toDate setBackgroundColor:[UIColor clearColor]];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, toDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [toDate.layer addSublayer:bottomBorder];
    
    [filterView addSubview:toDate];
    
    ypos = ypos + fromLabel.frame.size.height + fromDate.frame.size.height + 20;
    
    UILabel *taskLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 300, 15)];
    taskLabel.textColor = [UIColor blackColor];
    taskLabel.font = delegate.ooredoo;
    taskLabel.text = @"Task ID";
    [filterView addSubview:taskLabel];
    
    ypos = ypos + taskLabel.frame.size.height + 20;
    
    filterTaskId = [[UITextField alloc] initWithFrame:CGRectMake(30, ypos, 300, 25)];
    filterTaskId.keyboardType=UIKeyboardTypeNumberPad;
    filterTaskId.font=delegate.contentFont;
    filterTaskId.borderStyle=UITextBorderStyleRoundedRect;
    filterTaskId.delegate=self;
    filterTaskId.placeholder = @"Task Id";
    filterTaskId.textColor = delegate.dimColor;
    filterTaskId.autocorrectionType = UITextAutocorrectionTypeNo;
    filterTaskId.text = [filterData objectForKey:@"taskId"];
    filterTaskId.borderStyle = UITextBorderStyleNone;
    [filterTaskId setBackgroundColor:[UIColor clearColor]];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, filterTaskId.frame.size.height - 1, filterTaskId.frame.size.width-50, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [filterTaskId.layer addSublayer:bottomBorder];
    
    [filterView addSubview:filterTaskId];
    
    ypos = ypos + filterTaskId.frame.size.height + 20;
    
    UILabel *typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    typeLabel.textColor = [UIColor blackColor];
    typeLabel.font = delegate.ooredoo;
    typeLabel.text = @"Task Type";
    [filterView addSubview:typeLabel];
    
    ypos = ypos + typeLabel.frame.size.height + 20;
    
    TNCircularRadioButtonData *type1 = [TNCircularRadioButtonData new];
    type1.labelText = @"Task";
    type1.identifier = @"MyTasks";
    type1.circleActiveColor = type1.labelActiveColor = delegate.dimBlack;
    type1.circlePassiveColor = type1.labelPassiveColor = delegate.dimColor;
    type1.borderRadius = 12;
    
    type1.circleRadius = 5;
    
    TNCircularRadioButtonData *type2 = [TNCircularRadioButtonData new];
    type2.labelText = @"Assignments";
    type2.identifier = @"Assignments";
    type2.circleActiveColor = type2.labelActiveColor = delegate.dimBlack;
    type2.circlePassiveColor = type2.labelPassiveColor = delegate.dimColor;
    type2.borderRadius = 12;
    type2.circleRadius = 5;
    
    TNCircularRadioButtonData *type3 = [TNCircularRadioButtonData new];
    type3.labelText = @"Both";
   // type3.labelFont = delegate.contentFont;
    type3.circleActiveColor = type3.labelActiveColor = delegate.dimBlack;
    type3.circlePassiveColor = type3.labelPassiveColor = delegate.dimColor;
    type3.identifier = @"Both";
    //type3.selected = YES;
    type3.borderRadius = 12;
    type3.circleRadius = 5;
    
    if ([[filterData objectForKey:@"taskType"] isEqualToString:@"MyTasks"])
    {
        type1.selected = YES;
    }
    else if ([[filterData objectForKey:@"taskType"] isEqualToString:@"Assignments"])
    {
        type2.selected = YES;
    }
    else
        type3.selected = YES;
    
    typeGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[type1, type2, type3] layout:TNRadioButtonGroupLayoutHorizontal];
    typeGroup.identifier = @"My group";
    [typeGroup create];
    //typeGroup.position = CGPointMake(30, ypos);
    typeGroup.labelFont = delegate.contentFont;
    typeGroup.labelColor = delegate.dimBlack;
    typeGroup.frame = CGRectMake(30, ypos, 300, 20);
    [filterView addSubview:typeGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(typeGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:typeGroup];
    
    ypos = ypos + typeGroup.frame.size.height + 30;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, 40)];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: footerView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    //footerView.layer.mask = maskLayer1;
    [filterView addSubview:footerView];
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2-10, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.normalFont;
    [submitButton setTitle:NSLocalizedString(@"APPLY_FILTER", nil) forState:UIControlStateNormal];
    submitButton.layer.cornerRadius = 5.0;
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag=1000;
    [footerView addSubview:submitButton];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2+10, 0, footerView.frame.size.width/2-10, 40)];
    cancelButton.backgroundColor = [UIColor whiteColor];
    cancelButton.clipsToBounds = YES;
    cancelButton.titleLabel.font = delegate.normalFont;
    [cancelButton setTitleColor: delegate.redColor forState:UIControlStateNormal];
    cancelButton.layer.cornerRadius = 5.0;
    cancelButton.layer.borderColor = delegate.redColor.CGColor;
    cancelButton.layer.borderWidth =  1.0;
    [cancelButton setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    [cancelButton setTitleColor:delegate.redColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.tag=1001;
    [footerView addSubview:cancelButton];
    
    ypos = ypos + submitButton.frame.size.height + 20;
    
    filterView.frame = CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight);
    
    //tab.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    
    NavBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(dateTimePicker.frame.origin.x, dateTimePicker.frame.origin.y-44, dateTimePicker.frame.size.width, 44)];
    NavBar.barStyle = UIBarStyleDefault;
    NavBar.backgroundColor = [UIColor grayColor];
    [filterView addSubview:NavBar];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(DonePicker)];
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = backButton;
    NavBar.items = @[ navItem ];
    NavBar.hidden = TRUE;
    
    [self.view addSubview:filterView];
}
-(void)DoneBtn {
    dateTimePicker.hidden = TRUE;
    dateTimePicker.hidden = TRUE;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (100 == textField.tag){
        isFromDate = TRUE;
        toDate.text = @"";
        [fromDate resignFirstResponder];
        dateTimePicker.hidden = FALSE;
        NavBar.hidden = FALSE;
    }
    else if (101 == textField.tag){
        isFromDate = FALSE;
        [toDate resignFirstResponder];
        dateTimePicker.hidden = FALSE;
         NavBar.hidden = FALSE;
    }
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
