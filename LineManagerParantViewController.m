//
//  LineManagerParantViewController.m
//  iWork
//
//  Created by Shailendra on 31/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "LineManagerParantViewController.h"
#import "Header.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define kDefaultEdgeInsets UIEdgeInsetsMake(5, 10, 5, 10)

@interface LineManagerParantViewController (){
   
    NSArray *btnArray;
    AppDelegate *delegate;
    IBOutlet UIImageView *ProfileImageView;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *UserNameLabel;
    IBOutlet UILabel *NatificationLabel;
    NSString *isGetNotification;
}
@property (weak) IBOutlet UIScrollView *containerScrollView;
@property (weak) IBOutlet UIScrollView *menuScrollView;
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray;
-(void) buttonPressed:(id) sender;
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets;
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr;
@end

@implementation LineManagerParantViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    
    UserNameLabel.text = @"";
    NatificationLabel.layer.cornerRadius = 9.0;
    NatificationLabel.clipsToBounds = YES;
    NatificationLabel.backgroundColor = delegate.yellowColor;
    NatificationLabel.textColor = delegate.redColor;

    
    ProfileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    ProfileImageView.layer.borderWidth = 2;
    ProfileImageView.layer.cornerRadius = 27;
    ProfileImageView.clipsToBounds = YES;
    
    NSDictionary *Dic = [ApplicationState getUserLoginData];
    
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
        UserNameLabel.text = [NSString stringWithFormat:@"Hi, %@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]];
    }
    else{
        UserNameLabel.text = @" ";
    }
    
    NSString *URL = [NSString stringWithFormat:@"%@images/%@",BASE_URL,[[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]];
    URL = [URL stringByReplacingOccurrencesOfString:@"iworkapi" withString:@"iwork"];
    
    NSLog(@"URL==%@",URL);
    
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]))){
        NSURL *Url = [NSURL URLWithString:URL];
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL:Url]];
        if(images == nil){
            NSLog(@"image nil");
            ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            
            [ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
    }
    
//    if(IsSafeStringPlus(TrToString(delegate.dataFull[@"dp"]))){
//        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:delegate.dataFull[@"dp"]]]];
//        if(images == nil){
//            NSLog(@"image nil");
//            ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
//        }
//        else{
//            NSString *URL = [NSString stringWithFormat:@"%@",delegate.dataFull[@"dp"]];
//            [ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
//        }
//    }
//    else{
//        ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
//    }
//    if(IsSafeStringPlus(TrToString(delegate.dataFull[@"name"]))){
//        UserNameLabel.text = [NSString stringWithFormat:@"%@", delegate.dataFull[@"name"]];
//    }
//    else{
//        UserNameLabel.text = @"";
//    }
//    if(IsSafeStringPlus(TrToString(delegate.dataFull[@"notification_count"]))){
//        NatificationLabel.text = [NSString stringWithFormat:@"%@", delegate.dataFull[@"notification_count"]];
//    }
//    else{
//        NatificationLabel.text = @"";
//    }
//    if([NatificationLabel.text integerValue] == 0){
//        NatificationLabel.alpha = 0;
//    }
//    else{
//        NatificationLabel.alpha = 1;
//    }
    
    NSString *HeaderStr = NSLocalizedString(@"IPM", nil);
    HeaderStr = [HeaderStr stringByReplacingOccurrencesOfString:@"iPM" withString:[NSString stringWithFormat:@"%@",[@"&#9432;PM" stringByConvertingHTMLToPlainText]]];
    HeaderLabel.text = HeaderStr;
    
    btnArray = [[NSArray alloc]initWithObjects: NSLocalizedString(@"MY_TASK", nil),NSLocalizedString(@"MY_TEAM", nil), nil];
    [self addButtonsInScrollMenu:btnArray];
   
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    LinemanagerMyTaskViewController *oneVC = [storyBoard instantiateViewControllerWithIdentifier:@"LinemanagerMyTaskViewController"];
  

    LineManagerMyTeamViewController *twoVC = [storyBoard instantiateViewControllerWithIdentifier:@"LineManagerMyTeamViewController"];
    NSArray *controllerArray = @[oneVC, twoVC];
    [self addChildViewControllersOntoContainer:controllerArray];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNotifications) name:@"Notification" object:nil];
    
}
#pragma mark - Add Menu Buttons in Menu Scroll View
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray
{
    CGFloat buttonHeight = self.menuScrollView.frame.size.height;
    CGFloat cWidth = 0.0f;
    
    for (int i = 0 ; i<buttonArray.count; i++)
    {
        NSString *tagTitle = [buttonArray objectAtIndex:i];
        
        CGFloat buttonWidth = [self widthForMenuTitle:tagTitle buttonEdgeInsets:kDefaultEdgeInsets];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if(i == 0 ){
            button.frame = CGRectMake(cWidth, 0.0f, SCREEN_WIDTH/2-20, buttonHeight);
        }
        else{
            button.frame = CGRectMake(SCREEN_WIDTH/2+20, 0.0f, SCREEN_WIDTH/2-20, buttonHeight);
        }
        [button setTitle:tagTitle forState:UIControlStateNormal];
        button.titleLabel.font = delegate.normalFont;
        
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [self.menuScrollView addSubview:button];
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(20, button.frame.size.height - 5, buttonWidth+30, 2)];
        bottomView.backgroundColor = [UIColor redColor];
        bottomView.tag = 1001;
        [button addSubview:bottomView];
        if (i == 0){
            button.selected = YES;
            [bottomView setHidden:NO];
        }
        else{
            [bottomView setHidden:YES];
        }
        cWidth += buttonWidth;
    }
    self.menuScrollView.contentSize = CGSizeMake(cWidth, self.menuScrollView.frame.size.height);
}


/**
 *  Any Of the Top Menu Button Press Action
 *
 *  @param sender id of the button pressed
 */

#pragma mark - Menu Button press action
-(void) buttonPressed:(id) sender
{
    UIButton *senderbtn = (UIButton *) sender;
    float buttonWidth = 0.0f;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        UIButton *btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == senderbtn.tag){
            btn.selected = YES;
            [bottomView setHidden:NO];
        }
        else{
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    [self.containerScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * senderbtn.tag, 0) animated:YES];
    
    float xx = SCREEN_WIDTH * (senderbtn.tag) * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}

/**
 *  Calculating width of button added on top menu
 *
 *  @param title            Title of the Button
 *  @param buttonEdgeInsets Edge Insets for the title
 *
 *  @return Width of button
 */

#pragma mark - Calculate width of menu button
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets
{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]};
    
    CGSize size = [title sizeWithAttributes:attributes];
    return CGSizeMake(size.width + buttonEdgeInsets.left + buttonEdgeInsets.right, size.height + buttonEdgeInsets.top + buttonEdgeInsets.bottom).width;
}


/**
 *  Adding all related controllers in to the container
 *
 *  @param controllersArr Array containing objects of all controllers
 */
#pragma mark - Adding all related controllers in to the container
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr
{
    for (int i = 0 ; i < controllersArr.count; i++)
    {
        UIViewController *vc = (UIViewController *)[controllersArr objectAtIndex:i];
        CGRect frame = CGRectMake(0, 0, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height);
        frame.origin.x = SCREEN_WIDTH * i;
        vc.view.frame = frame;
        
        [self addChildViewController:vc];
        [self.containerScrollView addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
    self.containerScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * controllersArr.count + 1, self.containerScrollView.frame.size.height);
    self.containerScrollView.pagingEnabled = YES;
    self.containerScrollView.delegate = self;
}


#pragma mark - Scroll view delegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = (scrollView.contentOffset.x / SCREEN_WIDTH);
    
    UIButton *btn;
    float buttonWidth = 0.0;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == page){
            btn.selected = YES;
            buttonWidth = btn.frame.size.width;
            [bottomView setHidden:NO];
        }
        else{
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    float xx = scrollView.contentOffset.x * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
//    if([isGetNotification isEqualToString:@"YES"]){
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        AppContainerViewController *newView = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
//        [self.navigationController pushViewController:newView animated:YES];
//    }
//    else{
//        [[self navigationController] popViewControllerAnimated:YES];
//    }
    [[self navigationController] popViewControllerAnimated:YES];

    
}
-(void)viewWillAppear:(BOOL)animated{
    [self UpdateNotifications];
}
- (IBAction)notificationTapped:(id)sender {
   [delegate DeleteNotificationCounter:@"IPM"];
  
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NotificationScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationScreen"];
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(IBAction)GotoProfile:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"PMS";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)UpdateNotifications{
    NatificationLabel.alpha=0.0;
    NatificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
    if ([NatificationLabel.text intValue]>0) {
        NatificationLabel.alpha=1.0;
        NatificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
    }
}
-(IBAction)SearchAction:(id)sender{
    CustomSearchController *viewController= [[CustomSearchController alloc] init];
    [[self navigationController] pushViewController:viewController animated: YES];
}
@end
