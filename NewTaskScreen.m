#import "NewTaskScreen.h"
#import "Header.h"

@interface NewTaskScreen ()<UITableViewDelegate,UITableViewDataSource,FCAlertViewDelegate>
{
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    IBOutlet UILabel *headerlab;
    IBOutlet UILabel *NoDataLabel;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSMutableArray *newArray;
    int TagValue;
    UIRefreshControl *refreshControl;
    WebApiService *Api;
    NSString *handelId;
    NSString* taskID, *actionType;
    NSString *CheckValue;
    NSString *Status;
    int offset;
    int limit;
}
@end
@implementation NewTaskScreen
@synthesize taskType;


- (void)viewDidLoad{
    [super viewDidLoad];
    [self EmplyArray];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    handelId = [NSString stringWithFormat:@"%@",[delegate.dataFull objectForKey:@"handsetId"]];
    TagValue = 0;
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 50;
    tab.backgroundColor = delegate.BackgroudColor;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    if (1 == taskType)
        headerlab.text=NSLocalizedString(@"MY_TASK", nil);
    else
        headerlab.text=NSLocalizedString(@"MY_ASSIGNMENT", nil);
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [tab registerNib:Nib forCellReuseIdentifier:@"CELL"];
    self.view.backgroundColor = delegate.BackgroudColor;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskAndAssignmentWebApi) withObject:nil afterDelay:0.5];
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self EmplyArray];
    [self TaskAndAssignmentWebApi];
}
-(void)EmplyArray{
    offset = 0;
    limit = 50;
    newArray = [[NSMutableArray alloc] init];
}
-(void) TaskAndAssignmentWebApi{
    if(delegate.isInternetConnected){
        if(taskType == 1){
           Status = @"me";
        }
        else{
          Status = @"linemanager";
        }
        params = @ {
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        ResponseDic =  [Api WebApi:params Url:[NSString stringWithFormat:@"MeLineManagerCountList?employeeID=%@&status=%@",[ApplicationState userId],Status]];
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"]intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"TaskAndAssignmentResponse==%@",ResponseDic);
            NoDataLabel.hidden = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    NoDataLabel.hidden = YES;
                }
                else if(newArray.count == 0){
                   [self NoDataFound];
                }
                [tab reloadData];
                
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(newArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No record found";
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
    if(ArraysValue.count>0){
        offset = offset + 50;
        [self TaskAndAssignmentWebApi];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [newArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [newArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.DetailsBtn.tag = indexPath.section;
    [Cell.DetailsBtn addTarget:self action:@selector(StartCancelAction:) forControlEvents:UIControlEventTouchUpInside];
    Cell.RatingView.hidden = YES;
    Cell.StausImageView.hidden = NO;
    Cell.StatusLabel.hidden = NO;
    if(newArray.count>0){
        NSDictionary * responseData = [newArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
    }
    if ([[[newArray objectAtIndex:indexPath.section] objectForKey:@"taskStatus"] isEqualToString:@"New"]){
        [Cell.DetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    }
    else{
        [Cell.DetailsBtn setTitle:NSLocalizedString(@"START", nil) forState:UIControlStateNormal];
    }
    return Cell;
}
-(void)StartCancelAction:(UIButton*)sender{
    TagValue = sender.tag;
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]){
        taskID = [[newArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        [self showAlertCancelTask];
    }
    else if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"START", nil)]){
        taskID = [[newArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        [self showAlertStartTask];
    }
}
-(void)showAlertCancelTask{
    actionType = @"cancel";
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeWarning];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"CANCEL_TASK", nil) withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_CANCEL", nil) withCustomImage:nil
          withDoneButtonTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) andButtons:@[NSLocalizedString(@"OKAY", nil)]];
}
-(void)showAlertStartTask{
    actionType = @"start";
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeSuccess];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"START_TASK", nil) withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_START", nil) withCustomImage:nil
          withDoneButtonTitle:NSLocalizedString(@"OKAY", nil) andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]];
}
- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView {
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(setStatus) withObject:nil afterDelay:0.5];
}
- (void) setStatus{
    if(delegate.isInternetConnected){
        if([actionType isEqualToString:@"cancel"]){
            params = @ {
                @"comment" : @"",
                @"status" : @"4",
                @"taskId" : taskID,
                @"user_id" : [ApplicationState userId]
            };
            ResponseDic =  [Api WebApi:params Url:@"cancelTask"];
        }
        else if ([actionType isEqualToString:@"start"]){
             ResponseDic =  [Api WebApi:nil Url:[NSString stringWithFormat:@"startButton?employeeId=%@&taskId=%@",[ApplicationState userId],taskID]];
        }
        if([[ResponseDic valueForKey:@"statusCode"]intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            [self EmplyArray];
            [self TaskAndAssignmentWebApi];
        }
        else{
             [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
      [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (void) FCAlertView:(FCAlertView *)alertView clickedButtonIndex:(NSInteger)index buttonTitle:(NSString *)title {
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
