//
//  CompleteViewController.m
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "CompleteViewController.h"
#import "Header.h"

@interface CompleteViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *CompleteTableView;
    NSMutableArray *CompleteArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
}
@end

@implementation CompleteViewController
@synthesize CompleteDics;

- (void)viewDidLoad {
    offset = 0;
    limit = 50;
    Api = [[WebApiService alloc] init];
    CompleteArray = [[NSMutableArray alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    CompleteTableView.estimatedRowHeight = 50;
    CompleteTableView.backgroundColor = delegate.BackgroudColor;
    CompleteTableView.rowHeight = UITableViewAutomaticDimension;
    CompleteTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CompleteTableView.bounds.size.width, 10.f)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [CompleteTableView addSubview:refreshControl];

    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [CompleteTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    [self CompleteApi];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"COMPLETE" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    CompleteArray = [[NSMutableArray alloc] init];
    [self CompleteApi];
}
- (void)reloadData{
    offset = 0;
    limit = 50;
    CheckValue = @"Refresh";
    CompleteArray = [[NSMutableArray alloc] init];
    [self CompleteApi];
}
- (void)CompleteApi {
    
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"FILTER"]){
            ResponseDic =  [Api WebApi:params Url:@"getEmployeeTaskList"];
        }
        else{
            params = @ {
                @"fromDate": @"",
                @"handsetId":@"",
                @"status": @"4",
                @"taskId": @"",
                @"taskType": @"",
                @"toDate": @"",
                @"user_id"  : [CompleteDics valueForKey:@"empId"],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
            ResponseDic =  [Api WebApi:params Url:@"getEmployeeTaskList"];
        }
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            NSLog(@"CompleteResponseDic==%@",ResponseDic);
            NoDataLabel.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]];
                    NoDataLabel.hidden = YES;
                }
                else if(ResponseArrays.count == 0 && CompleteArray.count == 0){
                    NoDataLabel.hidden = NO;
                    NoDataLabel.text = @"No record found";
                }
                [CompleteTableView reloadData];
                
            }
            else{
                if(CompleteArray.count == 0){
                    NoDataLabel.hidden = NO;
                    NoDataLabel.text = @"No record found";
                }
            }
            [CompleteTableView reloadData];
        }
        else{
            if(CompleteArray.count == 0){
                NoDataLabel.hidden = NO;
                NoDataLabel.text = @"No record found";
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
        NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            if([CheckValue isEqualToString:@"FILTER"]){
                NSMutableDictionary *NewDic = [params mutableCopy];
                for (int i = 0 ; i< [NewDic count];i++){
                    if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                        [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                    }
                    else{
                        [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                    }
                }
                params = [NewDic mutableCopy];
            }
            [self CompleteApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [CompleteArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [CompleteArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];

    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoEmployeeDetails:)];
    [Cell setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:gesture];
    Cell.tag = indexPath.section;
    Cell.DetailsBtn.tag = indexPath.section;
    [Cell.DetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    Cell.RatingView.hidden = NO;
    Cell.StausImageView.hidden = YES;
    Cell.StatusLabel.hidden = YES;
    if(CompleteArray.count>0){
        NSDictionary * responseData = [CompleteArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
    }
    return Cell;
}
-(void)GotoEmployeeDetails:(UIGestureRecognizer*)Gesture{
    TagValue = Gesture.view.tag;
    [self gotoDetails];
}
-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    [self gotoDetails];
}
-(void)gotoDetails{
    NSString* str = [[CompleteArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    if ([[[CompleteArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
        TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
    else{
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
