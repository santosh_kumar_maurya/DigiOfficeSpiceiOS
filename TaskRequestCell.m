//
//  TaskRequestCell.m
//  iWork
//
//  Created by Shailendra on 17/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "TaskRequestCell.h"
#import "Header.h"
@implementation TaskRequestCell


- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    
    self.layer.cornerRadius = 7.0f;
    self.layer.shadowRadius = 3.0f;
    self.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
    self.layer.shadowOpacity = 0.7f;
    self.layer.masksToBounds = NO;
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.TaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.TaskView.layer.mask = maskLayer;
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.RejectBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){6.0}].CGPath;
    self.RejectBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.ApproveBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    self.ApproveBtn.layer.mask = maskLayer2;
    
    CAShapeLayer * maskLayer3 = [CAShapeLayer layer];
    maskLayer3.path = [UIBezierPath bezierPathWithRoundedRect: self.NeedRevisionBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){6.0}].CGPath;
    self.NeedRevisionBtn.layer.mask = maskLayer3;
    
    CAShapeLayer * maskLayer4 = [CAShapeLayer layer];
    maskLayer4.path = [UIBezierPath bezierPathWithRoundedRect: self.CompleteApproveBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    self.CompleteApproveBtn.layer.mask = maskLayer4;
    
    
    
}
- (void)configureCell:(NSDictionary *)info{
    
    if(IsSafeStringPlus(TrToString(info[@"taskId"]))){
        _TaskIdLabel.text  = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),info[@"taskId"]];
    }
    else{
        _TaskIdLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        NSNumber *datenumber = info[@"creationDate"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskName"]))){
        _TaskNameLabel.text  = info[@"taskName"];
    }
    else{
        _TaskNameLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"empName"]))){
        _EmployeeNameLabel.text  = info[@"empName"];
    }
    else{
        _EmployeeNameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"duration"]))){
        _DurationLabel.text  = info[@"duration"];
    }
    else{
        _DurationLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"kpi"]))){
        _KPILabel.text  = info[@"kpi"];
    }
    else{
        _KPILabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"comment"]))){
        _CommentLabel.text  = info[@"comment"];
    }
    else{
        _CommentLabel.text  = @" ";
    }
    
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm:aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
