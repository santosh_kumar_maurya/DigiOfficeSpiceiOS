#import "MyKPIDetailsScreen.h"
#import "Header.h"

@interface MyKPIDetailsScreen ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>{
   
    UIRefreshControl *refreshControl;
    WebApiService *Api;
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    IBOutlet UILabel *headerlab;
    IBOutlet UILabel *NoDataLabel;
    NSMutableArray *dataArray;
    int TagValue;
    NSString *kpiId;
    UIView *CoomentView;
    UITextView *CommentTextView;
    NSDictionary *ResponseDic;
    NSDictionary *params;
    NSString *handsetId;
    int offset;
    int limit;
}

@end
@implementation MyKPIDetailsScreen
@synthesize kpiId,KPIName;

- (void)viewDidLoad{
    
    [super viewDidLoad];
    offset = 0;
    limit = 50;
    Api = [[WebApiService alloc] init];
    dataArray = [[NSMutableArray alloc] init];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    headerlab.text = KPIName;
    TagValue = 0;
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [tab registerNib:Nib forCellReuseIdentifier:@"CELL"];
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 50;
    tab.backgroundColor = delegate.BackgroudColor;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    self.view.backgroundColor = delegate.BackgroudColor;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(KPIDatailsApi) withObject:nil afterDelay:0.5];
}
-(void)KPIDatailsApi{
    if(delegate.isInternetConnected){
        params = @ {
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        ResponseDic = [Api WebApi:params Url:[NSString stringWithFormat:@"kpiListDetail?employeeID=%@&id=%@",[ApplicationState userId],kpiId]];
        NSLog(@"KPIDeatilsResponseDic=%@",ResponseDic);
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            NoDataLabel.hidden = YES;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                    NoDataLabel.hidden = YES;
                     [tab reloadData];
                }
                else if(ResponseArrays.count == 0 && dataArray.count == 0){
                    [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else{
            [self NoDataFound];
        }
        
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(dataArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No record found";
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self KPIDatailsApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [dataArray addObject:BindDataDic];
    }
}
- (void)reloadData{
    offset = 0;
    limit = 50;
    dataArray = [[NSMutableArray alloc] init];
    [self KPIDatailsApi];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return [dataArray count];;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    Cell.DetailsBtn.tag = indexPath.section;
    [Cell.DetailsBtn addTarget:self action:@selector(StartCancelDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    if(dataArray.count>0){
        NSDictionary * responseData = [dataArray objectAtIndex:indexPath.section];
        [Cell configureCell:responseData];
        if ([[[dataArray objectAtIndex:indexPath.section] objectForKey:@"status"] isEqualToString:@"New"]){
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
            
            Cell.StatusLabel.text = @"New";
            Cell.StatusLabel.textColor = delegate.pendingColor;
            Cell.StausImageView.image = [UIImage imageNamed:@"pending_new"];
        }
        else if ([[[dataArray objectAtIndex:indexPath.section] valueForKey:@"status"] isEqualToString:@"new"]){
            
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
            Cell.StatusLabel.text = @"New";
            Cell.StatusLabel.textColor = delegate.pendingColor;
            Cell.StausImageView.image = [UIImage imageNamed:@"pending_new"];
        }
        else if ([[[dataArray objectAtIndex:indexPath.section] objectForKey:@"status"]  isEqualToString:@"In-Progress"]){
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
        }
        else if ([[[dataArray objectAtIndex:indexPath.section] valueForKey:@"status"]  isEqualToString:@"Assigned"]){
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"START", nil) forState:UIControlStateNormal];
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
        }
        else if ([[[dataArray objectAtIndex:indexPath.section] valueForKey:@"status"] isEqualToString:@"Declined"] ||([[[dataArray objectAtIndex:indexPath.section] valueForKey:@"status"] isEqualToString:@"Discarded"])){
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =YES;
        }
        else if ([[[dataArray objectAtIndex:indexPath.section] valueForKey:@"status"] isEqualToString:@"Approved"] ||([[[dataArray objectAtIndex:indexPath.section] valueForKey:@"status"] isEqualToString:@"Rejected"])){
            Cell.RatingView.hidden = NO;
            Cell.StausImageView.hidden = YES;
            Cell.StatusLabel.hidden = YES;
            Cell.DetailsBtn.hidden =NO;
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
        }
        else{
            Cell.RatingView.hidden = YES;
            Cell.StausImageView.hidden = NO;
            Cell.StatusLabel.hidden = NO;
            Cell.DetailsBtn.hidden =NO;
            [Cell.DetailsBtn setTitle:NSLocalizedString(@"DETAILS", nil) forState:UIControlStateNormal];
        }
    }
    return Cell;
}
-(void)CommentView{
    
    CoomentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    CoomentView.backgroundColor = [UIColor clearColor];
    UIImageView *ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    ImageView.backgroundColor = [UIColor blackColor];
    ImageView.alpha = 0.3;
    UIView *InnerView = [[UIView alloc] initWithFrame:CGRectMake(10, delegate.deviceheight/2 - 100, delegate.devicewidth -20, 150)];
    CGPoint center = InnerView.center;
    center.x = CoomentView.center.x;
    InnerView.center = center;
    InnerView.layer.cornerRadius = 5.0;
    InnerView.backgroundColor = [UIColor whiteColor];
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(0, InnerView.frame.size.height-40, InnerView.frame.size.width, 40)];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: BtnView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    BtnView.layer.mask = maskLayer1;
    
    UILabel *AddCommentLabel;
        AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 45)];
    AddCommentLabel.backgroundColor = delegate.yellowColor;
    AddCommentLabel.textColor = [UIColor blackColor];
    AddCommentLabel.font = delegate.headFont;
    AddCommentLabel.textAlignment = NSTextAlignmentCenter;
    if([[[dataArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
        AddCommentLabel.text = NSLocalizedString(@"START_TASK", nil);
    }
    else{
        AddCommentLabel.text = NSLocalizedString(@"CANCEL_TASK_SMALL", nil);
    }
    
    CAShapeLayer * AddCommentmaskLayer1 = [CAShapeLayer layer];
    AddCommentmaskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: AddCommentLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){5.0}].CGPath;
    AddCommentLabel.layer.mask = AddCommentmaskLayer1;
    
    UILabel *AddCommentStaticLabel;
    AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, AddCommentLabel.frame.origin.y+AddCommentLabel.frame.size.height + 10, delegate.devicewidth-40, 40)];
    AddCommentStaticLabel.textColor = [UIColor blackColor];
    AddCommentStaticLabel.font = delegate.normalFont;
    AddCommentStaticLabel.numberOfLines = 0;
    AddCommentStaticLabel.textAlignment = NSTextAlignmentCenter;
    if([[[dataArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
       AddCommentStaticLabel.text = NSLocalizedString(@"ARE_YOU_SURE_START", nil);
    }
    else{
       AddCommentStaticLabel.text = NSLocalizedString(@"ARE_YOU_SURE_CANCEL", nil);
    }
    
    UIButton *SubmitBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, BtnView.frame.size.width/2, 40)];
    [SubmitBtn addTarget:self action:@selector(SubmitAction) forControlEvents:UIControlEventTouchUpInside];
    SubmitBtn.backgroundColor = delegate.yellowColor;
    SubmitBtn.titleLabel.font = delegate.normalFont;
    [SubmitBtn setTitle:NSLocalizedString(@"OKAY", nil) forState:UIControlStateNormal];
    
    UIButton *CancelBtn=[[UIButton alloc] initWithFrame:CGRectMake(BtnView.frame.size.width/2, 0, BtnView.frame.size.width/2, 40)];
    [CancelBtn addTarget:self action:@selector(CancelAction) forControlEvents:UIControlEventTouchUpInside];
    CancelBtn.backgroundColor = delegate.redColor;
    CancelBtn.titleLabel.font = delegate.normalFont;
    [CancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    [BtnView addSubview:SubmitBtn];
    [BtnView addSubview:CancelBtn];
    [InnerView addSubview:BtnView];
    [InnerView addSubview:AddCommentLabel];
    [InnerView addSubview:AddCommentStaticLabel];
    [CoomentView addSubview:ImageView];
    [CoomentView addSubview:InnerView];
    
    [self.view addSubview:CoomentView];
}
-(void)CancelAction{
    CoomentView.hidden = YES;
}
-(void)SubmitAction{
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(CancelTaskSubmitAction) withObject:nil afterDelay:0.5];
    
}
-(void)CancelTaskSubmitAction{
    if(delegate.isInternetConnected){
        NSString* TaskID = [[dataArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if([[[dataArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
            ResponseDic =  [Api WebApi:nil Url:[NSString stringWithFormat:@"startButton?employeeId=%@&taskId=%@",[ApplicationState userId],TaskID]];
        }
        else{
            params = @ {
                @"comment" : @"",
                @"status" : @"4",
                @"taskId" : TaskID,
                @"user_id" : [ApplicationState userId]
            };
            ResponseDic =  [Api WebApi:params Url:@"cancelTask"];
        }
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            CoomentView.hidden = YES;
            [self KPIDatailsApi];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)StartCancelDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]||[sender.titleLabel.text isEqualToString:NSLocalizedString(@"START", nil)]){
         [self CommentView];
    }
    else{
        NSString* str = [[dataArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if ([[[dataArray objectAtIndex:TagValue] objectForKey:@"status"] isEqualToString:@"In-Progress"]){
            TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
            viewController.taskID = str;
            viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else{
            HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
            viewController.taskID = str;
            viewController.isManager = TRUE;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
    }
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
