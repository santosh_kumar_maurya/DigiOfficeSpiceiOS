//
//  KPICell.m
//  iWork
//
//  Created by Shailendra on 29/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "KPICell.h"

@implementation KPICell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    self.backgroundColor = [UIColor clearColor];
    _CustomView.layer.cornerRadius = 6.0;
    _CustomView.maskView.layer.cornerRadius = 7.0f;
    _CustomView.layer.shadowRadius = 3.0f;
    _CustomView.layer.shadowColor = [UIColor blackColor].CGColor;
    _CustomView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _CustomView.layer.shadowOpacity = 0.7f;
    _CustomView.layer.masksToBounds = NO;
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
