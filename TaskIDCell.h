//
//  TaskIDCell.h
//  iWork
//
//  Created by Shailendra on 23/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskIDCell : UITableViewCell

@property(nonatomic,retain) IBOutlet UITextField *TaskIDTextField;
@end
