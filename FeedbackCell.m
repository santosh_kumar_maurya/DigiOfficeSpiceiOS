//
//  FeedbackCell.m
//  iWork
//
//  Created by Shailendra on 01/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "FeedbackCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Header.h"

@implementation FeedbackCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    
    UIBezierPath *shadowPath1 = [UIBezierPath bezierPathWithRect:self.bounds];
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.layer.shadowOpacity = 0.3f;
    self.layer.shadowPath = shadowPath1.CGPath;
    
    
    _ProfileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _ProfileImageView.layer.borderWidth = 1;
    _ProfileImageView.layer.cornerRadius = 25;
    _ProfileImageView.clipsToBounds = YES;
    
   
    
    _GiveFeedbackBtn.layer.cornerRadius =4;
    _GiveFeedbackBtn.layer.masksToBounds =YES;
    _GiveFeedbackBtn.imageView.layer.cornerRadius = 7.0f;
    _GiveFeedbackBtn.layer.shadowRadius = 3.0f;
    _GiveFeedbackBtn.layer.shadowColor = [UIColor redColor].CGColor;
    _GiveFeedbackBtn.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _GiveFeedbackBtn.layer.shadowOpacity = 0.7f;
    _GiveFeedbackBtn.layer.masksToBounds = NO;
    
    
    // Initialization code
}
- (void)configureCell:(NSDictionary *)info{

    if(IsSafeStringPlus(TrToString(info[@"userImg"]))){
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:info[@"userImg"]]]];
        if(images == nil){
            NSLog(@"image nil");
            _ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            NSString *URL = [NSString stringWithFormat:@"%@",info[@"userImg"]];
            [_ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        _ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
    }
    if(IsSafeStringPlus(TrToString(info[@"taskId"]))){
        _TaskIdLabel.text  = [NSString stringWithFormat:@"%@",info[@"taskId"]];
    }
    else{
        _TaskIdLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"completedBy"]))){
        _UserNameLabel.text  = info[@"completedBy"];
    }
    else{
        _UserNameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskName"]))){
        _TaskNameLabel.text  = info[@"taskName"];
    }
    else{
        _TaskNameLabel.text  = @" ";
    }
    
}
@end
