//
//  AllViewController.m
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AllViewController.h"
#import "Header.h"

@interface AllViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    IBOutlet UITableView *AllTableView;
    NSMutableArray *AllArray;
    WebApiService *Api;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UILabel *NoDataLabel;
    int TagValue;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
}

@end

@implementation AllViewController
@synthesize AllDics;
- (void)viewDidLoad {
    offset = 0;
    limit = 50;
    AllArray = [[NSMutableArray alloc] init];
    Api = [[WebApiService alloc] init];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    AllTableView.estimatedRowHeight = 50;
    AllTableView.backgroundColor = delegate.BackgroudColor;
    AllTableView.rowHeight = UITableViewAutomaticDimension;
    AllTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, AllTableView.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [AllTableView addSubview:refreshControl];
    UINib *Nib = [UINib nibWithNibName:@"EmployeeStatusCell" bundle:nil];
    [AllTableView registerNib:Nib forCellReuseIdentifier:@"CELL"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"ALL" object:nil];
     [self AllWebApi];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    AllArray = [[NSMutableArray alloc] init];
    [self AllWebApi];
}
- (void)reloadData{
    offset = 0;
    limit = 50;
    CheckValue = @"Refresh";
    AllArray = [[NSMutableArray alloc] init];
    [self AllWebApi];
}
- (void)AllWebApi {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"FILTER"]){
             ResponseDic =  [Api WebApi:params Url:@"getEmployeeTaskList"];
        }
        else{
            params = @ {
                @"fromDate": @"",
                @"handsetId": @"",
                @"status": @"0",
                @"taskId": @"",
                @"taskType": @"",
                @"toDate": @"",
                @"user_id"  : [AllDics valueForKey:@"empId"],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
            ResponseDic =  [Api WebApi:params Url:@"getEmployeeTaskList"];
        }
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            NSLog(@"AllResponseDic==%@",ResponseDic);
            NoDataLabel.hidden = YES;
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]];
                    NoDataLabel.hidden = YES;
                }
                else if(ResponseArrays.count == 0 && AllArray.count == 0){
                    NoDataLabel.hidden = NO;
                    NoDataLabel.text = @"No record found";
                }
                [AllTableView reloadData];
            }
            else{
                if(AllArray.count == 0){
                    NoDataLabel.hidden = NO;
                    NoDataLabel.text = @"No record found";
                }
            }
            [AllTableView reloadData];
        }
        else{
            if(AllArray.count == 0){
                NoDataLabel.hidden = NO;
                NoDataLabel.text = @"No record found";
            }
            [AllTableView reloadData];
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"]))){
        NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            if([CheckValue isEqualToString:@"FILTER"]){
                NSMutableDictionary *NewDic = [params mutableCopy];
                for (int i = 0 ; i< [NewDic count];i++){
                    if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                        [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                    }
                    else{
                        [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                    }
                }
                params = [NewDic mutableCopy];
            }
            [self AllWebApi];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [AllArray addObject:BindDataDic];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [AllArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeStatusCell *Cell = (EmployeeStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
     Cell.tag = indexPath.section;
    Cell.DetailsBtn.tag = indexPath.section;
    
    [Cell.DetailsBtn addTarget:self action:@selector(GotoEmployeeDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoEmployeeDetails:)];
    [Cell setUserInteractionEnabled:YES];
    [Cell addGestureRecognizer:gesture];
    
    Cell.RatingView.hidden = YES;
    Cell.StausImageView.hidden = NO;
    Cell.StatusLabel.hidden = NO;
    if(AllArray.count>0){
        NSDictionary * responseData = [AllArray objectAtIndex:indexPath.section];
        if(IsSafeStringPlus(TrToString(responseData[@"taskId"]))){
            Cell.TaskIdLabel.text  = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),responseData[@"taskId"]];
        }
        else{
            Cell.TaskIdLabel.text  = @" ";
        }
        if(IsSafeStringPlus(TrToString(responseData[@"creationDate"]))){
            NSNumber *datenumber = responseData[@"creationDate"];
            Cell.DateLabel.text = [self DateFormateChange:datenumber];
        }
        else{
            Cell.DateLabel.text  = @" ";
        }
        if(IsSafeStringPlus(TrToString(responseData[@"taskName"]))){
            Cell.TaskNameLabel.text  = responseData[@"taskName"];
        }
        else{
            Cell.TaskNameLabel.text  = @" ";
        }
        if(IsSafeStringPlus(TrToString(responseData[@"type"]))){
            Cell.CreatedByLabel.text  = responseData[@"type"];
        }
        else{
            Cell.CreatedByLabel.text  = @" ";
        }
        if(IsSafeStringPlus(TrToString(responseData[@"duration"]))){
            Cell.DurationLabel.text  = responseData[@"duration"];
        }
        else{
            Cell.DurationLabel.text  = @" ";
        }
        
        if(IsSafeStringPlus(TrToString(responseData[@"kpi"]))){
            Cell.KPILabel.text  = responseData[@"kpi"];
        }
        else{
            Cell.KPILabel.text  = @" ";
        }
        if(IsSafeStringPlus(TrToString(responseData[@"taskStatus"]))){
            Cell.StatusLabel.text  = responseData[@"taskStatus"];
        }
        else{
            Cell.StatusLabel.text  = @" ";
        }
        if(IsSafeStringPlus(TrToString(responseData[@"rating"]))){
            Cell.RatingView.rating  = [responseData[@"rating"] floatValue];
            Cell.RatingView.starFillColor = delegate.redColor;
            Cell.RatingView.starSize = 15.0;
        }
        else{
            Cell.RatingView.rating  = 0;
        }
        NSString *statusType;
        NSString *imageType;
        UIColor *colorType;
        if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Cancelled"]){
            statusType = @"Cancelled";
            imageType = @"cancelled_new";
            colorType = delegate.redColor;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Discarded"]){
            statusType = @"Discarded";
            imageType = @"discarded_new";
            colorType = delegate.discardedColor;
        }
        else if([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Declined"]){
            statusType = @"Declined";
            imageType = @"declined_new";
            colorType = delegate.failedColor;
        }
        else if([[responseData objectForKey:@"taskStatus"] isEqualToString:@"FAILED"]){
            statusType = @"Declined";
            imageType = @"Declined_new";
            colorType = delegate.failedColor;
        }
        else if([[responseData objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
            statusType = @"In-Progress";
            imageType = @"inprogressblue";
            colorType = delegate.inprogColor;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Submitted"]){
            statusType = @"Submitted";
            imageType = @"submitted_new";
            colorType = delegate.submittedColor;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"CLOSED"]){
            statusType = @"Assigned";
            imageType = @"confirmed";
            colorType = delegate.confColor;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
            statusType = @"Assigned";
            imageType = @"assigned";
            colorType = delegate.assignColor;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"New"]){
            statusType = @"New";
            imageType = @"pending_new";
            colorType = delegate.pendingColor;
        }
        else if([[responseData valueForKey:@"taskStatus"] isEqualToString:@"new"]){
            statusType = @"New";
            imageType = @"pending_new";
            colorType = delegate.pendingColor;
        }
        else if ([[responseData objectForKey:@"taskStatus"] isEqualToString:@"Approved"]||[[responseData objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]){
            Cell.RatingView.hidden = NO;
            Cell.StausImageView.hidden = YES;
            Cell.StatusLabel.hidden = YES;
        }
        Cell.StatusLabel.text = statusType;
        Cell.StatusLabel.textColor = colorType;
        Cell.StausImageView.image = [UIImage imageNamed:imageType];
    }
    return Cell;
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm:aa"];
    return [dateformate stringFromDate:Newdate];
}
-(void)GotoEmployeeDetails:(UIGestureRecognizer*)Gesture{
    TagValue = Gesture.view.tag;
    [self gotoDetails];
}
-(void)GotoEmployeeDetailsAction:(UIButton*)sender{
    TagValue = sender.tag;
    [self gotoDetails];
}
-(void)gotoDetails{
    NSString* str = [[AllArray objectAtIndex:TagValue] objectForKey:@"taskId"];
    if ([[[AllArray objectAtIndex:TagValue] objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
        TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
    else{
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = str;
        viewController.isManager = TRUE;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
