//
//  MultiSelectViewController.m
//  MultiSelectUIXibVersion
//`
//  Created by Terry Bu on 10/28/14.
//  Copyright (c) 2014 TurnToTech. All rights reserved.
//

#import "CustomSearchController.h"
#import "Header.h"

@interface CustomSearchController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate,UISearchResultsUpdating>{
    WebApiService *Api;
    int offset;
    int limit;
    NSDictionary *ResponseDic;
    NSDictionary *params;
    NSString* newText;
    AppDelegate *delegate;
    UILabel *headerlab;
    UITableView *tab;
    NSMutableArray *savedArrayOfEmployees;
    int selectedCounter;
    UIBarButtonItem *selectedContactsCounterButton;
    UISearchController *searchController;
    NSMutableArray *searchResults1;
    NSMutableArray *subElements;
}
@end

@implementation CustomSearchController
- (void)viewDidLoad {
    offset = 0;
    limit = 50;
    Api = [[WebApiService alloc] init];
     self.view.backgroundColor = delegate.BackgroudColor;
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=TRUE;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    searchResults1 = [[NSMutableArray array]init];
    subElements = [[NSMutableArray array]init];
    [self ScreenDesign];
}
- (void) ScreenDesign{
    UIView *screenView=[[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    screenView.backgroundColor=delegate.BackgroudColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor=[ColorCategory PurperColor];
    [screenView addSubview:headerview];
    
    UIButton *backButton=[[UIButton alloc] initWithFrame:CGRectMake(5, 35, 30, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(sendButton:) forControlEvents:UIControlEventTouchUpInside];
    CGPoint center = backButton.center;
    center.y = headerview.frame.size.height / 2 +3;
    [backButton setCenter:center];
    backButton.tag=30;
    [headerview addSubview:backButton];
    
    headerlab = [[UILabel alloc] initWithFrame:CGRectMake(45, backButton.frame.origin.y+14, 200, 16)];
    headerlab.textColor=delegate.yellowColor;
    headerlab.font=delegate.headFont;
    headerlab.text=NSLocalizedString(@"SESRCH_LOCATION", nil);
    [headerview addSubview:headerlab];
    
    tab=[[UITableView alloc] initWithFrame:CGRectMake(0, delegate.headerheight+10, screenView.frame.size.width,screenView.frame.size.height)];
    tab.delegate=self;
    tab.dataSource=self;
    tab.backgroundColor=[UIColor whiteColor];
    tab.estimatedRowHeight = 200.0;
    tab.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    tab.estimatedRowHeight = 100.0;
    tab.rowHeight = UITableViewAutomaticDimension;
    
    [screenView addSubview:tab];
    
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    searchController.searchResultsUpdater = self;
    searchController.searchBar.delegate = self;
    [searchController.searchBar sizeToFit];
    [searchController.searchBar setShowsCancelButton:NO];
    searchController.dimsBackgroundDuringPresentation = NO;
    tab.tableHeaderView = searchController.searchBar;
    [self.view addSubview:screenView];
}

#pragma mark - UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        searchController.searchBar.showsCancelButton = NO;
    });
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        searchController.active = NO;
    });
}
-(BOOL) searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    searchResults1 = [[NSMutableArray array]init];
    newText = [searchBar.text stringByReplacingCharactersInRange:range withString:text];
    newText = [newText stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    if(![newText isEqualToString:@""]){
       [self fetchData:newText];
    }
    else{
        [searchResults1 removeAllObjects];
        [tab reloadData];
    }
    return YES;
}
#pragma mark Search Bar Methods
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController {
    return;
}
#pragma mark Tableview Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = (int)[searchResults1 count];
    return count;
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
    }
    NSString *fullName = [[searchResults1 valueForKey:@"taskName"] objectAtIndex:indexPath.row];
    UILabel *taskId =
    [[UILabel alloc] initWithFrame:CGRectMake(20, 10, delegate.devicewidth, 15)];
    taskId.text = fullName;
    taskId.textColor = [UIColor blackColor];
    taskId.font = delegate.contentFont;
    [cell addSubview:taskId];
    
    UILabel *createdAt =
    [[UILabel alloc] initWithFrame:CGRectMake(20, 27, delegate.devicewidth, 15)];
    createdAt.text = @"Task Name";
    createdAt.textColor = delegate.dimBlack;
    createdAt.font = delegate.contentSmallFont;
    [cell addSubview:createdAt];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString* query = [[searchResults1 valueForKey:@"taskName"] objectAtIndex:indexPath.row];
    searchController.active = NO;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    SearchDetailsScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"SearchDetailsScreen"];
    ObjViewController.query = query;
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
- (void) fetchData:(NSString*) data{
    if(delegate.isInternetConnected){
        params = @ {
            @"taskName": data,
            @"userId"  : [ApplicationState userId],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        ResponseDic =  [Api WebApi:params Url:@"searchByTaskName"];
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            NSLog(@"SearchResponseDic==%@",ResponseDic);
            if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskName"]))){
                NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"taskName"];
                if(ResponseArrays.count>0){
                    [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"taskName"]];
                }
                else if(ResponseArrays.count == 0 && searchResults1.count == 0){
                    
                }
            }
            else{
                if(searchResults1.count == 0){
                    
                }
            }
        }
        else{
            
        }
        [tab reloadData];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskName"]))){
        NSArray *ArraysValue =  [[ResponseDic valueForKey:@"object"] valueForKey:@"taskName"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self fetchData:newText];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [searchResults1 addObject:BindDataDic];
    }
}
#pragma mark IBActions and custom methods
- (IBAction) sendButton: (id) sender {
    UIButton *btn=(UIButton *)sender;
    if (30 == btn.tag){
        [[self navigationController] popViewControllerAnimated:YES];
    }
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
