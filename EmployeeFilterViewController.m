//
//  EmployeeFilterViewController.m
//  iWork
//
//  Created by Shailendra on 08/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeFilterViewController.h"
#import "Header.h"


@interface EmployeeFilterViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    
    NSMutableArray *arrayForBool;
    NSMutableArray *FilterArray;
   
    IBOutlet UITableView *FilterTableView;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *CancelFilterBtn;
    IBOutlet UIButton *ApplyFilterBtn;
    
    IBOutlet UIView *BottomView;
    NSMutableArray *PlusImageArray;
    NSMutableArray *MinusImageArray;
    
    NSIndexPath *indexPath3;
    RatingCell *Cell3;
    NSIndexPath *indexPath4;
    MyWorkLocationCell *Cell4;
    NSIndexPath *indexPath5;
    DurationCell *Cell5;
    
    IBOutlet UIView *DatePickerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    
    NSString *DatePickerSelectionStr;
    NSString *StartDateStr;
    NSString *EndDateStr;
    
    NSString *LocationSting;
    NSString *TrackID;
    NSString *StartDate;
    NSString *EndDate;
    NSDictionary *UserDic;
    NSString *TaskSelect;
    
    NSDateFormatter *dateFormat;
    int offset;
    int limit;
    AppDelegate *delegate;


}

@end

@implementation EmployeeFilterViewController
@synthesize isComefrom,EmployeeDic;
- (void)viewDidLoad {
    offset = 0;
    limit = 50;
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    TrackID = @"";
    StartDate = @"";
    EndDate = @"";
    TaskSelect = @"";
    DatePickerSelectionStr = @"";
    arrayForBool = [[NSMutableArray alloc] init];
    PlusImageArray = [[NSMutableArray alloc] initWithObjects:@"Plus",@"Plus",@"Plus", nil];
    MinusImageArray = [[NSMutableArray alloc] initWithObjects:@"Minus",@"Minus",@"Minus", nil];
    FilterArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"DURATION_FILTER", nil),NSLocalizedString(@"TASK_ID", nil),NSLocalizedString(@"TASK_TYPE", nil), nil];
    
    UINib *SearchNib = [UINib nibWithNibName:@"RatingCell" bundle:nil];
    [FilterTableView registerNib:SearchNib forCellReuseIdentifier:@"CELL12"];
    
    UINib *LocationNib = [UINib nibWithNibName:@"MyWorkLocationCell" bundle:nil];
    [FilterTableView registerNib:LocationNib forCellReuseIdentifier:@"CELL1"];
    
    UINib *SelectionNib = [UINib nibWithNibName:@"DurationCell" bundle:nil];
    [FilterTableView registerNib:SelectionNib forCellReuseIdentifier:@"CELL2"];
    arrayForBool=[[NSMutableArray alloc]init];
   
    for (int i=0; i<[FilterArray count]; i++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    FilterTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, FilterTableView.bounds.size.width, 0.01f)];

    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    CancelBtn.layer.cornerRadius = 3.0;
    CancelBtn.clipsToBounds = YES;
    
    CancelFilterBtn.layer.borderWidth = 1.0;
    CancelFilterBtn.layer.borderColor = [ColorCategory PurperColor].CGColor;
    CancelFilterBtn.layer.cornerRadius = 3.0;
    CancelFilterBtn.clipsToBounds = YES;
    CancelFilterBtn.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    ApplyFilterBtn.layer.cornerRadius = 3.0;
    BottomView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    self.view.backgroundColor = [UIColor whiteColor];
    FilterTableView.backgroundColor = [UIColor whiteColor];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [FilterArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        DurationCell * Cell = (DurationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        
        if([DatePickerSelectionStr isEqualToString:@"Start"] || [DatePickerSelectionStr isEqualToString:@"End"]){
            Cell.StartDateTextField.delegate = self;
            Cell.EndDateTextField.delegate = self;
            Cell.StartDateTextField.inputView = DatePickerViewBg;
            Cell.EndDateTextField.inputView = DatePickerViewBg;
            Cell.DateView.hidden = NO;
            if([DatePickerSelectionStr isEqualToString:@"Start"]){
                Cell.StartDateTextField.text = StartDateStr;
                Cell.EndDateTextField.text = EndDateStr;
            }
            else if ([DatePickerSelectionStr isEqualToString:@"End"]){
                Cell.EndDateTextField.text = EndDateStr;
                Cell.StartDateTextField.text = StartDateStr;
            }
        }
        else{
            Cell.StartDateTextField.delegate = self;
            Cell.EndDateTextField.delegate = self;
            Cell.StartDateTextField.inputView = DatePickerViewBg;
            Cell.EndDateTextField.inputView = DatePickerViewBg;
            Cell.DateView.hidden = NO;
        }
        return Cell;
    }
    else if (indexPath.section == 1){
        MyWorkLocationCell * Cell = (MyWorkLocationCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        Cell.LocationTextField.placeholder = NSLocalizedString(@"SEARCH_TASK_ID", nil);
        Cell.LocationTextField.keyboardType = UIKeyboardTypeDefault;
        return Cell;
    }
    else{
         RatingCell * Cell = (RatingCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL12" forIndexPath:indexPath];
        
        if([TaskSelect isEqualToString:@"task"]){
            Cell.LowestImageView.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
            Cell.LowestImageView.image = [UIImage imageNamed:@"RadioOff"];
        }
        if([TaskSelect isEqualToString:@"assignment"]){
            
            Cell.HigestImageView.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
            Cell.HigestImageView.image = [UIImage imageNamed:@"RadioOff"];
        }
        if([TaskSelect isEqualToString:@"both"]){
            
            Cell.AllImageView.image = [UIImage imageNamed:@"RadioOn"];
        }
        else{
            Cell.AllImageView.image = [UIImage imageNamed:@"RadioOff"];
        }
        [Cell.LowestBtn addTarget:self action:@selector(RatingSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.HigestBtn addTarget:self action:@selector(RatingSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
         [Cell.AllBtn addTarget:self action:@selector(RatingSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        return Cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0 ||indexPath.section == 1){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return 60;
        }
        else{
            return 0;
        }
    }
   else if(indexPath.section == 2){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return 117;
        }
        else{
            return 0;
        }
    }
    return 0;
    
}
-(void)RatingSelectBtn:(UIButton*)Sender{
    switch (Sender.tag) {
        case 0:
            TaskSelect = @"task";
            [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        case 1:
            TaskSelect = @"assignment";
            [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        case 2:
            TaskSelect = @"both";
            [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        default:
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
#pragma mark - Creating View for TableView Section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *Views=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,0)];
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FilterTableView.frame.size.width,44)];
    sectionView.tag=section;
    sectionView.backgroundColor  = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    UILabel *viewLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, FilterTableView.frame.size.width, 44)];
    viewLabel.backgroundColor=[UIColor clearColor];
    viewLabel.textAlignment = NSTextAlignmentLeft;
    viewLabel.textColor=[UIColor blackColor];
    viewLabel.font=[UIFont systemFontOfSize:14];
    viewLabel.text=[NSString stringWithFormat:@"%@",[FilterArray objectAtIndex:section]];
    [sectionView addSubview:viewLabel];
    
    UIImageView *ImageViews = [[UIImageView alloc] initWithFrame:CGRectMake(FilterTableView.frame.size.width-40, 17, 10, 10)];
    if ([[arrayForBool objectAtIndex:section] boolValue]){
        ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[MinusImageArray objectAtIndex:section]]];
    }
    else{
        ImageViews.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[PlusImageArray objectAtIndex:section]]];
    }
    [sectionView addSubview:ImageViews];
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    return  sectionView;
    return Views;
}
#pragma mark - Table header gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[FilterArray count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(IBAction)CancelAction:(id)sender{
    [self GetCellValue];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)MyTeamApplyFilter:(id)sender{
    [self GetCellValue];
    TrackID = Cell4.LocationTextField.text;
    StartDate  = Cell5.StartDateTextField.text;
    EndDate  = Cell5.EndDateTextField.text;
    if([isComefrom isEqualToString:@"Complete"]){
        UserDic = @ {
            @"fromDate": StartDate,
            @"handsetId": @"",
            @"status": @"4",
            @"taskId": TrackID,
            @"taskType": TaskSelect,
            @"toDate": EndDate,
            @"user_id"  : [EmployeeDic valueForKey:@"empId"],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        NSLog(@"UserDic===%@",UserDic);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"COMPLETE" object:nil userInfo:UserDic];
    }
    else if([isComefrom isEqualToString:@"OnSchudel"]){
        UserDic = @ {
            @"fromDate": StartDate,
            @"handsetId": @"",
            @"status": @"2",
            @"taskId": TrackID,
            @"taskType": TaskSelect,
            @"toDate": EndDate,
            @"user_id"  : [EmployeeDic valueForKey:@"empId"],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        NSLog(@"UserDic===%@",UserDic);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ONSCHUDLE" object:nil userInfo:UserDic];
    }
    else if([isComefrom isEqualToString:@"Behind"]){
        UserDic = @ {
            @"fromDate": StartDate,
            @"handsetId": @"",
            @"status": @"5",
            @"taskId": TrackID,
            @"taskType": TaskSelect,
            @"toDate": EndDate,
            @"user_id"  : [EmployeeDic valueForKey:@"empId"],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        NSLog(@"UserDic===%@",UserDic);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BEHIND" object:nil userInfo:UserDic];
    }
    else if([isComefrom isEqualToString:@"All"]){
        UserDic = @ {
            @"fromDate": StartDate,
            @"handsetId":@"",
            @"status": @"0",
            @"taskId": TrackID,
            @"taskType": TaskSelect,
            @"toDate": EndDate,
            @"user_id"  : [EmployeeDic valueForKey:@"empId"],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        NSLog(@"UserDic===%@",UserDic);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ALL" object:nil userInfo:UserDic];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    DatePickerViewBg.hidden = NO;
    [self GetCellValue];
    if([textField isEqual:Cell5.StartDateTextField]){
        DatePickerSelectionStr = @"Start";
    }
    else if([textField isEqual:Cell5.EndDateTextField]){
        DatePickerSelectionStr = @"End";
    }
}
-(IBAction)DatePickerDoneAction{
    DatePickerViewBg.hidden = YES;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if([DatePickerSelectionStr isEqualToString:@"Start"]){
        StartDateStr = [dateFormat stringFromDate: DatePicker.date];
        EndDateStr = @"";
    }
    else if([DatePickerSelectionStr isEqualToString:@"End"]){
        EndDateStr = [dateFormat stringFromDate: DatePicker.date];
    }
    [FilterTableView reloadData];
    if(![EndDateStr isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:StartDateStr];
        NSDate *endDate = [dateFormat dateFromString:EndDateStr];
        result = [startDate compare:endDate];
        
        if(result==NSOrderedAscending){
            NSLog(@"today is less");
            [self ReloadSection];
        }
        else if(result==NSOrderedDescending){
            NSLog(@"newDate is less");
            EndDateStr = @"";
            [self ShowAlert:@"You cannot select date earlier to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
}
-(void)ReloadSection{
     [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)GetCellValue{
    indexPath5 = [NSIndexPath indexPathForRow:0 inSection:0];
    Cell5 = [FilterTableView cellForRowAtIndexPath:indexPath5];
    [Cell5.StartDateTextField resignFirstResponder];
    [Cell5.EndDateTextField resignFirstResponder];
    indexPath4 = [NSIndexPath indexPathForRow:0 inSection:1];
    Cell4 = [FilterTableView cellForRowAtIndexPath:indexPath4];
    [Cell4.LocationTextField resignFirstResponder];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}

@end
