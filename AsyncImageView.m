//
//  AsyncImageView.m
//  Simu Tv
//
//  Created on 8/17/14.
//  Copyright (c) 2014. All rights reserved.
//

#import "AsyncImageView.h"
#import "QuartzCore/QuartzCore.h"

// This class demonstrates how the URL loading system can be used to make a UIView subclass
// that can download and display an image asynchronously so that the app doesn't block or freeze
// while the image is downloading. It works fine in a UITableView or other cases where there
// are multiple images being downloaded and displayed all at the same time. 

@implementation AsyncImageView
@synthesize myframe;


- (void)dealloc {
	[connection cancel]; //in case the URL is still downloading
    
}

// downloaad image from url
- (void)loadImageFromURL:(NSURL*)url {
	if (connection!=nil) { [connection cancel]; } //in case we are downloading a 2nd image
	if (data!=nil) { data =nil; }
	
//	UIActivityIndicatorView *myIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//	myIndicator.center = CGPointMake(myframe.size.width/2,myframe.size.height/2);
//	[myIndicator startAnimating];
//	[self addSubview:myIndicator];	
//    [myIndicator release];
    
   // UIImageView *imgvw=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, myframe.size.width, myframe.size.height)];
    //UIImage *myImage = [UIImage imageNamed:@"Icon-60"];
    //UIImage *myResizableImage = [myImage resizableImageWithCapInsets:UIEdgeInsetsMake(21.0, 13.0, 21.0, 13.0)];
   // imgvw.image=[UIImage imageNamed:@"icon.png"];
    //imgvw.image=myImage;
        
   // imgvw.tag=100;
	//[self addSubview:imgvw];
	
    
    
	NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:100.0];
	
	connection = [[NSURLConnection alloc] initWithRequest:request delegate:self]; //notice how delegate set to self object
	//TODO error handling, what if connection is nil?
}
- (void)loadImageFromFile:(NSString*)filename
{
    
    UIActivityIndicatorView *myIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	myIndicator.center = CGPointMake(myframe.size.width/2,myframe.size.height/2);
	[myIndicator startAnimating];
	[self addSubview:myIndicator];
    if ([[self subviews] count]>0) {
		//then this must be another image, the old one is still in subviews
		[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
	}
	
	//make an image view for the image
	UIImage *img = [UIImage imageWithContentsOfFile:filename];
	
	UIImageView* imageView = [[UIImageView alloc] initWithImage:img];
	//make sizing choices based on your needs, experiment with these. maybe not all the calls below are needed.
	imageView.frame = myframe;//CGRectMake(-2,-2,55,55);
    
	CALayer * l = [imageView layer];
	[l setMasksToBounds:YES];
    [l setCornerRadius:7.0];
	[self addSubview:imageView ];
	[imageView setNeedsLayout];
	[self setNeedsLayout];
}
- (void)loadImageFromData:(NSData*)imgdata 
{
    UIActivityIndicatorView *myIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	myIndicator.center = CGPointMake(myframe.size.width/2,myframe.size.height/2);
	[myIndicator startAnimating];
	[self addSubview:myIndicator];
    if ([[self subviews] count]>0) {
		//then this must be another image, the old one is still in subviews
		[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
	}
	
	//make an image view for the image
	UIImage *img = [UIImage imageWithData:imgdata];
	
	UIImageView* imageView = [[UIImageView alloc] initWithImage:img] ;
	//make sizing choices based on your needs, experiment with these. maybe not all the calls below are needed.
	imageView.frame = myframe;//CGRectMake(-2,-2,55,55);
    
    CALayer * l = [imageView layer];
	[l setMasksToBounds:YES];
    [l setCornerRadius:7.0];
	[self addSubview:imageView ];
	[imageView setNeedsLayout];
	[self setNeedsLayout];
}
- (void)loadImageFromImage:(UIImage*)tempimg
{
    UIActivityIndicatorView *myIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	myIndicator.center = CGPointMake(myframe.size.width/2,myframe.size.height/2);
	[myIndicator startAnimating];
	[self addSubview:myIndicator];
    if ([[self subviews] count]>0) {
		//then this must be another image, the old one is still in subviews
		[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
	}
	
	//make an image view for the image
	UIImage *img = tempimg;
	
	UIImageView* imageView = [[UIImageView alloc] initWithImage:img];
	//make sizing choices based on your needs, experiment with these. maybe not all the calls below are needed.
	imageView.frame = myframe;//CGRectMake(-2,-2,55,55);
    
	CALayer * l = [imageView layer];
	[l setMasksToBounds:YES];
    [l setCornerRadius:7.0];
	[self addSubview:imageView ];
	[imageView setNeedsLayout];
	[self setNeedsLayout]; 
}
//the URL connection calls this repeatedly as data arrives
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	if (data==nil) { data = [[NSMutableData alloc] initWithCapacity:2048]; } 
	[data appendData:incrementalData];
}

//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
	//so self data now has the complete image 
	connection=nil;
	if ([[self subviews] count]>0) {
		//then this must be another image, the old one is still in subviews
		[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
	}
	delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
	//make an image view for the image
	UIImage *img = [UIImage imageWithData:data];
	
	UIImageView* imageView = [[UIImageView alloc] initWithImage:img];
	//make sizing choices based on your needs, experiment with these. maybe not all the calls below are needed.
	imageView.frame = myframe;//CGRectMake(-2,-2,55,55);
		
	CALayer * l = [imageView layer];
	[l setMasksToBounds:YES];
    if (delegate.iscorner)
        [l setCornerRadius:7.0];
    else
        [l setCornerRadius:0.0];
	[self addSubview:imageView ];
	[imageView setNeedsLayout];
	[self setNeedsLayout];
	
	data=nil;
}

//just in case you want to get the image directly, here it is in subviews
- (UIImage*) image {
	UIImageView* iv = [[self subviews] objectAtIndex:0];
	return [iv image];
	
 
}

@end
