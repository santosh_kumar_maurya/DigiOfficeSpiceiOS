//
//  SubmitCell.m
//  iWork
//
//  Created by Shailendra on 05/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "SubmitCell.h"

@implementation SubmitCell

- (void)awakeFromNib {
    [super awakeFromNib];
  
    self.backgroundColor = [UIColor clearColor];
    _SubmitTaskView.layer.cornerRadius = 6.0;
    _SubmitTaskView.maskView.layer.cornerRadius = 7.0f;
    _SubmitTaskView.layer.shadowRadius = 3.0f;
    _SubmitTaskView.layer.shadowColor = [UIColor blackColor].CGColor;
    _SubmitTaskView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _SubmitTaskView.layer.shadowOpacity = 0.7f;
    _SubmitTaskView.layer.masksToBounds = NO;
    
    _SubmitProgressView.layer.cornerRadius = 6.0;
    _SubmitProgressView.maskView.layer.cornerRadius = 7.0f;
    _SubmitProgressView.layer.shadowRadius = 3.0f;
    _SubmitProgressView.layer.shadowColor = [UIColor blackColor].CGColor;
    _SubmitProgressView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _SubmitProgressView.layer.shadowOpacity = 0.7f;
    _SubmitProgressView.layer.masksToBounds = NO;
    
    _GiveFeedBackView.layer.cornerRadius = 6.0;
    _GiveFeedBackView.maskView.layer.cornerRadius = 7.0f;
    _GiveFeedBackView.layer.shadowRadius = 3.0f;
    _GiveFeedBackView.layer.shadowColor = [UIColor blackColor].CGColor;
    _GiveFeedBackView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _GiveFeedBackView.layer.shadowOpacity = 0.7f;
    _GiveFeedBackView.layer.masksToBounds = NO;

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
