//
//  MyTaskFilterViewController.m
//  iWork
//
//  Created by Shailendra on 20/07/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTaskFilterViewController.h"
#import "Header.h"

@interface MyTaskFilterViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    
    IBOutlet UITableView *FilterTableView;
    IBOutlet UIView *DatePickerViewBg;
    IBOutlet UIDatePicker *DatePicker;
    NSDateFormatter *dateFormat;
    int offset;
    int limit;
    AppDelegate *delegate;
    NSString *DatePickerSelectionStr;
    NSString *StartDateStr;
    NSString *EndDateStr;
    NSString *TrackID;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *CancelFilterBtn;
    IBOutlet UIButton *ApplyFilterBtn;
    IBOutlet UIView *BottomView;
    NSString *TaskID;
    NSString *TaskTypeSelect;
    NSString *TaskRequestStatusSelect;
    NSString *RatingSelect;
    NSString *HistoryTaskRequestStatusSelect;
    NSDictionary *UserInfo;
    NSIndexPath *indexPath1;
    TaskIDCell *Cell1;
    
}
@end

@implementation MyTaskFilterViewController
@synthesize isComeFrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    offset = 0;
    limit = 50;
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    TrackID = @"";
    TaskTypeSelect = @"both";
    DatePickerSelectionStr = @"";
    TaskRequestStatusSelect = @"Both";
    RatingSelect = @"all";
    HistoryTaskRequestStatusSelect = @"all";
    
    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    CancelBtn.layer.cornerRadius = 3.0;
    CancelBtn.clipsToBounds = YES;
    
    CancelFilterBtn.layer.borderWidth = 1.0;
    CancelFilterBtn.layer.borderColor = [ColorCategory PurperColor].CGColor;
    CancelFilterBtn.layer.cornerRadius = 3.0;
    CancelFilterBtn.clipsToBounds = YES;
    CancelFilterBtn.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
    
    ApplyFilterBtn.layer.cornerRadius = 3.0;
    BottomView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:236.0/255 blue:236.0/255.0 alpha:1];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
     return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    if([isComeFrom isEqualToString:@"NEW"]){
        return 4;
    }
    else if([isComeFrom isEqualToString:@"ONGOING"]){
        return 3;
    }
    else if([isComeFrom isEqualToString:@"HISTORY"]){
        return 5;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *Cell;
    if([isComeFrom isEqualToString:@"NEW"]){
        if(indexPath.row == 0){
            DateCell * Cell = (DateCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
            [Cell.StartDateButton addTarget:self action:@selector(StartDateAction) forControlEvents:UIControlEventTouchUpInside];
            [Cell.EndDateButton addTarget:self action:@selector(EndDateAction) forControlEvents:UIControlEventTouchUpInside];
            if([DatePickerSelectionStr isEqualToString:@"Start"] || [DatePickerSelectionStr isEqualToString:@"End"]){
                
                if([DatePickerSelectionStr isEqualToString:@"Start"]){
                    Cell.StartDateLabel.text = StartDateStr;
                    Cell.EndDateLabel.text = EndDateStr;
                }
                else if ([DatePickerSelectionStr isEqualToString:@"End"]){
                    Cell.StartDateLabel.text = EndDateStr;
                    Cell.EndDateLabel.text = StartDateStr;
                }
            }
            return Cell;
        }
        else if(indexPath.row == 1){
            TaskIDCell * Cell = (TaskIDCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            return Cell;
        }
        else if(indexPath.row == 2){
            TaskTypeCell * Cell = (TaskTypeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
           
            if([TaskTypeSelect  isEqualToString:@"task"]){
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"assignments"]){
                
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"both"]){
                
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.TaskButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AssignmentButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.BothButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
        else {
            TaskRequestStatusCell * Cell = (TaskRequestStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            
            if([TaskRequestStatusSelect isEqualToString:@"Assigned"]){
                Cell.AssignedImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AssignedImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskRequestStatusSelect isEqualToString:@"New"]){
                
                Cell.NewImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.NewImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskRequestStatusSelect isEqualToString:@"Both"]){
                
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.AssignedButton addTarget:self action:@selector(TaskRequestAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.NewButton addTarget:self action:@selector(TaskRequestAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.BothButton addTarget:self action:@selector(TaskRequestAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
    }
    else if([isComeFrom isEqualToString:@"ONGOING"]){
        if(indexPath.row == 0){
            DateCell * Cell = (DateCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
            [Cell.StartDateButton addTarget:self action:@selector(StartDateAction) forControlEvents:UIControlEventTouchUpInside];
            [Cell.EndDateButton addTarget:self action:@selector(EndDateAction) forControlEvents:UIControlEventTouchUpInside];
            if([DatePickerSelectionStr isEqualToString:@"Start"] || [DatePickerSelectionStr isEqualToString:@"End"]){
                
                if([DatePickerSelectionStr isEqualToString:@"Start"]){
                    Cell.StartDateLabel.text = StartDateStr;
                    Cell.EndDateLabel.text = EndDateStr;
                }
                else if ([DatePickerSelectionStr isEqualToString:@"End"]){
                    Cell.StartDateLabel.text = EndDateStr;
                    Cell.EndDateLabel.text = StartDateStr;
                }
            }
            return Cell;
        }
        else if(indexPath.row == 1){
            TaskIDCell * Cell = (TaskIDCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            return Cell;
        }
        else {
            TaskTypeCell * Cell = (TaskTypeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            
            if([TaskTypeSelect  isEqualToString:@"task"]){
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"assignments"]){
                
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"both"]){
                
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.TaskButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AssignmentButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.BothButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
        
    }
    else{
        if(indexPath.row == 0){
            DateCell * Cell = (DateCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
            [Cell.StartDateButton addTarget:self action:@selector(StartDateAction) forControlEvents:UIControlEventTouchUpInside];
            [Cell.EndDateButton addTarget:self action:@selector(EndDateAction) forControlEvents:UIControlEventTouchUpInside];
            if([DatePickerSelectionStr isEqualToString:@"Start"] || [DatePickerSelectionStr isEqualToString:@"End"]){
                
                if([DatePickerSelectionStr isEqualToString:@"Start"]){
                    Cell.StartDateLabel.text = StartDateStr;
                    Cell.EndDateLabel.text = EndDateStr;
                }
                else if ([DatePickerSelectionStr isEqualToString:@"End"]){
                    Cell.StartDateLabel.text = EndDateStr;
                    Cell.EndDateLabel.text = StartDateStr;
                }
            }
            return Cell;
        }
        else if(indexPath.row == 1){
            TaskIDCell * Cell = (TaskIDCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            return Cell;
        }
        else if(indexPath.row == 2){
            TaskTypeCell * Cell = (TaskTypeCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            
            if([TaskTypeSelect  isEqualToString:@"task"]){
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.TaskImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"assignments"]){
                
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AssignmentImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([TaskTypeSelect isEqualToString:@"both"]){
                
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.BothImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.TaskButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AssignmentButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.BothButton addTarget:self action:@selector(AskTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
        else if(indexPath.row == 3){
            HistoryRatingCell * Cell = (HistoryRatingCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL4" forIndexPath:indexPath];
            
            if([RatingSelect  isEqualToString:@"lowest"]){
                Cell.LowestImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.LowestImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([RatingSelect isEqualToString:@"highest"]){
                
                Cell.HighestImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.HighestImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([RatingSelect isEqualToString:@"all"]){
                
                Cell.AllImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AllImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.LowestButton addTarget:self action:@selector(RatingAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.HighestButton addTarget:self action:@selector(RatingAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AllButton addTarget:self action:@selector(RatingAction:) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
        else{
            HistoryTaskRequestStatusCell * Cell = (HistoryTaskRequestStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL5" forIndexPath:indexPath];
            
            if([HistoryTaskRequestStatusSelect  isEqualToString:@"Approved"]){
                Cell.ApprovedImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.ApprovedImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([HistoryTaskRequestStatusSelect isEqualToString:@"Rejected"]){
                
                Cell.RejectedImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.RejectedImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([HistoryTaskRequestStatusSelect isEqualToString:@"declined"]){
                
                Cell.DecliendImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.DecliendImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            if([HistoryTaskRequestStatusSelect isEqualToString:@"all"]){
                
                Cell.AllImageView.image = [UIImage imageNamed:@"RadioOn"];
            }
            else{
                Cell.AllImageView.image = [UIImage imageNamed:@"RadioOff"];
            }
            [Cell.ApprovedButton addTarget:self action:@selector(HistoryTaskRequestStatusAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.RejectedButton addTarget:self action:@selector(HistoryTaskRequestStatusAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.DecliendButton addTarget:self action:@selector(HistoryTaskRequestStatusAction:) forControlEvents:UIControlEventTouchUpInside];
            [Cell.AllButton addTarget:self action:@selector(HistoryTaskRequestStatusAction:) forControlEvents:UIControlEventTouchUpInside];
            
            return Cell;
        }
    }
    return Cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([isComeFrom isEqualToString:@"HISTORY"]){
        if(indexPath.row == 4){
            return 103;
        }
    }
    return 69;
}

-(void)AskTypeAction:(UIButton*)sender{
    switch (sender.tag) {
        case 0:
            TaskTypeSelect = @"task";
            [FilterTableView reloadData];
            break;
        case 1:
            TaskTypeSelect = @"assignments";
            [FilterTableView reloadData];
            break;
        case 2:
            TaskTypeSelect = @"both";
            [FilterTableView reloadData];
            break;
        default:
            break;
    }
}
-(void)TaskRequestAction:(UIButton*)sender{
    switch (sender.tag) {
        case 0:
            TaskRequestStatusSelect = @"Assigned";
            [FilterTableView reloadData];
            break;
        case 1:
            TaskRequestStatusSelect = @"New";
            [FilterTableView reloadData];
            break;
        case 2:
            TaskRequestStatusSelect = @"Both";
            [FilterTableView reloadData];
            break;
        default:
            break;
    }
}
-(void)RatingAction:(UIButton*)sender{
    switch (sender.tag) {
        case 0:
            RatingSelect = @"lowest";
            [FilterTableView reloadData];
            break;
        case 1:
            RatingSelect = @"highest";
            [FilterTableView reloadData];
            break;
        case 2:
            RatingSelect = @"all";
            [FilterTableView reloadData];
            break;
        default:
            break;
    }
}
-(void)HistoryTaskRequestStatusAction:(UIButton*)sender{
    switch (sender.tag) {
        case 0:
            HistoryTaskRequestStatusSelect = @"Approved";
            [FilterTableView reloadData];
            break;
        case 1:
            HistoryTaskRequestStatusSelect = @"Rejected";
            [FilterTableView reloadData];
            break;
        case 2:
            HistoryTaskRequestStatusSelect = @"declined";
            [FilterTableView reloadData];
            break;
        case 3:
            HistoryTaskRequestStatusSelect = @"all";
            [FilterTableView reloadData];
            break;

        default:
            break;
    }
}

-(void)StartDateAction{
    DatePickerSelectionStr = @"Start";
    DatePickerViewBg.hidden = NO;
}
-(void)EndDateAction{
    DatePickerSelectionStr = @"End";
    DatePickerViewBg.hidden = NO;

}
-(IBAction)CancelBtn:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)CancelFilterBtn:(id)sender{
   [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)SubmitBtn:(id)sender{
    
    indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    Cell1 = [FilterTableView cellForRowAtIndexPath:indexPath1];
    TrackID = Cell1.TaskIDTextField.text;
    if([StartDateStr isEqualToString:@"Start Date"]|| StartDateStr == nil){
        StartDateStr = @"";
    }
    if([EndDateStr isEqualToString:@"End Date"]|| EndDateStr == nil){
        EndDateStr = @"";
    }
    if([isComeFrom isEqualToString:@"NEW"]){
        UserInfo = @ {
            @"eEndDate" : EndDateStr,
            @"eStartDate" :StartDateStr,
            @"taskId" :TrackID,
            @"task_request_status": TaskRequestStatusSelect,
            @"task_type": TaskTypeSelect,
            @"employeeId" : [ApplicationState userId],
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        
        NSLog(@"UserInfo==%@",UserInfo);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NEWTASK" object:nil userInfo:UserInfo];
    }
    else if([isComeFrom isEqualToString:@"ONGOING"]){
        UserInfo = @ {
            @"end_date" : EndDateStr,
            @"start_date" :StartDateStr,
            @"taskId" :TrackID,
            @"task_type": TaskTypeSelect,
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        NSLog(@"UserInfo==%@",UserInfo);
       [[NSNotificationCenter defaultCenter] postNotificationName:@"ONGOINGTASK" object:nil userInfo:UserInfo];
    }
    else if([isComeFrom isEqualToString:@"HISTORY"]){
        UserInfo = @ {
            @"end_date" : EndDateStr,
            @"start_date" :StartDateStr,
            @"taskId" :TrackID,
            @"task_type": TaskTypeSelect,
            @"rating" : RatingSelect,
            @"task_request_status" : HistoryTaskRequestStatusSelect,
            @"offset" : [NSNumber numberWithInt:offset],
            @"limit" : [NSNumber numberWithInt:limit]
        };
        NSLog(@"UserInfo==%@",UserInfo);
      [[NSNotificationCenter defaultCenter] postNotificationName:@"HISTORYTASK" object:nil userInfo:UserInfo];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)DatePickerDoneAction{
    DatePickerViewBg.hidden = YES;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if([DatePickerSelectionStr isEqualToString:@"Start"]){
        StartDateStr = [dateFormat stringFromDate: DatePicker.date];
        EndDateStr = @"End Date";
    }
    else if([DatePickerSelectionStr isEqualToString:@"End"]){
        EndDateStr = [dateFormat stringFromDate: DatePicker.date];
    }
    [FilterTableView reloadData];
    if(![EndDateStr isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:StartDateStr];
        NSDate *endDate = [dateFormat dateFromString:EndDateStr];
        result = [startDate compare:endDate];
        if(result==NSOrderedAscending){
            [self ReloadSection];
        }
        else if(result==NSOrderedDescending){
            EndDateStr = @"End Date";
            StartDateStr = @"Start Date";
            [self ShowAlert:@"You cannot select date earlier to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
    [FilterTableView reloadData];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ReloadSection{
    [FilterTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
