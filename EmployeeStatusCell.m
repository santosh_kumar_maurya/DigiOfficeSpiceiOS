//
//  EmployeeStatusCell.m
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeStatusCell.h"
#import "Header.h"
@implementation EmployeeStatusCell

- (void)awakeFromNib {
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [super awakeFromNib];
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    self.layer.shadowRadius = 3.0f;
    self.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
    self.layer.shadowOpacity = 0.7f;
    self.layer.masksToBounds = NO;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.TaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.TaskView.layer.mask = maskLayer2;
    
    CAShapeLayer * maskLayer3 = [CAShapeLayer layer];
    maskLayer3.path = [UIBezierPath bezierPathWithRoundedRect: self.DetailsBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    self.DetailsBtn.layer.mask = maskLayer3;
    
}
- (void)configureCell:(NSDictionary *)info{
  
    if(IsSafeStringPlus(TrToString(info[@"taskId"]))){
        _TaskIdLabel.text  = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),info[@"taskId"]];
    }
    else{
        _TaskIdLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        if ([info[@"creationDate"] isKindOfClass:[NSString class]]){
            _DateLabel.text  = info[@"creationDate"];
        }
        else{
            NSNumber *datenumber = info[@"creationDate"];
            _DateLabel.text = [self DateFormateChange:datenumber];
        }
    }
    else if(IsSafeStringPlus(TrToString(info[@"creationOn"]))){
        NSNumber *datenumber = info[@"creationOn"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskName"]))){
        _TaskNameLabel.text  = info[@"taskName"];
    }
    else if(IsSafeStringPlus(TrToString(info[@"taskNAme"]))){
        _TaskNameLabel.text  = info[@"taskNAme"];
    }
    else{
        _TaskNameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"type"]))){
        _CreatedByLabel.text  = info[@"type"];
    }
    else if(IsSafeStringPlus(TrToString(info[@"taskType"]))){
        _CreatedByLabel.text  = info[@"taskType"];
    }
    else{
        _CreatedByLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"duration"]))){
        _DurationLabel.text  = info[@"duration"];
        if([_DurationLabel.text isEqualToString:@"-"]){
            _DurationLabel.text = [_DurationLabel.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
            _DurationStaticLabel.text = @"Overdue";
        }
    }
    else{
        _DurationLabel.text  = @" ";
    }
    
    if(IsSafeStringPlus(TrToString(info[@"kpi"]))){
        _KPILabel.text  = info[@"kpi"];
    }
    else{
        _KPILabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskStatus"]))){
        _StatusLabel.text  = info[@"taskStatus"];
    }
    else{
        _StatusLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"rating"]))){
        _RatingView.rating  = [info[@"rating"] floatValue];
        _RatingView.starFillColor = delegate.redColor;
        _RatingView.starSize = 10.0;
    }
    else{
        _RatingView.rating  = 0;
        _RatingView.starSize = 10.0;
    }
    NSString *statusType;
    NSString *imageType;
    UIColor *colorType;
    if ([[info objectForKey:@"taskStatus"] isEqualToString:@"Cancelled"]||[[info valueForKey:@"status"] isEqualToString:@"Cancelled"]){
        statusType = @"Cancelled";
        imageType = @"cancelled_new";
        colorType = delegate.redColor;
    }
    else if ([[info objectForKey:@"taskStatus"] isEqualToString:@"Discarded"]||[[info valueForKey:@"status"] isEqualToString:@"Discarded"]){
        statusType = @"Discarded";
        imageType = @"discarded_new";
        colorType = delegate.discardedColor;
    }
    else if([[info objectForKey:@"taskStatus"] isEqualToString:@"Declined"]||[[info valueForKey:@"status"] isEqualToString:@"Declined"]){
        statusType = @"Declined";
        imageType = @"declined_new";
        colorType = delegate.failedColor;
    }
    else if([[info objectForKey:@"taskStatus"] isEqualToString:@"FAILED"]||[[info valueForKey:@"status"] isEqualToString:@"FAILED"]){
        statusType = @"Declined";
        imageType = @"Declined_new";
        colorType = delegate.failedColor;
    }
    else if([[info objectForKey:@"taskStatus"] isEqualToString:@"In-Progress"]){
        statusType = @"In-Progress";
        imageType = @"inprogressblue";
        colorType = delegate.inprogColor;
    }
    else if([[info valueForKey:@"status"] isEqualToString:@"In-Progress"]){
        statusType = @"In-Progress";
        imageType = @"inprogressblue";
        colorType = delegate.inprogColor;
    }
    else if ([[info objectForKey:@"taskStatus"] isEqualToString:@"Submitted"]||[[info valueForKey:@"status"] isEqualToString:@"Submitted"]){
        statusType = @"Submitted";
        imageType = @"submitted_new";
        colorType = delegate.submittedColor;
    }
    else if ([[info objectForKey:@"taskStatus"] isEqualToString:@"CLOSED"]||[[info valueForKey:@"status"] isEqualToString:@"CLOSED"]){
        statusType = @"Assigned";
        imageType = @"confirmed";
        colorType = delegate.confColor;
    }
    else if ([[info objectForKey:@"taskStatus"] isEqualToString:@"Assigned"]){
        statusType = @"Assigned";
        imageType = @"assigned";
        colorType = delegate.assignColor;
    }
    else if([[info valueForKey:@"status"] isEqualToString:@"Assigned"]){
        statusType = @"Assigned";
        imageType = @"assigned";
        colorType = delegate.assignColor;
    }
    else if([[info valueForKey:@"new_status"] isEqualToString:@"Assigned"]){
        statusType = @"Assigned";
        imageType = @"assigned";
        colorType = delegate.assignColor;
    }
    else if ([[info objectForKey:@"taskStatus"] isEqualToString:@"New"]){
        statusType = @"New";
        imageType = @"pending_new";
        colorType = delegate.pendingColor;
    }
    else if([[info valueForKey:@"status"] isEqualToString:@"new"]){
        statusType = @"New";
        imageType = @"pending_new";
        colorType = delegate.pendingColor;
    }
    NSLog(@"statusType==%@",[info objectForKey:@"status"]);
    _StatusLabel.text = statusType;
    _StatusLabel.textColor = colorType;
    _StausImageView.image = [UIImage imageNamed:imageType];
}

-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm:aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
