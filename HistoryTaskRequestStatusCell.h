//
//  HistoryTaskRequestStatusCell.h
//  iWork
//
//  Created by Shailendra on 23/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTaskRequestStatusCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UIButton *ApprovedButton;
@property(nonatomic,retain)IBOutlet UIButton *RejectedButton;
@property(nonatomic,retain)IBOutlet UIButton *DecliendButton;
@property(nonatomic,retain)IBOutlet UIButton *AllButton;

@property(nonatomic,retain)IBOutlet UIImageView *ApprovedImageView;
@property(nonatomic,retain)IBOutlet UIImageView *RejectedImageView;
@property(nonatomic,retain)IBOutlet UIImageView *DecliendImageView;
@property(nonatomic,retain)IBOutlet UIImageView *AllImageView;
@end
