//
//  EmployeeDetailsParentViewController.m
//  iWork
//
//  Created by Shailendra on 07/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmployeeDetailsParentViewController.h"
#import "Header.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define kDefaultEdgeInsets UIEdgeInsetsMake(5, 10, 5, 10)
@interface EmployeeDetailsParentViewController ()
{
    NSArray *btnArray;
    AppDelegate *delegate;
    IBOutlet UILabel *HeaderLabel;
    int BtnTagValue;
   
}
@property (weak) IBOutlet UIScrollView *containerScrollView;
@property (weak) IBOutlet UIScrollView *menuScrollView;
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray;
-(void) buttonPressed:(id) sender;
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets;
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr;
@end

@implementation EmployeeDetailsParentViewController
@synthesize Dics;

- (void)viewDidLoad {

    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    _menuScrollView.backgroundColor= delegate.redColor;
    HeaderLabel.text = [Dics valueForKey:@"memberName"];
    
    btnArray = [[NSArray alloc]initWithObjects: NSLocalizedString(@"COMLETED", nil),NSLocalizedString(@"ON_SCHEDULE", nil),NSLocalizedString(@"BEHIND_SCHEDULE", nil),NSLocalizedString(@"ALL", nil),nil];
    [self addButtonsInScrollMenu:btnArray];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    
    CompleteViewController *oneVC = [storyBoard instantiateViewControllerWithIdentifier:@"CompleteViewController"];
    oneVC.CompleteDics = Dics;
    
    OnScheduleViewController *twoVC = [storyBoard instantiateViewControllerWithIdentifier:@"OnScheduleViewController"];
    twoVC.OnScheduleDics = Dics;
    
    BehindViewController *threeVC = [storyBoard instantiateViewControllerWithIdentifier:@"BehindViewController"];
    threeVC.BehindDics = Dics;
    
     AllViewController *fourVC = [storyBoard instantiateViewControllerWithIdentifier:@"AllViewController"];
    fourVC.AllDics = Dics;
    NSArray *controllerArray = @[oneVC, twoVC, threeVC, fourVC];
    [self addChildViewControllersOntoContainer:controllerArray];
   
    
}
#pragma mark - Add Menu Buttons in Menu Scroll View
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray
{
    CGFloat buttonHeight = self.menuScrollView.frame.size.height;
    CGFloat cWidth = 0.0f;
    
    for (int i = 0 ; i<buttonArray.count; i++)
    {
        NSString *tagTitle = [buttonArray objectAtIndex:i];
        
        CGFloat buttonWidth = [self widthForMenuTitle:tagTitle buttonEdgeInsets:kDefaultEdgeInsets];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.titleLabel.numberOfLines = 2;
        
//        if(i == 0 ){
            button.frame = CGRectMake(cWidth, 0.0f, SCREEN_WIDTH/4, buttonHeight);
        
//        }
//        else{
//            button.frame = CGRectMake(SCREEN_WIDTH/2+20, 0.0f, SCREEN_WIDTH/2-20, buttonHeight);
//        }
        [button setTitle:tagTitle forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
        
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [self.menuScrollView addSubview:button];
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height-2, SCREEN_WIDTH/4, 2)];
        bottomView.backgroundColor = [UIColor whiteColor];
        bottomView.tag = 1001;
        [button addSubview:bottomView];
        if (i == 0){
            button.selected = YES;
            [bottomView setHidden:NO];
        }
        else{
            [bottomView setHidden:YES];
        }
        cWidth += SCREEN_WIDTH/4;
    }
    self.menuScrollView.contentSize = CGSizeMake(cWidth, self.menuScrollView.frame.size.height);
}


/**
 *  Any Of the Top Menu Button Press Action
 *
 *  @param sender id of the button pressed
 */

#pragma mark - Menu Button press action
-(void) buttonPressed:(id) sender
{
    UIButton *senderbtn = (UIButton *) sender;
    float buttonWidth = 0.0f;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        UIButton *btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == senderbtn.tag){
            BtnTagValue = btn.tag;
            btn.selected = YES;
            [bottomView setHidden:NO];
        }
        else{
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    [self.containerScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * senderbtn.tag, 0) animated:YES];
    
    float xx = SCREEN_WIDTH * (senderbtn.tag) * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}
#pragma mark - Calculate width of menu button
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets
{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]};
    
    CGSize size = [title sizeWithAttributes:attributes];
    return CGSizeMake(size.width + buttonEdgeInsets.left + buttonEdgeInsets.right, size.height + buttonEdgeInsets.top + buttonEdgeInsets.bottom).width;
}

#pragma mark - Adding all related controllers in to the container
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr
{
    for (int i = 0 ; i < controllersArr.count; i++)
    {
        UIViewController *vc = (UIViewController *)[controllersArr objectAtIndex:i];
        CGRect frame = CGRectMake(0, 0, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height);
        frame.origin.x = SCREEN_WIDTH * i;
        vc.view.frame = frame;
        
        [self addChildViewController:vc];
        [self.containerScrollView addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
    self.containerScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * controllersArr.count + 1, self.containerScrollView.frame.size.height);
    self.containerScrollView.pagingEnabled = YES;
    self.containerScrollView.delegate = self;
}


#pragma mark - Scroll view delegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = (scrollView.contentOffset.x / SCREEN_WIDTH);
    
    UIButton *btn;
    float buttonWidth = 0.0;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        if (btn.tag == page){
             BtnTagValue = btn.tag;
            btn.selected = YES;
            buttonWidth = btn.frame.size.width;
            [bottomView setHidden:NO];
        }
        else{
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    float xx = scrollView.contentOffset.x * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    ObjAppContainerViewController.isComefrom = @"PMS";
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
- (IBAction)FilterBtnAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    EmployeeFilterViewController *ObjFilterViewController = [storyboard instantiateViewControllerWithIdentifier:@"EmployeeFilterViewController"];
    ObjFilterViewController.EmployeeDic = Dics;
    if(BtnTagValue == 0){
        ObjFilterViewController.isComefrom =@"Complete";
    }
    else if(BtnTagValue == 1){
        ObjFilterViewController.isComefrom =@"OnSchudel";
    }
    else if(BtnTagValue == 2){
        ObjFilterViewController.isComefrom =@"Behind";
    }
    else if(BtnTagValue == 3){
        ObjFilterViewController.isComefrom =@"All";
    }
    [self presentViewController:ObjFilterViewController animated:YES completion:nil];
}



@end
