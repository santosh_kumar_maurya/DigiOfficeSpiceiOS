//
//  MyKpiCell.m
//  iWork
//
//  Created by Shailendra on 05/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyKpiCell.h"

@implementation MyKpiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    CurrentPage = 0;
    _PageControl.currentPage = 0;
    _ScrollViews.delegate = self;
    self.backgroundColor = [UIColor clearColor];
    _MyKPIView.layer.cornerRadius = 6.0;
    _MyKPIView.maskView.layer.cornerRadius = 7.0f;
    _MyKPIView.layer.shadowRadius = 3.0f;
    _MyKPIView.layer.shadowColor = [UIColor blackColor].CGColor;
    _MyKPIView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _MyKPIView.layer.shadowOpacity = 0.7f;
    _MyKPIView.layer.masksToBounds = NO;
    
    _MyFeedbackView.layer.cornerRadius = 6.0;
    _MyFeedbackView.maskView.layer.cornerRadius = 7.0f;
    _MyFeedbackView.layer.shadowRadius = 3.0f;
    _MyFeedbackView.layer.shadowColor = [UIColor blackColor].CGColor;
    _MyFeedbackView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _MyFeedbackView.layer.shadowOpacity = 0.7f;
    _MyFeedbackView.layer.masksToBounds = NO;
    
    _FeedbackAsStackHolderView.layer.cornerRadius = 6.0;
    _FeedbackAsStackHolderView.maskView.layer.cornerRadius = 7.0f;
    _FeedbackAsStackHolderView.layer.shadowRadius = 3.0f;
    _FeedbackAsStackHolderView.layer.shadowColor = [UIColor blackColor].CGColor;
    _FeedbackAsStackHolderView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _FeedbackAsStackHolderView.layer.shadowOpacity = 0.7f;
    _FeedbackAsStackHolderView.layer.masksToBounds = NO;
    
    self.ScrollViews.contentSize = CGSizeMake(self.ScrollViews.frame.size.width + 90, self.ScrollViews.frame.size.height);
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView.contentOffset.x==0){
        CurrentPage = 0;
    }
    else if(scrollView.contentOffset.x>80){
        CurrentPage = 2;
    }
    else if(scrollView.contentOffset.x>30){
        CurrentPage = 1;
    }
    _PageControl.currentPage = CurrentPage;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
