//
//  ChartViewController.m
//  ChartsDemo
//
//  Copyright 2015 Daniel Cohen Gindi & Philipp Jahoda
//  A port of MPAndroidChart for iOS
//  Licensed under Apache License 2.0
//
//  https://github.com/danielgindi/Charts
//

#import "ChartViewController.h"
#import "CustomYValueFormatter.h"
#import "AFURLSessionManager.h"
#import "AlertController.h"
#import "AppContainerViewController.h"
@interface ChartViewController () //<ChartViewDelegate, IChartAxisValueFormatter>

//@property (nonatomic, strong) IBOutlet HorizontalBarChartView *chartView;

@end

@implementation ChartViewController
@synthesize graphData;

- (void)viewDidLoad{
    [super viewDidLoad];
    [UIView setAnimationsEnabled:NO];
    self.navigationController.navigationBarHidden=TRUE;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    oldData = graphData;
    self.activityIndicatorView = [UIActivityIndicatorView new];
    filterData = [[NSMutableDictionary alloc] init];
   // [self ScreenDesign];
    //[self resetFilterButton];
}
//- (void) ScreenDesign
//{
//    screenView=[[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
//    screenView.backgroundColor=delegate.BackgroudColor;
//    
//    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
//    headerview.backgroundColor=delegate.headerbgColler;
//    [screenView addSubview:headerview];
//    
//    UIButton *backButton=[[UIButton alloc] initWithFrame:CGRectMake(5, 35, 30, 40)];
//    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
//    backButton.tag=20;
//    CGPoint center = backButton.center;
//    center.y = headerview.frame.size.height / 2 +3;
//    [backButton setCenter:center];
//    [headerview addSubview:backButton];
//    
//    UIButton* HomeButton=[UIButton buttonWithType:UIButtonTypeCustom];
//    HomeButton.frame=CGRectMake(delegate.devicewidth-35, 10, 18, 18);
//    HomeButton.tag=20;
//    center = HomeButton.center;
//    center.y = headerview.frame.size.height / 2 + 3;
//    [HomeButton setCenter:center];
//    [HomeButton addTarget:self action:@selector(GotobackContainer) forControlEvents:UIControlEventTouchUpInside];
//    [HomeButton setImage:[UIImage imageNamed:@"HomeImage"]  forState:UIControlStateNormal];
//    [headerview addSubview:HomeButton];
//    
//    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(45, backButton.frame.origin.y+12, 200, 20)];
//    headerlab.textColor=[UIColor whiteColor];
//    headerlab.font=delegate.boldFont;
//    headerlab.text=NSLocalizedString(@"GRAPH_VIEW", nil);
//    [headerview addSubview:headerlab];
//    
//    int xpos = delegate.devicewidth - delegate.margin*2-20;
//    UIButton *filter=[[UIButton alloc] initWithFrame:CGRectMake(xpos, (delegate.headerheight-20)/2, 20, 20)];
//    [filter setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
//    [filter addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
//    filter.tag=30;
//    [headerview addSubview:filter];
//    
//    UIView *screenView1 = [[UIView alloc] initWithFrame:CGRectMake(delegate.margin, delegate.headerheight+10, delegate.devicewidth, delegate.deviceheight-delegate.headerheight-50)];
//    screenView1.backgroundColor = [UIColor clearColor];
//    
//    _chartView = [[HorizontalBarChartView alloc] initWithFrame:CGRectMake(0, 0, screenView1.frame.size.width-50,screenView1.frame.size.height)];
//    _chartView.delegate = self;
//    [screenView1 addSubview:_chartView];
//    _chartView.chartDescription.enabled = NO;
//    //[self.view addSubview:screenView];
//    [screenView addSubview:screenView1];
//    [self.view addSubview:screenView];
//    
//    _chartView.drawBarShadowEnabled = NO;
//    _chartView.drawValueAboveBarEnabled = YES;
//    _chartView.highlightFullBarEnabled = NO;
//    _chartView.pinchZoomEnabled = YES;
//    _chartView.drawBarShadowEnabled = NO;
//    _chartView.leftAxis.enabled = NO;
//    _chartView.rightAxis.axisMaximum = 12;
//    _chartView.rightAxis.axisMinimum = 0.0;
//    _chartView.rightAxis.drawGridLinesEnabled = NO;
//    _chartView.rightAxis.drawZeroLineEnabled = YES;
//    _chartView.rightAxis.labelCount = 12;
//    _chartView.rightAxis.granularityEnabled = TRUE;
//    _chartView.rightAxis.granularity = 1;
//    _chartView.rightAxis.valueFormatter = [[CustomYValueFormatter alloc] initForChart:_chartView];
//    _chartView.rightAxis.labelFont = [UIFont systemFontOfSize:9.f];
//    _chartView.rightAxis.labelFont = [UIFont systemFontOfSize:9.f];
//
//    // x-axis limit line
//    ChartLimitLine *llXAxis = [[ChartLimitLine alloc] initWithLimit:[self getMonth] label:@"You Are Here"];
//    llXAxis.lineWidth = 4.0;
//    llXAxis.labelPosition = ChartLimitLabelPositionRightTop;
//    llXAxis.valueFont = [UIFont systemFontOfSize:10.f];
//    
//    [_chartView.rightAxis addLimitLine:llXAxis];
//    _chartView.legend.enabled = NO;
//    
//    float ypos = _chartView.frame.size.height + delegate.headerheight;
//    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,
//                                                                 delegate.devicewidth, 0.5)];
//    lineView1.backgroundColor = delegate.borderColor;
//    //[screenView addSubview:lineView1];
//    
//    ypos = ypos + lineView1.frame.size.height + 25;
//    
//    UIView *footerView = [[UILabel alloc] initWithFrame:CGRectMake(0, ypos, delegate.devicewidth, 15)];
//    [screenView addSubview:footerView];
//    
//    UILabel *behind = [[UILabel alloc] initWithFrame:CGRectMake(255, 0, 70, 12)];
//    behind.textColor = delegate.dimBlack;
//    //behind.textAlignment = ALIGN_LEFT;
//    behind.font = delegate.contentFont;
//    behind.text = NSLocalizedString(@"BEHIND", nil);
//    xpos = delegate.devicewidth - behind.intrinsicContentSize.width - 10;//10 padding
//    behind.frame = CGRectMake(xpos, 0, behind.intrinsicContentSize.width, 12);
//    [footerView addSubview:behind];
//    
//    UIView *behindView = [[UILabel alloc] initWithFrame:CGRectMake(240, 0, 10, 10)];
//    behindView.backgroundColor = [UIColor colorWithRed:255/255.f green:0/255.f blue:0/255.f alpha:1.f];
//    xpos = xpos - 15;
//    behindView.frame = CGRectMake(xpos, 0, 10, 12);
//    [footerView addSubview:behindView];
//    
//    UILabel *sched = [[UILabel alloc] initWithFrame:CGRectMake(165, 0, 70, 12)];
//    sched.textColor = delegate.dimBlack;
//    sched.font = delegate.contentFont;
//    sched.text = NSLocalizedString(@"SCHEDUED", nil);
//    xpos = xpos - sched.intrinsicContentSize.width - 10;//10 padding
//    sched.frame = CGRectMake(xpos, 0, sched.intrinsicContentSize.width, 12);
//    [footerView addSubview:sched];
//    
//    UIView *schedView = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, 10, 10)];
//    schedView.backgroundColor = [UIColor colorWithRed:243/255.f green:124/255.f blue:33/255.f alpha:1.f];
//    xpos = xpos - 15;
//    schedView.frame = CGRectMake(xpos, 0, 10, 12);
//    [footerView addSubview:schedView];
//    
//    UILabel *comp = [[UILabel alloc] initWithFrame:CGRectMake(165, 0, 70, 12)];
//    comp.textColor = delegate.dimBlack;
//    //comp.textAlignment = ALIGN_LEFT;
//    comp.font = delegate.contentFont;
//    comp.text = NSLocalizedString(@"COMLETED", nil);
//    xpos = xpos - comp.intrinsicContentSize.width - 10;//10 padding
//    comp.frame = CGRectMake(xpos, 0, comp.intrinsicContentSize.width, 12);
//    [footerView addSubview:comp];
//    
//    UIView *compView = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, 10, 10)];
//    compView.backgroundColor = [UIColor colorWithRed:0/255.f green:112/255.f blue:192/255.f alpha:1.f];
//    xpos = xpos - 15;
//    compView.frame = CGRectMake(xpos, 0, 10, 12);
//    [footerView addSubview:compView];
//    [self updateChartData];
//}
//- (void)updateChartData{
//    [self setChartData];
//}
//- (void)setChartData{
//    ChartXAxis *xAxis = _chartView.xAxis;
//    xAxis.labelPosition = XAxisLabelPositionBottom;
//    xAxis.drawGridLinesEnabled = NO;
//    xAxis.drawAxisLineEnabled = YES;
//    //xAxis.axisMinimum = 0.0;
//    xAxis.labelCount = [graphData count];
//    xAxis.granularityEnabled = TRUE;
//    xAxis.granularity = 1.0;
//    xAxis.valueFormatter = self;
//    
//    NSMutableArray *completed = [NSMutableArray array];
//    NSMutableArray *onSchedule = [NSMutableArray array];
//    NSMutableArray *behind = [NSMutableArray array];
//    //NSMutableArray *yValues3 = [NSMutableArray array];
//    
//    for (int i=0;i<[graphData count];i++){
//        NSString* first = [[graphData objectAtIndex:i] objectForKey:@"startMonth"];
//        NSString* second = [[graphData objectAtIndex:i] objectForKey:@"endMonth"];
//        NSString* duration = [[graphData objectAtIndex:i] objectForKey:@"duration"];
//        double val1 = (double) [first doubleValue];
//        double val2 = (double) [second doubleValue];
//        if ([first isEqualToString:second]){
//            val2 = [duration doubleValue];
//            val2 = (val2/30);
//        }
//        if ([[[graphData objectAtIndex:i] objectForKey:@"taskType"] isEqualToString:@"Completed"]){
//            [completed addObject:[[BarChartDataEntry alloc] initWithX:(i+1)
//                                                              yValues:@[@(val1), @(val2)]]];
//        }
//        if ([[[graphData objectAtIndex:i] objectForKey:@"taskType"] isEqualToString:@"Behind"]){
//            [behind addObject:[[BarChartDataEntry alloc] initWithX:(i+1)
//                                                           yValues:@[@(val1), @(val2)]]];
//        }
//        if ([[[graphData objectAtIndex:i] objectForKey:@"taskType"] isEqualToString:@"Onschedule"]){
//            [onSchedule addObject:[[BarChartDataEntry alloc] initWithX:(i+1)
//                                                               yValues:@[@(val1), @(val2)]]];
//        }
//    }
//    BarChartDataSet *set = nil, *set1 = nil, *set2=nil;
//    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
//    
//    if ([completed count]>0){
//        set = [[BarChartDataSet alloc] initWithValues:completed label:@""];
//        set.drawIconsEnabled = NO;
//        set.axisDependency = AxisDependencyRight;
//        set.colors = @[
//                       [UIColor colorWithWhite:0.0/255 alpha:0.0],
//                       [UIColor colorWithRed:0/255.f green:112/255.f blue:192/255.f alpha:1.f]
//                       ];
//        
//        [dataSets addObject:set];
//    }
//    if ([onSchedule count]>0){
//        set1 = [[BarChartDataSet alloc] initWithValues:onSchedule label:@""];
//        set1.drawIconsEnabled = NO;
//        set1.axisDependency = AxisDependencyRight;
//        set1.colors = @[
//                        [UIColor colorWithWhite:0.0/255 alpha:0.0],
//                        [UIColor colorWithRed:243/255.f green:124/255.f blue:33/255.f alpha:1.f]
//                        ];
//        [dataSets addObject:set1];
//    }
//    if ([behind count]>0){
//        set2 = [[BarChartDataSet alloc] initWithValues:behind label:@""];
//        set2.axisDependency = AxisDependencyRight;
//        set2.drawIconsEnabled = NO;
//        set2.colors = @[
//                        [UIColor colorWithWhite:0.0/255 alpha:0.0],
//                        [UIColor colorWithRed:255/255.f green:0/255.f blue:0/255.f alpha:1.f]
//                        ];
//        [dataSets addObject:set2];
//    }
//    if (_chartView.data.dataSetCount > 0){
//        //set = (BarChartDataSet *)_chartView.data.dataSets[0];
//        //set.values = completed;
//        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
//        //data.barWidth = 8.5;
//        _chartView.data = data;
//        [_chartView.data notifyDataChanged];
//        [_chartView notifyDataSetChanged];
//    }
//    else{
//        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
//        //data.barWidth = 8.5;
//        _chartView.data = data;
//        [_chartView setNeedsDisplay];
//    }
//}
//-(long)getMonth{
//    NSDate *date = [NSDate date];
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
//    long month = [components month];
//    return month;
//}
//- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight{
//    NSLog(@"chartValueSelected, stack-index %ld", (long)highlight.dataSetIndex);
//    NSLog(@"chartValueSelected, stack-index %ld", (long)highlight.dataIndex);
//    [_chartView centerViewToAnimatedWithXValue:entry.x yValue:entry.y axis:[_chartView.data getDataSetByIndex:highlight.dataSetIndex].axisDependency duration:1.0];
//}
//
//- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView{
//    NSLog(@"chartValueNothingSelected");
//}
//#pragma mark - IAxisValueFormatter
//
//- (NSString *)stringForValue:(double)value axis:(ChartAxisBase *)axis{
//    if ((int)value <0)
//    return @"";
//    int index = ((int)value)-1;
//    NSString* data = [[graphData objectAtIndex:index] objectForKey:@"taskId"];
//    //return [NSString stringWithFormat:@"%d", (int)value];
//    return data;
//}
//-(IBAction)btnfun:(id)sender{
//    UIButton *btn=(UIButton *)sender;
//    if(btn.tag==20 || btn.tag == 101){
//        [[self navigationController] popViewControllerAnimated:YES];
//    }
//    else if (btn.tag == 30){
//        [resetButton removeFromSuperview];
//        [self filterView];
//    }
//    else if (btn.tag == 1000){
//        if ([fromDate.text isEqualToString:@""] || [toDate.text isEqualToString:@""]){
//           [self ShowAlert:NSLocalizedString(@"ENTER_DATE", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
//        }
//        else{
//            [screenView addSubview:resetButton];
//            [filterData setObject:fromDate.text forKey:@"fromDate"];
//            [filterData setObject:toDate.text forKey:@"toDate"];
//            [filterView removeFromSuperview];
//            [self startTheBgForFilter:^(id data, NSError *error) {
//                [self stopProgBar];
//                NSDictionary *reqdataDict = data;
//                if([[reqdataDict objectForKey:@"status"] isEqualToString:@"OK"]){
//                    NSArray *array = [[NSArray alloc] init];
//                    NSDictionary *reqdataDict1 = [reqdataDict objectForKey:@"myChart"];
//                    array = [reqdataDict1 mutableCopy];
//                    graphData = [array mutableCopy];
//                    [self updateChartData];
//                }
//                else {
//                     [self ShowAlert:[reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
//                }
//                
//            }];
//        }
//    }
//    else if (1001 == btn.tag){
//        //tab.backgroundColor = delegate.BackgroudColor;
//        [filterData setObject:@"" forKey:@"fromDate"];
//        [filterData setObject:@"" forKey:@"toDate"];
//        dateTimePicker.hidden = TRUE;
//        NavBar.hidden = TRUE;
//        [filterView removeFromSuperview];
//    }
//    else if (20000 == btn.tag){
//        graphData = oldData;
//        [filterData setObject:@"" forKey:@"fromDate"];
//        [filterData setObject:@"" forKey:@"toDate"];
//        [resetButton removeFromSuperview];
//        [self updateChartData];
//    }
//}
//-(void)filterView{
//    filterView = [[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
//    filterView.backgroundColor = [UIColor whiteColor];
//    filterView.layer.borderWidth = 0.5;
//    filterView.layer.borderColor = delegate.borderColor.CGColor;
//    
//    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
//    headerview.backgroundColor = delegate.headerbgColler;
//    [filterView addSubview:headerview];
//    
//    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(2*delegate.margin, 0,delegate.devicewidth, delegate.headerheight)];
//    headerlab.textColor=[UIColor whiteColor];
//    headerlab.font=delegate.headFont;
//    headerlab.text=NSLocalizedString(@"FILTERS", nil);
//    [headerview addSubview:headerlab];
//    
//    //filterView.layer.borderWidth = 0.5;
//    //filterView.layer.borderColor = delegate.borderColor.CGColor;
//    CGFloat ypos = 20 + delegate.headerheight + 20;
//    UILabel *fromLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
//    fromLabel.textColor=[UIColor blackColor];
//    fromLabel.font = delegate.contentFont;
//    fromLabel.text = NSLocalizedString(@"FROM", nil);
//    [filterView addSubview:fromLabel];
//    
//    dateTimePicker = [[UIDatePicker alloc] init];
//    dateTimePicker.datePickerMode = UIDatePickerModeDate;
//    dateTimePicker.date = [NSDate date];
//    [dateTimePicker addTarget:self
//                       action:@selector(dateTimeChange:)
//             forControlEvents:UIControlEventValueChanged];
//    
//    //dateTimePicker.minimumDate=[NSDate date];
//    [dateTimePicker setFrame: CGRectMake(0,(screenView.frame.size.height-162), screenView.frame.size.width, 162)];
//    dateTimePicker.backgroundColor = [UIColor whiteColor];
//    dateTimePicker.layer.zPosition = 1;
//    [filterView addSubview:dateTimePicker];
//    dateTimePicker.hidden = TRUE;
//    
//    NavBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(dateTimePicker.frame.origin.x, dateTimePicker.frame.origin.y-44, dateTimePicker.frame.size.width, 44)];
//    NavBar.barStyle = UIBarStyleDefault;
//    NavBar.backgroundColor = [UIColor grayColor];
//    [filterView addSubview:NavBar];
//    
//    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(DonePicker)];
//    UINavigationItem *navItem = [[UINavigationItem alloc] init];
//    navItem.rightBarButtonItem = backButton;
//    NavBar.items = @[ navItem ];
//    NavBar.hidden = TRUE;
//    
//    
//    fromDate=[[UITextField alloc] initWithFrame:CGRectMake(30, ypos+20, 120, 25)];
//    fromDate.keyboardType=UIKeyboardTypeDefault;
//    fromDate.font=delegate.contentFont;
//    fromDate.borderStyle=UITextBorderStyleRoundedRect;
//    fromDate.delegate=self;
//    fromDate.tag = 100;
//    fromDate.placeholder = @"Start Date";
//    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
//    image.image = [UIImage imageNamed:@"calendar"] ;
//    fromDate.leftViewMode = UITextFieldViewModeAlways;
//    [fromDate setLeftView:image];
//    fromDate.textColor = delegate.dimColor;
//    fromDate.text = [filterData objectForKey:@"fromDate"];
//    fromDate.borderStyle = UITextBorderStyleNone;
//    [fromDate setBackgroundColor:[UIColor clearColor]];
//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, fromDate.frame.size.width, 1.0);
//    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
//    [fromDate.layer addSublayer:bottomBorder];
//    fromDate.autocorrectionType = UITextAutocorrectionTypeNo;
//    [filterView addSubview:fromDate];
//    
//    int xpos = delegate.devicewidth - 130;
//    UILabel *toLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, 80, 15)];
//    toLabel.textColor=[UIColor blackColor];
//    toLabel.font = delegate.contentFont;
//    toLabel.text = @"To";
//    [filterView addSubview:toLabel];
//    
//    toDate=[[UITextField alloc] initWithFrame:CGRectMake(xpos, ypos+20, 120, 25)];
//    toDate.keyboardType=UIKeyboardTypeDefault;
//    toDate.font=delegate.contentFont;
//    toDate.borderStyle=UITextBorderStyleRoundedRect;
//    toDate.delegate=self;
//    toDate.tag = 101;
//    toDate.text = [filterData objectForKey:@"toDate"];
//    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
//    image1.image = [UIImage imageNamed:@"calendar"] ;
//    toDate.leftViewMode = UITextFieldViewModeAlways;
//    [toDate setLeftView:image1];
//    toDate.placeholder = @"End Date";
//    toDate.textColor = delegate.dimColor;
//    toDate.autocorrectionType = UITextAutocorrectionTypeNo;
//    
//    toDate.borderStyle = UITextBorderStyleNone;
//    [toDate setBackgroundColor:[UIColor clearColor]];
//    bottomBorder = [CALayer layer];
//    bottomBorder.frame = CGRectMake(0.0, toDate.frame.size.height - 1, toDate.frame.size.width, 1.0);
//    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
//    [toDate.layer addSublayer:bottomBorder];
//    
//    [filterView addSubview:toDate];
//    
//    ypos = ypos + toLabel.frame.size.height + 20 + toDate.frame.size.height;
//    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth-20, 40)];
//    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
//    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: footerView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
//    footerView.layer.mask = maskLayer1;
//    [filterView addSubview:footerView];
//    
//    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2, 40)];
//    submitButton.backgroundColor = delegate.yellowColor;
//    submitButton.clipsToBounds = YES;
//    submitButton.titleLabel.font = delegate.contentBigFont;
//    [submitButton setTitle:NSLocalizedString(@"APPLY_FILTER", nil) forState:UIControlStateNormal];
//    submitButton.titleLabel.font = delegate.normalFont;
//    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
//    submitButton.tag=1000;
//    [footerView addSubview:submitButton];
//    
//    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2, 0, footerView.frame.size.width/2, 40)];
//    
//    cancelButton.backgroundColor = [UIColor whiteColor];
//    //cancelButton.layer.cornerRadius = 15;
//    cancelButton.clipsToBounds = YES;
//    cancelButton.backgroundColor = delegate.redColor;
//    cancelButton.titleLabel.font = delegate.contentBigFont;
//    [cancelButton setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
//    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [cancelButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
//    cancelButton.tag=1001;
//    [footerView addSubview:cancelButton];
//    
//    dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
//    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
//    //[tab addSubview:dimView];
//    [screenView addSubview:filterView];
//    [screenView bringSubviewToFront:filterView];
//}
//-(void)initProgBar{
//    self.activityIndicatorView = [UIActivityIndicatorView new];
//    self.activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
//    self.activityIndicatorView.frame = CGRectMake(delegate.devicewidth/2-50, delegate.deviceheight-200, 100, 100);
//    self.activityIndicatorView.tag=1;
//    [self.view addSubview:self.activityIndicatorView];
//    self.activityIndicatorView.color = delegate.redColor;
//    [self.view setUserInteractionEnabled:NO];
//    [self.activityIndicatorView startAnimating];
//}
//-(void)stopProgBar{
//    //[self.activityIndicatorView removeFromSuperview];
//    [self.view setUserInteractionEnabled:YES];
//    [self.activityIndicatorView stopAnimating];
//}
//-(void) startTheBgForFilter:(void (^)(id responseObject, NSError *error))completionHandler
//{
//    if([delegate isInternetConnected]){
//        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
//        [_params setObject:@"post" forKey:@"action"];
//        [_params setObject:[NSString stringWithFormat:@"%@", [delegate.dataFull objectForKey:@"handsetId"]] forKey:@"handsetId"];
//        [_params setObject:[NSString stringWithFormat:@"%@", fromDate.text] forKey:@"fromDate"];
//        [_params setObject:[NSString stringWithFormat:@"%@", toDate.text] forKey:@"toDate"];
//        NSLog(@"_params===>%@",_params);
//        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//        //http://139.59.225.255/PMSframework/PmsRestApi/filter
//        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
//            URLString:@"http://139.59.225.255/PMSframework/PmsRestApi/myChart" parameters:_params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {} error:nil];
//        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//        NSURLSessionUploadTask *uploadTask;
//        uploadTask = [manager
//                      uploadTaskWithStreamedRequest:request
//                      progress:^(NSProgress * _Nonnull uploadProgress) {
//                          dispatch_async(dispatch_get_main_queue(), ^{
//                              [self initProgBar];
//                          });
//                      }
//                      completionHandler:^(NSURLResponse * _Nonnull response, NSData *data, NSError * _Nullable error) {
//                          if (error) {
//                             [self ShowAlert:[error description] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
//                          } else {
//                              NSLog(@"Success: %@", data);
//                              dispatch_async(dispatch_get_main_queue(), ^{
//                                  if (completionHandler) {
//                                      completionHandler(data, nil);
//                                  }
//                              });
//                          }
//                      }];
//        [uploadTask resume];
//    }
//    else{
//       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
//    }
//}
//-(void) resetFilterButton{
//    resetButton= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 90, 40)];
//    resetButton.backgroundColor = delegate.borderColor;
//    resetButton.titleLabel.font = delegate.contentBigFont;
//    [resetButton setTitle:@"Reset" forState:UIControlStateNormal];
//    [resetButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    resetButton.tag = 20000;
//    float y = self.view.bounds.size.height - resetButton.frame.size.height - 80;
//    [resetButton setFrame:CGRectMake(0, y, resetButton.frame.size.width, resetButton.frame.size.height)];
//    resetButton.layer.zPosition++;
//    CGPoint center = resetButton.center;
//    center.x = delegate.devicewidth / 2;
//    [resetButton setCenter:center];
//    [resetButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
//}
//- (void)dateTimeChange:(id)sender{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
//    if (TRUE ==  isFromDate){
//        fromDate.text=[dateFormatter stringFromDate:dateTimePicker.date];
//    }
//    else{
//        toDate.text=[dateFormatter stringFromDate:dateTimePicker.date];
//    }
//    //dateTimePicker.hidden = TRUE;
//}
//- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    if (100 == textField.tag){
//        isFromDate = TRUE;
//        [fromDate resignFirstResponder];
//        dateTimePicker.hidden = FALSE;
//        NavBar.hidden = FALSE;
//    }
//    else if (101 == textField.tag){
//        isFromDate = FALSE;
//        [toDate resignFirstResponder];
//        dateTimePicker.hidden = FALSE;
//        NavBar.hidden = FALSE;
//    }
//}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
