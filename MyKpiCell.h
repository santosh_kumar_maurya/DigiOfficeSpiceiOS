//
//  MyKpiCell.h
//  iWork
//
//  Created by Shailendra on 05/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyKpiCell : UITableViewCell<UIScrollViewDelegate>
{
    int CurrentPage;
}

@property(nonatomic,strong)IBOutlet UIView *MyKPIView;
@property(nonatomic,strong)IBOutlet UIView *MyFeedbackView;
@property(nonatomic,strong)IBOutlet UIView *FeedbackAsStackHolderView;
@property(nonatomic,strong)IBOutlet UIScrollView *ScrollViews;
@property(nonatomic,strong)IBOutlet UILabel *MyKPIStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *KPIStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *KPILabel;
@property(nonatomic,strong)IBOutlet UILabel *MyFeedbackStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackAsStackHolderStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackAsStackStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *FeedbackAsStackLabel;
@property(nonatomic,strong)IBOutlet UIPageControl *PageControl;
@end
