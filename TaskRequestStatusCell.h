//
//  TaskRequestStatusCell.h
//  iWork
//
//  Created by Shailendra on 23/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskRequestStatusCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UIButton *AssignedButton;
@property(nonatomic,retain)IBOutlet UIButton *NewButton;
@property(nonatomic,retain)IBOutlet UIButton *BothButton;
@property(nonatomic,retain)IBOutlet UIImageView *AssignedImageView;
@property(nonatomic,retain)IBOutlet UIImageView *NewImageView;
@property(nonatomic,retain)IBOutlet UIImageView *BothImageView;
@end
