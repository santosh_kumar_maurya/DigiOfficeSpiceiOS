#import "TaskDetailStakeHolderScreen.h"
#include "AsyncImageView.h"
#import "SSSnackbar.h"
#import "AlertController.h"
#import "AppContainerViewController.h"

const UIEdgeInsets textInsetsMine5 = {5, 10, 11, 17};
const UIEdgeInsets texttextInsetsSomeone5 = {5, 15, 11, 10};

@interface TaskDetailStakeHolderScreen ()
{
     UIRefreshControl *refreshControl;
     UIButton *screenView;
}
@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation TaskDetailStakeHolderScreen
@synthesize isShowDetail,count,taskID,taskType,dataDict, isMe, isManager;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad{
    [super viewDidLoad];
     self.view.backgroundColor = delegate.BackgroudColor;
    self.navigationController.navigationBarHidden=TRUE;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.activityIndicatorView = [UIActivityIndicatorView new];
    isShowDetail= TRUE;
    isMe = TRUE;
     [self ScreenDesign];
    [self TaskDetailsStakeHolderWebApi];
    [self getConversation];
}
-(void)TaskDetailsStakeHolderWebApi{
   
    if(delegate.isInternetConnected){
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        [_params setObject:@"post" forKey:@"action"];
        [_params setObject:[NSString stringWithFormat:@"%@", [delegate.dataFull objectForKey:@"handsetId"]] forKey:@"handsetId"];
        [_params setObject:[NSString stringWithFormat:@"%@", taskID] forKey:@"taskId"];
        NSString* url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/progressCard"];
        [self startTheBg_API:_params url:url completionHandler:^(id data, NSError *error) {
            [self stopProgBar];
            [refreshControl endRefreshing];
            NSDictionary *reqdataDict = data;
            dataArray = [[NSMutableArray alloc] init];
            if([[reqdataDict objectForKey:@"status"] isEqualToString:@"OK"]){
                NSArray *array = [[NSArray alloc] init];
                array = [reqdataDict objectForKey:@"data"];
                dataArray = [array mutableCopy];
                [tab reloadData];
            }
            else {
                [self ShowAlert:[reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }

}
- (void) ScreenDesign{
    mainScreenView=[[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    mainScreenView.backgroundColor=delegate.BackgroudColor;
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor=delegate.headerbgColler;
    [mainScreenView addSubview:headerview];
   
    UIButton *backButton=[[UIButton alloc] initWithFrame:CGRectMake(5, 35, 30, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    backButton.tag=20;
    CGPoint center = backButton.center;
    center.y = headerview.frame.size.height / 2 +3;
    [backButton setCenter:center];
    [headerview addSubview:backButton];
    
    UIButton* HomeButton=[UIButton buttonWithType:UIButtonTypeCustom];
    HomeButton.frame=CGRectMake(delegate.devicewidth-35, 10, 18, 18);
    HomeButton.tag=20;
    center = HomeButton.center;
    center.y = headerview.frame.size.height / 2 + 3;
    [HomeButton setCenter:center];
    [HomeButton addTarget:self action:@selector(GotobackContainer) forControlEvents:UIControlEventTouchUpInside];
    [HomeButton setImage:[UIImage imageNamed:@"HomeImage"]  forState:UIControlStateNormal];
    [headerview addSubview:HomeButton];
    
    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(45, backButton.frame.origin.y+12, 200, 20)];
    headerlab.textColor=delegate.yellowColor;
    headerlab.font=delegate.headFont;
    headerlab.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil), taskID];
    headerlab.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab];
    
    UIButton *flatButton=[[UIButton alloc] initWithFrame:CGRectMake(0, delegate.headerheight, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton.backgroundColor=[UIColor whiteColor];
    flatButton_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton_lab.backgroundColor = delegate.headerbgColler;
    flatButton_lab.text = NSLocalizedString(@"DETAILS", nil);
    flatButton_lab.textAlignment=ALIGN_CENTER;
    flatButton_lab.font=delegate.normalBold;
    flatButton_lab.textColor=[UIColor whiteColor];
    [flatButton addSubview:flatButton_lab];
    [flatButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton.tag=300;
    
    [mainScreenView addSubview:flatButton];
    
    UIButton *flatButton1=[[UIButton alloc] initWithFrame:CGRectMake(mainScreenView.frame.size.width/2,delegate.headerheight,mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton1.backgroundColor=[UIColor whiteColor];
    flatButton1_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton1_lab.text=NSLocalizedString(@"COMMENTS_CAPS", nil);
    flatButton1_lab.backgroundColor = delegate.headerbgColler;
    flatButton1_lab.textAlignment=ALIGN_CENTER;
    flatButton1_lab.font=delegate.normalBold;
    flatButton1_lab.textColor=[UIColor whiteColor];
    [flatButton1 addSubview:flatButton1_lab];
    [flatButton1 addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton1.tag=301;
    [mainScreenView addSubview:flatButton1];
    
    sel_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight-2, mainScreenView.frame.size.width/2, 2)];
    sel_lab.backgroundColor = [UIColor whiteColor];
    [mainScreenView addSubview:sel_lab];
    
    tab=[[UITableView alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight, mainScreenView.frame.size.width,mainScreenView.frame.size.height)];
    tab.delegate=self;
    tab.dataSource=self;
    tab.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab.backgroundColor=delegate.BackgroudColor;
    [mainScreenView addSubview:tab];
    [self.view addSubview:mainScreenView];
    [self AddRefreshControl];
}
-(void)AddRefreshControl{
    [refreshControl removeFromSuperview];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
}
- (void)reloadData{
    [self TaskDetailsStakeHolderWebApi];
    [self getConversation];
}
- (void)dateTimeChange:(id)sender{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    datelab.text=[dateFormatter stringFromDate:dateTimePicker.date];
    dateTimePicker.hidden = TRUE;
}
-(void)screenTaskDetail:(UITableViewCell *)cell{
    int rightPad = delegate.devicewidth-delegate.margin*2;
    UIButton *mainView=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight+150)];
    mainView.backgroundColor = [UIColor clearColor];
    [cell addSubview:mainView];
    
    int ypos = 15;
    UILabel *createdOnLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/3, 15)];
    createdOnLabel.text = NSLocalizedString(@"CREATED_ON", nil);
    createdOnLabel.font = delegate.contentSmallFont;
    createdOnLabel.textColor = [UIColor blackColor];
    createdOnLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOnLabel];
    
    UILabel *createdOn =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos+17, delegate.devicewidth/3, 15)];
    createdOn.text = [[dataArray objectAtIndex:0] objectForKey:@"creationDate"];
    createdOn.font = delegate.contentSmallFont;
    createdOn.textColor = delegate.dimBlack;
    createdOn.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOn];

    createdOnLabel.frame = CGRectMake(10, ypos, createdOn.intrinsicContentSize.width, 15);
    int xpos = delegate.devicewidth/2-((delegate.devicewidth/3)/2);
    UILabel *startDateLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/3, 15)];
    startDateLabel.text = NSLocalizedString(@"ACTUAL_START_DATE", nil);
    startDateLabel.textColor = [UIColor blackColor];
    startDateLabel.font = delegate.contentSmallFont;
    startDateLabel.textAlignment = ALIGN_LEFT;
    startDateLabel.textColor =[UIColor blackColor];
    [mainView addSubview:startDateLabel];
    
    UILabel *startDate =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, delegate.devicewidth/3, 15)];
    startDate.text = [[dataArray objectAtIndex:0] objectForKey:@"startDate"];
    startDate.font = delegate.contentSmallFont;
    startDate.textAlignment = ALIGN_LEFT;
    startDate.textColor = delegate.dimBlack;
    [mainView addSubview:startDate];
    
    xpos = delegate.devicewidth/2 - (startDate.intrinsicContentSize.width/2+20);
    startDateLabel.frame = CGRectMake(xpos, ypos, startDate.intrinsicContentSize.width+30, 15);
    startDate.frame = CGRectMake(xpos, ypos+17, startDate.intrinsicContentSize.width, 15);
    
    xpos = delegate.devicewidth-(delegate.devicewidth/3);
    UILabel *dayLeftLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/3, 15)];
    dayLeftLabel.text = NSLocalizedString(@"COMPLETION_DATE", nil);
    dayLeftLabel.textColor = [UIColor blackColor];
    dayLeftLabel.font = delegate.contentSmallFont;
    dayLeftLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:dayLeftLabel];
    
    xpos = delegate.devicewidth - dayLeftLabel.intrinsicContentSize.width - 15;
    dayLeftLabel.frame = CGRectMake(xpos, ypos, dayLeftLabel.intrinsicContentSize.width, 15);
    
    UILabel *dayLeft =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, dayLeftLabel.intrinsicContentSize.width, 15)];
    dayLeft.text = [[dataArray objectAtIndex:0] objectForKey:@"completeDate"];
    dayLeft.font = delegate.contentSmallFont;
    dayLeft.textAlignment = ALIGN_LEFT;
    dayLeft.textColor = delegate.dimBlack;
    [mainView addSubview:dayLeft];
    
    if (dayLeftLabel.intrinsicContentSize.width > dayLeft.intrinsicContentSize.width)
        dayLeft.frame = CGRectMake(xpos, ypos+17, dayLeftLabel.intrinsicContentSize.width, 15);
    else
        dayLeft.frame = CGRectMake(xpos, ypos+17, dayLeft.intrinsicContentSize.width, 15);

    int saveYPos = ypos = ypos + 30 + 15;
    screenView=[[UIButton alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, delegate.deviceheight)];
    screenView.backgroundColor=[UIColor whiteColor];
    screenView.layer.cornerRadius = 2.0;
    screenView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    screenView.layer.borderWidth = 0.5;
    screenView.layer.shadowRadius = 3.0f;
    screenView.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
    screenView.layer.shadowOpacity = 0.7f;
    screenView.layer.masksToBounds = NO;
    
    [mainView addSubview:screenView];
    cell.backgroundColor=[UIColor clearColor];
    
    ypos = 15;
    UILabel *taskNameLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, rightPad-delegate.margin*2, 15)];
    taskNameLabel.text = NSLocalizedString(@"TASK_NAME_WITH_COLON", nil);
    taskNameLabel.font = delegate.contentFont;
    taskNameLabel.textColor=[UIColor blackColor];
    taskNameLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:taskNameLabel];
    
    NSString *taskInfo = [[dataArray objectAtIndex:0] objectForKey:@"taskName"];
    if (taskInfo.length > 0){
    taskInfo = [NSString stringWithFormat:@"%@%@",[[taskInfo substringToIndex:1] uppercaseString],[taskInfo substringFromIndex:1] ];
    }
    
    xpos = taskNameLabel.intrinsicContentSize.width + 10;
    UILabel *taskName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 50)];
    taskName.text = taskInfo;
    taskName.font = delegate.contentFont;
    taskName.textColor=delegate.dimBlack;
    taskName.textAlignment = ALIGN_LEFT;
    taskName.numberOfLines = 0;
    [taskName sizeToFit];
    [screenView addSubview:taskName];
    
    ypos = ypos + taskName.frame.size.height + 15;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView];
    
    ypos = ypos + 15;
    UILabel *StartDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    StartDateStaticLabel.text = NSLocalizedString(@"START_DATE_WITH_COLON", nil);
    StartDateStaticLabel.font = delegate.contentFont;
    StartDateStaticLabel.textColor = [UIColor blackColor];
    StartDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateStaticLabel];
    
    xpos = StartDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *StartDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    StartDateValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"eStartDate"];
    StartDateValueLabel.font = delegate.contentFont;
    StartDateValueLabel.textColor = delegate.dimBlack;
    StartDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateValueLabel];
    
    ypos = ypos + StartDateStaticLabel.frame.size.height + 15;
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView1.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView1];
    
    ypos = ypos + 15;
    UILabel *EndDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    EndDateStaticLabel.text = NSLocalizedString(@"END_DATE_WITH_COLON", nil);
    EndDateStaticLabel.font = delegate.contentFont;
    EndDateStaticLabel.textColor = [UIColor blackColor];
    EndDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateStaticLabel];
    
    xpos = EndDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *EndDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    EndDateValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"eEndDate"];
    EndDateValueLabel.font = delegate.contentFont;
    EndDateValueLabel.textColor = delegate.dimBlack;
    EndDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateValueLabel];
    
    ypos = ypos + EndDateValueLabel.frame.size.height + 15;
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView2.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView2];
    
    ypos = ypos + 15;
    UILabel *DurationStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    DurationStaticLabel.text = NSLocalizedString(@"DURATION_WITH_COLON", nil);
    DurationStaticLabel.font = delegate.contentFont;
    DurationStaticLabel.textColor = [UIColor blackColor];
    DurationStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:DurationStaticLabel];
    
    xpos = EndDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *DurationValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    DurationValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"duration"];
    DurationValueLabel.font = delegate.contentFont;
    DurationValueLabel.textColor = delegate.dimBlack;
    DurationValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:DurationValueLabel];
    ypos = ypos + DurationValueLabel.frame.size.height + 15;
    
    UIView *lineView3 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView3.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView3];
    
    ypos = ypos + 15;
    UILabel *createdByLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    createdByLabel.text = NSLocalizedString(@"CREATED_BY_WITH_COLON", nil);
    createdByLabel.font = delegate.contentFont;
    createdByLabel.textColor = [UIColor blackColor];
    createdByLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdByLabel];
    
    UILabel *createdBy =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos+17, delegate.devicewidth/2, 15)];
    createdBy.text = [[dataArray objectAtIndex:0] objectForKey:@"createdBy"];
    createdBy.font = delegate.contentFont;
    createdBy.textColor = delegate.dimBlack;
    createdBy.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdBy];
    
    createdBy.frame = CGRectMake(createdByLabel.intrinsicContentSize.width+10, ypos, rightPad, 15);
    createdByLabel.frame = CGRectMake(10, ypos, createdByLabel.intrinsicContentSize.width, 15);
    
    ypos = ypos + createdByLabel.frame.size.height + 15;
    UIView *lineView0 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView0.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView0];
    
    ypos = ypos + 15;
    UILabel *kpiLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
    kpiLabel.text = NSLocalizedString(@"KPI_WITH_COLON", nil);
    kpiLabel.textColor = [UIColor blackColor];
    kpiLabel.font = delegate.contentFont;
    createdBy.numberOfLines = 0;
    kpiLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiLabel];
    
    xpos = kpiLabel.intrinsicContentSize.width + 10;
    UILabel *kpiName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-30, 50)];
    kpiName.text = [NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:0]  objectForKey:@"kpi"]];
    kpiName.textColor = delegate.dimBlack;
    kpiName.font = delegate.contentFont;
    kpiName.numberOfLines = 0;
    [kpiName sizeToFit];
    kpiName.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiName];
    
    if (kpiName.text.length > 0)
        ypos = ypos + kpiName.frame.size.height + 15;
    else
        ypos = ypos + kpiLabel.frame.size.height + 15;

    int subTaskHeight = 0;
    NSArray* subTaskData = [[dataArray objectAtIndex:0] objectForKey:@"subTask"];
    if (((NSNull*)subTaskData != [NSNull null]) && subTaskData.count > 0 && [subTaskData[0] length] > 0){
        for (int i=0; i < subTaskData.count; i++){
            UIView *lineView4 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView4.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView4];
            
            ypos = ypos + 15;
            UILabel *subtaskLabel =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            subtaskLabel.text = NSLocalizedString(@"SUBTASK_WITH_COLON", nil);
            subtaskLabel.font = delegate.contentFont;
            subtaskLabel.textColor = delegate.dimColor;;
            subtaskLabel.textAlignment = ALIGN_LEFT;
            [screenView addSubview:subtaskLabel];
            
            xpos = subtaskLabel.intrinsicContentSize.width + 10;
            UILabel *subtask =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 500)];
            subtask.text = subTaskData[i];
            subtask.font = delegate.contentFont;
            subtask.textColor = delegate.dimBlack;
            subtask.numberOfLines=0;
            subtask.textAlignment = ALIGN_LEFT;
            [subtask sizeToFit];
            [screenView addSubview:subtask];
            subTaskHeight = subTaskHeight + subtask.frame.size.width;
            ypos = ypos + subtask.frame.size.height + 15;
        }
    }
    
    UIView *lineView13 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView13.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView13];
    
    ypos = ypos + 15;
    UILabel *taskDetailLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth-delegate.margin*2-5, 15)];
    taskDetailLabel.text = NSLocalizedString(@"TASK_DETAILS_WITH_COLON", nil);
    taskDetailLabel.font = delegate.contentFont;
    taskDetailLabel.textColor = [UIColor blackColor];;
    taskDetailLabel.textAlignment = ALIGN_LEFT;
    taskDetailLabel.numberOfLines = 0;
    [taskDetailLabel sizeToFit];
    [screenView addSubview:taskDetailLabel];
    
    xpos = taskDetailLabel.intrinsicContentSize.width + 10;
    UILabel *taskDetail =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
    taskDetail.text = [[dataArray objectAtIndex:0] objectForKey:@"taskDetail"];
    taskDetail.font = delegate.contentFont;
    taskDetail.textColor = delegate.dimBlack;;
    taskDetail.textAlignment = ALIGN_LEFT;
    taskDetail.numberOfLines = 0;
    [taskDetail sizeToFit];
    [screenView addSubview:taskDetail];
    
    if (taskDetail.text.length > 0)
        ypos = ypos + taskDetail.frame.size.height + 15;
    else
        ypos = ypos + taskDetailLabel.frame.size.height + 15;
    
    UIView *lineView5 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView5.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView5];
    
    ypos = ypos + 15;
    UILabel *feedbackLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth-delegate.margin*2-5, 45)];
    feedbackLabel.font = delegate.contentFont;
    feedbackLabel.textColor = [UIColor blackColor];
    feedbackLabel.text =  [NSString stringWithFormat:NSLocalizedString(@"FEEDBACK_WITH_COLON", nil)];
    feedbackLabel.textAlignment = ALIGN_LEFT;
    feedbackLabel.numberOfLines = 0;
    [feedbackLabel sizeToFit];
    [screenView addSubview:feedbackLabel];
    
    xpos = feedbackLabel.intrinsicContentSize.width + 10;
    UILabel *feedback =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 45)];
    NSString* feedbackData = [[dataArray objectAtIndex:0] objectForKey:@"feedback"];
    if (((NSNull*)feedbackData != [NSNull null])){
        feedback.text = [NSString stringWithFormat:@"%@", feedbackData];
    }
    feedback.font = delegate.contentFont;
    feedback.textColor = delegate.dimBlack;
    feedback.textAlignment = ALIGN_LEFT;
    feedback.numberOfLines = 0;
    [feedback sizeToFit];
    [screenView addSubview:feedback];
    
    if (feedback.text.length > 0)
        ypos = ypos + feedback.frame.size.height + 15;
    else
        ypos = ypos + feedbackLabel.frame.size.height + 15;

    UIView *lineView15 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView15.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView15];
    
    ypos = ypos + 15;
    RateView* rv = [RateView rateViewWithRating:3.7f];
    rv.frame = CGRectMake(10, ypos, 100, 15);
    rv.tag = 88888;
    rv.starSize = 15.0;
    
    rv.rating = [[[dataArray objectAtIndex:0] objectForKey:@"rating"] floatValue];
    CGPoint center = rv.center;
    center.x = (delegate.devicewidth - delegate.margin*2)/2;
    [rv setCenter:center];
    
    rv.starFillColor = delegate.redColor;
    [screenView addSubview:rv];

    ypos = ypos + rv.frame.size.height + 15;
    BOOL isCommentMade = FALSE;
    NSMutableArray *stakeHolderData = [[dataArray objectAtIndex:0] objectForKey:@"stakeHolder"];
    
    if (((NSNull*)stakeHolderData != [NSNull null]) && stakeHolderData.count > 0){
        for (int i=0; i < stakeHolderData.count; i++){
            UIView *lineView4 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 15)];
            lineView4.backgroundColor = delegate.BackgroudColor;
            [screenView addSubview:lineView4];
            
            ypos = ypos + 30;
            UILabel *subtaskLabel =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            subtaskLabel.text =  NSLocalizedString(@"STAKE_HOLDER_WITH_COLON", nil);
            subtaskLabel.font = delegate.contentFont;
            subtaskLabel.textColor = delegate.dimColor;;
            subtaskLabel.textAlignment = ALIGN_LEFT;
            [screenView addSubview:subtaskLabel];
            xpos = subtaskLabel.intrinsicContentSize.width + 10;
            UILabel *subtask =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
            NSString* name = [[stakeHolderData objectAtIndex:0] objectForKey:@"stakeHolder"];
            subtask.text = name;
            if ([[delegate.dataFull objectForKey:@"employeeId"] isEqualToString:name])
                 isCommentMade = TRUE;
            subtask.font = delegate.contentFont;
            subtask.textColor = delegate.dimBlack;
            subtask.numberOfLines=0;
            subtask.textAlignment = ALIGN_LEFT;
            [subtask sizeToFit];
            [screenView addSubview:subtask];
            subTaskHeight = subTaskHeight + subtask.frame.size.width;
            ypos = ypos + subtaskLabel.frame.size.height + 15;
            
            UIView *lineView5 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView5.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView5];
            
            ypos = ypos + 15;
            UILabel *label2 =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            label2.text = NSLocalizedString(@"EFFECTIVE_COMPETENCY_WITH_COLON", nil);
            label2.font = delegate.contentFont;
            label2.textColor = [UIColor blackColor];;
            label2.textAlignment = ALIGN_LEFT;
            [screenView addSubview:label2];
            xpos = label2.intrinsicContentSize.width + 10;
            UILabel *value1 =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
            value1.text = [[stakeHolderData objectAtIndex:0] objectForKey:@"competency"];
            value1.font = delegate.contentFont;
            value1.textColor = delegate.dimBlack;
            value1.numberOfLines=0;
            value1.textAlignment = ALIGN_LEFT;
            [value1 sizeToFit];
            [screenView addSubview:value1];
            subTaskHeight = subTaskHeight + value1.frame.size.width;
            ypos = ypos + label2.frame.size.height + 15;

            UIView *lineView0 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView0.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView0];
            
            ypos = ypos + 15;
            UILabel *label4 =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            label4.text =  NSLocalizedString(@"LACKIMG_COMPETENCY_WITH_COLON", nil);
            label4.font = delegate.contentFont;
            label4.textColor = [UIColor blackColor];;
            label4.textAlignment = ALIGN_LEFT;
            [screenView addSubview:label4];
            xpos = label4.intrinsicContentSize.width + 10;
            UILabel *value2 =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
            value2.text = [[stakeHolderData objectAtIndex:0] objectForKey:@"competency1"];
            value2.font = delegate.contentFont;
            value2.textColor = delegate.dimBlack;
            value2.numberOfLines=0;
            value2.textAlignment = ALIGN_LEFT;
            [value2 sizeToFit];
            [screenView addSubview:value2];
            
            ypos = ypos + 15;
            UILabel *label1 =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            label1.text = NSLocalizedString(@"FEEDBACK_WITH_COLON", nil);
            label1.font = delegate.contentFont;
            label1.textColor = [UIColor blackColor];;
            label1.textAlignment = ALIGN_LEFT;
            [screenView addSubview:label1];
            xpos = label1.intrinsicContentSize.width + 10;
            UILabel *value =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
            
            value.text = [[stakeHolderData objectAtIndex:0] objectForKey:@"feedback"];
            value.font = delegate.contentFont;
            value.textColor = delegate.dimBlack;
            value.numberOfLines=0;
            value.textAlignment = ALIGN_LEFT;
            [value sizeToFit];
            [screenView addSubview:value];
            subTaskHeight = subTaskHeight + value.frame.size.width;
            ypos = ypos + label1.frame.size.height + 15;
            
            UIView *lineView7 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView7.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView7];
            subTaskHeight = subTaskHeight + value2.frame.size.width;
            ypos = ypos + label4.frame.size.height + 15;
        }
    }
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, ypos, delegate.devicewidth-delegate.margin*2, 40)];
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.contentBigFont;
    [submitButton setTitle: NSLocalizedString(@"GIVE_FEEDBACK", nil) forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag = 1000;
    [footerView addSubview:submitButton];
    if (FALSE == isCommentMade){
        [screenView addSubview:footerView];
        ypos = ypos + footerView.frame.size.height;
    }
    screenView.frame = CGRectMake(delegate.margin, saveYPos, delegate.devicewidth-delegate.margin*2, ypos);
}

- (void)showAlert {
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = NSLocalizedString(@"CANCEL_TASK", nil);
    alertViewController.message = NSLocalizedString(@"PLEASE_ADD_COMMENT", nil);
    alertViewController.titleFont = delegate.headFont;
    alertViewController.messageFont = delegate.contentBigFont;
    alertViewController.buttonTitleFont = delegate.contentFont;
    alertViewController.cancelButtonTitleFont = delegate.contentFont;
    alertViewController.cancelButtonColor = delegate.yellowColor;
    alertViewController.buttonColor = delegate.redColor;
    alertViewController.buttonCornerRadius = 20.0;
    NYAlertAction *submitAction = [NYAlertAction actionWithTitle:NSLocalizedString(@"SUBMIT_BTN_CAPS", nil)
    style:UIAlertActionStyleDefault handler:^(NYAlertAction *action) {
     [self dismissViewControllerAnimated:YES completion:nil];}];
    submitAction.enabled = NO;
    [alertViewController addAction:submitAction];
    [[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidChangeNotification
    object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
    UITextField *usernameTextField = [alertViewController.textFields firstObject];
    submitAction.enabled = ([usernameTextField.text length]);}];
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil)
     style:UIAlertActionStyleCancel handler:^(NYAlertAction *action) {[self dismissViewControllerAnimated:YES completion:nil];}]];
    [alertViewController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"YOUR_MESSAGE", nil);
        textField.font = delegate.contentFont;
    }];
    alertViewController.buttonColor = delegate.redColor;
    [self presentViewController:alertViewController animated:YES completion:nil];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)showCommentAlert {
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = NSLocalizedString(@"ADD_COMMENT", nil);
    alertViewController.message = NSLocalizedString(@"PLEASE_ADD_COMMENT", nil);
    alertViewController.titleColor = delegate.yellowColor;
    alertViewController.titleFont = delegate.headFont;
    alertViewController.messageFont = delegate.contentFont;
    alertViewController.buttonTitleFont = delegate.contentFont;
    alertViewController.cancelButtonTitleFont = delegate.contentFont;
    
    alertViewController.cancelButtonColor = delegate.yellowColor;
    alertViewController.buttonColor = delegate.redColor;
    alertViewController.buttonCornerRadius = 20.0;
    
    NYAlertAction *submitAction = [NYAlertAction actionWithTitle:NSLocalizedString(@"SUBMIT_BTN_CAPS", nil)style:UIAlertActionStyleDefault handler:^(NYAlertAction *action) {
    [self dismissViewControllerAnimated:YES completion:nil];
    UITextField *commentField = [alertViewController.textFields firstObject];
    NSString* str = commentField.text;
    UITextField *competency = alertViewController.textFields[1];
    NSString* str1 = competency.text;
    UITextField *subtaskField = [alertViewController.textFields lastObject];
    NSString* str2 = subtaskField.text;
    [self startTheBgFoFeedback:str competency:str1 competency1:str2];
                                                         }];
    submitAction.enabled = NO;
    [alertViewController addAction:submitAction];
    [[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidChangeNotification
                                                      object:nil queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
    UITextField *usernameTextField = [alertViewController.textFields firstObject];
    submitAction.enabled = ([usernameTextField.text length]);
                                                  }];
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) style:UIAlertActionStyleCancel handler:^(NYAlertAction *action) {
    [self dismissViewControllerAnimated:YES completion:nil]; }]];
    
    [alertViewController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"YOUR_MESSAGE", nil);
        textField.font = delegate.contentFont;
        [textField setReturnKeyType:UIReturnKeyDone];
        textField.delegate = self;
    }];
    
    NSMutableArray* bandArray = [[NSMutableArray alloc] init];
    [bandArray addObject:@"Learning Agility"];
    [bandArray addObject:@"Making a Difference"];
    [bandArray addObject:@"Translating Strategy into Action"];
    [bandArray addObject:@"Customer & Market Focus"];
    [bandArray addObject:@"TEffective Collaboration"];
    //[bandArray addObject:@"Social Distortion"];
    
    [alertViewController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.font = delegate.contentFont;
        self.downPicker = [[DownPicker alloc] initWithTextField:textField withData:bandArray];
        textField.placeholder = NSLocalizedString(@"EFFECTIVE_COMPETENCY_WITHOUT_COLON", nil);
    }];
    [alertViewController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.font = delegate.contentFont;
        self.downPicker = [[DownPicker alloc] initWithTextField:textField withData:bandArray];
        textField.placeholder = NSLocalizedString(@"LACKIMG_COMPETENCY_WITHOUT_COLON", nil);
    }];
    alertViewController.buttonColor = delegate.redColor;
    [self presentViewController:alertViewController animated:YES completion:nil];
}
-(void)CommentView{
    CoomentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    CoomentView.backgroundColor = [UIColor clearColor];
    UIImageView *ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    ImageView.backgroundColor = [UIColor blackColor];
    ImageView.alpha = 0.3;
    UIView *InnerView = [[UIView alloc] initWithFrame:CGRectMake(10, delegate.deviceheight/2 - 100, delegate.devicewidth -20, 200)];
    CGPoint center = InnerView.center;
    center.x = CoomentView.center.x;
    InnerView.center = center;
    InnerView.layer.cornerRadius = 5.0;
    InnerView.backgroundColor = [UIColor whiteColor];
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(0, InnerView.frame.size.height-45, InnerView.frame.size.width, 45)];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: BtnView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    BtnView.layer.mask = maskLayer1;
    
    UILabel *AddCommentLabel;
    if([SelectBtn isEqualToString:@"MARK"]){
        AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 0)];
    }
    else{
        AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 45)];
    }
    
    AddCommentLabel.backgroundColor = delegate.yellowColor;
    AddCommentLabel.textColor = [UIColor whiteColor];
    AddCommentLabel.font = delegate.headFont;
    AddCommentLabel.textAlignment = NSTextAlignmentCenter;
    AddCommentLabel.text = NSLocalizedString(@"ADD_COMMENT", nil);
    
    CAShapeLayer * AddCommentmaskLayer1 = [CAShapeLayer layer];
    AddCommentmaskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: AddCommentLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){5.0}].CGPath;
    AddCommentLabel.layer.mask = AddCommentmaskLayer1;
    
    UILabel *AddCommentStaticLabel;
    if([SelectBtn isEqualToString:@"MARK"]){
        AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 35, 150, 15)];
    }
    else{
        AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, AddCommentLabel.frame.origin.y+AddCommentLabel.frame.size.height + 10, 150, 15)];
    }
    
    AddCommentStaticLabel.textColor = [UIColor blackColor];
    AddCommentStaticLabel.font = delegate.contentFont;
    AddCommentStaticLabel.textAlignment = NSTextAlignmentLeft;
    AddCommentStaticLabel.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    
    CommentTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, AddCommentStaticLabel.frame.origin.y+AddCommentStaticLabel.frame.size.height + 10, AddCommentLabel.frame.size.width -20, BtnView.frame.origin.y - 90)];
    CommentTextView.font = delegate.contentFont;
    CommentTextView.delegate = self;
    CommentTextView.layer.cornerRadius = 5.0;
    CommentTextView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.layer.borderWidth = 0.5;
    CommentTextView.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    CommentTextView.backgroundColor = [UIColor clearColor];
    
    UIButton *SubmitBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, BtnView.frame.size.width/2, 45)];
    [SubmitBtn addTarget:self action:@selector(SubmitAction) forControlEvents:UIControlEventTouchUpInside];
    SubmitBtn.backgroundColor = delegate.yellowColor;
    SubmitBtn.titleLabel.font = delegate.normalFont;
    [SubmitBtn setTitle:NSLocalizedString(@"SUBMIT_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    UIButton *CancelBtn=[[UIButton alloc] initWithFrame:CGRectMake(BtnView.frame.size.width/2, 0, BtnView.frame.size.width/2, 45)];
    [CancelBtn addTarget:self action:@selector(CancelAction) forControlEvents:UIControlEventTouchUpInside];
    CancelBtn.backgroundColor = delegate.redColor;
    CancelBtn.titleLabel.font = delegate.normalFont;
    [CancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    [BtnView addSubview:SubmitBtn];
    [BtnView addSubview:CancelBtn];
    [InnerView addSubview:BtnView];
    [InnerView addSubview:CommentTextView];
    [InnerView addSubview:AddCommentLabel];
    [InnerView addSubview:AddCommentStaticLabel];
    [CoomentView addSubview:ImageView];
    [CoomentView addSubview:InnerView];
    
    [self.view addSubview:CoomentView];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:NSLocalizedString(@"ADD_COMMENT_SMALL", nil)]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
        textView.textColor = [UIColor darkGrayColor]; //optional
    }
    [textView resignFirstResponder];
}
-(void)CancelAction{
    CoomentView.hidden = YES;
}
-(void)SubmitAction{
    
    if([CommentTextView.text isEqualToString:NSLocalizedString(@"ADD_COMMENT_SMALL", nil)] ||[CommentTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_ADD_COMMENT", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
//        userComment = CommentTextView.text;
//        if([SelectBtn isEqualToString:@"MARK"]){
//            [self startTheBgForStatus];
//        }
//        else if([SelectBtn isEqualToString:@"COMMENT"]){
//            NSString* str = @"";
//            NSString *SubTask = [[dataConv objectAtIndex:0] objectForKey:@"subTask"];
//            if([SubTask isEqual:[NSNull null]]){
//                str = @"";
//            }
//            else{
//                if([SubTask isEqualToString:@"null"] ||[SubTask isEqualToString:@""]){
//                    str = @"";
//                }
//                else{
//                    str = SubTask;
//                }
//            }
//            
//            [self startTheBgForFollowOn:str :userComment];
//            CoomentView.hidden = YES;
//            [CommentTextView resignFirstResponder];
//        }
    }
}
-(void)sliderAction:(id)sender{
    UISlider *slider = (UISlider*)sender;
    lab1.text=[NSString stringWithFormat:@"%d Months",(int)slider.value ];
}
-(IBAction)btnfun:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==20){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else if(btn.tag==100){
       // [self showAlert];
    }
    else if(300 == btn.tag){
       // tab.backgroundColor = delegate.headerbgColler;
        [addButton removeFromSuperview];
        isShowDetail = TRUE;
        sel_lab.frame=CGRectMake(0, delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/2, 2);
        [tab reloadData];
        [self AddRefreshControl];

    }
    else if(301 == btn.tag){
        tab.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:addButton];
        isShowDetail = FALSE;
        sel_lab.frame=CGRectMake(delegate.devicewidth/2, delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/2, 2);
        [tab reloadData];
        [self AddRefreshControl];

    }
    else if (1000 == btn.tag){
        //[self showCommentAlert];
        SelectBtn = @"FEEDBACK";
       // [self CommentView];
        [self GotoGiveFeedbackPage];
    }
}
-(void)GotoGiveFeedbackPage{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbackViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbackViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [msgvw removeFromSuperview];
    if (isShowDetail){
        return 1;
    }
    else{
        if ([dataConv count] <= 0){
            [self noMessageScreen:NSLocalizedString(@"SORRY_NO_COMMENT", nil)];
        }
        else
            return [dataConv count];
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (TRUE == isShowDetail){
        return screenView.frame.size.height + 280;;
    }
    else{
        int indx=(int)indexPath.row;
        NSString * str = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
        UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        CGSize size1 = CGSizeMake(230,9999);
        CGRect textRect = [str boundingRectWithSize:size1
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:font}
                                            context:nil];
        CGSize size = textRect.size;
        int temp = size.height+textInsetsMine5.bottom+textInsetsMine5.top;
        return temp + 40;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(delegate.margin, 5, delegate.devicewidth-delegate.margin, 50);
    myLabel.font = delegate.boldFont;
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    return headerView;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
    }
    if (TRUE == isShowDetail){
        [self screenTaskDetail:cell];
    }
    else{
        int indx=(int)indexPath.row;
        CGFloat y = 0;
        UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        CGSize size1 = CGSizeMake(230,9999);
        NSString *str = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
        CGRect textRect = [str boundingRectWithSize:size1
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:font}
                                            context:nil];
        CGSize size = textRect.size;
        if ([[[dataConv objectAtIndex:indx] objectForKey:@"sentBy"] isEqualToString:@"ME"]){
            CGFloat x = delegate.devicewidth - size.width - textInsetsMine5.left - textInsetsMine5.right;
            UIImageView *profImg=[[UIImageView alloc] initWithFrame:CGRectMake(delegate.devicewidth-40, y+20,30, 30)];
            UIImage* imagePath = [[UIImage imageNamed:@"profile_image_default.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            profImg.image = imagePath;
            [cell addSubview:profImg];
            
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:indx] objectForKey:@"userImg"]))){
                UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]]]];
                if(images == nil){
                    NSLog(@"image nil");
                    profImg.image = [UIImage imageNamed:@"profile_image_default"];
                }
                else{
                    NSString *URL = [NSString stringWithFormat:@"%@",[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]];
                    [profImg sd_setImageWithURL:[NSURL URLWithString:URL]];
                }
            }
            else{
                profImg.image = [UIImage imageNamed:@"profile_image_default"];
            }
            profImg.layer.borderColor = [UIColor whiteColor].CGColor;
            profImg.layer.borderWidth = 1;
            profImg.layer.cornerRadius = 15;
            profImg.clipsToBounds = YES;
            
            UILabel *userImg=[[UILabel alloc] initWithFrame:CGRectMake(x-50, y+20,size.width+textInsetsMine5.left+textInsetsMine5.right, size.height+textInsetsMine5.bottom+textInsetsMine5.top)];
            userImg.layer.borderWidth = 2.0;
            userImg.layer.borderColor = delegate.yellowColor.CGColor;
            userImg.layer.cornerRadius = 8;
            userImg.layer.masksToBounds = true;
            [cell addSubview:userImg];
            
            UILabel *lastMsg=[[UILabel alloc] initWithFrame:CGRectMake(x-40, y+textInsetsMine5.top+22, size.width,size.height)];
            
            lastMsg.font=[UIFont systemFontOfSize:[UIFont systemFontSize]];
            lastMsg.textColor=delegate.blackcolor;
            lastMsg.lineBreakMode = NSLineBreakByWordWrapping;
            lastMsg.numberOfLines = 0;
            lastMsg.textAlignment = ALIGN_LEFT;
            lastMsg.text = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
            [cell addSubview:lastMsg];
            
            UILabel *subTask_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, y,delegate.devicewidth, 20)];
            subTask_lab.font=delegate.mediumFont;
            subTask_lab.textColor=delegate.blackcolor;
            subTask_lab.textAlignment = ALIGN_CENTER;
            
            if ([[dataConv objectAtIndex:indx] objectForKey:@"subTask"] != [NSNull null])
                subTask_lab.text = [[dataConv objectAtIndex:indx] objectForKey:@"subTask"];
            else
                subTask_lab.text = @"";
            
            [cell addSubview:subTask_lab];
            CGFloat ypos = y+textInsetsMine5.top+22+size.height+10;
            UILabel *date_lab=[[UILabel alloc] initWithFrame:CGRectMake(delegate.devicewidth-140, ypos,120, 20)];
            date_lab.font=delegate.smallFont2;
            date_lab.textColor=delegate.dimBlack;
            date_lab.text = [[dataConv objectAtIndex:indx] objectForKey:@"creationDate"];
            [cell addSubview:date_lab];
            isMe = FALSE;
        }
        else{
            isMe = TRUE;
            CGFloat x = 5;
            UIImageView *profImg=[[UIImageView alloc] initWithFrame:CGRectMake(x, y+20,30, 30)];
            UIImage* imagePath = [[UIImage imageNamed:@"profile_image_default.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            profImg.image = imagePath;
            [cell addSubview:profImg];
            
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:indx] objectForKey:@"userImg"]))){
                UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]]]];
                if(images == nil){
                    NSLog(@"image nil");
                    profImg.image = [UIImage imageNamed:@"profile_image_default"];
                }
                else{
                    NSString *URL = [NSString stringWithFormat:@"%@",[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]];
                    [profImg sd_setImageWithURL:[NSURL URLWithString:URL]];
                }
            }
            else{
                profImg.image = [UIImage imageNamed:@"profile_image_default"];
            }
            profImg.layer.borderColor = [UIColor whiteColor].CGColor;
            profImg.layer.borderWidth = 1;
            profImg.layer.cornerRadius = 15;
            profImg.clipsToBounds = YES;
            
            x = x + profImg.frame.size.width;
            UILabel *userImg=[[UILabel alloc] initWithFrame:CGRectMake(x+10, y+20,size.width+texttextInsetsSomeone5.left+texttextInsetsSomeone5.right, size.height+texttextInsetsSomeone5.bottom+texttextInsetsSomeone5.top)];
            
            userImg.layer.borderWidth = 2.0;
            userImg.layer.borderColor = delegate.redColor.CGColor;
            userImg.layer.cornerRadius = 8;
            userImg.layer.masksToBounds = true;
            [cell addSubview:userImg];
            
            UILabel *lastMsg=[[UILabel alloc] initWithFrame:CGRectMake(x+texttextInsetsSomeone5.left+2, y+texttextInsetsSomeone5.top+22, size.width,size.height)];
            lastMsg.font=[UIFont systemFontOfSize:[UIFont systemFontSize]];
            lastMsg.textColor=delegate.blackcolor;
            lastMsg.lineBreakMode = NSLineBreakByWordWrapping;
            lastMsg.numberOfLines = 0;
            lastMsg.textAlignment = ALIGN_LEFT;
            lastMsg.text = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
            [cell addSubview:lastMsg];
            CGFloat ypos = y+texttextInsetsSomeone5.top+22+size.height+10;
            UILabel *date_lab=[[UILabel alloc] initWithFrame:CGRectMake(10, ypos,150,20)];
            date_lab.font=delegate.smallFont2;
            date_lab.textColor=delegate.dimBlack;
            date_lab.text = [[dataConv objectAtIndex:indx] objectForKey:@"creationDate"];
            [cell addSubview:date_lab];
            
            x=10;
            UILabel *subTask_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, y,delegate.devicewidth, 20)];
            subTask_lab.font=delegate.mediumFont;
            subTask_lab.textColor=delegate.blackcolor;
            subTask_lab.textAlignment = ALIGN_CENTER;
            if ([[dataConv objectAtIndex:indx] objectForKey:@"subTask"] != [NSNull null])
                subTask_lab.text = [[dataConv objectAtIndex:indx] objectForKey:@"subTask"];
            else
                subTask_lab.text = @"";
            [cell addSubview:subTask_lab];
        }
    }
    return cell;
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    areaSelect.hidden=FALSE;
}
-(void) startTheBgForStatus:(NSString*)msg{
    if(delegate.isInternetConnected){
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        delegate.count = 2;
        [_params setObject:@"post" forKey:@"action"];
        [_params setObject:[NSString stringWithFormat:@"%@", [delegate.dataFull objectForKey:@"handsetId"]] forKey:@"handsetId"];
        [_params setObject:[NSString stringWithFormat:@"%@", taskID] forKey:@"taskId"];
        [_params setObject:[NSString stringWithFormat:@"suspend"] forKey:@"opType"];
        [_params setObject:[NSString stringWithFormat:@"%@", msg] forKey:@"comment"];
        NSString* url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/manageTask"];

        [self startTheBg_API:_params url:url completionHandler:^(id data, NSError *error) {
            [self stopProgBar];
            NSDictionary *reqdataDict = data;
            if([[reqdataDict objectForKey:@"status"] isEqualToString:@"OK"]){
                NSString* errorMsg = [reqdataDict objectForKey:@"msg"];
                SSSnackbar *snackbar = [delegate snackbarForQuickRunningItem:errorMsg];
                [snackbar show];
                [self.view makeToast:errorMsg];
                [[self navigationController] popViewControllerAnimated:YES];
            }
            else {
                 [self ShowAlert:[reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }];
    }
    else{
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void) startTheBgFoFeedback:(NSString*)feedback competency:(NSString*)msg competency1:(NSString*)msg1{
     if(delegate.isInternetConnected){
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    delegate.count = 2;
    [_params setObject:@"post" forKey:@"action"];
    [_params setObject:[NSString stringWithFormat:@"%@", [delegate.dataFull objectForKey:@"handsetId"]] forKey:@"handsetId"];
    [_params setObject:[NSString stringWithFormat:@"%@", taskID] forKey:@"taskId"];
    [_params setObject:[NSString stringWithFormat:@"%@", feedback] forKey:@"feedback"];
    [_params setObject:[NSString stringWithFormat:@"%@", msg] forKey:@"competency"];
    [_params setObject:[NSString stringWithFormat:@"%@", msg1] forKey:@"competency1"];
     NSString* url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/addstackfeedback"];
    [self startTheBg_API:_params url:url completionHandler:^(id data, NSError *error) {
        [self stopProgBar];
        NSDictionary *reqdataDict = data;
        if([[reqdataDict objectForKey:@"status"] isEqualToString:@"OK"]){
            [self ShowAlert: [reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        else {
            [self ShowAlert: [reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }];
     }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)initProgBar{
    self.activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    self.activityIndicatorView.frame = CGRectMake(delegate.devicewidth/2-50, delegate.deviceheight-200, 100, 100);
    self.activityIndicatorView.tag=1;
    [self.view addSubview:self.activityIndicatorView];
    self.activityIndicatorView.color = delegate.redColor;
    [self.view setUserInteractionEnabled:NO];
    [self.activityIndicatorView startAnimating];
}
-(void)stopProgBar{
    [self.view setUserInteractionEnabled:YES];
    [self.activityIndicatorView stopAnimating];
}
-(void)noMessageScreen:(NSString*)message{
    msgvw=[[UIView alloc] initWithFrame:tab.frame];
    msgvw.backgroundColor = delegate.BackgroudColor;
    UIImageView *nomsgimg;
    UILabel *tab_lab;
    if (delegate.deviceheight <= 460){
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-100)/2,
                                                               0, 100,100)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    else{
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-100)/2,
                                                               20, 100,100)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    nomsgimg.image=[UIImage imageNamed:@"no_comment"];
    [msgvw addSubview:nomsgimg];
    tab_lab.text = message;
    tab_lab.font=delegate.ooredoo;
    tab_lab.textColor=delegate.dimBlack;
    tab_lab.lineBreakMode = NSLineBreakByWordWrapping;
    tab_lab.numberOfLines = 0;
    tab_lab.textAlignment=ALIGN_CENTER;
    [msgvw addSubview:tab_lab];
    [tab addSubview:msgvw];
    tab.scrollEnabled=FALSE;
}
-(void) startTheBg_API:(NSMutableDictionary*) params  url:(NSString*) url completionHandler:(void (^)(id responseObject, NSError *error))completionHandler
{
    if([delegate isInternetConnected]){
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        } error:nil];
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self initProgBar];
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, NSData *data, NSError * _Nullable error) {
                          if (error) {
                               [self ShowAlert:[error description] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                          } else {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  if (completionHandler) {
                                      completionHandler(data, nil);
                                  }
                              });
                          }
                      }];
        [uploadTask resume];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void) getConversation{
    [self startTheBgForGetConvesations:^(id data, NSError *error) {
        [self stopProgBar];
        NSDictionary *reqdataDict = data;
        if([[reqdataDict objectForKey:@"status"] isEqualToString:@"OK"]){
            NSArray *array = [[NSArray alloc] init];
            array = [reqdataDict objectForKey:@"data"];
            dataConv = [array mutableCopy];
            [tab reloadData];
        }
        else {
            if (FALSE == isShowDetail){
                [self ShowAlert: [reqdataDict objectForKey:@"msg"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
    }];
}
-(void) startTheBgForGetConvesations:(void (^)(id responseObject, NSError *error))completionHandler
{
    if([delegate isInternetConnected]){
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        [_params setObject:@"post" forKey:@"action"];
        [_params setObject:[NSString stringWithFormat:@"%@", [delegate.dataFull objectForKey:@"handsetId"]] forKey:@"handsetId"];
        [_params setObject:[NSString stringWithFormat:@"%@", taskID] forKey:@"taskId"];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
         NSString* url = [NSString stringWithFormat:@"%@%@",BASE_URL_PMS,@"PMSframework/PmsRestApi/getConversation"];
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:_params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        } error:nil];
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, NSData *data, NSError * _Nullable error) {
                          if (error) {
                        [self ShowAlert:[error description] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
                          } else {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  if (completionHandler) {
                                      completionHandler(data, nil);
                                  }
                              });
                          }
                      }];
        [uploadTask resume];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
