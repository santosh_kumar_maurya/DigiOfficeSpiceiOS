

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface LoadingAgent : NSObject {
	UIView* main_view;
	UIActivityIndicatorView* wait;
	int loadingCount;
	AppDelegate *delegate;
	
}
/**
 * Checks whether Loading is being carried out or not
 */
- (BOOL) isBusy;
/**
 * call this function. Pass yes if you want to set busy mode.
 * pass No if your done with your busy state
 */
- (void) makeBusy:(BOOL)yesOrno;

/**
 * Messed up with updateBusyState? call this method to remove busy state
 */
- (void) forceRemoveBusyState;

/**
 * Factory method
 */
+ (LoadingAgent*)defaultAgent;

@end
