//
//  MyTaskScreenParentViewController.m
//  iWork
//
//  Created by Shailendra on 19/07/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTaskScreenParentViewController.h"
#import "Header.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define kDefaultEdgeInsets UIEdgeInsetsMake(5, 10, 5, 10)

@interface MyTaskScreenParentViewController ()
{
    NSArray *btnArray;
    AppDelegate *delegate;
    IBOutlet UILabel *HeaderLabel;
    int BtnTagValue;
    
}
@property (weak) IBOutlet UIScrollView *containerScrollView;
@property (weak) IBOutlet UIScrollView *menuScrollView;
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray;
-(void) buttonPressed:(id) sender;
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets;
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr;

@end

@implementation MyTaskScreenParentViewController
@synthesize isComeFrom;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    _menuScrollView.backgroundColor= [ColorCategory PurperColor];
    HeaderLabel.text = NSLocalizedString(@"MY_TASK", nil) ;
    btnArray = [[NSArray alloc]initWithObjects: NSLocalizedString(@"NEW", nil),NSLocalizedString(@"ONGOING", nil),NSLocalizedString(@"HISTORY", nil),nil];
    [self addButtonsInScrollMenu:btnArray];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];

    NewViewController *oneVC = [storyBoard instantiateViewControllerWithIdentifier:@"NewViewController"];
    
    OngoingViewController *twoVC = [storyBoard instantiateViewControllerWithIdentifier:@"OngoingViewController"];
    HistoryViewController *threeVC = [storyBoard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
    NSArray *controllerArray = @[oneVC, twoVC, threeVC];
    [self addChildViewControllersOntoContainer:controllerArray];
    
    
}
#pragma mark - Add Menu Buttons in Menu Scroll View
-(void) addButtonsInScrollMenu:(NSArray *)buttonArray
{
    CGFloat buttonHeight = self.menuScrollView.frame.size.height;
    CGFloat cWidth = 0.0f;
    
    for (int i = 0 ; i<buttonArray.count; i++)
    {
        NSString *tagTitle = [buttonArray objectAtIndex:i];
        
        CGFloat buttonWidth = [self widthForMenuTitle:tagTitle buttonEdgeInsets:kDefaultEdgeInsets];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.titleLabel.numberOfLines = 2;
        button.frame = CGRectMake(cWidth, 0.0f, SCREEN_WIDTH/3, buttonHeight);
        [button setTitle:tagTitle forState:UIControlStateNormal];
        button.titleLabel.font = delegate.normalBold;
        
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [self.menuScrollView addSubview:button];
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height-2, SCREEN_WIDTH/3, 2)];
        bottomView.backgroundColor = [UIColor whiteColor];
        bottomView.tag = 1001;
        [button addSubview:bottomView];
        if([isComeFrom isEqualToString:@"PROGRESS"]){
            if(i==1){
               [self OnGoingNotification];
                button.selected = YES;
                [bottomView setHidden:NO];
                
                [self.containerScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * i, 0) animated:YES];
                
                float xx = SCREEN_WIDTH * (i) * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
                [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
                
            }
            else{
                 [bottomView setHidden:YES];
            }
        }
        else{
            if (i == 0){
                button.selected = YES;
                [bottomView setHidden:NO];
            }
            else{
                [bottomView setHidden:YES];
            }
        }
        
        cWidth += SCREEN_WIDTH/3;
    }
    self.menuScrollView.contentSize = CGSizeMake(cWidth, self.menuScrollView.frame.size.height);
}
#pragma mark - Menu Button press action
-(void) buttonPressed:(id) sender
{
    UIButton *senderbtn = (UIButton *) sender;
    float buttonWidth = 0.0f;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        UIButton *btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        
        if (btn.tag == senderbtn.tag){
            BtnTagValue = btn.tag;
            if(BtnTagValue == 0){
                [self NewNotification];
            }
            else if(BtnTagValue == 1){
                [self OnGoingNotification];
            }
            else if(BtnTagValue == 2){
                [self BehindNotification];
            }
            btn.selected = YES;
            [bottomView setHidden:NO];
        }
        else{
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    [self.containerScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * senderbtn.tag, 0) animated:YES];
    
    float xx = SCREEN_WIDTH * (senderbtn.tag) * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}

/**
 *  Calculating width of button added on top menu
 *
 *  @param title            Title of the Button
 *  @param buttonEdgeInsets Edge Insets for the title
 *
 *  @return Width of button
 */

#pragma mark - Calculate width of menu button
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets
{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]};
    
    CGSize size = [title sizeWithAttributes:attributes];
    return CGSizeMake(size.width + buttonEdgeInsets.left + buttonEdgeInsets.right, size.height + buttonEdgeInsets.top + buttonEdgeInsets.bottom).width;
}

#pragma mark - Adding all related controllers in to the container
-(void) addChildViewControllersOntoContainer:(NSArray *)controllersArr
{
    for (int i = 0 ; i < controllersArr.count; i++)
    {
        UIViewController *vc = (UIViewController *)[controllersArr objectAtIndex:i];
        CGRect frame = CGRectMake(0, 0, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height);
        frame.origin.x = SCREEN_WIDTH * i;
        vc.view.frame = frame;
        
        [self addChildViewController:vc];
        [self.containerScrollView addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
    self.containerScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * controllersArr.count + 1, self.containerScrollView.frame.size.height);
    self.containerScrollView.pagingEnabled = YES;
    self.containerScrollView.delegate = self;
}


#pragma mark - Scroll view delegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = (scrollView.contentOffset.x / SCREEN_WIDTH);
    
    UIButton *btn;
    float buttonWidth = 0.0;
    for (UIView *subView in self.menuScrollView.subviews)
    {
        btn = (UIButton *) subView;
        UIView *bottomView = [btn viewWithTag:1001];
        if (btn.tag == page){
            BtnTagValue = btn.tag;
            if(BtnTagValue == 0){
                [self NewNotification];
            }
            else if(BtnTagValue == 1){
                [self OnGoingNotification];
            }
            else if(BtnTagValue == 2){
                [self BehindNotification];
            }
            btn.selected = YES;
            buttonWidth = btn.frame.size.width;
            [bottomView setHidden:NO];
        }
        else{
            btn.selected = NO;
            [bottomView setHidden:YES];
        }
    }
    
    float xx = scrollView.contentOffset.x * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    [self.menuScrollView scrollRectToVisible:CGRectMake(xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}
-(IBAction)HomeAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    ObjAppContainerViewController.isComefrom = @"PMS";
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
- (IBAction)FilterBtnAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyTaskFilterViewController *ObjFilterViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskFilterViewController"];
    if(BtnTagValue == 0){
        ObjFilterViewController.isComeFrom =@"NEW";
    }
    else if(BtnTagValue == 1){
        ObjFilterViewController.isComeFrom =@"ONGOING";
    }
    else if(BtnTagValue == 2){
        ObjFilterViewController.isComeFrom =@"HISTORY";
    }
    [self presentViewController:ObjFilterViewController animated:YES completion:nil];
}
-(void)NewNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NEW" object:nil];
}
-(void)OnGoingNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ONGOING" object:nil];
}
-(void)BehindNotification{
     [[NSNotificationCenter defaultCenter] postNotificationName:@"BEHIND" object:nil];
}
-(void)FilterNewNotification{
    
}
-(void)FilterOnGoingNotification{
    
}
-(void)FilterBehindNotification{
    
}

@end
