

#import "AlertScreen.h"
#import "AlertController.h"
#import "AppContainerViewController.h"
@interface AlertScreen ()

@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation AlertScreen
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=TRUE;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    tabdataarray=[[NSMutableArray alloc] init];
    [self ScreenDesign];
}
- (void) ScreenDesign{
    
    UIView *screenView=[[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    screenView.backgroundColor=delegate.BackgroudColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor=delegate.headerbgColler;
    [screenView addSubview:headerview];
    
    UIButton *backButton=[[UIButton alloc] initWithFrame:CGRectMake(5, 35, 30, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    backButton.tag=20;
    CGPoint center = backButton.center;
    center.y = headerview.frame.size.height / 2 +3;
    [backButton setCenter:center];
    [headerview addSubview:backButton];
    
    UIButton* HomeButton=[UIButton buttonWithType:UIButtonTypeCustom];
    HomeButton.frame=CGRectMake(delegate.devicewidth-35, 10, 18, 18);
    HomeButton.tag=20;
    center = HomeButton.center;
    center.y = headerview.frame.size.height / 2 + 3;
    [HomeButton setCenter:center];
    [HomeButton addTarget:self action:@selector(GotobackContainer) forControlEvents:UIControlEventTouchUpInside];
    [HomeButton setImage:[UIImage imageNamed:@"HomeImage"]  forState:UIControlStateNormal];
    [headerview addSubview:HomeButton];
    
    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(45, backButton.frame.origin.y+12, 200, 20)];
    headerlab.textColor=[UIColor whiteColor];
    headerlab.font=delegate.boldFont;
    headerlab.text=NSLocalizedString(@"MY_ALERTS", nil);
    headerlab.textAlignment=ALIGN_CENTER;
    [headerview addSubview:headerlab];
    
    tab=[[UITableView alloc] initWithFrame:CGRectMake(0, delegate.headerheight, screenView.frame.size.width,screenView.frame.size.height-(2*delegate.headerheight))];
    tab.delegate=self;
    tab.dataSource=self;
    tab.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab.backgroundColor=[UIColor clearColor];
    [screenView addSubview:tab];
    [self.view addSubview:screenView];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([tabdataarray count]==0){
        msgvw=[[UIView alloc] initWithFrame:tableView.frame];
        msgvw.backgroundColor=delegate.BackgroudColor;
        UIImageView *nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-200)/2,(msgvw.frame.size.height-230)/2, 200,200)];
        nomsgimg.image=[UIImage imageNamed:@"ic_noalert"];
        [msgvw addSubview:nomsgimg];
        
        UILabel *tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,(msgvw.frame.size.height-230)/2+200, msgvw.frame.size.width-delegate.margin, 30)];
        tab_lab.text=NSLocalizedString(@"NO_MESSAGE", nil);
        tab_lab.font=delegate.normalFont;
        tab_lab.numberOfLines=2;
        tab_lab.textColor=[UIColor blackColor];
        tab_lab.textAlignment=ALIGN_CENTER;
        [msgvw addSubview:tab_lab];
        [tableView addSubview:msgvw];
        tableView.scrollEnabled=FALSE;
    }
    else{
        [msgvw removeFromSuperview];
        tableView.scrollEnabled=TRUE;
    }
    return [tabdataarray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
    }
    UILabel *name_lab=[[UILabel alloc] initWithFrame:CGRectMake(delegate.margin, 1, tableView.frame.size.width-2*delegate.margin, 42)];
    name_lab.text=[[tabdataarray objectAtIndex:indexPath.row] objectForKey:@"name"];
    name_lab.textColor=[UIColor darkGrayColor];
    name_lab.font=delegate.normalFont;
    [cell addSubview:name_lab];
    UIImageView *divimg=[[UIImageView alloc] initWithFrame:CGRectMake(0, 43.5, tableView.frame.size.width-delegate.margin, 0.5)];
    divimg.image=[UIImage imageNamed:@"divider_gray"];
    [cell addSubview:divimg];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}
-(IBAction)btnfun:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==20){
        [[self navigationController] popViewControllerAnimated:YES];
    }
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
