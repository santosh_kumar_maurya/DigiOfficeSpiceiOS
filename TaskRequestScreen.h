#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NYAlertViewController.h"
#import "FCAlertView.h"
#import "AFURLSessionManager.h"
#import "NewTaskDetailScreen.h"

@interface TaskRequestScreen : UIViewController <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,FCAlertViewDelegate>
{
    AppDelegate *delegate;
   
    NSMutableArray *tabdataarray1,*tabdataarray2,*dataArray,*dataArray1;
    NSMutableDictionary *dataDict;
    UILabel *flatButton_lab,*flatButton1_lab,*sel_lab,*lab1,*datelab,*flatButton2_lab;
    int count;
    NSString *taskType, *taskID;
    BOOL isNewReq, isServerResponded;
    UIView *msgvw, *filterView, *screenView;
    UIAlertView *alertView;
    long taskRating;
    NSString* taskFeedback, *serverMsg, *serverMsg1;
    FCAlertView *alert;
    
    NSMutableDictionary *filterData;
    UIDatePicker *dateTimePicker;
    UITextField *fromDate,*toDate, *taskId;
    BOOL isFromDate;
    NSString *filterTaskId, *toFilterDate, *fromFilterDate;
    
}
@property (nonatomic, retain) NSString* taskType, *taskID, *taskFeedback, *serverMsg, *serverMsg1;
@property (nonatomic) int count;
@property (nonatomic) BOOL isNewReq;
@property (nonatomic) long taskRating;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@end
