//
//  TeamCell.h
//  iWork
//
//  Created by Shailendra on 04/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UIView *MyTeamView;
@property(nonatomic,strong)IBOutlet UIView *FilterView;
@property(nonatomic,strong)IBOutlet UILabel *MyTeamStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *CompletedStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *CompletedLabel;
@property(nonatomic,strong)IBOutlet UILabel *OnScheduleStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *OnScheduleLabel;
@property(nonatomic,strong)IBOutlet UILabel *BehindStaticLabel;
@property(nonatomic,strong)IBOutlet UILabel *BehindLabel;
@property(nonatomic,strong)IBOutlet UIImageView *ProfileImageView;
@property(nonatomic,strong)IBOutlet UILabel *UserNameLabel;
@property(nonatomic,strong)IBOutlet UIButton *FilterBtn;
@property(nonatomic,strong)IBOutlet NSLayoutConstraint *hight;
@property(nonatomic,strong)IBOutlet NSLayoutConstraint *hight1;
- (void)configureCell:(NSDictionary *)info;
@end
