#import "TaskStatusScreen.h"
#import "Header.h"

@interface TaskStatusScreen ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    IBOutlet UITableView *tab;
    IBOutlet UILabel *headerlab;
    IBOutlet UILabel *NoDataLabel;
    WebApiService *Api;
    NSString *handelId;
    NSString *reqType;
    NSMutableArray *newArray;
    NSMutableDictionary *dataDict;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    int offset;
    int limit;

}
@end
@implementation TaskStatusScreen
@synthesize taskType,isManager;

- (void)viewDidLoad{
    [super viewDidLoad];
    [self EmptyArray];
    self.view.backgroundColor = delegate.BackgroudColor;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    handelId = [NSString stringWithFormat:@"%@",[delegate.dataFull objectForKey:@"handsetId"]];
    if (1 == taskType)
        headerlab.text=NSLocalizedString(@"ONGOING_SCHEDULE", nil);
    else if (2 == taskType)
        headerlab.text=NSLocalizedString(@"ONGOING_BHIND_SCHEDULE", nil);
    else
        headerlab.text=NSLocalizedString(@"COMPLETE_TASK", nil);
    Api = [[WebApiService alloc] init];
    tab.estimatedRowHeight = 50;
    tab.backgroundColor = delegate.BackgroudColor;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 10.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    self.view.backgroundColor = delegate.BackgroudColor;
}
-(void) viewWillAppear:(BOOL)animated{
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchData) withObject:nil afterDelay:0.5];
}
-(void) fetchData{
    if(delegate.isInternetConnected){
        if(taskType == 3){
            params = @ {
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
            ResponseDic = [Api WebApi:params Url:[NSString stringWithFormat:@"CompletedCountList?employeeID=%@",[ApplicationState userId]]];
            [refreshControl endRefreshing];
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                NSLog(@"NewTaskResponseDic==%@",ResponseDic);
                NoDataLabel.hidden = YES;
                if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                    
                    NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                    if(ResponseArrays.count>0){
                        [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                        NoDataLabel.hidden = YES;
                        [tab reloadData];
                    }
                    else if(ResponseArrays.count == 0 && newArray.count == 0){
                       [self NoDataFound];
                    }
                }
                else{
                   [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
        else if(taskType == 1 || taskType == 2){
            NSString *Status;
            if(taskType == 1){
                Status = @"ongoing";
            }
            else if(taskType == 2){
                Status = @"behind";
            }
            params = @ {
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };

            [refreshControl endRefreshing];
            ResponseDic = [Api WebApi:params Url:[NSString stringWithFormat:@"onBehindCountList?employeeId=%@&status=%@",[ApplicationState userId],Status]];
            
            if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
                [LoadingManager hideLoadingView:self.view];
                NSLog(@"NewTaskResponseDic==%@",ResponseDic);
                NoDataLabel.hidden = YES;
                if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                    
                    NSMutableArray *ResponseArrays = [ResponseDic valueForKey:@"object"];
                    if(ResponseArrays.count>0){
                        [self CollectResponse:[ResponseDic valueForKey:@"object"]];
                        NoDataLabel.hidden = YES;
                        [tab reloadData];
                    }
                    else if(ResponseArrays.count == 0 && newArray.count == 0){
                        [self NoDataFound];
                    }
                }
                else{
                    [self NoDataFound];
                }
            }
            else{
                [self NoDataFound];
            }
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
      [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    if(newArray.count == 0){
        NoDataLabel.hidden = NO;
        NoDataLabel.text = @"No record found";
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
        NSArray *ArraysValue =  [ResponseDic valueForKey:@"object"];
        if(ArraysValue.count>0){
            offset = offset + 50;
            [self fetchData];
        }
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        [newArray addObject:BindDataDic];
    }
}
- (void)reloadData{
    [self EmptyArray];
    [self fetchData];
}
-(void)EmptyArray{
    offset = 0;
    limit = 50;
    newArray = [[NSMutableArray alloc] init];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return [newArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(taskType == 1 || taskType == 2){
        AskForProgressCell *Cell = (AskForProgressCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        Cell.DetailsBtn.tag = indexPath.section;
        [Cell.DetailsBtn addTarget:self action:@selector(GotoTaskDetails:) forControlEvents:UIControlEventTouchUpInside];
        if(newArray.count>0){
            NSDictionary * responseData = [newArray objectAtIndex:indexPath.section];
            [Cell configureCell:responseData];
        }
        return Cell;
    }
    else{
        FeedbackScreenCell *Cell = (FeedbackScreenCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        Cell.DetailsBtn.tag = indexPath.section;
        [Cell.DetailsBtn addTarget:self action:@selector(GotoTaskDetails:) forControlEvents:UIControlEventTouchUpInside];
        Cell.RatingView.hidden = NO;
        if(newArray.count>0){
            NSDictionary * responseData = [newArray objectAtIndex:indexPath.section];
            [Cell configureCell:responseData];
        }
        return Cell;
    }
}
-(void)GotoTaskDetails:(UIButton*)sender{
    int TagValue = sender.tag;
    if (taskType == 3){
        NSString* task = [[newArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        HistoryDetailScreen *viewController = [[HistoryDetailScreen alloc] init];
        viewController.taskID = task;
        viewController.isManager = isManager;
        [[self navigationController] pushViewController:viewController animated: YES];
    }
    else{
        NSString* task = [[newArray objectAtIndex:TagValue] objectForKey:@"taskId"];
        if (TRUE == isManager){
            TaskDetailManagerScreen *viewController = [[TaskDetailManagerScreen alloc] init];
            viewController.taskID = task;
            viewController.isManager = isManager;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
        else{
            TaskDetailScreen *viewController = [[TaskDetailScreen alloc] init];
            viewController.taskID = task;
            [[self navigationController] pushViewController:viewController animated: YES];
        }
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
