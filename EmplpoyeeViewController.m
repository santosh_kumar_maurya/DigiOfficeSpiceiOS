//
//  EmplpoyeeViewController.m
//  iWork
//
//  Created by Shailendra on 06/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "EmplpoyeeViewController.h"
#import "Header.h"
@interface EmplpoyeeViewController ()<UITableViewDelegate,UITextViewDelegate>{
    IBOutlet UITableView * EmployeeTableView;
    
    NSMutableArray *TaskArray;
    NSString *CheckValue;
    UIRefreshControl *refreshControl;
    AppDelegate *delegate;
    NSDictionary *params;
    IBOutlet UIImageView *ProfileImageView;
    IBOutlet UILabel *HeaderLabel;
    IBOutlet UILabel *UserNameLabel;
    IBOutlet UILabel *NatificationLabel;
    WebApiService *Api;
    NSDictionary *MyTaskDic;
    NSDictionary *ResponseDic;
}
@end

@implementation EmplpoyeeViewController

- (void)viewDidLoad {
    CheckValue = @"MYTASK";
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    Api = [[WebApiService alloc] init];
    UserNameLabel.text = @"";
    NatificationLabel.layer.cornerRadius = 9.0;
    NatificationLabel.clipsToBounds = YES;
    NatificationLabel.backgroundColor = delegate.yellowColor;
    NatificationLabel.textColor = delegate.redColor;
    
    ProfileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    ProfileImageView.layer.borderWidth = 2;
    ProfileImageView.layer.cornerRadius = 30;
    ProfileImageView.clipsToBounds = YES;
    
    NSString *HeaderStr = NSLocalizedString(@"IPM", nil);
    HeaderStr = [HeaderStr stringByReplacingOccurrencesOfString:@"iPM" withString:[NSString stringWithFormat:@"%@",[@"&#9432;PM" stringByConvertingHTMLToPlainText]]];
    HeaderLabel.text = HeaderStr;
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = delegate.BackgroudColor;
    EmployeeTableView.estimatedRowHeight = 50;
    EmployeeTableView.backgroundColor = delegate.BackgroudColor;
    EmployeeTableView.rowHeight = UITableViewAutomaticDimension;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [EmployeeTableView addSubview:refreshControl];
    
    NSDictionary *Dic = [ApplicationState getUserLoginData];
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]))){
        UserNameLabel.text = [NSString stringWithFormat:@"Hi, %@", [[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"name"]];
    }
    else{
        UserNameLabel.text = @" ";
    }
    
    NSString *URL = [NSString stringWithFormat:@"%@images/%@",BASE_URL,[[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]];
    URL = [URL stringByReplacingOccurrencesOfString:@"iworkapi" withString:@"iwork"];
    if(IsSafeStringPlus(TrToString([[[Dic valueForKey:@"responseObj"] valueForKey:@"object"] valueForKey:@"dp"]))){
        NSURL *Url = [NSURL URLWithString:URL];
        UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL:Url]];
        if(images == nil){
            NSLog(@"image nil");
            ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
        }
        else{
            
            [ProfileImageView sd_setImageWithURL:[NSURL URLWithString:URL]];
        }
    }
    else{
        ProfileImageView.image = [UIImage imageNamed:@"profile_image_default"];
    }
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(TaskWebApi) withObject:nil afterDelay:0.5];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationData:) name:@"MYTASK" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNotifications) name:@"Notification" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)NotificationData:(NSNotification*)notification{
    params = notification.userInfo;
    CheckValue = @"FILTER";
    [self TaskWebApi];
}
-(void)viewWillAppear:(BOOL)animated{
    [self UpdateNotifications];
}
-(void)UpdateNotifications{
    NatificationLabel.alpha=0.0;
    NatificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
    if ([NatificationLabel.text intValue]>0) {
        NatificationLabel.alpha=1.0;
        NatificationLabel.text = [NSString stringWithFormat:@"%d",[delegate FetchNotificationCounter:@"IPM"]];
    }
}
- (void)reloadData{
    CheckValue = @"Refresh";
    [self TaskWebApi];
}
- (void)TaskWebApi {
    if(delegate.isInternetConnected){
        if([CheckValue isEqualToString:@"MYTASK"]||[CheckValue isEqualToString:@"Refresh"]||[CheckValue isEqualToString:@"FILTER"]){
            if(![CheckValue isEqualToString:@"FILTER"])
                params = @ {
                    @"e_date": @"",
                    @"employee_Id": [ApplicationState userId],
                    @"s_date": @""
                };
            ResponseDic =  [Api WebApi:params Url:@"myDashTaskCount"];
        }
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"]intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                MyTaskDic = [ResponseDic valueForKey:@"object"];
            }
            [EmployeeTableView reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic objectForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        SubmitCell *Cell = (SubmitCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        return Cell;
    }
    else if(indexPath.row == 1){
        MyTaskCell *Cell = (MyTaskCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"me"]))){
            Cell.TaskCreationLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic  valueForKey:@"me"]];
        }
        else{
             Cell.TaskCreationLabel.text  = @" ";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"line_manager"]))){
            Cell.TaskCompletionLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"line_manager"]];
        }
        else{
            Cell.TaskCompletionLabel.text  = @"";
        }
        return Cell;
    }
    else if(indexPath.row == 2){
        TaskStatusCell *Cell = (TaskStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"approved_rejected"]))){
            Cell.CompleteLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"approved_rejected"]];
        }
        else{
            Cell.CompleteLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"ongoing_on_schedule"]))){
            Cell.OnScheduleLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"ongoing_on_schedule"]];
        }
        else{
            Cell.OnScheduleLabel.text  = @"";
        }
        
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"ongoing_behind_schedule"]))){
            Cell.BehindLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"ongoing_behind_schedule"]];
        }
        else{
            Cell.BehindLabel.text  = @"";
        }
        return Cell;
    }
    else{
        MyKpiCell *Cell = (MyKpiCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
        Cell.ScrollViews.bounces =YES;
        Cell.ScrollViews.pagingEnabled = YES;
        Cell.ScrollViews.contentSize = CGSizeMake(Cell.ScrollViews.frame.size.width + 90, Cell.ScrollViews.frame.size.height);
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"kpi"]))){
            Cell.KPILabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"kpi"]];
        }
        else{
            Cell.KPILabel.text  = @"";
        }
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"feedback_as_stackholder"]))){
            Cell.FeedbackAsStackLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"feedback_as_stackholder"]];
        }
        else{
            Cell.FeedbackAsStackLabel.text  = @"";
        }
        if(IsSafeStringPlus(TrToString([MyTaskDic valueForKey:@"myfeedback"]))){
            Cell.FeedbackLabel.text = [NSString stringWithFormat:@"%@",[MyTaskDic valueForKey:@"myfeedback"]];
        }
        else{
            Cell.FeedbackLabel.text  = @"";
        }
        return Cell;
    }
}
-(IBAction)SubmitTaskAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    CreateTaskScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateTaskScreen"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)SubmitProgressAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyTaskScreenParentViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskScreenParentViewController"];
    viewController.isComeFrom = @"PROGRESS";
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)GiveFeedBackAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbackViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbackViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)GotoTaskAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NewTaskScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewTaskScreen"];
    viewController.taskType = 1;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)GotoAssignmentAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NewTaskScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewTaskScreen"];
    viewController.taskType = 2;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)ViewAllTasksAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyTaskScreenParentViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MyTaskScreenParentViewController"];
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)CompleteAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskStatusScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskStatusScreen"];
    viewController.taskType = 3;
    viewController.isManager = NO;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)OnSchuduleAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskStatusScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskStatusScreen"];
    viewController.taskType = 1;
    viewController.isManager = NO;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)BhindAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    TaskStatusScreen *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TaskStatusScreen"];
    viewController.taskType = 2;
    viewController.isManager = NO;
    [[self navigationController] pushViewController:viewController animated: YES];
}
-(IBAction)FilterAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    GiveFeedbacFilterViewController *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"GiveFeedbacFilterViewController"];
    ObjController.isComeFrom =@"MYTASK";
    [[self navigationController] presentViewController:ObjController animated:YES completion:nil];
}
-(IBAction)KPIAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    MyKPIScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyKPIScreen"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)FeedbackAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    FeedbackScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackScreen"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)FeedbackAsStackHoderAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    FeedbackStackHolderViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackStackHolderViewController"];
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:1];
    MyKpiCell *Cell = [EmployeeTableView cellForRowAtIndexPath:indexPath];
    if([scrollView isEqual:Cell.ScrollViews]){
        Cell.ScrollViews.contentSize = CGSizeMake(Cell.ScrollViews.frame.size.width + 90, Cell.ScrollViews.frame.size.height);
    }
}
-(IBAction)GotoProfile:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    ObjViewController.isComeFrom = @"PMS";
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
- (IBAction)notificationTapped:(id)sender {
    [delegate DeleteNotificationCounter:@"IPM"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NotificationScreen *ObjController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationScreen"];
    [[self navigationController] pushViewController:ObjController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
- (IBAction)HomeAction:(id)sender {
     [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)SearchAction:(id)sender{
    CustomSearchController *viewController= [[CustomSearchController alloc] init];
    [[self navigationController] pushViewController:viewController animated: YES];
}

@end
