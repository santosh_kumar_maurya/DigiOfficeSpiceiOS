#import "HistoryDetailScreen.h"
#import  "Header.h"

const UIEdgeInsets textInsetsMine2 = {5, 10, 11, 17};
const UIEdgeInsets textInsetsSomeone2 = {5, 15, 11, 10};

@interface HistoryDetailScreen ()
{
    UIRefreshControl *refreshControl;
    UIButton *screenView;
    WebApiService *Api;
    NSDictionary *ResponseDic;
    NSDictionary *params;
    NSArray* subTaskData;
    NSMutableArray *stakeHolderData;
}
@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation HistoryDetailScreen
@synthesize isShowDetail,count,taskID,taskType,dataDict, isMe, isManager;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    Api = [[WebApiService alloc] init];
    self.view.backgroundColor = delegate.BackgroudColor;
    self.navigationController.navigationBarHidden=TRUE;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    isServerResponded = FALSE;
    isShowDetail= TRUE;
    isMe = TRUE;
    [self ScreenDesign];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(HistoryWebApi) withObject:nil afterDelay:0.5];
}
- (void) ScreenDesign{
    
    mainScreenView=[[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    mainScreenView.backgroundColor=[ColorCategory PurperColor];
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor = [ColorCategory PurperColor];
    [mainScreenView addSubview:headerview];
    
    UIButton *backButton=[[UIButton alloc] initWithFrame:CGRectMake(5, 35, 30, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackBtn"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    backButton.tag=20;
    CGPoint center = backButton.center;
    center.y = headerview.frame.size.height / 2 +3;
    [backButton setCenter:center];
    [headerview addSubview:backButton];
    
    UIButton* HomeButton=[UIButton buttonWithType:UIButtonTypeCustom];
    HomeButton.frame=CGRectMake(delegate.devicewidth-35, 10, 18, 18);
    HomeButton.tag=20;
    center = HomeButton.center;
    center.y = headerview.frame.size.height / 2 + 3;
    [HomeButton setCenter:center];
    [HomeButton addTarget:self action:@selector(GotobackContainer) forControlEvents:UIControlEventTouchUpInside];
    [HomeButton setImage:[UIImage imageNamed:@"HomeImage"]  forState:UIControlStateNormal];
    [headerview addSubview:HomeButton];
    
    UILabel *headerlab=[[UILabel alloc] initWithFrame:CGRectMake(45, backButton.frame.origin.y+12, 200, 20)];
    headerlab.textColor=delegate.yellowColor;
    headerlab.font=delegate.headFont;
    headerlab.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),taskID];
    headerlab.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab];
    
    UIButton *flatButton=[[UIButton alloc] initWithFrame:CGRectMake(0, delegate.headerheight, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton.backgroundColor=[UIColor whiteColor];
    flatButton_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton_lab.text = NSLocalizedString(@"DETAILS_CAPS", nil);
    flatButton_lab.textAlignment=ALIGN_CENTER;
    flatButton_lab.font = delegate.normalBold;
    flatButton_lab.backgroundColor = [ColorCategory PurperColor];
    flatButton_lab.textColor = [UIColor whiteColor];
    [flatButton addSubview:flatButton_lab];
    [flatButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton.tag=300;
    
    [mainScreenView addSubview:flatButton];
    
    UIButton *flatButton1=[[UIButton alloc] initWithFrame:CGRectMake(mainScreenView.frame.size.width/2,delegate.headerheight,mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton1.backgroundColor=[UIColor whiteColor];
    flatButton1_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainScreenView.frame.size.width/2, delegate.tabheight)];
    flatButton1_lab.text = NSLocalizedString(@"COMMENTS_CAPS", nil);
    flatButton1_lab.textAlignment = ALIGN_CENTER;
    flatButton1_lab.font = delegate.normalBold;
    flatButton1_lab.textColor = [UIColor whiteColor];
    flatButton1_lab.backgroundColor = [ColorCategory PurperColor];
    [flatButton1 addSubview:flatButton1_lab];
    [flatButton1 addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    flatButton1.tag=301;
    [mainScreenView addSubview:flatButton1];
    
    sel_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight-2, mainScreenView.frame.size.width/2, 2)];
    sel_lab.backgroundColor = [UIColor whiteColor];
    [mainScreenView addSubview:sel_lab];
    
    tab=[[UITableView alloc] initWithFrame:CGRectMake(0, delegate.headerheight+delegate.tabheight, mainScreenView.frame.size.width,mainScreenView.frame.size.height)];
    
    tab.delegate=self;
    tab.dataSource=self;
    tab.separatorStyle=UITableViewCellSeparatorStyleNone;
    tab.backgroundColor= [UIColor clearColor];
    [mainScreenView addSubview:tab];
    [self.view addSubview:mainScreenView];
    [self AddRefreshControl];
    
}
-(void)HistoryWebApi{
    if(delegate.isInternetConnected){
        ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getTaskDetail?taskId=%@&userId=%@",taskID,[ApplicationState userId]]];
        NSLog(@"ResponseDic=%@",ResponseDic);
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            isServerResponded = TRUE;
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                dataArray = [[ResponseDic valueForKey:@"object"] valueForKey:@"teamTaskDetail"];
                subTaskData = [[ResponseDic valueForKey:@"object"] valueForKey:@"subtask"];
                stakeHolderData = [[ResponseDic valueForKey:@"object"] valueForKey:@"stakeholderData"];
            }
            else{
                [LoadingManager hideLoadingView:self.view];
               [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            }
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
        [tab reloadData];
    }
    else{
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }

}
-(void)AddRefreshControl{
    [refreshControl removeFromSuperview];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
}
- (void)reloadData{
    [self HistoryWebApi];
    [self getConversation];
}
-(void)screenTaskDetail:(UITableViewCell *)cell
{
    int rightPad = delegate.devicewidth-delegate.margin*2;
    UIButton *mainView=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    mainView.backgroundColor = delegate.BackgroudColor;
    [cell addSubview:mainView];
    
    int ypos = 15;
    UILabel *createdOnLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/3, 15)];
    createdOnLabel.text = NSLocalizedString(@"CREATED_ON", nil);
    createdOnLabel.font = delegate.contentSmallFont;
    createdOnLabel.textColor = [UIColor blackColor];
    createdOnLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOnLabel];
    
    UILabel *createdOn =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos+17, delegate.devicewidth/3, 15)];
   
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"creationDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"creationDate"];
        createdOn.text = [self DateFormateChange:datenumber];
    }
    else{
        createdOn.text = @"";
    }
    createdOn.font = delegate.contentSmallFont;
    createdOn.textColor = delegate.dimBlack;
    createdOn.textAlignment = ALIGN_LEFT;
    [mainView addSubview:createdOn];
    
    createdOnLabel.frame = CGRectMake(10, ypos, createdOn.intrinsicContentSize.width, 15);
    
    int xpos = delegate.devicewidth/2-((delegate.devicewidth/3)/2);
    UILabel *startDateLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, 170, 15)];
    startDateLabel.text = NSLocalizedString(@"ACTUAL_START_DATE", nil);
    startDateLabel.textColor = [UIColor blackColor];
    startDateLabel.font = delegate.contentSmallFont;
    startDateLabel.textAlignment = ALIGN_LEFT;
    [mainView addSubview:startDateLabel];
    
    UILabel *startDate =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, delegate.devicewidth/3, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"actualStartDate"];
        startDate.text = [self DateFormateChange:datenumber];
    }
    else{
        startDate.text = @"";
    }
    
    startDate.font = delegate.contentSmallFont;
    startDate.textAlignment = ALIGN_LEFT;
    startDate.textColor = delegate.dimBlack;
    [mainView addSubview:startDate];
    
  //  xpos = delegate.devicewidth/2 - (startDate.intrinsicContentSize.width/2);
    startDateLabel.frame = CGRectMake(xpos, ypos, 90, 15);
    startDate.frame = CGRectMake(xpos, ypos+17, startDate.intrinsicContentSize.width, 15);
    
    xpos = delegate.devicewidth-(delegate.devicewidth/3);
    UILabel *dayLeftLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/3, 15)];
    dayLeftLabel.text =NSLocalizedString(@"COMPLETION_DATE", nil);
    dayLeftLabel.textColor = [UIColor blackColor];
    dayLeftLabel.font = delegate.contentSmallFont;
    dayLeftLabel.textAlignment = ALIGN_LEFT;
    dayLeftLabel.textColor = [UIColor blackColor];
    [mainView addSubview:dayLeftLabel];
    
    xpos = delegate.devicewidth - dayLeftLabel.intrinsicContentSize.width - 10;
    dayLeftLabel.frame = CGRectMake(xpos, ypos, dayLeftLabel.intrinsicContentSize.width, 15);
    
    UILabel *dayLeft =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos+17, dayLeftLabel.intrinsicContentSize.width, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"completionDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"completionDate"];
        dayLeft.text = [self DateFormateChange:datenumber];
    }
    else{
        dayLeft.text = @"";
    }
    dayLeft.font = delegate.contentSmallFont;
    dayLeft.textAlignment = ALIGN_LEFT;
    dayLeft.textColor = delegate.dimBlack;
    [mainView addSubview:dayLeft];

    int saveYPos = ypos = ypos + 30 + 15;
    screenView=[[UIButton alloc] initWithFrame:CGRectMake(delegate.margin, ypos, delegate.devicewidth-delegate.margin*2, delegate.deviceheight)];
    screenView.backgroundColor=[UIColor whiteColor];
    screenView.layer.cornerRadius = 5.0;
    screenView.layer.shadowOpacity = 0.3;
    screenView.layer.shadowRadius  = 2;
    [mainView addSubview:screenView];
    cell.backgroundColor=[UIColor clearColor];
    
    ypos = 15;
    
    NSString* statusType = @"Approved";
    NSString* imageType = @"confirmed";
    NSString* msgType = @"Submitted On: ";
    NSString* msgTypeValue = @"Submitted On";
    UIColor *colorType = delegate.confColor;
    if ([[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Cancelled"]){
        statusType = @"Cancelled";
        imageType = @"cancelled_new";
        msgType = @"Cancelled On: ";
       
        if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"suspendDate"]))){
            NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"suspendDate"];
            msgTypeValue = [self DateFormateChange:datenumber];
        }
        else{
            msgTypeValue = @"";
        }
       
        colorType = delegate.redColor;
        
        UILabel *status1 =
        [[UILabel alloc] initWithFrame:CGRectMake(rightPad/2, ypos, rightPad/2-delegate.margin, 20)];
        status1.font = delegate.headFont;
        status1.textAlignment = ALIGN_CENTER;
        status1.text = statusType;
        status1.textColor = colorType;
        [status1 sizeToFit];
        CGPoint center = status1.center;
        center.x = screenView.frame.size.width / 2 + 20;
        [status1 setCenter:center];
        [screenView addSubview:status1];
        
        UIImageView *statusLogo;
        statusLogo=[[UIImageView alloc] initWithFrame:CGRectMake(rightPad/2, ypos, 20, 20)];
        statusLogo.image=[UIImage imageNamed:imageType];
        center = statusLogo.center;
        center.x = screenView.frame.size.width / 2 - 40;
        [statusLogo setCenter:center];
        [screenView addSubview:statusLogo];
        
        ypos = ypos + statusLogo.frame.size.height + 15;
        UIView *lineView0 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
        lineView0.backgroundColor = delegate.borderColor;
        [screenView addSubview:lineView0];
        ypos = ypos + 15;
    }
    else if ([[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Discarded"]){
        statusType = @"Discarded";
        imageType = @"discarded_new";
        colorType = delegate.discardedColor;
    }
    else if ([[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Declined"]){
        statusType = @"Declined";
        imageType = @"declined_new";
        colorType = delegate.failedColor;
    }
    else if ([[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Approved"]){
    }
    else if ([[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Submitted"]){
        statusType = @"Submitted";
        imageType = @"submitted_new";
        msgType = @"Submitted On: ";
        
        if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"completeDate"]))){
            NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"completeDate"];
            msgTypeValue = [self DateFormateChange:datenumber];
        }
        else{
            msgTypeValue = @"";
        }
        
        colorType = delegate.submittedColor;
        
        UILabel *status1 =
        [[UILabel alloc] initWithFrame:CGRectMake(rightPad/2, ypos, rightPad/2-delegate.margin, 20)];
        status1.font = delegate.headFont;
        status1.textAlignment = ALIGN_CENTER;
        status1.text = statusType;
        status1.textColor = colorType;
        [status1 sizeToFit];
        CGPoint center = status1.center;
        center.x = screenView.frame.size.width / 2 + 20;
        [status1 setCenter:center];
        [screenView addSubview:status1];
        
        UIImageView *statusLogo;
        statusLogo=[[UIImageView alloc] initWithFrame:CGRectMake(rightPad/2, ypos, 20, 20)];
        statusLogo.image=[UIImage imageNamed:imageType];
        center = statusLogo.center;
        center.x = screenView.frame.size.width / 2 - 40;
        [statusLogo setCenter:center];
        [screenView addSubview:statusLogo];
        
        ypos = ypos + statusLogo.frame.size.height + 15;
        UIView *lineView0 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
        lineView0.backgroundColor = delegate.borderColor;
        [screenView addSubview:lineView0];
        ypos = ypos + 15;
        
    }
    else if ([[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]){
        statusType = @"Declined";
        imageType = @"Declined_new";
        if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"completeDate"]))){
            NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"completeDate"];
            msgTypeValue = [self DateFormateChange:datenumber];
        }
        else{
            msgTypeValue = @"";
        }
        colorType = delegate.rejectedColor;
    }
    else{
        if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"completeDate"]))){
            NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"completeDate"];
            msgTypeValue = [self DateFormateChange:datenumber];
        }
        else{
            msgTypeValue = @"";
        }
    }
    
    //ypos = 15;
    if (![[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"] &&
        ![[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Approved"]){
//        UILabel *status1 =
//        [[UILabel alloc] initWithFrame:CGRectMake(rightPad/2, ypos+2, rightPad/2-delegate.margin, 20)];
//        status1.font = delegate.headFont;
//        status1.textAlignment = ALIGN_CENTER;
//        status1.text = statusType;
//        status1.textColor = colorType;
//        [status1 sizeToFit];
//        CGPoint center = status1.center;
//        center.x = screenView.frame.size.width / 2 + 20;
//        [status1 setCenter:center];
//        [screenView addSubview:status1];
//        
//        UIImageView *statusLogo;
//        statusLogo=[[UIImageView alloc] initWithFrame:CGRectMake(rightPad/2, ypos, 20, 20)];
//        statusLogo.image=[UIImage imageNamed:imageType];
//        center = statusLogo.center;
//        center.x = screenView.frame.size.width / 2 - 40;
//        [statusLogo setCenter:center];
//        [screenView addSubview:statusLogo];
//        
//        ypos = ypos + statusLogo.frame.size.height + 15;
//        UIView *lineView0 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
//        lineView0.backgroundColor = delegate.borderColor;
//        [screenView addSubview:lineView0];
//        ypos = ypos + 15;
    }
    
    UILabel *taskNameLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, rightPad-delegate.margin*2, 15)];
    taskNameLabel.text = NSLocalizedString(@"TASK_NAME_WITH_COLON", nil);
    taskNameLabel.font = delegate.contentFont;
    taskNameLabel.textColor=[UIColor blackColor];
    taskNameLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:taskNameLabel];
    
    NSString *taskInfo = [[dataArray objectAtIndex:0] objectForKey:@"taskName"];
    if (taskInfo.length > 0){
        taskInfo = [NSString stringWithFormat:@"%@%@",[[taskInfo substringToIndex:1] uppercaseString],[taskInfo substringFromIndex:1] ];
    }
    
    xpos = taskNameLabel.intrinsicContentSize.width + 10;
    UILabel *taskName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 50)];
    taskName.text = taskInfo;
    taskName.font = delegate.contentFont;
    taskName.textColor=delegate.dimBlack;
    taskName.textAlignment = ALIGN_LEFT;
    taskName.numberOfLines = 0;
    [taskName sizeToFit];
    [screenView addSubview:taskName];
    
    ypos = ypos + taskName.frame.size.height + 15;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView];
    
    ypos = ypos + 15;
    UILabel *StartDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    StartDateStaticLabel.text = NSLocalizedString(@"START_DATE_WITH_COLON", nil);
    StartDateStaticLabel.font = delegate.contentFont;
    StartDateStaticLabel.textColor = [UIColor blackColor];
    StartDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateStaticLabel];
    
    xpos = StartDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *StartDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    
    
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"startDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"startDate"];
        StartDateValueLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        StartDateValueLabel.text = @"";
    }
    StartDateValueLabel.font = delegate.contentFont;
    StartDateValueLabel.textColor = delegate.dimBlack;
    StartDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:StartDateValueLabel];
    
    ypos = ypos + StartDateStaticLabel.frame.size.height + 15;
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView1.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView1];
    
    ypos = ypos + 15;
    UILabel *EndDateStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    EndDateStaticLabel.text = NSLocalizedString(@"END_DATE_WITH_COLON", nil);
    EndDateStaticLabel.font = delegate.contentFont;
    EndDateStaticLabel.textColor = [UIColor blackColor];
    EndDateStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateStaticLabel];
    
    xpos = EndDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *EndDateValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    if(IsSafeStringPlus(TrToString([[dataArray objectAtIndex:0] objectForKey:@"endDate"]))){
        NSNumber *datenumber = [[dataArray objectAtIndex:0] objectForKey:@"endDate"];
        EndDateValueLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        EndDateValueLabel.text = @"";
    }
    EndDateValueLabel.font = delegate.contentFont;
    EndDateValueLabel.textColor = delegate.dimBlack;
    EndDateValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:EndDateValueLabel];
    
    ypos = ypos + EndDateValueLabel.frame.size.height + 15;
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView2.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView2];
    
    ypos = ypos + 15;
    UILabel *DurationStaticLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    DurationStaticLabel.text = NSLocalizedString(@"DURATION_WITH_COLON", nil);
    DurationStaticLabel.font = delegate.contentFont;
    DurationStaticLabel.textColor = [UIColor blackColor];
    DurationStaticLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:DurationStaticLabel];
    
    xpos = EndDateStaticLabel.intrinsicContentSize.width + 10;
    UILabel *DurationValueLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, delegate.devicewidth/2, 15)];
    DurationValueLabel.text = [[dataArray objectAtIndex:0] objectForKey:@"duration"];
    DurationValueLabel.font = delegate.contentFont;
    DurationValueLabel.textColor = delegate.dimBlack;
    DurationValueLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:DurationValueLabel];
    ypos = ypos + DurationValueLabel.frame.size.height + 15;
    
    UIView *lineView3 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView3.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView3];
    
    ypos = ypos + 15;
    UILabel *createdByLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
    createdByLabel.text =  NSLocalizedString(@"TYPE_WITH_COLON", nil);
    createdByLabel.font = delegate.contentFont;
    createdByLabel.textColor = [UIColor blackColor];
    createdByLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdByLabel];
    createdByLabel.frame = CGRectMake(10, ypos, createdByLabel.intrinsicContentSize.width, 15);
    
    UILabel *createdBy =
    [[UILabel alloc] initWithFrame:CGRectMake(10+createdByLabel.intrinsicContentSize.width, ypos, rightPad, 15)];
    createdBy.text = [[dataArray objectAtIndex:0] objectForKey:@"type"];
    createdBy.font = delegate.contentFont;
    createdBy.textColor = delegate.dimBlack;
    createdBy.textAlignment = ALIGN_LEFT;
    [screenView addSubview:createdBy];
    
    ypos = ypos + createdBy.frame.size.height + 15;
    UIView *lineView33 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView33.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView33];
    
//    ypos = ypos + 15;
//    UILabel *taskDurationLabel =
//    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
//    taskDurationLabel.text = msgType;
//    taskDurationLabel.textColor = [UIColor blackColor];
//    taskDurationLabel.font = delegate.contentFont;
//    taskDurationLabel.textAlignment = ALIGN_LEFT;
//   [screenView addSubview:taskDurationLabel];
//    
//    taskDurationLabel.frame = CGRectMake(10, ypos, taskDurationLabel.intrinsicContentSize.width, 15);
//    
//    UILabel *taskDuration =
//    [[UILabel alloc] initWithFrame:CGRectMake(10+taskDurationLabel.intrinsicContentSize.width, ypos, rightPad, 15)];
//    taskDuration.text = msgTypeValue;
//    taskDuration.font = delegate.contentFont;
//    taskDuration.textAlignment = ALIGN_LEFT;
//    taskDuration.textColor = delegate.dimBlack;
//    [screenView addSubview:taskDuration];
//    
//    ypos = ypos + taskDuration.frame.size.height + 15;
//    UIView *lineView11 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
//    lineView11.backgroundColor = delegate.borderColor;
//    [screenView addSubview:lineView11];
    
    ypos = ypos + 15;
    UILabel *kpiLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
    kpiLabel.text = NSLocalizedString(@"KPI_WITH_COLON", nil);
    kpiLabel.textColor = [UIColor blackColor];
    kpiLabel.font = delegate.contentFont;
    createdBy.numberOfLines = 0;
    kpiLabel.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiLabel];
    
    xpos = kpiLabel.intrinsicContentSize.width + 10;
    UILabel *kpiName =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-30, 50)];
    kpiName.text = [NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:0]  objectForKey:@"kpi"]];
    kpiName.textColor = delegate.dimBlack;
    kpiName.font = delegate.contentFont;
    kpiName.numberOfLines = 0;
    [kpiName sizeToFit];
    kpiName.textAlignment = ALIGN_LEFT;
    [screenView addSubview:kpiName];
    
    if (kpiName.text.length > 0)
        ypos = ypos + kpiName.frame.size.height + 15;
    else
        ypos = ypos + kpiLabel.frame.size.height + 15;
    
    int subTaskHeight = 0;
//    NSArray* subTaskData = [[dataArray objectAtIndex:0] objectForKey:@"subTask"];
    if (((NSNull*)subTaskData != [NSNull null]) && subTaskData.count > 0 && [subTaskData[0] length] > 0){
        for (int i=0; i < subTaskData.count; i++){
            UIView *lineView4 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView4.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView4];
            
            ypos = ypos + 15;
            UILabel *subtaskLabel =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            subtaskLabel.text = NSLocalizedString(@"SUBTASK_WITH_COLON", nil);
            subtaskLabel.font = delegate.contentFont;
            subtaskLabel.textColor = [UIColor blackColor];
            subtaskLabel.textAlignment = ALIGN_LEFT;
            [screenView addSubview:subtaskLabel];
            
            xpos = subtaskLabel.intrinsicContentSize.width + 10;
            UILabel *subtask =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 500)];
            subtask.text = subTaskData[i];
            subtask.font = delegate.contentFont;
            subtask.textColor = delegate.dimBlack;
            subtask.numberOfLines=0;
            subtask.textAlignment = ALIGN_LEFT;
            [subtask sizeToFit];
            [screenView addSubview:subtask];
            subTaskHeight = subTaskHeight + subtask.frame.size.width;
            ypos = ypos + subtask.frame.size.height + 15;
        }
    }
   UIView *lineView5 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
    lineView5.backgroundColor = delegate.borderColor;
    [screenView addSubview:lineView5];
    
    ypos = ypos + 15;
    UILabel *taskDetailLabel =
    [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth-delegate.margin*2-5, 15)];
    taskDetailLabel.text = NSLocalizedString(@"TASK_DETAILS_WITH_COLON", nil);
    taskDetailLabel.font = delegate.contentFont;
    taskDetailLabel.textColor = [UIColor blackColor];
    taskDetailLabel.textAlignment = ALIGN_LEFT;
    taskDetailLabel.numberOfLines = 0;
    [taskDetailLabel sizeToFit];
    [screenView addSubview:taskDetailLabel];
    
    xpos = taskDetailLabel.intrinsicContentSize.width + 10;
    UILabel *taskDetail =
    [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
    taskDetail.text = [[dataArray objectAtIndex:0] objectForKey:@"taskDetail"];
    taskDetail.font = delegate.contentFont;
    taskDetail.textColor = delegate.dimBlack;;
    taskDetail.textAlignment = ALIGN_LEFT;
    taskDetail.numberOfLines = 0;
    [taskDetail sizeToFit];
    [screenView addSubview:taskDetail];
    
    if (taskDetail.text.length > 0)
        ypos = ypos + taskDetail.frame.size.height + 15;
    else
        ypos = ypos + taskDetailLabel.frame.size.height + 15;

    if ([[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Approved"] ||
        [[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Rejected"]){
        
        UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
        lineView2.backgroundColor = delegate.borderColor;
        [screenView addSubview:lineView2];

//        ypos = ypos + 15;
//        UILabel *endDateLabel =
//        [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth/2, 15)];
//        endDateLabel.text =NSLocalizedString(@"END_DATE_WITH_COLON", nil);
//        endDateLabel.font = delegate.contentFont;
//        endDateLabel.textColor = [UIColor blackColor];
//        endDateLabel.textAlignment = ALIGN_LEFT;
//        [screenView addSubview:endDateLabel];
//        
//        UILabel *startTime =
//        [[UILabel alloc] initWithFrame:CGRectMake(10+endDateLabel.intrinsicContentSize.width, ypos, delegate.devicewidth/2, 15)];
//        startTime.text = [NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:0] objectForKey:@"completeEndDate"]];
//        startTime.font = delegate.contentFont;
//        startTime.textColor = delegate.dimBlack;
//        startTime.textAlignment = ALIGN_LEFT;
//        [screenView addSubview:startTime];
//        
//        ypos = ypos + endDateLabel.frame.size.height + 15;
//        UIView *lineView13 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
//        lineView13.backgroundColor = delegate.borderColor;
//        [screenView addSubview:lineView13];
        
        ypos = ypos + 15;
        UILabel *feedbackLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth-delegate.margin*2-5, 45)];
        feedbackLabel.font = delegate.contentFont;
        feedbackLabel.textColor = [UIColor blackColor];
        feedbackLabel.text =  [NSString stringWithFormat:NSLocalizedString(@"FEEDBACK_WITH_COLON", nil)];
        feedbackLabel.textAlignment = ALIGN_LEFT;
        feedbackLabel.numberOfLines = 0;
        [feedbackLabel sizeToFit];
        [screenView addSubview:feedbackLabel];
        
        xpos = feedbackLabel.intrinsicContentSize.width + 10;
        UILabel *feedback =
        [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 45)];
       
        NSString* feedbackData = [[dataArray objectAtIndex:0] objectForKey:@"feedback"];
        if (((NSNull*)feedbackData != [NSNull null])){
            feedback.text = [NSString stringWithFormat:@"%@", feedbackData];
        }
        feedback.font = delegate.contentFont;
        feedback.textColor = delegate.dimBlack;
        feedback.textAlignment = ALIGN_LEFT;
        feedback.numberOfLines = 0;
        [feedback sizeToFit];
        [screenView addSubview:feedback];
        
        if (feedback.text.length > 0)
            ypos = ypos + feedback.frame.size.height + 15;
        else
            ypos = ypos + feedbackLabel.frame.size.height + 15;
    
        UIView *lineView3 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
        lineView3.backgroundColor = delegate.borderColor;
        [screenView addSubview:lineView3];
        
        ypos = ypos + 15;
        RateView* rv = [RateView rateViewWithRating:3.7f];
        rv.frame = CGRectMake(10, ypos, 100, 15);
        rv.tag = 88888;
        rv.starSize = 15.0;
        rv.rating = [[[dataArray objectAtIndex:0] objectForKey:@"rating"] floatValue];
        CGPoint center = rv.center;
        center.x = (delegate.devicewidth - delegate.margin*2)/2;
        [rv setCenter:center];
        rv.starFillColor = delegate.redColor;
        [screenView addSubview:rv];
        ypos = ypos + rv.frame.size.height + 15;
    }
    
    if ([[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Cancelled"] ||
        [[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Submitted"]){
        
        UIView *lineView13 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
        lineView13.backgroundColor = delegate.borderColor;
        [screenView addSubview:lineView13];
        
        ypos = ypos + 15;
        UILabel *feedbackLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth-delegate.margin*2-5, 45)];
        feedbackLabel.font = delegate.contentFont;
        feedbackLabel.textColor = [UIColor blackColor];
        feedbackLabel.text =NSLocalizedString(@"COMMINT_WITH_COLON", nil);
        feedbackLabel.textAlignment = ALIGN_LEFT;
        feedbackLabel.numberOfLines = 0;
        [feedbackLabel sizeToFit];
        [screenView addSubview:feedbackLabel];
        
        xpos = feedbackLabel.intrinsicContentSize.width + 10;
        UILabel *feedback =
        [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 45)];
        NSString* feedbackData = @"";
        if ([[[dataArray objectAtIndex:0] objectForKey:@"taskStatus"] isEqualToString:@"Cancelled"])
            feedbackData = [[dataArray objectAtIndex:0] objectForKey:@"feedback"];
        else
            feedbackData = [[dataArray objectAtIndex:0] objectForKey:@"employeeComment"];
        if (((NSNull*)feedbackData != [NSNull null])){
            feedback.text = [NSString stringWithFormat:@"%@", feedbackData];
        }
        feedback.font = delegate.contentFont;
        feedback.textColor = delegate.dimBlack;
        feedback.textAlignment = ALIGN_LEFT;
        feedback.numberOfLines = 0;
        [feedback sizeToFit];
        [screenView addSubview:feedback];
        if (feedback.text.length > 0)
            ypos = ypos + feedback.frame.size.height + 15;
        else
            ypos = ypos + feedbackLabel.frame.size.height + 15;
    }
    
//    NSMutableArray *stakeHolderData = [[dataArray objectAtIndex:0] objectForKey:@"stakeHolder"];
    if (((NSNull*)stakeHolderData != [NSNull null]) && stakeHolderData.count > 0){
        
        for (int i=0; i < stakeHolderData.count; i++){
            UIView *lineView4 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 15)];
            lineView4.backgroundColor = delegate.BackgroudColor;
            [screenView addSubview:lineView4];
            
            ypos = ypos + 30;
            UILabel *label =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            label.text = NSLocalizedString(@"STAKE_HOLDER_WITH_COLON", nil);
            label.font = delegate.contentFont;
            label.textColor = [UIColor blackColor];
            label.textAlignment = ALIGN_LEFT;
            [screenView addSubview:label];
            xpos = label.intrinsicContentSize.width + 10;
            UILabel *subtask =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
            
            if(IsSafeStringPlus(TrToString([[stakeHolderData objectAtIndex:i] objectForKey:@"stakeholderName"]))){
               subtask.text = [NSString stringWithFormat:@"%@",[[stakeHolderData objectAtIndex:i] objectForKey:@"stakeholderName"]];
            }
            else{
               subtask.text = @"";
            }
            
            subtask.font = delegate.contentFont;
            subtask.textColor = delegate.dimBlack;
            subtask.numberOfLines=0;
            subtask.textAlignment = ALIGN_LEFT;
            [subtask sizeToFit];
            [screenView addSubview:subtask];
            subTaskHeight = subTaskHeight + subtask.frame.size.width;
            ypos = ypos + label.frame.size.height + 15;
            
            UIView *lineView5 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView5.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView5];
            
            ypos = ypos + 15;
            UILabel *label2 =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            label2.text = NSLocalizedString(@"EFFECTIVE_COMPETENCY_WITH_COLON", nil);
            label2.font = delegate.contentFont;
            label2.textColor = [UIColor blackColor];
            label2.textAlignment = ALIGN_LEFT;
            [screenView addSubview:label2];
            xpos = label2.intrinsicContentSize.width + 10;
            UILabel *value1 =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
            if(IsSafeStringPlus(TrToString([[stakeHolderData objectAtIndex:i] objectForKey:@"effectiveCompetency"]))){
                value1.text = [NSString stringWithFormat:@"%@",[[stakeHolderData objectAtIndex:i] objectForKey:@"effectiveCompetency"]];
            }
            else{
                value1.text = @"";
            }
            
            value1.font = delegate.contentFont;
            value1.textColor = delegate.dimBlack;
            value1.numberOfLines=0;
            value1.textAlignment = ALIGN_LEFT;
            [value1 sizeToFit];
            [screenView addSubview:value1];
            subTaskHeight = subTaskHeight + value1.frame.size.width;
            ypos = ypos + value1.frame.size.height + 15;
            
            UIView *lineView0 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView0.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView0];
            
            ypos = ypos + 15;
            UILabel *label4 =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            label4.text = NSLocalizedString(@"LACKIMG_COMPETENCY_WITH_COLON", nil);
            label4.font = delegate.contentFont;
            label4.textColor = [UIColor blackColor];
            label4.textAlignment = ALIGN_LEFT;
            [screenView addSubview:label4];
            xpos = label4.intrinsicContentSize.width + 10;
            UILabel *value2 =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
            if(IsSafeStringPlus(TrToString([[stakeHolderData objectAtIndex:i] objectForKey:@"lackingCompetencey"]))){
                value2.text = [NSString stringWithFormat:@"%@",[[stakeHolderData objectAtIndex:i] objectForKey:@"lackingCompetencey"]];
            }
            else{
                value2.text = @"";
            }
            value2.font = delegate.contentFont;
            value2.textColor = delegate.dimBlack;
            value2.numberOfLines=0;
            value2.textAlignment = ALIGN_LEFT;
            [value2 sizeToFit];
            [screenView addSubview:value2];
            
            ypos = ypos + value2.frame.size.height + 15;
            UIView *lineView7 = [[UIView alloc] initWithFrame:CGRectMake(0, ypos,rightPad, 0.3)];
            lineView7.backgroundColor = delegate.borderColor;
            [screenView addSubview:lineView7];

            ypos = ypos +  15;
            UILabel *label1 =
            [[UILabel alloc] initWithFrame:CGRectMake(10, ypos, delegate.devicewidth, 15)];
            label1.text = NSLocalizedString(@"FEEDBACK_WITH_COLON", nil);
            label1.font = delegate.contentFont;
            label1.textColor = [UIColor blackColor];;
            label1.textAlignment = ALIGN_LEFT;
            [screenView addSubview:label1];
            xpos = label1.intrinsicContentSize.width + 10;
            UILabel *value =
            [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, rightPad-xpos, 100)];
            if(IsSafeStringPlus(TrToString([[stakeHolderData objectAtIndex:i] objectForKey:@"feedback"]))){
                value.text = [NSString stringWithFormat:@"%@",[[stakeHolderData objectAtIndex:i] objectForKey:@"feedback"]];
            }
            else{
                value.text = @"";
            }
            value.font = delegate.contentFont;
            value.textColor =  delegate.dimBlack;
            value.numberOfLines=0;
            value.textAlignment = ALIGN_LEFT;
            [value sizeToFit];
            [screenView addSubview:value];
            subTaskHeight = subTaskHeight + value.frame.size.width;
            subTaskHeight = subTaskHeight + value2.frame.size.width;
            ypos = ypos + label4.frame.size.height + 15;
        }
    }
    screenView.frame = CGRectMake(delegate.margin, saveYPos, delegate.devicewidth-delegate.margin*2, ypos);
}


-(IBAction)btnfun:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==20){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else if(btn.tag==300){
        isShowDetail = TRUE;
        tab.backgroundColor = delegate.BackgroudColor;
        sel_lab.frame=CGRectMake(0, delegate.headerheight+delegate.tabheight-2, delegate.devicewidth/2, 2);
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(HistoryWebApi) withObject:nil afterDelay:0.5];
    }
    else if(btn.tag==301){
        isShowDetail = FALSE;
        tab.backgroundColor = delegate.BackgroudColor;
        sel_lab.frame=CGRectMake(delegate.devicewidth/2, delegate.headerheight+delegate.tabheight-2,  delegate.devicewidth/2, 2);
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(getConversation) withObject:nil afterDelay:0.5];
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (FALSE == isServerResponded)
        return 0;
    [msgvw removeFromSuperview];
    if (isShowDetail){
        return 1;
    }
    else{
        if ([dataConv count] <= 0){
            [self noMessageScreen:NSLocalizedString(@"SORRY_NO_COMMENT", nil)];
        }
        else
            return [dataConv count];
    }
    return 0;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
    }

    if (TRUE == isShowDetail){
        [self screenTaskDetail:cell];
    }
    else{
        cell.backgroundColor = delegate.BackgroudColor;
        int indx=(int)indexPath.row;
        CGFloat y = 0;
        UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        CGSize size1 = CGSizeMake(230,9999);
        NSString *str = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
        CGRect textRect = [str boundingRectWithSize:size1 options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        
        CGSize size = textRect.size;
        if ([[[dataConv objectAtIndex:indx] objectForKey:@"userId"] isEqualToString:[ApplicationState userId]]){
            CGFloat x = delegate.devicewidth - size.width - textInsetsMine2.left - textInsetsMine2.right;
            
            UIImageView *profImg=[[UIImageView alloc] initWithFrame:CGRectMake(delegate.devicewidth-40, y+20,30, 30)];
            UIImage* imagePath = [[UIImage imageNamed:@"profile_image_default"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            profImg.image = imagePath;
            [cell addSubview:profImg];
            
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:indx] objectForKey:@"userImg"]))){
                UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]]]];
                if(images == nil){
                    NSLog(@"image nil");
                    profImg.image = [UIImage imageNamed:@"profile_image_default"];
                }
                else{
                    NSString *URL = [NSString stringWithFormat:@"%@",[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]];
                    [profImg sd_setImageWithURL:[NSURL URLWithString:URL]];
                }
            }
            else{
                profImg.image = [UIImage imageNamed:@"profile_image_default"];
            }
            profImg.layer.borderColor = [UIColor whiteColor].CGColor;
            profImg.layer.borderWidth = 1;
            profImg.layer.cornerRadius = 15;
            profImg.clipsToBounds = YES;
            
            
            UILabel *userImg=[[UILabel alloc] initWithFrame:CGRectMake(x-50, y+20,size.width+textInsetsMine2.left+textInsetsMine2.right, size.height+textInsetsMine2.bottom+textInsetsMine2.top)];
            
            userImg.layer.borderWidth = 2.0;
            userImg.layer.borderColor = delegate.yellowColor.CGColor;
            userImg.layer.cornerRadius = 8;
            userImg.layer.masksToBounds = true;
            [cell addSubview:userImg];
            
            UILabel *lastMsg=[[UILabel alloc] initWithFrame:CGRectMake(x-40, y+textInsetsMine2.top+22, size.width,size.height)];
            lastMsg.font=[UIFont systemFontOfSize:[UIFont systemFontSize]];
            lastMsg.textColor=delegate.blackcolor;
            lastMsg.lineBreakMode = NSLineBreakByWordWrapping;
            lastMsg.numberOfLines = 0;
            lastMsg.textAlignment = ALIGN_LEFT;
            lastMsg.text = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
            [cell addSubview:lastMsg];
            
//            UILabel *subTask_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, y,delegate.devicewidth, 20)];
//            subTask_lab.font=delegate.mediumFont;
//            subTask_lab.textColor=delegate.blackcolor;
//            subTask_lab.textAlignment = ALIGN_CENTER;
//            NSString *SubTask = [[dataConv objectAtIndex:indx] objectForKey:@"subtask"];
//            if([SubTask isEqual:[NSNull null]]){
//                subTask_lab.text = @"";
//            }
//            else{
//                if([SubTask isEqualToString:@"null"] ||[SubTask isEqualToString:@""]|| [SubTask isEqualToString:@"(null)"]){
//                    subTask_lab.text = @"";
//                }
//                else{
//                    subTask_lab.text = SubTask;
//                }
//            }
//
//            [cell addSubview:subTask_lab];
            
            CGFloat ypos = y+textInsetsMine2.top+22+size.height+10;
            UILabel *date_lab=[[UILabel alloc] initWithFrame:CGRectMake(delegate.devicewidth-110, ypos,120, 20)];
            date_lab.font=delegate.smallFont2;
            date_lab.textColor=delegate.dimBlack;
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:0] objectForKey:@"creationDate"]))){
                NSNumber *datenumber = [[dataConv objectAtIndex:0] objectForKey:@"creationDate"];
                date_lab.text = [self DateFormateChange:datenumber];
            }
            else{
                date_lab.text = @"";
            }
            [cell addSubview:date_lab];
            isMe = FALSE;
        }
        else{
            isMe = TRUE;
            CGFloat x = 5;
            UIImageView *profImg=[[UIImageView alloc] initWithFrame:CGRectMake(x, y+20,30, 30)];
            UIImage* imagePath = [[UIImage imageNamed:@"profile_image_default"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            profImg.image = imagePath;
            [cell addSubview:profImg];
            
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:indx] objectForKey:@"userImg"]))){
                UIImage *images = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]]]];
                if(images == nil){
                    NSLog(@"image nil");
                    profImg.image = [UIImage imageNamed:@"profile_image_default"];
                }
                else{
                    NSString *URL = [NSString stringWithFormat:@"%@",[[dataConv objectAtIndex:indx] objectForKey:@"userImg"]];
                    [profImg sd_setImageWithURL:[NSURL URLWithString:URL]];
                }
            }
            else{
                profImg.image = [UIImage imageNamed:@"profile_image_default"];
            }
            profImg.layer.borderColor = [UIColor whiteColor].CGColor;
            profImg.layer.borderWidth = 1;
            profImg.layer.cornerRadius = 15;
            profImg.clipsToBounds = YES;
            
            x = x + profImg.frame.size.width;
            UILabel *userImg=[[UILabel alloc] initWithFrame:CGRectMake(x+10, y+20,size.width+textInsetsSomeone2.left+textInsetsSomeone2.right, size.height+textInsetsSomeone2.bottom+textInsetsSomeone2.top)];
            
            userImg.layer.borderWidth = 2.0;
            userImg.layer.borderColor = delegate.redColor.CGColor;
            userImg.layer.cornerRadius = 8;
            userImg.layer.masksToBounds = true;
            [cell addSubview:userImg];
            
            UILabel *lastMsg=[[UILabel alloc] initWithFrame:CGRectMake(x+textInsetsSomeone2.left+2, y+textInsetsSomeone2.top+22, size.width,size.height)];
            lastMsg.font=[UIFont systemFontOfSize:[UIFont systemFontSize]];
            lastMsg.textColor=delegate.blackcolor;
            lastMsg.lineBreakMode = NSLineBreakByWordWrapping;
            lastMsg.numberOfLines = 0;
            lastMsg.textAlignment = ALIGN_LEFT;
            lastMsg.text = [[dataConv objectAtIndex:indx] objectForKey:@"comment"];
            [cell addSubview:lastMsg];
            
            CGFloat ypos = y+textInsetsSomeone2.top+22+size.height+10;
            UILabel *date_lab=[[UILabel alloc] initWithFrame:CGRectMake(10, ypos,200,20)];
            date_lab.font=delegate.smallFont2;
            date_lab.textColor=delegate.dimBlack;
            if(IsSafeStringPlus(TrToString([[dataConv objectAtIndex:0] objectForKey:@"creationDate"]))){
                NSNumber *datenumber = [[dataConv objectAtIndex:0] objectForKey:@"creationDate"];
                date_lab.text = [self DateFormateChange:datenumber];
            }
            else{
                date_lab.text = @"";
            }
            [cell addSubview:date_lab];
            
            x=10;
//            UILabel *subTask_lab=[[UILabel alloc] initWithFrame:CGRectMake(0, y,delegate.devicewidth, 20)];
//            subTask_lab.font=delegate.mediumFont;
//            subTask_lab.textColor=delegate.blackcolor;
//            subTask_lab.textAlignment = ALIGN_CENTER;
//           
//            NSString *SubTask = [[dataConv objectAtIndex:indx] objectForKey:@"subtask"];
//            if([SubTask isEqual:[NSNull null]]){
//                subTask_lab.text = @"";
//            }
//            else{
//                if([SubTask isEqualToString:@"null"] ||[SubTask isEqualToString:@""]|| [SubTask isEqualToString:@"(null)"]){
//                    subTask_lab.text = @"";
//                }
//                else{
//                    subTask_lab.text = SubTask;
//                }
//            }
//            [cell addSubview:subTask_lab];
        }
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (TRUE == isShowDetail){
        return screenView.frame.size.height + 180;
    }
    else{
        int indx=(int)indexPath.row;
        NSString * str = [[dataConv objectAtIndex:indx] objectForKey:@"employeeComment"];
        UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        CGSize size1 = CGSizeMake(230,9999);
        CGRect textRect = [str boundingRectWithSize:size1 options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        CGSize size = textRect.size;
        int temp = size.height+textInsetsMine2.bottom+textInsetsMine2.top;
        return temp + 40;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(delegate.margin, 5, delegate.devicewidth-delegate.margin, 50);
    myLabel.font = delegate.boldFont;
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    return headerView;
}
-(void)noMessageScreen:(NSString*)message{
    msgvw=[[UIView alloc] initWithFrame:tab.frame];
    msgvw.backgroundColor = delegate.BackgroudColor;
    tab.backgroundColor = delegate.BackgroudColor;
    UIImageView *nomsgimg;
    UILabel *tab_lab;
    if (delegate.deviceheight <= 460){
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-100)/2,
                                                               0, 100,100)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    else{
        nomsgimg=[[UIImageView alloc] initWithFrame:CGRectMake((msgvw.frame.size.width-100)/2,
                                                               20, 100,100)];
        tab_lab=[[UILabel alloc] initWithFrame:CGRectMake(0,100, delegate.devicewidth, 80)];
    }
    nomsgimg.image=[UIImage imageNamed:@"no_comment"];
    [msgvw addSubview:nomsgimg];
    tab_lab.text = message;
    tab_lab.font=delegate.ooredoo;
    tab_lab.textColor=delegate.dimBlack;
    tab_lab.lineBreakMode = NSLineBreakByWordWrapping;
    tab_lab.numberOfLines = 0;
    tab_lab.textAlignment=ALIGN_CENTER;
    [msgvw addSubview:tab_lab];
    [tab addSubview:msgvw];
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
-(void) getConversation{
    
    if([delegate isInternetConnected]){
        ResponseDic = [Api WebApi:nil Url:[NSString stringWithFormat:@"getComment?taskId=%@&userId=%@",taskID,[ApplicationState userId]]];
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"] intValue]==5){
            [LoadingManager hideLoadingView:self.view];
            if(IsSafeStringPlus(TrToString([ResponseDic valueForKey:@"object"]))){
                dataConv = [[ResponseDic valueForKey:@"object"] valueForKey:@"commentData"];
            }
            else{
                [self noMessageScreen:NSLocalizedString(@"NO_RECORD", nil)];
            }
            [tab reloadData];
        }
        else{
             [LoadingManager hideLoadingView:self.view];
           [self noMessageScreen:NSLocalizedString(@"NO_RECORD", nil)];
        }
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY"];
    return [dateformate stringFromDate:Newdate];
}
@end
