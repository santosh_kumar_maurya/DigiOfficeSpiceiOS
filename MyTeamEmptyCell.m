//
//  MyTeamEmptyCell.m
//  iWork
//
//  Created by Shailendra on 26/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "MyTeamEmptyCell.h"

@implementation MyTeamEmptyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    _FilterView.layer.cornerRadius = 6.0;
    _FilterView.maskView.layer.cornerRadius = 7.0f;
    _FilterView.layer.shadowRadius = 3.0f;
    _FilterView.layer.shadowColor = [UIColor blackColor].CGColor;
    _FilterView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _FilterView.layer.shadowOpacity = 0.7f;
    _FilterView.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
