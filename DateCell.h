//
//  DateCell.h
//  iWork
//
//  Created by Shailendra on 23/08/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UILabel *StartDateLabel;
@property(nonatomic,retain)IBOutlet UILabel *EndDateLabel;
@property(nonatomic,retain)IBOutlet UIButton *StartDateButton;
@property(nonatomic,retain)IBOutlet UIButton *EndDateButton;
@end
