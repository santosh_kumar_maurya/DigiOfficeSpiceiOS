//
//  TaskStatusCell.m
//  iWork
//
//  Created by Shailendra on 05/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "TaskStatusCell.h"

@implementation TaskStatusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    _MyTaskStatusView.layer.cornerRadius = 6.0;
    _MyTaskStatusView.maskView.layer.cornerRadius = 7.0f;
    _MyTaskStatusView.layer.shadowRadius = 3.0f;
    _MyTaskStatusView.layer.shadowColor = [UIColor blackColor].CGColor;
    _MyTaskStatusView.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
    _MyTaskStatusView.layer.shadowOpacity = 0.7f;
    _MyTaskStatusView.layer.masksToBounds = NO;
    
    self.MyTaskView.backgroundColor = [UIColor whiteColor];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: self.MyTaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.MyTaskView.layer.mask = maskLayer1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
