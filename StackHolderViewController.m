//
//  StackHolderViewController.m
//  iWork
//
//  Created by Shailendra on 25/05/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "StackHolderViewController.h"
#import "Header.h"
#import "ASJTag.h"
#import "ASJTagsView.h"

@interface StackHolderViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    IBOutlet UITableView *StackHolderTableView;
    NSMutableArray *StackHolderNameArray;
    NSMutableArray *StackHolderIDArray;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *DoneBtn;
    IBOutlet UIButton *NextOneBtn;
    IBOutlet UIButton *NextTwoBtn;
    IBOutlet UIButton *PreviousBtn;
    IBOutlet UIView *NextView;
    IBOutlet UIView *NextPreviousView;
    IBOutlet UIView *SearchBgView;
    IBOutlet UITextField *SearchTextField;
    IBOutlet UIScrollView *ScrollViews;
    IBOutlet NSLayoutConstraint *hight;
    NSString *users_idStr;
    NSMutableArray *SelectStackHolderIDArray;
    NSMutableArray *SelectStackHolderNameArray;
    IBOutlet ASJTagsView *tagsView;
    NSDictionary *ResponseDic;
    NSDictionary *params;
    WebApiService *Api;
    AppDelegate *delegate;
    NSString *NameStr;
    int Start;
    int Limit;
}
@end

@implementation StackHolderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NameStr = @"";
    Start = 0;
    Limit = 10;
    Api = [[WebApiService alloc] init];
    SelectStackHolderIDArray = [[NSMutableArray alloc] init];
    SelectStackHolderNameArray = [[NSMutableArray alloc] init];
     hight.constant = 0;
    delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    SearchBgView.layer.borderColor = [UIColor colorWithRed:230/255.0 green:229/255.0 blue:230/255.0 alpha:1].CGColor;
    SearchBgView.layer.borderWidth = 0.5;
    SearchBgView.layer.cornerRadius = 5;
    SearchBgView.clipsToBounds = YES;
    
    CancelBtn.layer.borderColor =[UIColor whiteColor].CGColor;
    CancelBtn.layer.borderWidth = 1.0;
    CancelBtn.layer.cornerRadius = 3;
    CancelBtn.clipsToBounds = YES;
    
    DoneBtn.layer.borderColor =[UIColor whiteColor].CGColor;
    DoneBtn.layer.borderWidth = 1.0;
    DoneBtn.layer.cornerRadius = 3;
    DoneBtn.clipsToBounds = YES;
    
    PreviousBtn.layer.borderColor =delegate.redColor.CGColor;
    PreviousBtn.layer.borderWidth = 1.0;
    PreviousBtn.layer.cornerRadius = 2;
    PreviousBtn.clipsToBounds = YES;
  
    NextOneBtn.layer.cornerRadius = 3;
    NextOneBtn.clipsToBounds = YES;
    
    NextTwoBtn.layer.cornerRadius = 3;
    NextTwoBtn.clipsToBounds = YES;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetStakHolderListApi) withObject:nil afterDelay:0.5];
    tagsView.tagColorTheme = TagColorThemeDefault;
    [self handleTagBlocks];
}
- (void)GetStakHolderListApi {
    
    if(delegate.isInternetConnected){
        params = @ {
            @"stakeholderId": @"",
            @"searchBy": NameStr,
            @"startFrom" : [NSNumber numberWithInt:Start],
            @"resultLimit" : [NSNumber numberWithInt:Limit],
            @"userId" : [ApplicationState userId]
        };
        ResponseDic = [Api WebApi:params Url:@"getStakeholderList"];
        if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            StackHolderNameArray = [[NSMutableArray alloc] init];
            StackHolderNameArray = [[[ResponseDic valueForKey:@"object"] valueForKey:@"stakeholder"] valueForKey:@"stakeholderName"];
            StackHolderIDArray = [[NSMutableArray alloc] init];
            StackHolderIDArray = [[[ResponseDic valueForKey:@"object"] valueForKey:@"stakeholder"] valueForKey:@"stakeholderId"];
            if(StackHolderNameArray.count == 10){
                NextView.hidden = NO;
            }
            else if(StackHolderNameArray.count > 10){
                NextView.hidden = NO;
            }
            else{
                NextView.hidden = YES;
                NextPreviousView.hidden = YES;
            }
            [StackHolderTableView reloadData];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
        [StackHolderTableView reloadData];
    }
    else{
         [LoadingManager hideLoadingView:self.view];
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [StackHolderNameArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StackHolderCell *Cell = (StackHolderCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    if(StackHolderNameArray.count>0){
        Cell.NameLabel.text = [StackHolderNameArray objectAtIndex:indexPath.row];
    }
    return Cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(SelectStackHolderIDArray.count<3){
        NSDictionary *userInfo = [ApplicationState getUserLoginData];
        NSString *StakeHolderName = [StackHolderNameArray objectAtIndex:indexPath.row];
        NSString *UserName = userInfo[@"name"];
        if([StakeHolderName isEqualToString:UserName]){
        }
        else{
            if(![SelectStackHolderIDArray containsObject:[StackHolderIDArray objectAtIndex:indexPath.row]]){
                [SelectStackHolderIDArray addObject:[StackHolderIDArray objectAtIndex:indexPath.row]];
                [SelectStackHolderNameArray addObject:[StackHolderNameArray objectAtIndex:indexPath.row]];
                hight.constant = 40;
                [tagsView addTag:[StackHolderNameArray objectAtIndex:indexPath.row]];
            }
        }
    }
    else{
        [self ShowAlert:NSLocalizedString(@"LIMIT_EXCEED", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
- (void)handleTagBlocks{
    [tagsView setDeleteBlock:^(NSString *tagText, NSInteger idx){
         [SelectStackHolderNameArray removeObjectAtIndex:idx];
         [SelectStackHolderIDArray removeObjectAtIndex:idx];
         [tagsView deleteTagAtIndex:idx];
         if(SelectStackHolderNameArray.count == 0){
             hight.constant = 0;
         }
     }];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NameStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(NameStr.length>2){
        [StackHolderTableView reloadData];
        [self GetStakHolderListApi];
    }
    else if([NameStr isEqualToString:@""]){
        Start = 0;
        Limit = 10;
        [StackHolderTableView reloadData];
        [self GetStakHolderListApi];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [SearchTextField resignFirstResponder];
    return YES;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)DoneBtnAction:(id)sender{
    if(SelectStackHolderIDArray.count>0){
        NSString *SelectedStackHolderID = [SelectStackHolderIDArray componentsJoinedByString:@","];
        NSString *SelectedStackHolderName = [SelectStackHolderNameArray componentsJoinedByString:@","];
        NSMutableDictionary *Dic = [[NSMutableDictionary alloc] init];
        [Dic setValue:SelectedStackHolderID forKey:@"StackHolderID"];
        [Dic setValue:SelectedStackHolderName forKey:@"StackHolderName"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StackHolder" object:nil userInfo:Dic];
      [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        [self ShowAlert:NSLocalizedString(@"LIMIT_EXCEED", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(IBAction)NextOneBtnAction:(id)sender{
    Start = Start+10;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetStakHolderListApi) withObject:nil afterDelay:0.5];
    NextView.hidden = YES;
    NextPreviousView.hidden = NO;
}
-(IBAction)NextTwoBtnAction:(id)sender{
    Start = Start+10;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetStakHolderListApi) withObject:nil afterDelay:0.5];
}
-(IBAction)PreviousBtnAction:(id)sender{
    Start = Start-10;
    if(Start == 0){
        NextView.hidden = NO;
        NextPreviousView.hidden = YES;
    }
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetStakHolderListApi) withObject:nil afterDelay:0.5];
}
-(IBAction)SyncBtnAction:(id)sender{
    Start = 0;
    Limit = 10;
    NameStr = @"";
    SearchTextField.text = @"";
    NextView.hidden = NO;
    NextPreviousView.hidden = YES;
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(GetStakHolderListApi) withObject:nil afterDelay:0.5];
}
-(IBAction)CancelBtnAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end
