
#import "TaskRequestScreen.h"
#import "Header.h"
@interface TaskRequestScreen ()<UITextViewDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,FCAlertViewDelegate>
{
    UINavigationBar *NavBar;
    UIRefreshControl *refreshControl;
    UIView *CoomentView;
    NSString *SelectBtn;
    UITextView *CommentTextView;
    
    IBOutlet UIView *OuterView;
    IBOutlet UIView *InterView;
    IBOutlet UIView *ButtonView;
    IBOutlet HCSStarRatingView *RatingView;
    IBOutlet UILabel *ApproveTaskStaticLabel;
    IBOutlet UILabel *RatingLabel;
    IBOutlet UIButton *SubmitBtn;
    IBOutlet UIButton *CloseBtn;
    IBOutlet UITextView *FeedTextView;
    float RateValue;
    NSDateFormatter *dateFormat;
    NSDictionary *params;
    NSDictionary *ResponseDic;
    NSString *Status;
    WebApiService *Api;
    int offset;
    int limit;
    NSString *CheckValue;
    IBOutlet UITableView *tab;
    IBOutlet UIButton *TaskCreationBtn;
    IBOutlet UIButton *TaskCompletionBtn;
    IBOutlet UILabel *TaskCreationLabel;
    IBOutlet UILabel *TaskCompletionLabel;
    IBOutlet UILabel *HeaderLabel;
    int tagValue;
    IBOutlet UILabel *Nodatalabel;
    IBOutlet UIButton *resetButton;
    NSString *SelectBtnString;

}
@end

#ifdef __IPHONE_6_0
# define ALIGN_LEFT NSTextAlignmentLeft
# define ALIGN_CENTER NSTextAlignmentCenter
# define ALIGN_RIGHT NSTextAlignmentRight
#else
# define ALIGN_LEFT UITextAlignmentLeft
# define ALIGN_CENTER UITextAlignmentCenter
# define ALIGN_RIGHT UITextAlignmentRight
#endif


@implementation TaskRequestScreen

@synthesize count,taskType,taskID,taskFeedback,taskRating,isNewReq,serverMsg,serverMsg1;
- (void)viewDidLoad{
    [super viewDidLoad];
    RateValue = 0.0;
    offset = 0;
    limit = 50;
    CheckValue =@"";
    Api = [[WebApiService alloc] init];
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    filterData = [[NSMutableDictionary alloc] init];
    tab.estimatedRowHeight = 50;
    tab.backgroundColor = delegate.BackgroudColor;
    tab.rowHeight = UITableViewAutomaticDimension;
    tab.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tab.bounds.size.width, 8.f)];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];
    [tab addSubview:refreshControl];
    taskRating = 0;
    taskFeedback = @"";
    serverMsg = NSLocalizedString(@"NO_PENDING_REQUEST", nil);
    serverMsg1 = NSLocalizedString(@"NO_COMPLETE_REQUEST", nil);
    [self.view addSubview:OuterView];
    OuterView.hidden = YES;
    RatingView.tintColor = delegate.redColor;
    dataArray = [[NSMutableArray alloc] init];
    dataArray1 = [[NSMutableArray alloc] init];
    [self FeedbackLayout];
    if(isNewReq == YES){
        TaskCreationLabel.hidden = NO;
        TaskCompletionLabel.hidden = YES;
    }
    else{
        TaskCreationLabel.hidden = YES;
        TaskCompletionLabel.hidden = NO;
    }
    HeaderLabel.text = NSLocalizedString(@"TASK_REQUESTE_QUEUE", nil);
     self.view.backgroundColor = delegate.BackgroudColor;
}
-(void) viewWillAppear:(BOOL)animated{
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
    [filterData setObject:@"" forKey:@"fromDate"];
    [filterData setObject:@"" forKey:@"toDate"];
    [filterData setObject:@"" forKey:@"taskId"];
}
-(void) fetchDetail{
    if(delegate.isInternetConnected){
        if(isNewReq == YES){
           Status = @"1";
        }
        else{
           Status = @"3";
        }
        if([CheckValue isEqualToString:@"FILTER"]){
            params = @ {
                @"fromDate": fromDate.text,
                @"handset_id": @"",
                @"status": Status,
                @"taskId": taskId.text,
                @"toDate": toDate.text,
                @"user_id": [ApplicationState userId],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        else{
            params = @ {
                @"fromDate": @"",
                @"handset_id": @"",
                @"status": Status,
                @"taskId": @"",
                @"toDate": @"",
                @"user_id": [ApplicationState userId],
                @"offset" : [NSNumber numberWithInt:offset],
                @"limit" : [NSNumber numberWithInt:limit]
            };
        }
        ResponseDic = [Api WebApi:params Url:@"viewAllTeamTask"];
        isServerResponded = TRUE;
        [refreshControl endRefreshing];
        if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            NSLog(@"CompleteResponseDic==%@",ResponseDic);
            Nodatalabel.hidden = YES;
           if(isNewReq == YES){
                if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskCreation"]))){
                   Nodatalabel.hidden = YES;
                   NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"taskCreation"];
                   if(ResponseArrays.count>0){
                       [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"taskCreation"]];
                       [tab reloadData];
                   }
                   else if(dataArray.count == 0){
                      [self NoDataFound];
                   }
               }
               else{
                   [self NoDataFound];
               }
           }
           else{
               if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskCompletion"]))){
                   NSMutableArray *ResponseArrays = [[ResponseDic valueForKey:@"object"] valueForKey:@"taskCompletion"];
                   if(ResponseArrays.count>0){
                       [self CollectResponse:[[ResponseDic valueForKey:@"object"] valueForKey:@"taskCompletion"]];
                       [tab reloadData];
                   }
                   else if(dataArray.count == 0){
                       [self NoDataFound];
                   }
               }
               else{
                   [self NoDataFound];
               }
           }
        }
        else{
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert:[ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
       [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)NoDataFound{
    [LoadingManager hideLoadingView:self.view];
    if(dataArray.count == 0){
        Nodatalabel.hidden = NO;
        Nodatalabel.text = @"No record found";
    }
    [tab reloadData];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSArray *ArraysValue;
    if(isNewReq == YES){
        if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskCreation"]))){
            ArraysValue = [[ResponseDic objectForKey:@"object"] valueForKey:@"taskCreation"];
        }
    }
    else{
        if(IsSafeStringPlus(TrToString([[ResponseDic valueForKey:@"object"] valueForKey:@"taskCompletion"]))){
           ArraysValue = [[ResponseDic objectForKey:@"object"] valueForKey:@"taskCompletion"];
        }
    }
    if(ArraysValue.count>0){
        offset = offset + 50;
        if([CheckValue isEqualToString:@"FILTER"]){
            NSMutableDictionary *NewDic = [params mutableCopy];
            for (int i = 0 ; i< [NewDic count];i++){
                if([[[NewDic allKeys] objectAtIndex:i] isEqualToString:@"offset"]){
                    [NewDic setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
                }
                else{
                    [NewDic setValue:[[NewDic allValues] objectAtIndex:i] forKey:[[NewDic allKeys] objectAtIndex:i]];
                }
            }
            params = [NewDic mutableCopy];
        }
        [self fetchDetail];
    }
}
-(void)CollectResponse:(NSMutableArray*)Array{
    
    for (int i = 0 ; i<Array.count ; i++){
        NSMutableDictionary *BindDataDic = [[NSMutableDictionary alloc] init];
        NSDictionary *Dic = [Array objectAtIndex:i];
        for (NSString *key in [Dic allKeys]){
            [BindDataDic setObject:[Dic objectForKey:key] forKey:key];
        }
        if(isNewReq == YES){
            [dataArray addObject:BindDataDic];
        }
        else{
           [dataArray1 addObject:BindDataDic];
        }
    }
}
-(IBAction)BackAction:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(IBAction)HomeAction:(id)sender{
    [self GotobackContainer];
}
-(IBAction)FilterAction:(id)sender{
    [self filterView];
}
- (void)reloadData{
    offset = 0;
    limit = 50;
    dataArray = [[NSMutableArray alloc] init];
    dataArray1 = [[NSMutableArray alloc] init];
    [self fetchDetail];
}
-(IBAction)GotoDetails:(UIGestureRecognizer*)sender{
    tagValue = sender.view.tag;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PMS" bundle:nil];
    NewTaskDetailScreen *ObjViewController = [storyboard instantiateViewControllerWithIdentifier:@"NewTaskDetailScreen"];
    
    if(isNewReq == TRUE){
       ObjViewController.isNewReq = TRUE;
        ObjViewController.taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    }
    else{
       ObjViewController.isNewReq = FALSE;
        ObjViewController.taskID = [[dataArray1 objectAtIndex:tagValue] objectForKey:@"taskId"];
    }
    
    [[self navigationController] pushViewController:ObjViewController animated:YES];
}
-(IBAction)btnfun:(id)sender{
    UIButton *btn=(UIButton *)sender;
      if(10000 == btn.tag){
        if ((([fromDate.text isEqualToString:@""] || [toDate.text isEqualToString:@""]) && [taskId.text isEqualToString:@""])){
            [self ShowAlert:NSLocalizedString(@"SELECT_FILTER", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            return;
        }
        else{
            [filterData setObject:fromDate.text forKey:@"fromDate"];
            [filterData setObject:toDate.text forKey:@"toDate"];
            if (taskId.text != nil)
            [filterData setObject:taskId.text forKey:@"taskId"];
            [filterView removeFromSuperview];
            CheckValue = @"FILTER";
            resetButton.hidden = NO;
            [self ClearData];
            [LoadingManager showLoadingView:self.view];
            [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
        }
    }
    else if (10001 == btn.tag){
        [filterData setObject:@"" forKey:@"fromDate"];
        [filterData setObject:@"" forKey:@"toDate"];
        [filterData setObject:@"" forKey:@"taskId"];
        dateTimePicker.hidden = TRUE;
        NavBar.hidden = TRUE;
        CheckValue = @"";
        resetButton.hidden = YES;
        [filterView removeFromSuperview];
    }
}
-(IBAction)ResetBtnAction:(id)sender{
    CheckValue = @"";
    resetButton.hidden = YES;
    [self ClearData];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
}
-(IBAction)TaskCrationAction:(id)sender{
    isNewReq = YES;
    TaskCreationLabel.hidden = NO;
    TaskCompletionLabel.hidden = YES;
    [self ClearData];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
}
-(IBAction)TaskCompletionAction:(id)sender{
    isNewReq = NO;
    TaskCreationLabel.hidden = YES;
    TaskCompletionLabel.hidden = NO;
    [self ClearData];
    [LoadingManager showLoadingView:self.view];
    [self performSelector:@selector(fetchDetail) withObject:nil afterDelay:0.5];
}
-(void)ClearData{
    [filterData setObject:@"" forKey:@"fromDate"];
    [filterData setObject:@"" forKey:@"toDate"];
    [filterData setObject:@"" forKey:@"taskId"];
    tab.backgroundColor = delegate.BackgroudColor;
    offset = 0;
    limit = 50;
    dataArray = [[NSMutableArray alloc] init];
    dataArray1 = [[NSMutableArray alloc] init];
}
-(void)DonePicker{
    NavBar.hidden = YES;
    dateTimePicker.hidden = TRUE;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    if (TRUE ==  isFromDate){
        fromDate.text=[dateFormat stringFromDate:dateTimePicker.date];
    }
    else{
        toDate.text=[dateFormat stringFromDate:dateTimePicker.date];
    }
    
    if(![toDate.text isEqualToString:@""]){
        NSComparisonResult result;
        NSDate *startDate = [dateFormat dateFromString:fromDate.text];
        NSDate *endDate = [dateFormat dateFromString:toDate.text];
        result = [startDate compare:endDate];
        if(result==NSOrderedAscending){
            
        }
        else if(result==NSOrderedDescending){
            toDate.text = @"";
            [self ShowAlert:@"You cannot select date earlier to start date" ButtonTitle:NSLocalizedString(@"OKAY", nil)];
        }
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    if (TRUE == isNewReq){
        return [dataArray count];
    }
    else{
        return [dataArray1 count];
    }
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if(isNewReq == YES){
        TaskRequestCell *Cell = (TaskRequestCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoDetails:)];
        [Cell addGestureRecognizer:gestureRecognizer];
        //Cell.userInteractionEnabled = YES;
        Cell.tag = indexPath.section;
        //gestureRecognizer.cancelsTouchesInView = NO;
        
        Cell.RejectBtn.tag = indexPath.section;
        Cell.ApproveBtn.tag = indexPath.section;
        [Cell.RejectBtn addTarget:self action:@selector(RejectBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.ApproveBtn addTarget:self action:@selector(ApproveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        if(dataArray.count>0){
            NSDictionary * responseData = [dataArray objectAtIndex:indexPath.section];
            [Cell configureCell:responseData];
        }
        return Cell;
    }
    else{
        TaskRequestCell *Cell = (TaskRequestCell*)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GotoDetails:)];
        [Cell addGestureRecognizer:gestureRecognizer];
//        Cell.userInteractionEnabled = YES;
        Cell.tag = indexPath.section;
        
        Cell.NeedRevisionBtn.tag = indexPath.section;
        Cell.CompleteApproveBtn.tag = indexPath.section;
        [Cell.NeedRevisionBtn addTarget:self action:@selector(NeedRevisionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.CompleteApproveBtn addTarget:self action:@selector(CompleteApproveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                if(dataArray1.count>0){
            NSDictionary * responseData = [dataArray1 objectAtIndex:indexPath.section];
            [Cell configureCell:responseData];
        }
        return Cell;
    }
}
-(IBAction)RejectBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    taskType = @"2";
    SelectBtnString = @"REJECT";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    [self showRejectAlertForNew];
   
}
-(IBAction)ApproveBtnAction:(UIButton*)sender{
    tagValue = sender.tag;
    taskType = @"1";
    SelectBtnString = @"APPROVE";
    taskID = [[dataArray objectAtIndex:tagValue] objectForKey:@"taskId"];
    [self showApproveAlertForNew];
}
-(IBAction)NeedRevisionBtnAction:(UIButton*)sender{
    SelectBtnString = @"";
    tagValue = sender.tag;
    taskType = @"3";
    taskID = [[dataArray1 objectAtIndex:tagValue] objectForKey:@"taskId"];
    [self CreateCommentView];
    
}
-(IBAction)CompleteApproveBtnAction:(UIButton*)sender{
    SelectBtnString = @"";
    tagValue = sender.tag;
    taskType = @"1";
    taskID = [[dataArray1 objectAtIndex:tagValue] objectForKey:@"taskId"];
    OuterView.hidden = NO;
}
-(void) startTheBgForStatus{
    if(delegate.isInternetConnected){
        if(OuterView.hidden == NO){
            int ConvertRating = (int)RateValue;
            params = @ {
                @"status": taskType,
                @"taskId": taskID,
                @"employeeId": [ApplicationState userId],
                @"rating" : [NSNumber numberWithInt:ConvertRating],
                @"feedback" : FeedTextView.text
            };
        }
        else{
            NSString *Comment = @"";
            Comment = CommentTextView.text;
            if(Comment == nil){
                Comment = @"";
            }
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"user_id": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
            else{
                params = @ {
                    @"status": taskType,
                    @"taskId": taskID,
                    @"userId": [ApplicationState userId],
                    @"comment" : Comment
                };
            }
            
        }
        NSLog(@"params==%@",params);
        if(OuterView.hidden == NO){
            ResponseDic = [Api WebApi:params Url:@"taskApproval"];
        }
        else{
            if([SelectBtnString isEqualToString:@"REJECT"]||[SelectBtnString isEqualToString:@"APPROVE"]){
                ResponseDic = [Api WebApi:params Url:@"updateTaskStatus"];
            }
            else{
               ResponseDic = [Api WebApi:params Url:@"needRevision"];
            }
        }
        if([[ResponseDic valueForKey:@"statusCode"] intValue] == 5){
            [LoadingManager hideLoadingView:self.view];
            [self ShowAlert: [[ResponseDic valueForKey:@"object"] valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
            CoomentView.hidden = YES;
            OuterView.hidden = YES;
            [self ClearData];
            [self fetchDetail];
        }
        else{
            [LoadingManager hideLoadingView:self.view];
           [self ShowAlert: [ResponseDic valueForKey:@"message"] ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
        }
    }
    else{
        [LoadingManager hideLoadingView:self.view];
      [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }

}
- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView {
    [self startTheBgForStatus];
}
- (void)showApproveAlertForNew {
    
    alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeSuccess];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.tag = 11;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"APPROVE_TASK", nil) withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_APPROVE", nil)
              withCustomImage:nil withDoneButtonTitle:NSLocalizedString(@"OKAY", nil)
                   andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]];
}
- (void)showRejectAlertForNew {
    alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    [alert makeAlertTypeWarning];
    alert.colorScheme = alert.flatRed;
    alert.cornerRadius = 7;
    alert.tag = 10;
    alert.firstButtonBackgroundColor = delegate.yellowColor;
    alert.firstButtonTitleColor = [UIColor whiteColor];
    [alert showAlertWithTitle:NSLocalizedString(@"REJECT_TASK", nil) withSubtitle:NSLocalizedString(@"ARE_YOU_SURE_REJECT", nil) withCustomImage:nil withDoneButtonTitle:NSLocalizedString(@"OKAY", nil)
                   andButtons:@[NSLocalizedString(@"CANCEL_BTN_CAPS", nil)]];
}
-(void)filterView{
    filterView = [[UIView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    filterView.backgroundColor = [UIColor whiteColor];
    filterView.layer.borderWidth = 0.5;
    filterView.layer.borderColor = delegate.borderColor.CGColor;
    
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.headerheight)];
    headerview.backgroundColor = delegate.headerbgColler;
    [filterView addSubview:headerview];
    
    UILabel *headerlab1=[[UILabel alloc] initWithFrame:CGRectMake(2*delegate.margin, 0,delegate.devicewidth, delegate.headerheight)];
    headerlab1.textColor=[UIColor whiteColor];
    headerlab1.font=delegate.headFont;
    headerlab1.text=NSLocalizedString(@"FILTERS", nil);
    headerlab1.textAlignment=ALIGN_LEFT;
    [headerview addSubview:headerlab1];

    dateTimePicker = [[UIDatePicker alloc] init];
    dateTimePicker.datePickerMode = UIDatePickerModeDate;
    dateTimePicker.backgroundColor=[UIColor whiteColor];
    //[dateTimePicker setMinimumDate:[NSDate date]];
    [dateTimePicker addTarget:self  action:@selector(dateTimeChange:) forControlEvents:UIControlEventValueChanged];

    dateTimePicker.layer.zPosition = 1;
    [dateTimePicker setFrame: CGRectMake(0,(filterView.frame.size.height-162), filterView.frame.size.width, 162)];
    [filterView addSubview:dateTimePicker];
    dateTimePicker.hidden = TRUE;
    
    NavBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(dateTimePicker.frame.origin.x, dateTimePicker.frame.origin.y-44, dateTimePicker.frame.size.width, 44)];
    NavBar.barStyle = UIBarStyleDefault;
    NavBar.backgroundColor = [UIColor grayColor];
    [filterView addSubview:NavBar];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(DonePicker)];
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = backButton;
    NavBar.items = @[ navItem ];
    NavBar.hidden = TRUE;
    
    CGFloat ypos = delegate.headerheight + 20;
    UILabel *fromLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 80, 15)];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.font = delegate.contentFont;
    fromLabel.text = NSLocalizedString(@"FROM", nil);
    [filterView addSubview:fromLabel];
    
    fromDate=[[UITextField alloc] initWithFrame:CGRectMake(30, ypos+20, 120, 25)];
    fromDate.keyboardType=UIKeyboardTypeDefault;
    fromDate.font=delegate.contentFont;
    fromDate.borderStyle=UITextBorderStyleRoundedRect;
    fromDate.delegate = self;
    fromDate.tag = 100;
    fromDate.placeholder = NSLocalizedString(@"START_DATE", nil);
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    image.image = [UIImage imageNamed:@"calendar"] ;
    fromDate.leftViewMode = UITextFieldViewModeAlways;
    [fromDate setLeftView:image];
    fromDate.textColor = delegate.dimColor;
    fromDate.borderStyle = UITextBorderStyleNone;
    [fromDate setBackgroundColor:[UIColor clearColor]];
    fromDate.text = [filterData objectForKey:@"fromDate"];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, fromDate.frame.size.height - 1, fromDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [fromDate.layer addSublayer:bottomBorder];
    
    fromDate.autocorrectionType = UITextAutocorrectionTypeNo;
    [filterView addSubview:fromDate];
    
    int xpos = delegate.devicewidth - 140;
    UILabel *toLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, ypos, 80, 15)];
    toLabel.textColor = [UIColor blackColor];
    toLabel.font = delegate.contentFont;
    toLabel.text =NSLocalizedString(@"TO", nil);
    [filterView addSubview:toLabel];
    
    ypos = ypos + 20;
    
    toDate=[[UITextField alloc] initWithFrame:CGRectMake(xpos, ypos, 120, 25)];
    toDate.keyboardType=UIKeyboardTypeDefault;
    toDate.font=delegate.contentFont;
    toDate.borderStyle=UITextBorderStyleRoundedRect;
    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    image1.image = [UIImage imageNamed:@"calendar"] ;
    toDate.leftViewMode = UITextFieldViewModeAlways;
    [toDate setLeftView:image1];
    toDate.delegate=self;
    toDate.tag = 101;
    toDate.placeholder =  NSLocalizedString(@"END_DATE", nil);
    toDate.textColor = delegate.dimColor;
    toDate.autocorrectionType = UITextAutocorrectionTypeNo;
    toDate.text = [filterData objectForKey:@"toDate"];
    toDate.borderStyle = UITextBorderStyleNone;
    [toDate setBackgroundColor:[UIColor clearColor]];
    bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0, toDate.frame.size.height - 1, toDate.frame.size.width, 1.0);
    bottomBorder.backgroundColor = delegate.borderColor.CGColor;
    [toDate.layer addSublayer:bottomBorder];
    
    [filterView addSubview:toDate];
    
    ypos = ypos + toDate.frame.size.height + 20;{
        UILabel *taskLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, ypos, 300, 15)];
        taskLabel.textColor = [UIColor blackColor];
        taskLabel.font = delegate.contentFont;
        taskLabel.text = NSLocalizedString(@"TASK_ID", nil);
        [filterView addSubview:taskLabel];
        
        ypos = ypos + taskLabel.frame.size.height + 20;
        
        taskId = [[UITextField alloc] initWithFrame:CGRectMake(30, ypos, 300, 25)];
        taskId.keyboardType=UIKeyboardTypeNumberPad;
        taskId.font=delegate.contentFont;
        taskId.borderStyle=UITextBorderStyleRoundedRect;
        taskId.delegate=self;
        taskId.placeholder =NSLocalizedString(@"TASK_ID", nil);
        taskId.textColor = delegate.dimColor;
        taskId.autocorrectionType = UITextAutocorrectionTypeNo;
        taskId.text = [filterData objectForKey:@"taskId"];
        taskId.borderStyle = UITextBorderStyleNone;
        [taskId setBackgroundColor:[UIColor clearColor]];
        CALayer* bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0, taskId.frame.size.height - 1, taskId.frame.size.width-50, 1.0);
        bottomBorder.backgroundColor = delegate.borderColor.CGColor;
        [taskId.layer addSublayer:bottomBorder];
        [filterView addSubview:taskId];
        ypos = ypos + taskId.frame.size.height + 20;
    }
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(delegate.margin, ypos+30, delegate.devicewidth-delegate.margin*2, 40)];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: footerView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    //footerView.layer.mask = maskLayer1;
    [filterView addSubview:footerView];
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, footerView.frame.size.width/2-10, 40)];
    submitButton.backgroundColor = delegate.redColor;
    submitButton.clipsToBounds = YES;
    submitButton.titleLabel.font = delegate.normalFont;
    [submitButton setTitle:NSLocalizedString(@"APPLY_FILTER", nil) forState:UIControlStateNormal];
    submitButton.layer.cornerRadius = 5.0;
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.tag=10000;
    [footerView addSubview:submitButton];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2+10, 0, footerView.frame.size.width/2-10, 40)];
    cancelButton.backgroundColor = [UIColor whiteColor];
    cancelButton.clipsToBounds = YES;
    cancelButton.titleLabel.font = delegate.normalFont;
    [cancelButton setTitleColor: delegate.redColor forState:UIControlStateNormal];
    cancelButton.layer.cornerRadius = 5.0;
    cancelButton.layer.borderColor = delegate.redColor.CGColor;
    cancelButton.layer.borderWidth =  1.0;
    [cancelButton setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    [cancelButton setTitleColor:delegate.redColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(btnfun:) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.tag=10001;
    [footerView addSubview:cancelButton];

    [self.view addSubview:filterView];
    [self.view bringSubviewToFront:filterView];
}

-(void) startTheBgForFilter{
    if([delegate isInternetConnected]){
        CheckValue = @"FILTER";
    }
    else{
        [self ShowAlert:NSLocalizedString(@"NO_INTERNET", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
}
-(void)CreateCommentView{
    CoomentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, delegate.devicewidth, delegate.deviceheight)];
    CoomentView.backgroundColor = [UIColor clearColor];
    UIImageView *ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, delegate.barheight, delegate.devicewidth, delegate.deviceheight)];
    ImageView.backgroundColor = [UIColor blackColor];
    ImageView.alpha = 0.3;
    UIView *InnerView = [[UIView alloc] initWithFrame:CGRectMake(10, delegate.deviceheight/2 - 100, delegate.devicewidth -20, 200)];
    CGPoint center = InnerView.center;
    center.x = CoomentView.center.x;
    InnerView.center = center;
    InnerView.layer.cornerRadius = 5.0;
    InnerView.backgroundColor = [UIColor whiteColor];
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(0, InnerView.frame.size.height-45, InnerView.frame.size.width, 45)];
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: BtnView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0}].CGPath;
    BtnView.layer.mask = maskLayer1;
    
    UILabel *AddCommentLabel;
    if([SelectBtn isEqualToString:@"CANCEL"]){
        AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 0)];
    }
    else{
        AddCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, InnerView.frame.size.width, 45)];
    }
    AddCommentLabel.backgroundColor = delegate.yellowColor;
    AddCommentLabel.textColor = [UIColor whiteColor];
    AddCommentLabel.font = delegate.headFont;
    AddCommentLabel.textAlignment = NSTextAlignmentCenter;
    AddCommentLabel.text = NSLocalizedString(@"NEED_REVISION", nil);
    
    CAShapeLayer * AddCommentmaskLayer1 = [CAShapeLayer layer];
    AddCommentmaskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: AddCommentLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){5.0}].CGPath;
    AddCommentLabel.layer.mask = AddCommentmaskLayer1;
    
    UILabel *AddCommentStaticLabel;
    if([SelectBtn isEqualToString:@"CANCEL"]){
        AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 35, 150, 15)];
    }
    else{
        AddCommentStaticLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, AddCommentLabel.frame.origin.y+AddCommentLabel.frame.size.height + 10, 150, 15)];
    }
    
    AddCommentStaticLabel.textColor = [UIColor blackColor];
    AddCommentStaticLabel.font = delegate.contentFont;
    AddCommentStaticLabel.textAlignment = NSTextAlignmentLeft;
    AddCommentStaticLabel.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    
    CommentTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, AddCommentStaticLabel.frame.origin.y+AddCommentStaticLabel.frame.size.height + 10, AddCommentLabel.frame.size.width -20, BtnView.frame.origin.y - 90)];
    CommentTextView.font = delegate.contentFont;
    CommentTextView.delegate = self;
    CommentTextView.layer.cornerRadius = 5.0;
    CommentTextView.layer.borderColor = delegate.borderColor.CGColor;
    CommentTextView.layer.borderWidth = 0.5;
    CommentTextView.textColor = [UIColor lightGrayColor]; //optional
    CommentTextView.text = NSLocalizedString(@"ADD_COMMENT_SMALL", nil);
    CommentTextView.backgroundColor = [UIColor clearColor];
    
    SubmitBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, BtnView.frame.size.width/2, 45)];
    [SubmitBtn addTarget:self action:@selector(SubmitAction) forControlEvents:UIControlEventTouchUpInside];
    SubmitBtn.backgroundColor = delegate.yellowColor;
    SubmitBtn.titleLabel.font = delegate.normalFont;
    [SubmitBtn setTitle:NSLocalizedString(@"SUBMIT_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    UIButton *CancelBtn=[[UIButton alloc] initWithFrame:CGRectMake(BtnView.frame.size.width/2, 0, BtnView.frame.size.width/2, 45)];
    [CancelBtn addTarget:self action:@selector(CancelAction) forControlEvents:UIControlEventTouchUpInside];
    CancelBtn.backgroundColor = delegate.redColor;
    CancelBtn.titleLabel.font = delegate.normalFont;
    [CancelBtn setTitle:NSLocalizedString(@"CANCEL_BTN_CAPS", nil) forState:UIControlStateNormal];
    
    [BtnView addSubview:SubmitBtn];
    [BtnView addSubview:CancelBtn];
    [InnerView addSubview:BtnView];
    [InnerView addSubview:CommentTextView];
    [InnerView addSubview:AddCommentLabel];
    [InnerView addSubview:AddCommentStaticLabel];
    [CoomentView addSubview:ImageView];
    [CoomentView addSubview:InnerView];
    
    [self.view addSubview:CoomentView];
}
-(void)SubmitAction{
    
    if([CommentTextView.text isEqualToString:NSLocalizedString(@"ADD_COMMENT_SMALL", nil)] ||[CommentTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_ADD_COMMENT", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        taskFeedback = CommentTextView.text;
        [self startTheBgForStatus];
        [CommentTextView resignFirstResponder];
    }
   
}
-(void)CancelAction{
    CoomentView.hidden = YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (100 == textField.tag){
        isFromDate = TRUE;
        [fromDate resignFirstResponder];
        dateTimePicker.hidden = FALSE;
        NavBar.hidden = FALSE;
    }
    else if (101 == textField.tag){
        isFromDate = FALSE;
        [toDate resignFirstResponder];
        dateTimePicker.hidden = FALSE;
        NavBar.hidden = FALSE;
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView isEqual:FeedTextView]){
        if ([textView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor]; //optional
        }
    }
    else{
        if ([textView.text isEqualToString:@"Add Comment"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor]; //optional
        }
    }
    
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView isEqual:FeedTextView]){
        if ([textView.text isEqualToString:@""]) {
            textView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
            textView.textColor = [UIColor lightGrayColor]; //optional
        }
    }
    else{
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Add Comment";
            textView.textColor = [UIColor lightGrayColor]; //optional
        }
    }
    [textView resignFirstResponder];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(IBAction)FeedbackCancelAction:(id)sender{
    OuterView.hidden = YES;
    FeedTextView.text = NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil);
    RatingView.value = 0.0;
    RatingLabel.text = @"";
    FeedTextView.textColor = [UIColor lightGrayColor];
    [FeedTextView resignFirstResponder];
}
-(IBAction)FeedbackSubmitAction:(id)sender{
    
    if (RateValue == 0.0){
       [self ShowAlert:NSLocalizedString(@"PLEASE_FILL_THE_STAR", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else if([FeedTextView.text isEqualToString:NSLocalizedString(@"FEEDBACK_WITHOUT_COLON", nil)] ||[FeedTextView.text isEqualToString:@""]){
        [self ShowAlert:NSLocalizedString(@"PLEASE_GIVE_THE_FEEDBACK", nil) ButtonTitle:NSLocalizedString(@"CLOSE", nil)];
    }
    else{
        taskFeedback = FeedTextView.text;
        taskRating = RateValue;
        [FeedTextView resignFirstResponder];
        [LoadingManager showLoadingView:self.view];
        [self performSelector:@selector(startTheBgForStatus) withObject:nil afterDelay:0.3];
    }
}
- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    RateValue = sender.value;
    if(RateValue == 0.0){
        RatingLabel.hidden = YES;
    }
    else if(RateValue == 1.0){
        RatingLabel.text = @"Below Expectation";
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 2.0){
       RatingLabel.text = @"Meet Expectation";
        RatingLabel.hidden = NO;
    }
    else if(RateValue == 3.0){
        RatingLabel.text = @"Above Expectation";
        RatingLabel.hidden = NO;
    }
}
-(void)FeedbackLayout{
    
    InterView.layer.cornerRadius = 6;
    InterView.clipsToBounds = YES;
    
    FeedTextView.layer.cornerRadius = 6;
    FeedTextView.layer.borderWidth = 1;
    FeedTextView.layer.borderColor = delegate.borderColor.CGColor;
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: ApproveTaskStaticLabel.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    ApproveTaskStaticLabel.layer.mask = maskLayer;
    
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: SubmitBtn.bounds byRoundingCorners: UIRectCornerBottomLeft cornerRadii: (CGSize){6.0}].CGPath;
    SubmitBtn.layer.mask = maskLayer1;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: CloseBtn.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    CloseBtn.layer.mask = maskLayer2;
    
    CAShapeLayer * maskLayer3 = [CAShapeLayer layer];
    maskLayer3.path = [UIBezierPath bezierPathWithRoundedRect: ButtonView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    ButtonView.layer.mask = maskLayer3;
}
-(void)GotobackContainer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppContainerViewController *ObjAppContainerViewController = [storyboard instantiateViewControllerWithIdentifier:@"AppContainerViewController"];
    [[self navigationController] pushViewController:ObjAppContainerViewController animated:YES];
}
-(void)ShowAlert:(NSString*)MsgTitle ButtonTitle:(NSString*)BtnTitle{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AlertController *ObjAlertController = [storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    ObjAlertController.MessageBtnStr = BtnTitle;
    ObjAlertController.MessageTitleStr = MsgTitle;
    ObjAlertController.view.backgroundColor = [UIColor clearColor];
    ObjAlertController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:ObjAlertController animated:YES completion:nil];
}
@end

