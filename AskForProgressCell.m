//
//  AskForProgressCell.m
//  iWork
//
//  Created by Shailendra on 08/06/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import "AskForProgressCell.h"
#import "Header.h"

@implementation AskForProgressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius =6;
    self.layer.masksToBounds =YES;
    
    self.layer.cornerRadius = 7.0f;
    self.layer.shadowRadius = 3.0f;
    self.layer.shadowOffset = CGSizeMake(.0f, 0.0f);
    self.layer.shadowOpacity = 0.7f;
    self.layer.masksToBounds = NO;
    
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.TaskView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){6.0}].CGPath;
    self.TaskView.layer.mask = maskLayer2;
    
    CAShapeLayer * maskLayer3 = [CAShapeLayer layer];
    maskLayer3.path = [UIBezierPath bezierPathWithRoundedRect: self.DetailsBtn.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){6.0}].CGPath;
    self.DetailsBtn.layer.mask = maskLayer3;
}

- (void)configureCell:(NSDictionary *)info{
    
    _TaskNameStaticLabel.text = NSLocalizedString(@"TASK_NAME_WITH_COLON", nil);
    _TypeStaticLabel.text = NSLocalizedString(@"TYPE_WITH_COLON", nil);
    _TimeLeftStaticLabel.text = NSLocalizedString(@"TIME_LEFT_WITH_COLON", nil);
    _KPIStaticLabel.text = NSLocalizedString(@"KPI_WITH_COLON", nil);
    
    
    if(IsSafeStringPlus(TrToString(info[@"creationDate"]))){
        NSNumber *datenumber = info[@"creationDate"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"creartedOn"]))){
        NSNumber *datenumber = info[@"creartedOn"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else if(IsSafeStringPlus(TrToString(info[@"created_on"]))){
        NSNumber *datenumber = info[@"created_on"];
        _DateLabel.text = [self DateFormateChange:datenumber];
    }
    else{
        _DateLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"kpi"]))){
        _KPILabel.text  = info[@"kpi"];
    }
    else{
        _KPILabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskId"]))){
        _TaskIdLabel.text  = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"TASK_ID_WITH_COLON", nil),info[@"taskId"]];
    }
    else{
        _TaskIdLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"taskName"]))){
        _TaskNameLabel.text  = info[@"taskName"];
    }
    else{
        _TaskNameLabel.text  = @" ";
    }
    if(IsSafeStringPlus(TrToString(info[@"timeleft"]))){
        _TimeLeftLabel.text  = info[@"timeleft"];
    }
    else if(IsSafeStringPlus(TrToString(info[@"timeLeft"]))){
         _TimeLeftLabel.text  = info[@"timeLeft"];
    }
    else{
        _TimeLeftLabel.text  = @" ";
    }
    if([_TimeLeftLabel.text containsString:@"-"]){
        _TimeLeftLabel.text = [_TimeLeftLabel.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        _TimeLeftStaticLabel.text = @"Overdue: ";
       
    }
    if(IsSafeStringPlus(TrToString(info[@"type"]))){
        _TypeLabel.text  = info[@"type"];
    }
    else if(IsSafeStringPlus(TrToString(info[@"taskType"]))){
        _TypeLabel.text  = info[@"taskType"];
    }
    else{
        _TypeLabel.text  = @" ";
    }
}
-(NSString*)DateFormateChange:(NSNumber*)datenumber{
    
    NSString *DateStr = [NSString stringWithFormat:@"%@",datenumber];
    double miliSec = [DateStr doubleValue];
    NSDate* takeOffDate = [NSDate dateWithTimeIntervalSince1970:miliSec/1000];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd hh:mm:aa"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:takeOffDate]; // Convert date to string
    NSDate *Newdate = [dateformate dateFromString:dateStr];
    [dateformate setDateFormat:@"dd MMM YYYY hh:mm:aa"];
    return [dateformate stringFromDate:Newdate];
}
@end
