//
//  FeedbackScreenCell.h
//  iWork
//
//  Created by Shailendra on 18/07/17.
//  Copyright © 2017 Sanjay Singh Rathor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
@interface FeedbackScreenCell : UITableViewCell
{
    AppDelegate *delegate;
    NSString *statusType;
    NSString *imageType;
    UIColor *colorType;
}
@property (weak, nonatomic) IBOutlet UIView *TaskView;
@property (weak, nonatomic) IBOutlet UILabel *TaskIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *DateLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *FeedbackStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *FeedbackLabel;;
@property (weak, nonatomic) IBOutlet UIView *DetailsView;
@property (weak, nonatomic) IBOutlet UILabel *KPIDetailsStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *KPIDetailsLabel;
@property (weak, nonatomic) IBOutlet UIButton *DetailsBtn;
@property (weak, nonatomic) IBOutlet RateView *RatingView;
@property (weak, nonatomic) IBOutlet UILabel *StatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *StausImageView;

- (void)configureCell:(NSDictionary *)info;
@end
